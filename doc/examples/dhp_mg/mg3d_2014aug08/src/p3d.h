
      real*dsp dx, dy, dz, xyzmin(3), xsize, ysize, zsize
      real*dsp eps0, sorfac1
      real*dsp qsrc(100),xsrc(100),ysrc(100),zsrc(100),rsrc(100)
      real*dsp ampk, phasek
      integer ksrc(3)
      integer nx, ny, nz, nb, nsrcs, npen, iverbose_trace
      integer ntx,nty,ntz, itx,ity,itz, neighbor(-1:1,-1:1,-1:1)
      integer ncycles, nlevels, nstages,nsteps(10000),ilevel(10000)
      integer niterate, nfinish, maxvsize, nperdump, myrank, nranks
      character*8 cRunName, cOutSymb(100), cOutName(100)

      common /p3dcom/ dx, dy, dz, xyzmin, xsize, ysize, zsize
      common /p3dcom/ eps0, sorfac1
      common /p3dcom/ qsrc, xsrc, ysrc, zsrc, rsrc, ampk,phasek
      common /p3dcom/ nx, ny, nz, nb, nsrcs, npen, ksrc
      common /p3dcom/  ntx,nty,ntz, itx,ity,itz, neighbor
      common /p3dcom/ ncycles, nlevels, nstages, nsteps, ilevel
      common /p3dcom/ niterate, nfinish, iverbose_trace
      common /p3dcom/ maxvsize, nperdump, myrank, nranks
      common /p3dcom/ cRunName, cOutSymb, cOutName
