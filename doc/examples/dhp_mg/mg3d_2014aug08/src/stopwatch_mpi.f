C=====================================================================72
      subroutine stopwatch_start(ctimer)                                
      include "mpif.h"                                                  
      character*20 ctimer                                               
      parameter(NMAX=1000)                                              
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)                                       
      character*20 ctimers(NMAX)                                        
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers      
      data ntimers/0/

      if(ntimers .eq. 0) t0 = MPI_WTIME()

      ! Is this a pre existing timer?
      isum = -1                      
      do i=1,ntimers                 
        if(ctimer .eq. ctimers(i)) isum = i
      enddo                                

      if(isum .gt. 0) then
        starttime(isum) = MPI_WTIME()-t0
        ntimes(isum,1)  = ntimes(isum,1) + 1
      else                                  
        if(ntimers .ge. NMAX) return
        ntimers = ntimers + 1
        ctimers(ntimers)   = ctimer
        starttime(ntimers) = MPI_WTIME()-t0
        sumtime(ntimers)   = 0.0d0
        ntimes(ntimers,1)  = 1
        ntimes(ntimers,2)  = 0
      endif

      return
      end   

C=====================================================================72
      subroutine stopwatch_stop(ctimer)                                 
      include "mpif.h"                                                  
      character*20 ctimer                                               
      parameter(NMAX=1000)                                              
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)                                       
      character*20 ctimers(NMAX)                                        
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers      
      real*8 time                                                       

      ! Is this a pre existing timer?
      isum = -1                      
      do i=1,ntimers                 
        if(ctimer .eq. ctimers(i)) isum = i
      enddo                                
      if(isum .lt. 0) return               

      time = MPI_WTIME()-t0
      sumtime(isum) = sumtime(isum) + (time-starttime(isum))
      starttime(isum) = time
        ntimes(isum,2)  = ntimes(isum,2) + 1

      return
      end

C=====================================================================72
      subroutine stopwatch_stopr(ctimer)                                 
      include "mpif.h"                                                  
      character*20 ctimer                                               
      parameter(NMAX=1000)                                              
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)                                       
      character*20 ctimers(NMAX)                                        
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers      
      real*8 time                                                       

      logical bready
      integer nrec(100), maxrec(100)
      real*8 tstart(10000,100), tstop(10000,100)
      common /timerec/ tstart, tstop, nrec, maxrec, isready
      data isready/.false./
      if(.not. isready) then
        do i = 1, 100
          nrec(i) = 0
          maxrec(i) = 0
        enddo
        isready = .true.
      endif

      ! Is this a pre existing timer?
      isum = -1                      
      do i=1,ntimers                 
        if(ctimer .eq. ctimers(i)) isum = i
      enddo                                
      if(isum .lt. 0) return               

      time = MPI_WTIME()-t0
      nrec(isum) = nrec(isum) + 1
      if(nrec(isum) .gt. 10000) nrec(isum) = 1
      maxrec(isum) = max(maxrec(isum), nrec(isum))
      tstart(nrec(isum),isum) = starttime(isum)
      tstop(nrec(isum),isum)  = time
      sumtime(isum) = sumtime(isum) + (time-starttime(isum))
      starttime(isum) = time
      ntimes(isum,2)  = ntimes(isum,2) + 1

      return
      end

C=====================================================================72
      subroutine stopwatch_write_rec()                                 
      include "mpif.h"                                                  
      character*20 ctimer                                               
      parameter(NMAX=1000)                                              
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)                                       
      character*20 ctimers(NMAX)                                        
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers      
      real*8 time                                                       

      logical bready
      integer nrec(100), maxrec(100)
      real*8 tstart(10000,100), tstop(10000,100)
      common /timerec/ tstart, tstop, nrec, maxrec, isready

      character*50 cFile

      call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierr)
       !       123456789 123456789 123456789 123456789 123456789 
      cFile = "timetrace_0000_                                   "
      cFile(11:11) = char(ichar('0')+    myrank/1000    )
      cFile(12:12) = char(ichar('0')+mod(myrank/100 ,10))
      cFile(13:13) = char(ichar('0')+mod(myrank/10  ,10))
      cFile(14:14) = char(ichar('0')+mod(myrank     ,10))
      do i = 1, 100
        if(maxrec(i) .gt. 0) then
          n = len(trim(ctimers(i)))
          cFile(16:15+n) = trim(ctimers(i))
          open(unit=11,file=cFile,form="formatted")
          do j = 1, maxrec(i)
            tmid  = 0.5d0 * (tstop(j,i) + tstart(j,i))
            thalf = 0.5d0 * (tstop(j,i) - tstart(j,i))
            write (11,999) tstart(j,i),tstop(j,i),tmid,thalf
999         format(4f12.6)
          enddo
          close(11)
        endif
      enddo

      return
      end

C=====================================================================72

      subroutine stopwatch_write(cfile)
      include "mpif.h"
      character*20 cfile
      parameter(NMAX=1000)
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)
      character*20 ctimers(NMAX)
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers

      if(ntimers .le. 0) return

      if(cfile(1:6) .eq. "stdout") then
        write (6,*) "Timer name         Total time [sec]",
     1              "    # starts     # stops"
        do i=1,ntimers
          write (6,999) ctimers(i), sumtime(i), (ntimes(i,j),j=1,2)
999       format(a20, f16.6, 2i12)
        enddo
      else
        open(unit=11, file=cfile, form="formatted")
        write (11,*) "Timer name         Total time [sec]",
     1               "    # starts     # stops"
        do i=1,ntimers
          write (11,999) ctimers(i), sumtime(i), (ntimes(i,j),j=1,2)
        enddo
        close(11)
      endif

      return
      end

C=====================================================================72
      subroutine stopwatch_write_ranks(cfile)
      include "mpif.h"
      character*20 cfile
      parameter(NMAX=1000)
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      integer      ntimes(NMAX,2)
      character*20 ctimers(NMAX)
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers
      integer ntimeall(2,1024), nranks, irank
      real*8  timeall(1024)

      call MPI_COMM_SIZE (MPI_COMM_WORLD, nranks, ierr)
      call MPI_COMM_RANK (MPI_COMM_WORLD, myrank, ierr)

      ntimers0 = ntimers
      call MPI_Bcast(ntimers0,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
      if(ntimers0 .le. 0) return


      if(cfile(1:6) .eq. "stdout") then
        if(myrank .eq. 0) then
        write (6,*) "Timer name            Number    Rank 0",
     1               "    Rank 1  ..."
        endif
        do i=1,ntimers0

          call stopwatch_gettime(ctimers(i),timeall,ntimeall)
          if(myrank .eq. 0) then
            write (6,999) ctimers(i), ntimes(i,1),
     1                    (timeall(j),j=1,nranks)
          endif
999       format(a20,i9, 800f10.3)
c99       format(a20,i10, 20f12.3)

        enddo
      else
        open(unit=11, file=cfile, form="formatted")
        if(myrank .eq. 0) then
        write (11,*) "Timer name            Number    Rank 0",
     1               "    Rank 1  ..."
        endif
        do i=1,ntimers0
          call stopwatch_gettime(ctimers(i),timeall,ntimeall)
          if(myrank .eq. 0) then
            write (11,999) ctimers(i), ntimes(i,1),
     1                     (timeall(j),j=1,nranks)
          endif
        enddo
        close(11)
      endif

      return
      end


C=====================================================================72
      subroutine stopwatch_gettime(ctimer,timeall,ntimeall)
      include "mpif.h"
      character*20 ctimer, ctimer0
      parameter(NMAX=1000)
      real*8       starttime(NMAX), sumtime(NMAX)
      DOUBLE PRECISION t0
      real*8       timeall(*), mytime
      integer      ntimes(NMAX,2),ntimeall(2,*), myntime(2)
      character*20 ctimers(NMAX)
      common /timelap/ starttime,sumtime,t0,ntimers,ntimes,ctimers

      ctimer0 = ctimer
      call MPI_Bcast(ctimer0,20,MPI_BYTE,0,MPI_COMM_WORLD,ierr)

      itimer = -1
      do i=1,ntimers
        if(ctimer0 .eq. ctimers(i)) itimer = i
      enddo

      if(itimer .gt. 0) then
        myntime(1) = ntimes(itimer,1)
        myntime(2) = ntimes(itimer,2)
        mytime     = sumtime(itimer)
      else
        myntime(1) = -1
        myntime(2) = -1
        mytime     = -1.0d0
      endif

        call MPI_Gather(myntime,1,MPI_DOUBLE_PRECISION,ntimeall,
     1                  1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

        call MPI_Gather(mytime,1,MPI_DOUBLE_PRECISION,timeall,
     1                  1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

      return
      end

