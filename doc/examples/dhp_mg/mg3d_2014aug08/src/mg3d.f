c
c Multi-Grid solver for Matrix equation Ax=b
c David H. Porter  <dhp@umn.edu>
c Minnesota Supercomputing Institute
c University of Minnesota
c
c 2013 May   9   5-point molecule implemented
c 2013 July  3   Serial multi-Grid implemented
c 2013 July 20   Serial Gauss-Seidel (SOR) implemented
c 2013 July 25   Thread parallel red-black implemented
c 2013 Aug  16   Blend (restriction) method for matrix factors implemented
c 2013 Aug  27   Adapted for HPEM
c 2013 Sept  3   1st touch optimization implemented
c 2013 Sept  5   reworked parallel region in mg_blend_factors_w for consistency with 1st-touch
c 2013 Sept  6   9-point molecule implemented (not tested)
c 2013 Sept 14   9-point molecule tested & debugged in HPEM
c 2013 Sept 24   mg_reset_sor_factors_9pt implemented & tested
c 2013 Sept 26   mg_autotune_sorfac implemented & tested
c 2013 Sept 27   mg_autotune_sorfac tested in HPEM
c 2013 Oct  24   mg_..._0p3d routine implemented for level=1
c 2013 Nov   2   mg3d.f all levels & trheads implemented with threads
c 2013 Nov   8   mg_exchange_boundaries (non-overlapped) implemented & used
c 2013 Nov   9   mg_calc_residual1_0p3d, mg_trace implemented
c 2013 Nov   9   mg_exchange)... routines implemented
c 2013 Nov  11   timers in mg_exchange...
c 2013 Nov  11   All residuals caluclated in mg_sor_strip_iterate_0p3d
c 2013 Nov  11   mg_exgnange_... thread parallel
c 2013 Nov  14   mg_exgnange_... thread parallel: works for ntz=2 (but not ntz=4)
c 2013 Nov  14   mmg_get_residual_max_rms_0p3d reduction of errt
c 2013 Nov  15   barries put in mg exchange recvcopy: now works w/ ntXYZ=114
c 2013 Nov  16   Fixed bugs in routines mg_sor..., mg_exchange..., mg_copy...
c 2013 Nov  16   MPI YZ-decompostion w/ threads tested and works
c 2013 Nov  16   Only thread 0 does copies in mg_copy... routines
c 2013 Nov  19   Full overlapped 3D (xyz) decompositon, tested & working
c 2013 Nov  26   nclct(iLev) set in gen_level_offsets_3d
c 2013 Nov  27   global barriers in mg_sor... & mg_trace only if nclct=1
c 2013 Nov  29   set hierarchy of collected levels (mg_init_vars_3d0p)
c 2013 Nov  29   independent set of nieghbors for each level
c 2013 Nov  29   map of ranks (from/to) for collecting & splitting
c 2013 Dec   2   mg_blend_fld  called before mg_blend_params
c 2013 Dec   3   mg_refine_fld called before mg_refine_params
c 2013 Dec   4   neighbors copied from nbr(:,:,:,L) in mg_*_params

c TODO
c     * routine: mg_gather_fld(L,v)
c       + return if not gathering the field
c       + call mg_gath_postrecv(L)
c       + call mg_gath_copysend(L,v(is(L+1)))
c       + CALC dims of target field in v(is(L): nxc,nyc,nzc
c         - blend_params has not been called yet
c         - mesh of target fld depends on nrefine, nclct(:), and L
c         - dims bassed through args to mg_gath_recvcopy
c       + call mg_gath_recvcopy(L+1,nxc,nyc,nzc,v(is(L+1))
c       + call mg_gath_waitsend(L)
c
c     * routine: mg_scatter_fld
c       + return if not scattering the field
c       + call mg_scat_postrecv(L)
c       + call mg_scat_copysend(L,v(is(L)))
c       + call mg_scat_recvcopy(L,v(is(L+1))
c       + call mg_scat_waitsend(L)
c
c     * routines: mg_[gath,scat]_[postrecv,copysend,recvcopy,waitsend]
c       +  mg_ath* and mg_scat* routines patterened after mg_exchange*
c       + mg_gath* for collecting blended levels from ranks with finer mesh
c       + mg_scat* for distributing coarse solution back to ranks with finer mesh
c
c     * Routines mg_copy_res_to_buf  mg_copy_buf_to_phi
c       + buffers (in mg3d.h): bres(MAXBUF) and bphi(MAXBUF,MAXFINERANKS)
c
c     * Skip work if rank is not involved on a given level
c       + retrun at top of any routine not involved
c         - in mg_blend_fld: 
c             if(max(nx) return   ! at top of routine
c         - mg_gather_fld, mg_scatter_fld
c             if(max(nxs(L),nxs(L+1)).le.0) return   ! at top of routine
c             if(nclct(L).eq.nclct(L+1)) return      ! at top of routine
c             Note: each routine inside does work based on MPI xfers
c         - in mg_refine_fld, mg_blend_fld, mg_iterate: if(nx.le.0) return
  
C=====================================================================72
      subroutine mg_run_cycle(v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      if(nstages0 .lt. 0) then
        write (6,*) "PROBLEM in mg_run_cycle:"
        write (6,*) "Multi-Grid cycle is not set."
        stop
      endif
      call mg_cycle(nstages0, ilevel0, nsteps0, v)
      return
      end

C=====================================================================72
      subroutine mg_cycle(nstages, ilevel, nsteps, v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      integer nstages, ilevel(10000), nsteps(10000), L, istage

      ifld = 1
      isrc = iisrc(1)
      ires = iires(1)

      L = 1
      nx = nx0
      ny = ny0
      call mg_refine_params(1)
      do istage = 1, nstages
        if(ilevel(istage) .gt. L) then
ctimer        call stopwatch_start("blend               ")
         call calc_residual_par(v(is(L)),L)
         call mg_blend_fld(v(is(L+1)),v(is(L)))
         call mg_gather_fld(L,v)
         L = ilevel(istage)
         call mg_blend_params(L)
         call set_to_zero(v(is(L)))
ctimer        call stopwatch_stop("blend               ")
        endif
        if(ilevel(istage) .lt. L) then
ctimer        call stopwatch_start("refine              ")
         L = ilevel(istage)
         call mg_refine_params(L)
         call mg_scatter_fld(L,v)
         call mg_refine_fld(v(is(L+1)), v(is(L)))
ctimer        call stopwatch_stop("refine              ")
        endif

        call mg_iterate(v(is(L)), nsteps(istage),L,nstages)
      enddo

      return
      end

C=====================================================================72
      subroutine mg_finish(nfinish,v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      integer nfinish

      ! Do nfinish more iterations for extra smoothing
      ifld = 1
      isrc = iisrc(1)
      ires = iires(1)
      call mg_iterate(v(is(1)), nfinish,1, 1)

      return
      end

C=====================================================================72
      subroutine mg_init_vars_3d0p()
      implicit none
#include "mg3d.h"
      real*dsp eps1
      integer i, j
      nrefine = 2
      minres  = 16
      nb   = 1
      nfld = 1
      nvar = 3
      ifld = 1     ! filed(s) to be solved for: PHI = static potential
      ires = 2     ! residual field(s)
      isrc = 3     ! source field(s)
      iires(1) = 2
      iisrc(1) = 3
      npen = 1000000
      mycomm = -1   ! an invalid value (must be set by a call to mg_set_comm

      cVarSymb( 1) = "PHI     "
      cVarSymb( 2) = "Error   "
      cVarSymb( 3) = "S       "

      do j = 1, 6
        do i = 1, 100
          ibcs(j,i)    = 0       ! default: periodic
          nclct(i) = 1
        enddo
        bc_values(j) = 0.0d0   ! default value for PHI on boundary is 0 (if fixed boundary vaule)
      enddo

      sorfac = 0.95   ! optimal for well conditioned problems
      do i = 1, 100
        sorfacs(i) = sorfac
      enddo

      iter_type = 10          ! Default iteration type: SOR Red-Black 3d
      iverbose_trace = 0     ! Default: no trace output
      nstages0 = -1
      matrix_type = 10  ! for 0-point 3d numerical molecule

      return
      end

C=====================================================================72
      subroutine mg_set_comm(mycomm_in, neighbor_in,ntx1,nty1,ntz1)
      implicit none
#include "mg3d.h"
      integer mycomm_in, neighbor_in(-1:1,-1:1,-1:1), i,j,k
      integer ntx1, nty1, ntz1
      mycomm = mycomm_in
      myrank = neighbor_in(0,0,0)
      ntx = ntx1
      nty = nty1
      ntz = ntz1
      do k = -1, 1
      do j = -1, 1
      do i = -1, 1
        neighbor(i,j,k) = neighbor_in(i,j,k)
      enddo
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_gen_neighbors(myrank, ntx,nty,ntz,neighbor)
      implicit none
      integer myrank, ntx,nty,ntz,neighbor(-1:1,-1:1,-1:1)
      integer itx,ity,itz, itxn,ityn,itzn, i,j,k

      if(myrank .ge. ntx*nty*ntz) then
        neighbor(0,0,0) = -1
        return
      endif

      itx = mod(myrank          ,ntx)
      ity = mod(myrank/ ntx     ,nty)
      itz =     myrank/(ntx*nty)

      do k = -1, 1
      do j = -1, 1
      do i = -1, 1
        itxn = mod(itx+i+ntx, ntx)
        ityn = mod(ity+j+nty, nty)
        itzn = mod(itz+k+ntz, ntz)
        neighbor(i,j,k) = itxn + ntx*(ityn + nty*itzn)
      enddo
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_gen_collect(L)
      implicit none
#include "mg3d.h"
      integer L, nmultx,nmulty,nmultz, ix0,iy0,iz0,ix1,iy1,iz1, i0,i1

      nclctp(L) = 0
      iclctw(L) = -1
      if(L .ge. maxlevels) return

      nmultx = mtx(L) / mtx(L+1)
      nmulty = mty(L) / mty(L+1)
      nmultz = mtz(L) / mtz(L+1)

      if(max(nmultx,nmulty,nmultz) .le. 1) return

      do iz0 = 0, mtz(L)-1
      do iy0 = 0, mty(L)-1
      do ix0 = 0, mtx(L)-1
        i0 = ix0 + mtx(L)*(iy0 + mty(L)*iz0)
        ix1 = ix0 / nmultx
        iy1 = iy0 / nmulty
        iz1 = iz0 / nmultz
        i1 = ix1 + mtx(L+1)*(iy1 + mty(L+1)*iz1)
        if(myrank .eq. i0) iclctw(L) = i1
        if(myrank .eq. i1) then
          nclctp(L) = nclctp(L) + 1
          iclctp(nclctp(L),L) = i0
        endif
      enddo
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_report_nbr_clct()
      implicit none
#include "mg3d.h"
      integer L, ix0,iy0,iz0, ir(0:8,0:8,0:8), i, irank, ierr
       do L = 1,maxlevels
         if(myrank .eq. 0) then
           write (6,*) "=========================================="
           write (6,*) "L = ", L
          do iz0 = 0, mtz(L)-1
          do iy0 = 0, mty(L)-1
          do ix0 = 0, mtx(L)-1
            ir(ix0,iy0,iz0) = ix0 + mtx(L)*(iy0 + mty(L)*iz0)
          enddo
          enddo
          enddo
          do iz0=mtz(L)-1,0,-1
          write (6,*) "iz0=",iz0
          do iy0=mty(L)-1,0,-1
            write (6,999) (ir(ix0,iy0,iz0),ix0=0,mtx(l)-1)
999         format(20i3)
          enddo
          enddo
         endif
            
        call MPI_BARRIER(mycomm, ierr)
        do irank = 0, mtx(L)*mty(l)*mtz(l)-1
        if(irank .eq. myrank) then
c          write (6,998) nbr(0,0,0,L), nbr(-1, 0, 0,L),nbr( 1, 0, 0,L),
c     1                                nbr( 0,-1, 0,L),nbr( 0, 1, 0,L),
c     1                                nbr( 0, 0,-1,L),nbr( 0, 0, 1,L)
c998       format("me="i3,"    NBRS:",3(" ",2i3))

c          if(iclctw(L) .ge. 0) write (6,997) myrank,iclctw(L)
c997       format("rank=",i3,"    sendto:",i3)

          if(nclctp(L) .gt. 0)
     1      write (6,996) myrank, (iclctp(i,L),i=1,nclctp(L))
996        format("rank=",i3,"    recv from:",64i3)

        endif
        call MPI_BARRIER(mycomm, ierr)
        enddo  ! irank
       enddo   ! L

      return
      end
C=====================================================================72
      subroutine mg_set_npencil(npen_in)
      implicit none
#include "mg3d.h"
      integer npen_in
      npen = npen_in
      return
      end

C=====================================================================72
      subroutine mg_set_iter_type(iter_type_in)
      implicit none
#include "mg3d.h"
      integer iter_type_in
      iter_type = iter_type_in
      return
      end

C=====================================================================72
      subroutine mg_set_sor_factor(sor)
      implicit none
#include "mg3d.h"
      real*dsp sor
      integer i
      do i = 1, 100
        sorfacs(i) = sor
      enddo
      sorfac = sor
      return
      end

C=====================================================================72
      subroutine mg_set_sor_factors(sor)
      implicit none
#include "mg3d.h"
      real*dsp sor(100)
      integer i
      if(sor(1) .gt. 0.0d0) sorfacs(1) = sor(1)
      do i = 2, 100
        if(sor(i) .gt. 0.0d0) then
          sorfacs(i) = sor(i)
        else
          sorfacs(i) = sorfacs(i-1)
        endif
      enddo
      sorfac = sorfacs(1)
      return
      end

C=====================================================================72
      subroutine mg_set_boundaries_3d(ibcs1, bcv)
      implicit none
#include "mg3d.h"
      real*dsp bcv(6)
      integer ibcs1(6), i, j
      do j = 1, 6
        ibcs(j,1)    = ibcs1(j)    ! BCs for solution field "PHI"
        bc_values(j) = bcv(j)      ! for fixed value boundaries
      enddo

      ! set boundary conditions for all fields based on choice set for PHI
      ! IF fixed value for PHI, then use continuation for the rest
      do j=1,6
      do i = 2, 100
          ibcs(j,i) = ibcs1(j)
          if(ibcs1(j) .eq. 2) ibcs(j,i) = 1
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_set_trace_verbosity(iverbose)
      implicit none
#include "mg3d.h"
      integer iverbose
      iverbose_trace = iverbose
      return
      end
C=====================================================================72
      subroutine mg_set_cycle_explicit(nstages_in,ilevel_in,nsteps_in)
      implicit none
#include "mg3d.h"
      integer i, nstages_in,ilevel_in(10000),nsteps_in(10000)
      nstages0 = nstages_in
      do i = 1, nstages0
        nsteps0(i) = nsteps_in(i)
        ilevel0(i) = ilevel_in(i)
      enddo
      return
      end
C=====================================================================72
      subroutine mg_set_cycle(cType, nlevels,nsteps_in)
      implicit none
#include "mg3d.h"
      character*(*) cType
      integer i, nlevels, nsteps_in

      call mg_gen_cycle(cType, nlevels,nstages0,ilevel0)

      do i=1,nstages0
        nsteps0(i) = nsteps_in
      enddo

      return
      end

C=====================================================================72
      subroutine mg_gen_cycle(cType, nlevels,nstages,ilevel)
      implicit none
      character*(*) cType
      integer nlevels,nstages,ilevel(10000)
      nstages = -1
      if(cType.eq."TV") call mg_gen_tv_cycle(nlevels,nstages,ilevel)
      if(cType.eq. "V") call mg_gen_v_cycle(nlevels,nstages,ilevel)
      if(cType(1:1) .eq. "W")
     1     call mg_gen_w_cycle(cType,nlevels,nstages,ilevel)
      if(nstages .lt. 0) then
         write (6,*) "Problem in mg_set_cycle:"
         write (6,*) "Cycle type ",ctype," not supported"
         stop
       endif
      return
      end

C=====================================================================72
      subroutine mg_gen_v_cycle(nlevels,nstages,ilevel)
      implicit none
      integer nlevels,nstages,ilevel(10000), level

      ! For scaling tests: W cycles fall back
      ! Want same number of finest grid steps in each case
      if(nlevels .eq. 1) then
        nstages = 2
        ilevel(1) = 1
        ilevel(2) = 1
        return
      endif

      nstages = 1
      ilevel(nstages) = 1
      do level = 2, nlevels
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      do level = nlevels-1, 1, -1
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      return
      end

C=====================================================================72
      subroutine mg_gen_tv_cycle(nlevels,nstages,ilevel)
      implicit none
      integer nlevels,nstages,ilevel(10000), level

      if(nlevels .eq. 1) then
        nstages = 1
        ilevel(nstages) = 1
        return
      endif

      nstages = 0
      do level = 2, nlevels
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      do level = nlevels-1, 1, -1
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo

      return
      end

C=====================================================================72
      subroutine mg_gen_w_cycle(cType,nlevels,nstages,ilevel)
      implicit none
      character*(*) cType
      integer nlevels,nstages,ilevel(10000), level, nmid

      if(len(cType) .lt. 2) then
        write (6,*) "PROBLEM in mg_gen_w_cycle, cType needs depth #"
        write (6,*) "cType = ", cType
        stop
      endif

      ! For scaling tests: W cycles fall backs
      ! Want same number of finest grid steps in each case
      if(nlevels .eq. 1) then
        nstages = 2
        ilevel(1) = 1
        ilevel(2) = 1
        return
      endif
      if(nlevels .eq. 2) then
        nstages = 3
        ilevel(1) = 1
        ilevel(2) = 2
        ilevel(3) = 1
        return
      endif

      nmid = -1
      read (cType,999) nmid
999   format("W",i)
      if(1.ge.nmid  .or.   nmid.ge.nlevels) then
        write (6,*) "PROBLEM in mg_gen_w_cycle"
        write (6,*) "  nmid must satisfy: 1 < nmid < nlevels"
        write (6,*) "nmid,nlevels = ", nmid, nlevels
        write (6,*) "cType = ", cType
        stop
      endif


      nstages = 1
      ilevel(nstages) = 1

      do level = 2, nlevels
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      do level = nlevels-1, nmid, -1
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      do level = nmid, nlevels
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo
      do level = nlevels-1, 1, -1
        nstages = nstages + 1
        ilevel(nstages) = level
      enddo

      return
      end

C=====================================================================72
      subroutine gen_level_offsets_3d(nlevels,nx1,ny1,nz1,nb1,maxvsize)
      implicit none
#include "mg3d.h"
      integer nlevels, nx1,ny1,nz1,nb1,maxvsize, L, nsize
      integer nsizemin, nblock

      integer nmultx, nmulty, nmultz

      iter = 0
      lev1_iter = 0
      fcycle = 0.0d0
      nx0 = nx1
      ny0 = ny1
      nz0 = nz1
      nb   = nb1
      maxlevels = nlevels

      is(1) = 1
      nx = nx0
      ny = ny0
      nz = nz0
      nblock = 1024
      nclct(1) = 1
      mtx(1) = ntx
      mty(1) = nty
      mtz(1) = ntz
      do L = 1, maxlevels

        ! debug
        if(myrank.eq.0)
     1     write (6,999) L,nclct(L),nx,ny,nz,mtx(L),mty(L),mtz(L)
999     format("# L=",i2,"   nclct=",i4,"   Nxyz=",3i4,"   Mxyz=",3i2)
        ! debug

        nxs(L) = nx
        nys(L) = ny
        nzs(L) = nz
        nfieldsize(L) = (nx+2*nb)*(ny+2*nb)*(nz+2*nb)
        nsizemin = nvar * nfieldsize(L)
        nsize = nblock * ((nsizemin + nblock-1)/nblock)
        is(L+1) = is(L) + nsize
        if(L .le. maxlevels) then
        nmultx = min(mtx(L),max(1, (nrefine*minres)/min(nx,ny,nz)))
        nmulty = min(mty(L),max(1, (nrefine*minres)/min(nx,ny,nz)))
        nmultz = min(mtz(L),max(1, (nrefine*minres)/min(nx,ny,nz)))
        nclct(L+1) = max(nmultx,nmulty,nmultz) * nclct(L)
        mtx(L+1) = mtx(L) / nmultx
        mty(L+1) = mty(L) / nmulty
        mtz(L+1) = mtz(L) / nmultz
        nx = (nmultx * nx) / nrefine
        ny = (nmulty * ny) / nrefine
        nz = (nmultz * nz) / nrefine
        endif
      enddo
      maxv = is(maxlevels+1)-1
      maxvsize = maxv

      L = 1
      nx = nx0
      ny = ny0
      nz = nz0

      ! Default limits for do loops in sor_rb_iterate
      ixmaxs(1) = nx0
      iymaxs(1) = ny0

      do L = 1, maxlevels
        call mg_gen_neighbors(myrank,mtx(L),mty(L),mtz(L),
     1                        nbr(-1,-1,-1,L))
        call mg_refine_params(L)
        call mg_set_work_order(L)
        call mg_gen_collect(L)
      enddo
      call mg_refine_params(1)
c      call mg_report_nbr_clct()
c      call myexit("stopping for debug of level offsets")

      return
      end

C=====================================================================72
      subroutine mg_set_work_order(L)
      implicit none
#include "mg3d.h"
      integer L, nbz1, nbz2, nby1, nby2, nbx1, nbx2

      if(mycomm .lt. 0) call myexit("MG3D: communictor not set")
      
       nijkwork(L) = 0
       nbz1 = 0
       if(neighbor(0,0,-1).ne.myrank) then
         nbz1 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) =  1
         ijkminmax(2,1,nijkwork(L),L) = nx
         ijkminmax(1,2,nijkwork(L),L) =  1
         ijkminmax(2,2,nijkwork(L),L) = ny
         ijkminmax(1,3,nijkwork(L),L) =  1
         ijkminmax(2,3,nijkwork(L),L) = nb
       endif
       nbz2 = 0
       if(neighbor(0,0, 1).ne.myrank) then
         nbz2 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) =  1
         ijkminmax(2,1,nijkwork(L),L) = nx
         ijkminmax(1,2,nijkwork(L),L) =  1
         ijkminmax(2,2,nijkwork(L),L) = ny
         ijkminmax(1,3,nijkwork(L),L) = nz+1-nb
         ijkminmax(2,3,nijkwork(L),L) = nz
       endif
       nby1 = 0
       if(neighbor(0,-1,0).ne.myrank) then
         nby1 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) =  1
         ijkminmax(2,1,nijkwork(L),L) = nx
         ijkminmax(1,2,nijkwork(L),L) =  1
         ijkminmax(2,2,nijkwork(L),L) =  1
         ijkminmax(1,3,nijkwork(L),L) =  1+nbz1
         ijkminmax(2,3,nijkwork(L),L) = nz-nbz2
       endif
       nby2 = 0
       if(neighbor(0, 1,0).ne.myrank) then
         nby2 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) =  1
         ijkminmax(2,1,nijkwork(L),L) = nx
         ijkminmax(1,2,nijkwork(L),L) = ny+1-nb
         ijkminmax(2,2,nijkwork(L),L) = ny
         ijkminmax(1,3,nijkwork(L),L) =  1 + nbz1
         ijkminmax(2,3,nijkwork(L),L) = nz - nbz2
       endif
       nbx1 = 0
       if(neighbor(-1,0,0).ne.myrank) then
         nbx1 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) =  1
         ijkminmax(2,1,nijkwork(L),L) =  1
         ijkminmax(1,2,nijkwork(L),L) =  1 + nby1
         ijkminmax(2,2,nijkwork(L),L) = ny - nby2
         ijkminmax(1,3,nijkwork(L),L) =  1 + nbz1
         ijkminmax(2,3,nijkwork(L),L) = nz - nbz2
       endif
       nbx2 = 0
       if(neighbor( 1,0,0).ne.myrank) then
         nbx2 = nb
         nijkwork(L) = nijkwork(L)+1
         ijkminmax(1,1,nijkwork(L),L) = nx + 1 - nb
         ijkminmax(2,1,nijkwork(L),L) = nx
         ijkminmax(1,2,nijkwork(L),L) =  1 + nby1
         ijkminmax(2,2,nijkwork(L),L) = ny - nby2
         ijkminmax(1,3,nijkwork(L),L) =  1 + nbz1
         ijkminmax(2,3,nijkwork(L),L) = nz - nbz2
       endif

       nijkwork(L) = nijkwork(L)+1
       ijkminmax(1,1,nijkwork(L),L) =  1 + nbx1
       ijkminmax(2,1,nijkwork(L),L) = nx - nbx2
       ijkminmax(1,2,nijkwork(L),L) =  1 + nby1
       ijkminmax(2,2,nijkwork(L),L) = ny - nby2
       ijkminmax(1,3,nijkwork(L),L) =  1 + nbz1
       ijkminmax(2,3,nijkwork(L),L) = nz - nbz2

c      call mg_report_ijkwork(L)

      return
      end
C=====================================================================72
      subroutine mg_report_ijkwork(L)
      implicit none
#include "mg3d.h"
      integer L, iwork, i, j
 
      if(myrank .eq. 0) then
      write (6,*) "nijkwork(L) = ", nijkwork(L)
      write (6,*) "================================================"
      write (6,*) "nx,ny,nz = ",nx,ny,nz
      do iwork = 1, nijkwork(L)
        write (6,999) ((ijkminmax(i,j,iwork,L),i=1,2),j=1,3)
999     format("IJK min max: ", 3(4x,2i4))
      enddo
      endif

      return
      end

C=====================================================================72
c Multi-gird operations
c assumes periodic geometry
c  
      subroutine mg_blend_params(L)
      implicit none
#include "mg3d.h"
      integer L, i,j,k
c      nx = nx / nrefine
c      ny = ny / nrefine
      sorfac = sorfacs(L)
      nx = nxs(L)
      ny = nys(L)
      nz = nzs(L)
      do k = -1, 1
      do j = -1, 1
      do i = -1, 1
        neighbor(i,j,k)  = nbr(i,j,k,L)
      enddo
      enddo
      enddo
      ixmax = ixmaxs(L)
      iymax = iymaxs(L)
c      write (6,*) "BLEND : nx,ny = ", nx, ny
      return
      end

      subroutine mg_refine_params(L)
      implicit none
#include "mg3d.h"
      integer L, i,j,k
c      nx = nx * nrefine
c      ny = ny * nrefine
      sorfac = sorfacs(L)
      nx = nxs(L)
      ny = nys(L)
      nz = nzs(L)
      do k = -1, 1
      do j = -1, 1
      do i = -1, 1
        neighbor(i,j,k)  = nbr(i,j,k,L)
      enddo
      enddo
      enddo
      ixmax = ixmaxs(L)
      iymax = iymaxs(L)
c      write (6,*) "REFINE: nx,ny = ", nx, ny
      return
      end

c=====================================================================72
      subroutine mg_report_nbr(cstr, L, nghbr)
      implicit none
#include "mg3d.h"
      character*(*) cstr
      integer L, nghbr(-1:1,-1:1,-1:1), i,j,k
      if(myrank .ne. 0) return
      write (6,*) "================================================="
      write (6,*) "L,array: ", L, cstr
      do j = 1, -1, -1
        write (6,999) ((nghbr(i,j,k),i=-1,1),k=-1,1)
999     format(3("   ",3i6))
      enddo
      write (6,*) "================================================="
      return
      end

c=====================================================================72
      subroutine mg_blend_fld(vc, vf)
      implicit none
#include "mg3d.h"
      real*dsp vc(1-nb:nx/nrefine+nb,
     1            1-nb:ny/nrefine+nb,
     1            1-nb:nz/nrefine+nb,nvar)
      real*dsp vf(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)

      if(neighbor(0,0,0) .lt. 0) return

      if(matrix_type .eq. 10) call mg_blend_fld_0p3d(vc, vf)

      return
      end

c=====================================================================72
      subroutine mg_blend_fld_0p3d(vc, vf)
      implicit none
#include "mg3d.h"
      real*dsp vc(1-nb:(nx/nrefine)+nb,
     1            1-nb:(ny/nrefine)+nb,
     1            1-nb:(nz/nrefine)+nb,nvar)
      real*dsp vf(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      real*dsp sum
      integer ix,iy,iz,ixf,iyf,izf,iz0,iz1,izd,ixc,iyc,izc

ctimer      call stopwatch_start("mg_blend_fld_0p3d   ")
!$omp parallel private(ix,iy,iz,iz0,iz1,izd,ixc,iyc,izc,ixf,iyf,izf,sum)
!$omp& shared(vc,vf,ixmax,iymax, ires,iwgt,isrc,nrefine)
      call mg_gen_iz_limits(iz0,iz1)
      izd = mod(iz0-1,nrefine)
      if(izd .gt. 0) iz0 = iz0 + nrefine - izd

      do iz = iz0, iz1, nrefine
      izc = 1 + (iz-1)/nrefine
      do iy =   1,  ny, nrefine
      iyc = 1 + (iy-1)/nrefine
      do ix =   1,  nx, nrefine
      ixc = 1 + (ix-1)/nrefine
        sum = 0.0d0
         do izf = iz, iz+nrefine-1
         do iyf = iy, iy+nrefine-1
         do ixf = ix, ix+nrefine-1
           sum = sum + vf(ixf,iyf,izf,ires)
        enddo
        enddo
        enddo
        vc(ixc,iyc,izc,isrc) = sum / dble(nrefine)
      enddo
      enddo
      enddo
!$omp end parallel
ctimer      call stopwatch_stop("mg_blend_fld_0p3d   ")

      return
      end

c      write (6,*) "In mg_refine_field_0p3d"
c      write (6,*) "ires,isrc = ", ires, isrc
c      iz = nz/2
c      do iy = ny, 1, -1
c        write (6,999) iy, (vc(ix,iy,iz,isrc), ix=nx/2,nx)
c999     format("# iy,vc: ", i4, 1p20e12.4)
c      enddo
c      if(nx .ge. 0) stop


c=====================================================================72
      subroutine mg_refine_fld(fc,ff)
      implicit none
#include "mg3d.h"
      real*dsp fc(1-nb:(nx/nrefine)+nb,
     1            1-nb:(ny/nrefine)+nb,
     1            1-nb:(nz/nrefine)+nb, nvar)
      real*dsp ff(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)

      if(neighbor(0,0,0) .lt. 0) return

      if(matrix_type .eq. 10) call mg_refine_fld_0p3d(fc,ff)

      return
      end

c=====================================================================72
      subroutine mg_refine_fld_0p3d(fc,ff)
      implicit none
#include "mg3d.h"
      real*dsp fc(1-nb:(nx/nrefine)+nb,
     1            1-nb:(ny/nrefine)+nb,
     1            1-nb:(nz/nrefine)+nb, nvar)
      real*dsp ff(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      real*dsp del
      integer ix,iy,iz, iz0,iz1, ixf,iyf,izf, izd, ixc,iyc,izc

!$omp parallel private(ix,iy,iz,iz0,iz1,izd,ixc,iyc,izc,ixf,iyf,izf,del)
!$omp& shared(fc,ff,ifld,nx,ny,nz,nrefine)
      call mg_gen_iz_limits(iz0,iz1)
      izd = mod(iz0-1,nrefine)
      if(izd .gt. 0) iz0 = iz0 + nrefine - izd
      do iz = iz0, iz1, nrefine
      izc = 1 + (iz-1)/nrefine
      do iy =   1,  ny, nrefine
      iyc = 1 + (iy-1)/nrefine
      do ix =   1,  nx, nrefine
      ixc = 1 + (ix-1)/nrefine
        del = fc(ixc,iyc,izc,ifld)   ! +sx*(dble(ixf)-xmid)+sy*(dble(iyf)-ymid)
        do izf = iz, iz+nrefine-1
        do iyf = iy, iy+nrefine-1
        do ixf = ix, ix+nrefine-1
           ff(ixf,iyf,izf,ifld) = ff(ixf,iyf,izf,ifld) + del
        enddo
        enddo
        enddo
      enddo
      enddo
      enddo
!$omp end parallel

      return
      end

C=====================================================================72
C=====================================================================72
      subroutine mg_iterate(v, nsteps, L, nstages)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb,1-nb:nz+nb, nvar)
      integer L, it, nsteps, nstages

      ! Return if not updating mesh on this level
      if(nbr(0,0,0,L) .lt. 0) return

      ! Reduce for trace not supported other communictors
      ! This is only a debug/diagnostic is is probably not needed
      ! for blended levels which are also callected to fewer ranks
      if(iverbose_trace .ge. 2 .and. nclct(L) .eq. 1) call mg_trace(v)

ctimer      call stopwatch_start("mg_iterate          ")
      if(iter_type .eq. 10) call mg_sor_strip_iterate_0p3d(v,nsteps,L)
      iter = iter + nsteps
ctimer      call stopwatch_stop("mg_iterate          ")

      if(L .eq. 1) lev1_iter = lev1_iter + nsteps
      fcycle = fcycle + 1.0d0 / dble(nstages)
      if(iverbose_trace .ge. 3 .and. nclct(L) .eq. 1) call mg_trace(v)

      return
      end

C=====================================================================72
      subroutine set_to_zero(v)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,nvar)
      integer ix,iy,iz,iv
      if(neighbor(0,0,0) .lt. 0) return
      do iz=1,nz
      do iy=1,ny
      do ix=1,nx
        v(ix,iy,iz,ifld) = 0.0d0
      enddo
      enddo
      enddo
      return
      end

C=====================================================================72
      subroutine mg_boundaries_3d(a,iv)
      implicit none
#include "mg3d.h"
      real*dsp a(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb)
      integer iv,ix, iy, iz, k

      if(ibcs(1,iv) .eq. 0) then      ! X Periodic
      do k = 1, nb
        do iz = 1, nz
        do iy = 1, ny
          a( 1-k,iy,iz) = a(nx+1-k,iy,iz)
          a(nx+k,iy,iz) = a(     k,iy,iz)
        enddo
      enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 0) then      ! Y Periodic
      do k = 1, nb
        do iz = 1, nz
        do ix = 1, nx
          a(ix, 1-k,iz) = a(ix,ny+1-k,iz)
          a(ix,ny+k,iz) = a(ix,     k,iz)
        enddo
      enddo
      enddo
      endif
      if(ibcs(5,iv) .eq. 0) then      ! Z Periodic
      do k = 1, nb
        do iy = 1, ny
        do ix = 1, nx
          a(ix,iy, 1-k) = a(ix,iy,nz+1-k)  
          a(ix,iy,nz+k) = a(ix,iy,     k)  
        enddo
      enddo
      enddo
      endif

      return
      end

C=====================================================================72
      subroutine mg_boundaries_par_3d(a,iv,iz0,iz1)
      implicit none
#include "mg3d.h"
      real*dsp a(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb)
      integer iv,ix, iy, iz, k, iz0, iz1

      if(neighbor(1,0,0) .eq. myrank) then
      if(ibcs(1,iv) .eq. 0) then      ! X Periodic
      do k = 1, nb
        do iz = iz0, iz1
        do iy = 1, ny
          a( 1-k,iy,iz) = a(nx+1-k,iy,iz)
          a(nx+k,iy,iz) = a(     k,iy,iz)
        enddo
      enddo
      enddo
      endif
      endif
      if(neighbor(0,1,0) .eq. myrank) then
      if(ibcs(3,iv) .eq. 0) then      ! Y Periodic
      do k = 1, nb
        do iz = iz0, iz1
        do ix = 1, nx
          a(ix, 1-k,iz) = a(ix,ny+1-k,iz)
          a(ix,ny+k,iz) = a(ix,     k,iz)
        enddo
      enddo
      enddo
      endif
      endif
      if(neighbor(0,0,1) .eq. myrank) then
      if(ibcs(5,iv) .eq. 0) then      ! Z Periodic
        if(iz0 .le. 1) then
        do k = 1, nb
          do iy = 1, ny
          do ix = 1, nx
            a(ix,iy, 1-k) = a(ix,iy,nz+1-k)  
          enddo
        enddo
        enddo
        endif
        if(iz1 .ge. nz) then
        do k = 1, nb
          do iy = 1, ny
          do ix = 1, nx
            a(ix,iy,nz+k) = a(ix,iy,     k)  
          enddo
        enddo
        enddo
        endif
      endif
      endif

      return
      end

C=====================================================================72
c
c ibdy
c 1     Low X
c 2     High X
c 3     Low Y
c 4     High Y
c
c  ibcs(ibdy)
c  0      Periodic
c  1      Mirror
c  2      Fixed value
c  3      Continuation  (extrapolate curvature?)
c
      subroutine mg_boundaries(a,iv)
      implicit none
#include "mg3d.h"
      real*dsp a(1-nb:nx+nb,1-nb:ny+nb)
      integer iv,ix, iy, k

      if(ibcs(1,iv) .eq. 0) then      ! X Periodic
      do k = 1, nb
        do iy = 1, ny
          a( 1-k,iy) = a(nx+1-k,iy)  
          a(nx+k,iy) = a(     k,iy)  
        enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 0) then      ! Y Periodic
      do k = 1, nb
        do ix = 1, nx
          a(ix, 1-k) = a(ix,ny+1-k)  
          a(ix,ny+k) = a(ix,     k)  
        enddo
      enddo
      endif


      if(ibcs(1,iv) .eq. 1) then      ! Mirror X-low
      do iy = 1, ny
      do k = 1, nb
        a( 1-k,iy) = a(     k,iy)  
      enddo
      enddo
      endif
      if(ibcs(2,iv) .eq. 1) then      ! Mirror X-high
      do iy = 1, ny
      do k = 1, nb
        a(nx+k,iy) = a(nx+1-k,iy)  
      enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 1) then      ! Mirror Y-low
      do k = 1, nb
      do ix = 1, nx
        a(ix, 1-k) = a(ix,k)  
      enddo
      enddo
      endif
      if(ibcs(4,iv) .eq. 1) then      ! Mirror Y-high
      do k = 1, nb
      do ix = 1, nx
        a(ix,ny+k) = a(ix,ny+1-k)  
      enddo
      enddo
      endif


      if(ibcs(1,iv) .eq. 2) then      ! Fixed value X-low
      do iy = 1, ny
      do k = 1, nb
        a( 1-k,iy) = bc_values(1)
      enddo
      enddo
      endif
      if(ibcs(2,iv) .eq. 2) then      ! Fixed value X-high
      do iy = 1, ny
      do k = 1, nb
        a(nx+k,iy) = bc_values(2)
      enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 2) then      ! Fixed value Y-low
      do k = 1, nb
      do ix = 1, nx
        a(ix, 1-k) = bc_values(3)
      enddo
      enddo
      endif
      if(ibcs(4,iv) .eq. 2) then      ! Fixed value Y-high
      do k = 1, nb
      do ix = 1, nx
        a(ix,ny+k) = bc_values(4)
      enddo
      enddo
      endif

      return
      end
C=====================================================================72

      subroutine mg_boundaries_par(a,iv, iy1,iy2)
      implicit none
#include "mg3d.h"
      real*dsp a(1-nb:nx+nb,1-nb:ny+nb)
      integer iy1, iy2, iv,ix, iy, k

      if(ibcs(1,iv) .eq. 0) then      ! X Periodic
      do k = 1, nb
        do iy = iy1, iy2
          a( 1-k,iy) = a(nx+1-k,iy)  
          a(nx+k,iy) = a(     k,iy)  
        enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 0) then      ! Y Periodic
      do k = 1, nb
        if(iy1 .eq. 1) then
        do ix = 1, nx
          a(ix, 1-k) = a(ix,ny+1-k)  
        enddo
        endif
        if(iy2 .eq. ny) then
        do ix = 1, nx
          a(ix,ny+k) = a(ix,     k)  
        enddo
        endif
      enddo
      endif


      if(ibcs(1,iv) .eq. 1) then      ! Mirror X-low
      do iy = iy1, iy2
      do k = 1, nb
        a( 1-k,iy) = a(     k,iy)  
      enddo
      enddo
      endif
      if(ibcs(2,iv) .eq. 1) then      ! Mirror X-high
      do iy = iy1, iy2
      do k = 1, nb
        a(nx+k,iy) = a(nx+1-k,iy)  
      enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 1 .and. iy1.eq.1) then      ! Mirror Y-low
      do k = 1, nb
      do ix = 1, nx
        a(ix, 1-k) = a(ix,k)  
      enddo
      enddo
      endif
      if(ibcs(4,iv) .eq. 1 .and. iy2.eq.ny) then      ! Mirror Y-high
      do k = 1, nb
      do ix = 1, nx
        a(ix,ny+k) = a(ix,ny+1-k)  
      enddo
      enddo
      endif


      if(ibcs(1,iv) .eq. 2) then      ! Fixed value X-low
      do iy = iy1, iy2
      do k = 1, nb
        a( 1-k,iy) = bc_values(1)
      enddo
      enddo
      endif
      if(ibcs(2,iv) .eq. 2) then      ! Fixed value X-high
      do iy = iy1, iy2
      do k = 1, nb
        a(nx+k,iy) = bc_values(2)
      enddo
      enddo
      endif
      if(ibcs(3,iv) .eq. 2 .and. iy1.eq.1) then      ! Fixed value Y-low
      do k = 1, nb
      do ix = 1, nx
        a(ix, 1-k) = bc_values(3)
      enddo
      enddo
      endif
      if(ibcs(4,iv) .eq. 2 .and. iy2.eq.ny) then      ! Fixed value Y-high
      do k = 1, nb
      do ix = 1, nx
        a(ix,ny+k) = bc_values(4)
      enddo
      enddo
      endif

      return
      end

C=====================================================================72
      subroutine mg_get_residual_max_rms(v,errmax,errrms)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,nvar)
      real*dsp err, errsum2, errmax, errrms
      integer ix, iy, iym, iyp

      if(matrix_type .eq. 10) then
        call mg_get_residual_max_rms_0p3d(v,errmax,errrms)
      endif

      return
      end

C=====================================================================72
      subroutine mg_get_residual_max_rms_0p3d(v,errt,errrms)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,nvar)
      real*dsp err, es2,es2t, errmax,errt, errrms, fnzones
      integer ix, iy, iz, iym, iyp, izm, izp, ierr

      call mg_sor_strip_iterate_0p3d(v,0,1)
      errmax  = 0.0d0
      es2 = 0.0d0
      do iz = 1, nz
      do iy = 1, ny
      do ix = 1, nx
         err =    v(ix  ,iy ,iz,2)
         errmax = max(errmax, err)
         es2 = es2 + err**2
      enddo
      enddo
      enddo
      if(dsp .eq. 4) then
        call mpi_reduce(es2,es2t,1,MPI_REAL   ,MPI_SUM,0,mycomm,ierr)
        call mpi_reduce(errmax,errt,1,MPI_REAL  ,MPI_MAX,0,mycomm,ierr)
      else
        call mpi_reduce(es2,es2t,1,MPI_DOUBLE,MPI_SUM,0,mycomm,ierr)
        call mpi_reduce(errmax,errt,1,MPI_DOUBLE,MPI_MAX,0,mycomm,ierr)
      endif
      fnzones = ntx*nty*ntz*nx*ny*nz
      if(myrank .eq. 0) errrms = sqrt(es2t/fnzones)

      return
      end
C=====================================================================72
      subroutine calc_residual_par(v,L)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,nvar)
      integer L

      if(nbr(0,0,0,L) .lt. 0) return


ctimer      call stopwatch_start("calc_residual_par   ")
      if(matrix_type .eq. 10) call mg_sor_strip_iterate_0p3d(v,0,L)
ctimer      call stopwatch_stop("calc_residual_par   ")

c      call mg_report_res_sum2(v,L)
c      call myexit("stopping for debug in calc_residual")

      return
      end

C=====================================================================72
      subroutine mg_report_res_sum2(v,L)
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb,1-nb:ny+nb,1-nb:nz+nb,nvar)
      integer L, ix,iy,iz

      do iz = 1, nz
      do iy = 1, ny
      do ix = 1, nx
      enddo
      enddo
      enddo

      return
      end
C=====================================================================72
      subroutine mg_blend_factors_all(v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      integer L

      nx = nx0
      ny = ny0

      do L = 2, maxlevels
        ixmaxs(L) = ixmaxs(L-1) / nrefine
        iymaxs(L) = iymaxs(L-1) / nrefine
        call mg_blend_params(L)
        sorfac = sorfacs(L)
c if(matrix_type.eq. 10) call mg_blend_factors_3d(v(is(L)),v(is(L-1)))
      enddo

      call mg_blend_params(1)

      return
      end

C=====================================================================72
      subroutine mg_sor_strip_iterate_0p3d(v,msteps,L)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      real*dsp fac6
      integer it, ix,iy,iz, iym,iyp,izm,izp, jz0,jz1,iwork, ierr
      integer istrip,istrip0,istrip1,msteps,nsteps,L
      integer iz0, iz1, ix0,ix1, iy0,iy1,  mythread, itarget
      real*dsp r(8192)

      nsteps = max(1, msteps)
      itarget = 1
      if(msteps .le. 0) itarget = 2
      fac6 = sorfac / 6.0d0

ccccccccccccccccc
ccc      if(nclct(L) .eq. 1) call MPI_BARRIER(mycomm, ierr)
ccccccccccccccccc

!$omp parallel private(it,ix,iy,iz,iym,iyp,izm,izp,ix0,ix1),
!$omp& private(istrip,r,istrip0,istrip1,iz0,iz1,iy0,iy1,mythread),
!$omp& private(jz0,jz1,iwork),
!$omp& shared(v,nsteps,ifld,nb,ixmax,iymax,fac6,npen),
!$omp& shared(nijkwork,ijkminmax,L,itarget)

      call mg_gen_iz_limits(jz0,jz1)
      call mg_get_mythread(mythread)

      call mg_exchange_postrecv(1,v,mythread)
      call mg_exchange_copysend(1,v,mythread)

      do it = 1, nsteps

      do istrip0 = 1, 2
        istrip1 = 3 - istrip0    ! next istrip0

      call mg_boundaries_par_3d(v(1-nb,1-nb,1-nb,ifld),ifld,jz0,jz1)

      call mg_exchange_recvcopy(istrip0,v,mythread)
      if(it.lt.nsteps .or. istrip0.lt.2)
     1      call mg_exchange_postrecv(istrip1,v,mythread)

      do iwork = 1, nijkwork(L)

      if(iwork .eq. nijkwork(L)) then
        call mg_exchange_waitsend(istrip0,v,mythread)
        if(it.lt.nsteps .or. istrip0.lt.2)
     1        call mg_exchange_copysend(istrip1,v,mythread)
      endif

ctimer      if(mythread .eq. 0)
ctimer     1  call stopwatch_start("compute             ")

      iz0 = max(jz0,ijkminmax(1,3,iwork,L))
      iz1 = min(jz1,ijkminmax(2,3,iwork,L))

      do iz=iz0, iz1
c      if(mythread .eq.  0) call myprobe()

      izm = iz - 1
      izp = iz + 1

      iy0 = ijkminmax(1,2,iwork,L)
      iy1 = ijkminmax(2,2,iwork,L)
      do iy = iy0, iy1
      iym = iy - 1
      iyp = iy + 1

      ix0 = ijkminmax(1,1,iwork,L)
      ix1 = ijkminmax(2,1,iwork,L)
      istrip = mod(iy+iz+istrip0, 2)
      if(ix0 .eq.  2) ix0 = 3 - 2*istrip
      if(ix0 .eq. nx) then
        ix0 = nx - istrip
        ix1 = nx + istrip - 1
      endif


      if(ix0 .le. ix1) then
!DIR$ VECTOR ALWAYS
c      do ix=1+istrip, nx, 2
      do ix=ix0+istrip, ix1, 2
         r(ix) = v(ix  ,iy ,iz ,3)
     1         - v(ix  ,iy ,iz ,1) * 6.0d0
     1         + v(ix-1,iy ,iz ,1)
     1         + v(ix+1,iy ,iz ,1)
     1         + v(ix  ,iym,iz ,1)
     1         + v(ix  ,iyp,iz ,1)
     1         + v(ix  ,iy ,izp,1)
     1         + v(ix  ,iy ,izm,1)
      enddo   ! ix

      if(itarget .eq. 1) then
!DIR$ VECTOR ALWAYS
      do ix=ix0+istrip, ix1, 2
        v(ix,iy,iz,1) = v(ix,iy,iz,1) + fac6 * r(ix)
      enddo   ! ix
      endif

      if(itarget .eq. 2) then
!DIR$ VECTOR ALWAYS
      do ix=ix0+istrip, ix1, 2
        v(ix,iy,iz,2) = r(ix)
      enddo   ! ix
      endif
      endif   ! (ix0 .lt. ix1) if block

      enddo   ! iy
      enddo   ! iz
ctimer      if(mythread .eq. 0)
ctimer     1  call stopwatch_stop("compute             ")
      enddo   ! iwork
!$omp barrier
      enddo   ! istrip

      enddo   ! it
!$omp end parallel

      return
      end

C=====================================================================72
      subroutine myprobe()
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer istatus(MPI_STATUS_SIZE), ierr, iflag

ctimer      call stopwatch_start("myprobe             ")
      call MPI_IPROBE(MPI_ANY_SOURCE, MPI_ANY_TAG, mycomm,
     1                  iflag, istatus, ierr)
ctimer      call stopwatch_stop("myprobe             ")

      return
      end
C=====================================================================72
      subroutine mg_copy_v_to_buf(istrip0,i1,i2,j1,j2,k1,k2,v,buf)
      implicit none
#include "mg3d.h"
      integer istrip0,   myt
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer i1,i2,j1,j2,k1,k2,ic, i,j,k,istrip, iz0,iz1,ka,kb,ia,ib
      real*dsp buf(1000000)

      ! For Z-face
c      call mg_gen_iz_limits(iz0,iz1)
c      ka = max(k1,iz0)
c      kb = min(k2,iz1)
c      ic = (nx/2) * (ka - k1)
      call mg_get_mythread(myt)
      if(myt .ne. 0) return
      ka = k1
      kb = k2
      ic = 0

      do k = ka, kb
      do j = j1, j2
      if(istrip0 .lt. 2) then
        istrip = mod(1+j+k+istrip0, 2)
        ia = i1
        ib = i2
        if(i2 .eq. 1) then
          ia = i1 - istrip
          ib = i2 - istrip
        endif
        if(i1 .eq. nx) then
          ia = i1 - istrip
          ib = i2 + istrip - 1
        endif

        do i = ia+istrip, ib, 2
          ic = ic + 1
          buf(ic) = v(i,j,k,1)
        enddo
      else
        do i = i1, i2
          ic = ic + 1
          buf(ic) = v(i,j,k,1)
        enddo
      endif
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_copy_buf_to_v(istrip0,i1,i2,j1,j2,k1,k2,v,buf)
      implicit none
#include "mg3d.h"
      integer istrip0,   myt
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer i1,i2,j1,j2,k1,k2,ic, i,j,k,istrip, iz0,iz1,ka,kb,ia,ib
      real*dsp buf(1000000)

c      call mg_gen_iz_limits(iz0,iz1)
c      if(iz0 .eq.  1) iz0 = 1-nb
c      if(iz1 .eq. nz) iz1 = nz+nb
cc      if(iz1.eq.nz .and. iz0.le.nz) iz1 = nz+nb
c      ka = max(k1,iz0)
c      kb = min(k2,iz1)
c      ic = (nx/2) * (ka - k1)
      call mg_get_mythread(myt)
      if(myt .ne. 0) return
      ka = k1
      kb = k2
      ic = 0

      do k = ka, kb
      do j = j1, j2
      if(istrip0 .lt. 2) then
        istrip = mod(1+j+k+istrip0, 2)

        ia = i1
        ib = i2
        if(i1 .eq. nx+1) then
          ia = i1 - istrip
          ib = i2 - istrip
        endif
        if(i2 .eq. 0) then
          ia = i1 - istrip
          ib = i2 + istrip - 1
        endif

        do i = ia+istrip, ib, 2
          ic = ic + 1
          v(i,j,k,1) = buf(ic)
        enddo
      else
        do i = i1,i2
          ic = ic + 1
          v(i,j,k,1) = buf(ic)
        enddo
      endif
      enddo
      enddo

      return
      end


C=====================================================================72
c >>>>> begin mpi routines

c Exchange Boudary routines
C=====================================================================72
      subroutine mg_exchange_postrecv(istrip0,v,myt)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer istrip0
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer status(MPI_STATUS_SIZE),ierr,ixm,ixp,iym,iyp,izm,izp,myt

      if(myt .ne. 0) return

      ! Post recieves early

ctimer      call stopwatch_start("exchange_postrecv   ")

      ixm = neighbor(-1,0,0)
      ixp = neighbor(1,0,0)
      nbytesx = dsp * nb * ny * nz
      if(istrip0 .lt. 2) nbytesx = nbytesx / 2
      if(ixm .ne. myrank)
     1  call MPI_IRECV(bxmr,nbytesx,MPI_BYTE,ixm,24,mycomm,hxmr,ierr)
      if(ixp .ne. myrank)
     1  call MPI_IRECV(bxpr,nbytesx,MPI_BYTE,ixp,25,mycomm,hxpr,ierr)

      iym = neighbor(0,-1,0)
      iyp = neighbor(0,1,0)
      nbytesy = dsp * nb * nx * nz
      if(istrip0 .lt. 2) nbytesy = nbytesy / 2
      if(iym .ne. myrank)
     1  call MPI_IRECV(bymr,nbytesy,MPI_BYTE,iym,22,mycomm,hymr,ierr)
      if(iyp .ne. myrank)
     1  call MPI_IRECV(bypr,nbytesy,MPI_BYTE,iyp,23,mycomm,hypr,ierr)

      izm = neighbor(0,0,-1)
      izp = neighbor(0,0,1)
      nbytesz = dsp * nb * nx * ny
      if(istrip0 .lt. 2) nbytesz = nbytesz / 2
      if(izm .ne. myrank)
     1  call MPI_IRECV(bzmr,nbytesz,MPI_BYTE,izm,20,mycomm,hzmr,ierr)
      if(izp .ne. myrank)
     1  call MPI_IRECV(bzpr,nbytesz,MPI_BYTE,izp,21,mycomm,hzpr,ierr)

ctimer      call stopwatch_stop("exchange_postrecv   ")

      return
      end
c-------------------------------------------
      subroutine mg_exchange_copysend(istrip0,v,myt)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer istrip0
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer status(MPI_STATUS_SIZE),ierr,ixm,ixp,iym,iyp,izm,izp,myt

ctimer      if(myt .eq. 0)
ctimer     1  call stopwatch_start("exchange_copysend   ")
      ! Copy interior data to buffers & send to neighbors
      ixm = neighbor(-1,0,0)
      if(ixm .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,1,1,1,ny,1,nz,v,bxms)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(bxms,nbytesx,MPI_BYTE,ixm,25,mycomm,hxms,ierr)
      endif

      ixp = neighbor(1,0,0)
      if(ixp .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,nx,nx,1,ny,1,nz,v,bxps)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(bxps,nbytesx,MPI_BYTE,ixp,24,mycomm,hxps,ierr)
      endif


      iym = neighbor(0,-1,0)
      if(iym .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,1,nx,1,1,1,nz,v,byms)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(byms,nbytesy,MPI_BYTE,iym,23,mycomm,hyms,ierr)
      endif

      iyp = neighbor(0,1,0)
      if(iyp .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,1,nx,ny,ny,1,nz,v,byps)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(byps,nbytesy,MPI_BYTE,iyp,22,mycomm,hyps,ierr)
      endif


      izm = neighbor(0,0,-1)
      if(izm .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,1,nx,1,ny,1,1,v,bzms)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(bzms,nbytesz,MPI_BYTE,izm,21,mycomm,hzms,ierr)
      endif

      izp = neighbor(0,0,1)
      if(izp .ne. myrank) then
        call mg_copy_v_to_buf(istrip0,1,nx,1,ny,nz,nz,v,bzps)
!$omp barrier
        if(myt .eq. 0)
     1  call MPI_ISEND(bzps,nbytesz,MPI_BYTE,izp,20,mycomm,hzps,ierr)
!$omp barrier
      endif
ctimer      if(myt .eq. 0)
ctimer     1   call stopwatch_stop("exchange_copysend   ")

      return
      end
c-------------------------------------------
      subroutine mg_exchange_recvcopy(istrip0,v,myt)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer istrip0
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer status(MPI_STATUS_SIZE),ierr,ixm,ixp,iym,iyp,izm,izp,myt

!$omp barrier

ctimer      if(myt .eq. 0)
ctimer     1  call stopwatch_start("exchange_recvcopy   ")
      ! Wait for data to arrive & copy into boundaries

      if(myt .eq. 0) then

      ixm = neighbor(-1,0,0)
      if(ixm .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hxmr,status,ierr)
        call mg_copy_buf_to_v(istrip0,   0,   0,1,ny,1,nz,v,bxmr)
      endif

      ixp = neighbor(1,0,0)
      if(ixp .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hxpr,status,ierr)
        call mg_copy_buf_to_v(istrip0,nx+1,nx+1,1,ny,1,nz,v,bxpr)
      endif


      iym = neighbor(0,-1,0)
      if(iym .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hymr,status,ierr)
        call mg_copy_buf_to_v(istrip0,1,nx,   0,   0,1,nz,v,bymr)
      endif

      iyp = neighbor(0,1,0)
      if(iyp .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hypr,status,ierr)
        call mg_copy_buf_to_v(istrip0,1,nx,ny+1,ny+1,1,nz,v,bypr)
      endif


      izm = neighbor(0,0,-1)
      if(izm .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hzmr,status,ierr)
        call mg_copy_buf_to_v(istrip0, 1,nx, 1,ny,   0,   0,v,bzmr)
      endif

      izp = neighbor(0,0,1)
      if(izp .ne. myrank) then
        if(myt.eq.0) call mpi_wait(hzpr,status,ierr)
        call mg_copy_buf_to_v(istrip0, 1,nx, 1,ny,nz+1,nz+1,v,bzpr)
      endif

      endif
!$omp barrier

ctimer      if(myt .eq. 0)
ctimer     1  call stopwatch_stop("exchange_recvcopy   ")

      return
      end
c-------------------------------------------
      subroutine mg_exchange_waitsend(istrip0,v,myt)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer istrip0
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer status(MPI_STATUS_SIZE),ierr,ixm,ixp,iym,iyp,izm,izp,myt

      if(myt .eq. 0) then
ctimer      call stopwatch_start("exchange_waitsend   ")
      ! Wait on send handles to clean up
      ixm = neighbor(-1,0,0)
      ixp = neighbor(1,0,0)
      if(ixm .ne. myrank) call mpi_wait(hxms,status,ierr)
      if(ixp .ne. myrank) call mpi_wait(hxps,status,ierr)

      iym = neighbor(0,-1,0)
      iyp = neighbor(0,1,0)
      if(iym .ne. myrank) call mpi_wait(hyms,status,ierr)
      if(iyp .ne. myrank) call mpi_wait(hyps,status,ierr)

      izm = neighbor(0,0,-1)
      izp = neighbor(0,0,1)
      if(izm .ne. myrank) call mpi_wait(hzms,status,ierr)
      if(izp .ne. myrank) call mpi_wait(hzps,status,ierr)
ctimer      call stopwatch_stop("exchange_waitsend   ")
      endif
!$omp barrier

      return
      end
C=====================================================================72

c Gather Blended Residuals (=sources in next level) routines
C=====================================================================72
      subroutine mg_gather_fld(LP,v)
      implicit none
      include 'mpif.h'
      integer LP, LW
      real*dsp v(maxv)
#include "mg3d.h"

      if(nbr(0,0,0,LP) .lt. 0) return

      LW = LP + 1
      call mg_gath_postrecv(LP)
      call mg_gath_copysend(LP,v(is(LW)))
      call mg_gath_recvcopy(LP,LW,v(is(LW)))
      call mg_gath_waitsend(LP)

      return
      end
C=====================================================================72
      subroutine mg_gath_postrecv(LP)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, i, n, ierr

      if(nclctp(LP) .le. 0) return

ctimer      call stopwatch_start("gath_postrecv       ")
      ! Post recieves to collect peices (p)
      n = (dsp * nxs(LP) * nys(LP) * nzs(LP)) / (nrefine**3)
      do i = 1, nclctp(LP)
        call MPI_IRECV(bnp(1,i),n,MPI_BYTE,iclctp(i,LP),30,mycomm,
     1                 hclctp(i),ierr)
      enddo
ctimer      call stopwatch_stop("gath_postrecv       ")

      return
      end
c-------------------------------------------
      subroutine mg_gath_copysend(LP,vp)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, n, ic, ix, iy, iz, ierr
      real*dsp vp(1-nb:nx/nrefine+nb,
     1            1-nb:ny/nrefine+nb,
     1            1-nb:nz/nrefine+nb, nvar)

      if(iclctw(LP) .lt. 0) return

ctimer      call stopwatch_start("gath_copysend       ")
      n = (dsp * nx * ny * nz) / (nrefine**3)
      ic = 0
      do iz = 1, nz/nrefine
      do iy = 1, ny/nrefine
      do ix = 1, nx/nrefine
        ic = ic + 1
        b1p(ic) = vp(ix,iy,iz,isrc)
      enddo
      enddo
      enddo
      call MPI_ISEND(b1p,n,MPI_BYTE,iclctw(LP),30,mycomm,hclctw,ierr)
ctimer      call stopwatch_stop("gath_copysend       ")

      return
      end
c-------------------------------------------
      subroutine mg_gath_recvcopy(LP,LW,vw)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, LW
      real*dsp vw(1-nb:nxs(LW)+nb,1-nb:nys(LW)+nb,1-nb:nzs(LW)+nb,nvar)
      integer status(MPI_STATUS_SIZE), ierr
      integer nxp,nyp,nzp, npx,npy,npz, ipx,ipy,ipz,ip, ix,iy,iz
      integer ixoff, iyoff, izoff, ic

      if(nclctp(LP) .le. 0) return
ctimer      call stopwatch_start("gath_recvcopy       ")
      nxp = nxs(LP) / nrefine    ! X dim of pieces recieved
      nyp = nys(LP) / nrefine
      nzp = nzs(LP) / nrefine
      npx = nxs(LW) / nxp        ! Numberof recieved pieces in X dir
      npy = nys(LW) / nyp
      npz = nzs(LW) / nzp
      ip = 0
      do ipz = 1, npz
      do ipy = 1, npy
      do ipx = 1, npx            ! Index of piece recieved in X dir
        ip = ip + 1              ! Index of piece recived in bnp buffer
        ixoff = nxp * (ipx-1)    ! Offset of piece recieved in X dir
        iyoff = nyp * (ipy-1)
        izoff = nzp * (ipz-1)
        call mpi_wait(hclctp(ip),status,ierr)
        ic = 0
        do iz = 1+izoff, nzp+izoff
        do iy = 1+iyoff, nyp+iyoff
        do ix = 1+ixoff, nxp+ixoff
          ic = ic + 1
          vw(ix,iy,iz,isrc) = bnp(ic,ip)
        enddo
        enddo
        enddo
      enddo
      enddo
      enddo
ctimer      call stopwatch_stop("gath_recvcopy       ")

      return
      end
c-------------------------------------------
      subroutine mg_gath_waitsend(LP)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP
      integer status(MPI_STATUS_SIZE),ierr
      if(iclctw(LP) .lt. 0) return
ctimer      call stopwatch_start("gath_waitsend       ")
      call mpi_wait(hclctw,status,ierr)   ! Wait on send handle to clean up
ctimer      call stopwatch_stop("gath_waitsend       ")

      return
      end
C=====================================================================72

c Scatter Back Solution Fields routines
C=====================================================================72
      subroutine mg_scatter_fld(LP,v)
      implicit none
      include 'mpif.h'
      integer LP, LW
      real*dsp v(maxv)
#include "mg3d.h"

      if(nbr(0,0,0,LP) .lt. 0) return

      LW = LP + 1
      call mg_scat_postrecv(LP)
      call mg_scat_copysend(LP,LW,v(is(Lw)))
      call mg_scat_recvcopy(LP,v(is(LW)))
      call mg_scat_waitsend(LP)

      return
      end

C=====================================================================72
      subroutine mg_scat_postrecv(LP)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, i, n, ierr

      if(iclctw(LP) .lt. 0) return

ctimer      call stopwatch_start("scat_postrecv       ")
      ! Post recieves to collect peices (p)
      n = (dsp * nxs(LP) * nys(LP) * nzs(LP)) / (nrefine**3)
      call MPI_IRECV(b1p,n,MPI_BYTE,iclctw(LP),31,mycomm,
     1               hclctw,ierr)
ctimer      call stopwatch_stop("scat_postrecv       ")

      return
      end
c-------------------------------------------
      subroutine mg_scat_copysend(LP,LW,vw)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, LW
      real*dsp vw(1-nb:nxs(LW)+nb,1-nb:nys(LW)+nb,1-nb:nzs(LW)+nb,nvar)
      integer status(MPI_STATUS_SIZE), ierr
      integer nxp,nyp,nzp, npx,npy,npz, ipx,ipy,ipz,ip, ix,iy,iz
      integer ixoff, iyoff, izoff, ic, n

      if(nclctp(LP) .le. 0) return
ctimer      call stopwatch_start("scat_copysend       ")
      nxp = nxs(LP) / nrefine    ! X dim of pieces to send
      nyp = nys(LP) / nrefine
      nzp = nzs(LP) / nrefine
      npx = nxs(LW) / nxp        ! Number (in X dir) of pieces to send
      npy = nys(LW) / nyp
      npz = nzs(LW) / nzp
      ip = 0
      do ipz = 1, npz
      do ipy = 1, npy
      do ipx = 1, npx            ! X-index of piece to send
        ip = ip + 1              ! Index in bnp buffer of piece to send
        ixoff = nxp * (ipx-1)    ! X-offset of piece to send
        iyoff = nyp * (ipy-1)
        izoff = nzp * (ipz-1)
        ic = 0
        do iz = 1+izoff, nzp+izoff
        do iy = 1+iyoff, nyp+iyoff
        do ix = 1+ixoff, nxp+ixoff
          ic = ic + 1
          bnp(ic,ip) = vw(ix,iy,iz,ifld)
        enddo
        enddo
        enddo
        n = (dsp * nxs(LP) * nys(LP) * nzs(LP)) / (nrefine**3)
        call MPI_ISEND(bnp(1,ip),n,MPI_BYTE,iclctp(ip,LP),31,mycomm,
     1               hclctp(ip),ierr)
      enddo
      enddo
      enddo
ctimer      call stopwatch_stop("scat_copysend       ")

      return
      end
c-------------------------------------------
      subroutine mg_scat_recvcopy(LP,vp)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP, LW, ic, ix, iy, iz
      real*dsp vp(1-nb:nx/nrefine+nb,
     1            1-nb:ny/nrefine+nb,
     1            1-nb:nz/nrefine+nb, nvar)
      integer status(MPI_STATUS_SIZE),ierr

      if(iclctw(LP) .lt. 0) return

ctimer      call stopwatch_start("scat_recvcopy       ")
      call mpi_wait(hclctw,status,ierr)
      ic = 0
      do iz = 1, nz/nrefine
      do iy = 1, ny/nrefine
      do ix = 1, nx/nrefine
        ic = ic + 1
        vp(ix,iy,iz,ifld) = b1p(ic)
      enddo
      enddo
      enddo
ctimer      call stopwatch_stop("scat_recvcopy       ")

      return
      end
c-------------------------------------------
      subroutine mg_scat_waitsend(LP)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      integer LP
      integer status(MPI_STATUS_SIZE), ip,ierr
      if(nclctp(LP) .le. 0) return
ctimer      call stopwatch_start("scat_waitsend       ")
      ! Wait on send handles to clean up
      do ip = 1, nclctp(LP)
        call mpi_wait(hclctp(ip),status,ierr)
      enddo
ctimer      call stopwatch_stop("scat_waitsend       ")

      return
      end
C=====================================================================72
c >>>>> end mpi routines

C=====================================================================72
      subroutine mg_set_source_3d(srcfac,s,v)
      implicit none
#include "mg3d.h"
      real*dsp srcfac, s(nx,ny,nz)
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer ix,iy,iz

      do iz = 1, nz
      do iy = 1, ny
      do ix = 1, nx
          v(ix,iy,iz,3) = srcfac * s(ix,iy,iz)
      enddo
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_set_field_3d(fld,v)
      implicit none
#include "mg3d.h"
      real*dsp fld, s(nx,ny,nz)
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      integer ix,iy,iz

      do iz = 1, nz
      do iy = 1, ny
      do ix = 1, nx
          v(ix,iy,iz,1) = fld(ix,iy,iz)
      enddo
      enddo
      enddo

      return
      end

C=====================================================================72
      subroutine mg_1st_touch_3d(v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      integer L

      ! Do 1st touch of variable arrays on all levels
      do L = 1, maxlevels
        call mg_blend_params(L)
        call mg_1st_level_touch_3d(v(is(L)))
      enddo
      call mg_blend_params(1)

      return
      end

C=====================================================================72
      subroutine mg_1st_level_touch_3d(v)
      implicit none
#include "mg3d.h"
      real*dsp v(1-nb:nx+nb, 1-nb:ny+nb,1-nb:nz+nb,nvar)
      integer iz0,iz1,ix,iy,iz, iv
      integer it

      ! 1st touch by each thread of variable array (v) on currnt level

!$omp parallel private(ix,iy,iz, iv, it)
!$omp& private(iz0,iz1),
!$omp& shared(v,nvar,nx,ny,nz)

      call mg_gen_iz_limits(iz0,iz1)
      if(iz0 .eq.     1) iz0 = 1-nb
      if(iz1.eq.nz .and. iz0.le.nz) iz1 = nz+nb

      do iv=1,nvar
      do iz = iz0, iz1
      do iy = 1-nb, ny+nb
      do ix = 1-nb, nx+nb
        v(ix,iy,iz,iv) = 0.0d0
      enddo
      enddo
      enddo
      enddo

!$omp end parallel

      return
      end

C=====================================================================72
      subroutine mg_get_mythread(ithread)
      implicit none
      integer ithread, omp_get_thread_num
      ithread   = 0
copenmp      ithread   = omp_get_thread_num()
      return
      end

C=====================================================================72
      subroutine mg_gen_iz_limits(iz0,iz1)
      implicit none
#include "mg3d.h"
      integer iz0,iz1, nthreads,ithread,nperthread,  it
      integer omp_get_num_threads, omp_get_thread_num

      ! This routine assume it is run in the dynamic extent of an OpenMP parallel region
      ! It generates iz0 & iz1, which are thread local limits to iz
      nthreads  = 1
      ithread   = 0
copenmp      nthreads  = omp_get_num_threads()
copenmp      ithread   = omp_get_thread_num()
      nperthread = (nz+nthreads-1) / nthreads
      iz0 = 1 + nperthread * ithread
      iz1 = min(nz, iz0+nperthread-1)

      return
      end

C=====================================================================72
      subroutine mg_get_nx_ny_nz_nb_nvar(nx1,ny1,nz1,nb1,nvar1)
      implicit none
#include "mg3d.h"
      integer nx1,ny1,nz1,nb1,nvar1
      nx1 = nx
      ny1   = ny
      nz1   = nz
      nb1   = nb
      nvar1 = nvar
      return
      end

C=====================================================================72
      subroutine mg_get_nvar(nvar1)
      implicit none
#include "mg3d.h"
      integer nvar1
      nvar1 = nvar
      return
      end

C=====================================================================72
      subroutine mg_get_iter(iter1)
      implicit none
#include "mg3d.h"
      integer iter1
      iter1 = iter
      return
      end

C=====================================================================72
      subroutine mg_get_var_name(ivar, cvar)
      implicit none
#include "mg3d.h"
      integer ivar, m
      character*(*) cvar
      m = min(len(cVar), len(cVarSymb(ivar)))
      cvar(1:m) = cVarSymb(ivar)(1:m)
      return
      end

C=====================================================================72
      subroutine mg_get_var_array_3d(ivar, var, v)
      implicit none
#include "mg3d.h"
      integerivar, ix, iy, iz
      real*4 var(nx, ny, nz)
      real*dsp   v(1-nb:nx+nb, 1-nb:ny+nb, 1-nb:nz+nb, nvar)
      do iz = 1, nz
      do iy = 1, ny
      do ix = 1, nx
        var(ix,iy,iz) = v(ix,iy,iz,ivar)
      enddo
      enddo
      enddo
      
      return
      end

C=====================================================================72
      subroutine mg_trace(v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      real*dsp errmax, errrms, err, errmax0
      integer ierr
      common /trace_com_data/ errmax0
      data errmax0/-1.0d0/
ctimer      call stopwatch_start("trace               ")

      call mg_get_residual_max_rms(v,errmax,errrms)

      if(myrank .ne. 0) return

      if(errmax0 .lt. 0.0d0) errmax0 = abs(errmax)
      errmax = errmax / errmax0
      errrms = errrms / errmax0
      if(iter .eq. 0) write (6,998)
998   format("#   iter    RMS(err)    MAX(err)")
      write (6,999) iter, errrms, errmax, lev1_iter, fcycle
999   format(i8, 1p2e12.4, i8, f10.3)

ctimer      call stopwatch_stop("trace               ")

      return
      end
C=====================================================================72
      subroutine mg_reset_trace(v)
      implicit none
#include "mg3d.h"
      real*dsp v(maxv)
      real*dsp errmax, errrms, err, errmax0
      common /trace_com_data/ errmax0

      call mg_get_residual_max_rms(v,errmax,errrms)
      errmax0 = abs(errmax)

      return
      end
C=====================================================================72
      subroutine mg_dump_fields_3d(v,L,cRoot ,nDump)
      implicit none
      include 'mpif.h'
#include "mg3d.h"
      real*dsp v(maxv)
      integer L, ix,iy,iz,irank, ierr
      real*4 vout
      allocatable vout(:,:,:)
      integer NHEAD
      parameter (NHEAD = 64*1024)
      real time4, dtime4, dx
      real xyzranges(6)
      integer istep, i, nbytes, itx,ity,itz
      integer*2 abuf(NHEAD)
      character*(*) cRoot
      character*14 cDumpFile
      integer ndump

      cDumpFile        = cRoot(1:5) // "-0000-000"
      cDumpFile( 7: 7) = char(ichar('0') +     ndump/1000   )
      cDumpFile( 8: 8) = char(ichar('0') + mod(ndump/100,10))
      cDumpFile( 9: 9) = char(ichar('0') + mod(ndump/10 ,10))
      cDumpFile(10:10) = char(ichar('0') + mod(ndump    ,10))

      call calc_residual_par(v(is(L)),L)

      time4  = float(iter)
      dtime4 = 1.0
      istep  = iter
      dx = 1.0 / float(nx)
      xyzranges(1) = -0.5 * dx * float(nx*mtx(L))
      xyzranges(2) = -0.5 * dx * float(ny*mty(L))
      xyzranges(3) = -0.5 * dx * float(nz*mtz(L))
      xyzranges(4) =  0.5 * dx * float(nx*mtx(L))
      xyzranges(5) =  0.5 * dx * float(ny*mty(L))
      xyzranges(6) =  0.5 * dx * float(nz*mtz(L))

      ! Set dump paramters & list of variables
      call set_dump_parameters(mtx(L),mty(L),mtz(L))
      do i = 1, nvar
        call set_dump_var(i,cVarSymb(i),cVarSymb(i),"real4",0.0,0.0)
      enddo

      ! Format meta-data (header info) into a buffer (abuf).
      nbytes = 2*NHEAD
      itx = mod(myrank                ,mtx(L))
      ity = mod(myrank/ ntx           ,mty(L))
      itz =     myrank/(mtx(L)*mty(L))
      call format_adump_header(abuf,ndump,time4,dtime4,istep,xyzranges,
     1             nbytes,nvar, nx,ny,nz, itx,ity,itz,
     1             mtx(L),mty(L),mtz(L))

      ! Write header buffer to a file
      allocate(vout(nx,ny,nz))

      do irank = 0, mtx(L)*mty(L)*mtz(L)-1
        if(irank .eq. myrank) then
          open(unit=12,file=cDumpFile,form="binary",access="append")
          if(myrank .eq. 0) rewind(12)
          write (12) abuf
          do i = 1, nvar
            call mg_get_var_array_3d(i, vout, v(is(L)))
            write (12) vout
          enddo
          flush(12)
          close(12)
        endif   ! (irank .eq. myrank)
        call MPI_BARRIER(MPI_COMM_WORLD, ierr)
      enddo   ! irank

      deallocate(vout)

      return
      end

c=====================================================================72

