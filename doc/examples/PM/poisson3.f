      program phi


      use omp_lib

      implicit none

      real*8 r,rc

      real*8, dimension(:,:,:), allocatable :: x0,y0
      real*8, dimension(:,:,:), allocatable :: xi,yi

      integer n1,n2,n3,n1c,n2c,n3c
      integer m,i,j,k,nt,id

c     set size of grid

      m = 5

      n1 = 2**m
      n2 = 2**m
      n3 = 2**m

      allocate (x0(0:n1-1,0:n2-1,0:n3-1))
      allocate (y0(0:n1-1,0:n2-1,0:n3-1))
      allocate (xi(0:n1-1,0:n2-1,0:n3-1))
      allocate (yi(0:n1-1,0:n2-1,0:n3-1))

c      set number of threads

      nt = 8

      call OMP_SET_NUM_THREADS(nt)


c      define density distribution to for del^2 phi = x0
c      need imaginary values; set to zero

c      rc = dble(n1)*(2.d0**(-m+1))
      rc = 0.5d0

      n1c = n1/2
      n2c = n2/2
      n3c = n3/2



      !$omp parallel private(id,i)
      do k = 0,n3-1
      do j = 0,n2-1
        id = omp_get_thread_num()

      do i = id*n1/nt,(id+1)*n1/nt -1
        x0(i,j,k) = 0.d0
c        circular charge
         r = sqrt(dble((i-n1c))**2+dble((j-n2c))**2+dble((k-n3c))**2)
c         print *, i,j,k,r
         if(r .le. rc)x0(i,j,k) = 1.d0
         y0(i,j,k) = 0.d0
      enddo
      enddo
      enddo
      !$omp end parallel

      do k = 0,n3-1
      do j = 0,n2-1
      do i = 0,n1-1
        write(99,'(i4,i4,i4,1p2e12.3)') i,j,k,x0(i,j,k)
      enddo
      enddo
      enddo

      do i = 0,n1-1
        write(60,'(i5,1p2e12.3)')i,x0(i,n2c,n3c),y0(i,n2c,n3c)
      enddo



      call poisson(x0,y0,xi,yi,m,nt)




      do i = 0,n1-1
        write(61,'(i5,1p2e12.3)')i,xi(i,n2c,n3c),yi(i,n2c,n3c)
      enddo




      stop
      end program



      subroutine poisson(x0,y0,xi,yi,m,nt)
c      program poisson
c      x0 and y0 are the original 3D arrays of dimension 2**m
c      calls 'my' 3d fft routine
c      xt and yt are the transformed arrays, with itype = 1, -1 fixing direction

c      Construct 3D fft using 1D fft
c     This version uses OpenMP for outer do loops
c     needs -openmp compiler flag
      use omp_lib


      implicit none

      real*8, parameter ::       tpi = 8.d0*atan(1.d0)

      real*8 an1,an2,an3,fac
      real*8 x0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 y0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xi(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yi(0:2**m-1,0:2**m-1,0:2**m-1)
      integer n1,n2,n3,m,nt,id,i,j,k


      n1 = 2**m
      n2 = 2**m
      n3 = 2**m




C      open(unit =10, file = 'oftr.dat', form = 'binary')
C      open(unit =11, file = 'ofti.dat', form = 'binary')

c      write(10)x0
c      write(11)y0

      call fft3d(x0,y0,xt,yt,m,1,nt)

c     Now construct fft of potential using finite difference formula
c     note singularity for i = j = k = 0

      an1 = 1.d0/dble(n1)
      an2 = 1.d0/dble(n2)
      an3 = 1.d0/dble(n3)


      !$omp parallel private(id,i)
      do k = 0,n3-1
      do j = 0,n2-1
        id = omp_get_thread_num()

      do i = id*n1/nt,(id+1)*n1/nt -1
c     do i = 0,n1-1
      if(i+j+k .eq. 0 )then
        fac = 0.d0
      else
        fac =-0.5d0/
     >   (cos(tpi*i*an1)+cos(tpi*j*an2)+cos(tpi*k*an3) - 3.d0)
      endif
      xt(i,j,k) = fac*xt(i,j,k)
      yt(i,j,k) = fac*yt(i,j,k)
      enddo
      enddo
      enddo

c      Now find potential using inverse fft

      !$omp end parallel

      call fft3d(xt,yt,xi,yi,m,-1,nt)



C      open(unit =12, file = 'rftr.dat', form = 'binary')
C      open(unit =13, file = 'rfti.dat', form = 'binary')

c      write(12)xi
c      write(13)yi

c      stop
c      end program
      return
      end

       subroutine fft3d(x0,y0,xt,yt,m,itype,nt)
c      program 3dft
c      x0 and y0 are the original 3D arrays of dimension 2**m
c      xt and yt are the transformed arrays, with itype = 1, -1 fixing direction
c      nt is the number of threads specified in main

c      Construct 3D fft using 1D fft
c     Remember that the normal order fft is (n1+1)*(n2+1)*n3+1)
c     This version uses OpenMP for outer do loops
c     needs -openmp compiler flag
        use omp_lib

      implicit none


      real*8 x(0:2**m-1),y(0:2**m-1)
      real*8 x0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 y0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yt(0:2**m-1,0:2**m-1,0:2**m-1)
      integer m,n1,n2,n3,itype,nt
      integer id,i,j,k



c     set size of grid

      n1 = 2**m
      n2 = 2**m
      n3 = 2**m

c     Construct 3D fft by making n2*n3 1D passes of length n1
c     Followed by n1*n3 passes of length n2 on the second index
c     Followed by n1*n2 passes of length n3 on the third index
c     First decompose 3D array into a set of 1D vectors 

     
      !$omp parallel private(id,k,x,y)
        id = omp_get_thread_num()

      do k = id*n3/nt,(id+1)*n3/nt -1
       do j = 0,n2-1

        do i = 0,n1-1
	  x(i) = x0(i,j,k)
	  y(i) = y0(i,j,k)
        enddo


      call cfft(x,y,n1,m,itype)


        do i = 0,n1-1
	  xt(i,j,k) = x(i)
	  yt(i,j,k) = y(i)
        enddo

       enddo
      enddo

      !$omp end parallel

      !$omp parallel private(id,i,x,y)
        id = omp_get_thread_num()
      do i = id*n1/nt,(id+1)*n1/nt -1
        do k = 0,n3-1
         do j = 0,n2-1
	  x(j) = xt(i,j,k)
	  y(j) = yt(i,j,k)
         enddo

      call cfft(x,y,n2,m,itype)

         do j = 0,n2-1
	  xt(i,j,k) = x(j)
	  yt(i,j,k) = y(j)
         enddo

        enddo
       enddo

      !$omp end parallel

      !$omp parallel private(id,i,x,y)
        id = omp_get_thread_num()
      do i = id*n1/nt,(id+1)*n1/nt -1
        do j = 0,n2-1
         do k = 0,n3-1
	  x(k) = xt(i,j,k)
	  y(k) = yt(i,j,k)
         enddo

      call cfft(x,y,n3,m,itype)

         do k = 0,n3-1
	  xt(i,j,k) = x(k)
	  yt(i,j,k) = y(k)
         enddo

        enddo
       enddo

      !$omp end parallel

      return

      end

      
      SUBROUTINE CFFT( X, Y, N, M, ITYPE )
      implicit none
c
c  This is a very simple Cooley-Tukey Radix-2 DIF Complex FFT.
c 
c  The data sequence is of size N = 2**M.
c  X and Y contain the real and imaginary parts of the data.
c
c  ITYPE .ne. -1 for forward transform
c  ITYPE .eq. -1 for backward transform
c
c  The forward transform computes
c     Z(k) = sum_{j=0}^{N-1} z(j)*exp(-2ijk*pi/N)
c
c  The backward transform computes
c     z(j) = (1/N) * sum_{k=0}^{N-1} Z(k)*exp(2ijk*pi/N)
c
c
c  Steve Kifowit, 31 October 1998
c       
c
c ... Scalar arguments ...
      INTEGER  N, M, ITYPE
c ... Array arguments ...
      REAL*8  X(*), Y(*)
c ... Local scalars ...
      INTEGER  I, J, K, L, N1, N2, IE, IA
      REAL*8  C, S, XT, YT, P, A
c ... Parameters ...
      Real*8,PARAMETER :: TWOPI = 6.283185307179586476925287 
c ... Intrinsic functions
      INTRINSIC  SIN, COS
c
c ... Exe. statements ...
c
c ... Quick return ...
      IF ( N .EQ. 1 ) RETURN
c
c ... Conjugate if necessary ...
      IF ( ITYPE .EQ. -1 ) THEN
	 DO 1, I = 1, N
	    Y(I) = - Y(I)
 1       CONTINUE
      ENDIF
c
c ... Main loop ...
      P = TWOPI / N
      N2 = N
      DO 10, K = 1, M
         N1 = N2
         N2 = N2 / 2
         IE = N / N1
         IA = 1
         DO 20, J = 1, N2
	    A = ( IA - 1 ) * P
            C = COS( A ) 
            S = SIN( A )
            IA = IA + IE
            DO 30, I = J, N, N1
               L = I + N2
               XT = X(I) - X(L)
               X(I) = X(I) + X(L)
               YT = Y(I) - Y(L)
               Y(I) = Y(I) + Y(L)
               X(L) = C * XT + S * YT
               Y(L) = C * YT - S * XT
 30         CONTINUE
 20      CONTINUE
 10   CONTINUE
c
c ... Bit reversal permutation ...
 100  J = 1
      N1 = N - 1
      DO 104, I = 1, N1
         IF ( I .GE. J ) GOTO 101
         XT = X(J)
         X(J) = X(I)
         X(I) = XT
         YT = Y(J)
         Y(J) = Y(I)
         Y(I) = YT
 101     K = N / 2
 102     IF ( K .GE. J ) GOTO 103
         J = J - K
         K = K / 2
         GOTO 102
 103     J = J + K
 104  CONTINUE
c
c ... Conjugate and normalize if necessary ...
      IF ( ITYPE .EQ. -1 ) THEN
	 DO 3, I = 1, N
	    Y(I) = - Y(I) / N
            X(I) = X(I) / N
 3       CONTINUE
      ENDIF
      RETURN
c
c ... End of subroutine CFFT ...
c
      END
