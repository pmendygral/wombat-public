      integer, parameter :: ndim = 3       !# of dimensions
      real*8, parameter :: PI = 4.d0*atan(1.d0)

      real*8 dt,dx,lmax
      real*8 G,M1,M2

      integer nx,npart

      common/param/dt,dx,lmax
      common/paramint/nx,npart
      common/grav/G,M1,M2

