!     3d with charge assignment for many particles
!

      program PM

      use omp_lib

      implicit none

      include "common.inc"

      real*8 x,y,z
      real*8 tdump,dumpt
      real*8 t,r,tf
      real*8 grad_int(ndim), fptFD
      real*8 vtot,rmin,rmax,vmax
      real*8 H,a,Hfac,adot
      real*8 E,ecc,amaj,P
      real*8 theta, r_anly, thetold, thet, theta0, eps, err
      real*8 eta,calc_mag,dr,dv
      real*8 anly_phi, x0,y0,z0,anlyg(ndim)
      real*8 xtest(ndim)
!### xp,vp,gp,xtmp first index is ith particle, second index is ndim
      real*8, dimension(:,:), allocatable :: xp,vp,gp,xtmp 
      real*8, dimension(:) , allocatable :: M,speed,radii
      real*8, dimension(:,:,:), allocatable :: rho, rhoi
      real*8, dimension(:,:,:), allocatable :: phi, phii

      integer nstep,orbit,mm,nt,nxc
      integer i,j,k

      common/hist/dr,dv

      open(unit = 10, file = 'part.out' ) !### - output dist of particle positions
      open(unit = 20, file = 'orb.out'  ) !### - output tracking an individual particle
      open(unit = 56, file = 'fort.56'  ) !### - input positions
      open(unit = 57, file = 'fort.57'  ) !### - input velocities


      a = 1.d0
      H = 0.d0
      G = 1.d0
      adot = 0.d0


      nt = 8
      call OMP_SET_NUM_THREADS(nt)

      mm = 6
      nx = 2**mm
      nxc = nx/2

      npart = nx**3

      allocate (xp(npart,ndim))
      allocate (vp(npart,ndim))
      allocate (gp(npart,ndim))
      allocate (xtmp(npart,ndim))


      allocate (rho(nx,nx,nx) )
      allocate (rhoi(nx,nx,nx))
      allocate (phi(nx,nx,nx) )
      allocate (phii(nx,nx,nx))

      allocate (M(npart),radii(npart),speed(npart))


      M = 0.841886414313972/dble(npart)

      do i = 1,npart
        read(56,'(3(1x,1pe20.11E3))') xp(i,:)
        read(57,'(3(1x,1pe20.11E3))') vp(i,:)
        radii(i) = calc_mag(xp(i,:))
        speed(i) = calc_mag(vp(i,:))
      enddo


      print *,maxval(speed)
      print *,maxval(radii)

      dr = 1.3d0*maxval(radii) / 100.d0
      dv = 1.3d0*maxval(speed) / 100.d0

      lmax = 5.d0*maxval(radii)
      vmax = 1.3d0*maxval(speed)


      eta = 0.6d0
      dx = 2.d0*lmax / (nx-1)


      dt = eta*dx / vmax

      nstep = 0
      orbit = 1
      t = 0.d0
      dumpt = 500.d0*dt
!      tdump = dumpt
      tf = 5.d0

      tdump = tf


      call out(xp,vp,10)

      do while     (t.lt. tf)

c----------------------------------------------------------
c     1.) vec_x(t+1/2*dt) = vec_x(t) + 1/2*vec_v(t)*dt
c----------------------------------------------------------

      xtmp = xp + 0.5d0* vp* dt


      rho = 0.d0
      rhoi= 0.d0

      call charge_assign(xtmp,rho,rhoi,M)
      call poisson(-4.d0*PI*G*rho*dx**2,-4.d0*PI*G*rhoi*dx**2
     &      ,phi,phii,mm,nt)
      do j = 1,nx
        do i = 1,nx
!            write(61,*) i,j,phi(i,j,nx/2)
        enddo
      enddo

c----------------------------------------------------------
c     2.) vec_g(t+1/2*dt) = -grad(phi(vec_x(t+1/2*dt)))
c----------------------------------------------------------

      call force_interp(xtmp,gp,phi)

      H = adot/a
      Hfac = (1.d0 - H*dt)/(1.d0 + H*dt)
c----------------------------------------------------------
c     3.) vec_v(t+dt) = Hfac*vec_v(t) + a^-3/(1+H*dt) * vec_g(t+1/2*dt)*dt
c----------------------------------------------------------

      vp = Hfac* vp + (gp*dt*a**-3)/(1.d0 + H*dt)

c----------------------------------------------------------
c     4.) vec_x(t+dt) = x(t+1/2*dt) + 1/2*vec_v(t+dt)*dt
c----------------------------------------------------------

      xp = xtmp + 0.5d0* vp* dt


      t = t + dt
      nstep = nstep + 1

      print *, 'nstep, dt, t:' , nstep, dt, t

c----------------------------------------------------------

      do i = 1,npart
        speed(i) = calc_mag(vp(i,:))
      enddo
      vmax = maxval(speed)

      dt = eta*dx / vmax

c----------------------------------------------------------

      if (t.gt.tdump) then
        tdump = tdump + dumpt
        print *, 'dump at time ' ,t
        call out(xp,vp,11)
      endif



      call orbout(xp(119465,:),20)

      enddo


      print *, 'dx,dt:', dx, dt
      print *,''


      stop
      end program



c----------------------------------------------------------
c     write out data
c----------------------------------------------------------

      subroutine out(xp,vp,id)
      implicit none


      include "common.inc"

      real*8 xp(npart,ndim),vp(npart,ndim)
      real*8 speed(npart),radii(npart),calc_mag
      integer id,i,j

      real*8 dr,dv
      integer rhisto(101),vhisto(101)

      common/hist/dr,dv

      do i = 1, npart
        write(id,'(3(1x,1pe20.11E3))') xp(i,:)
        speed(i) = calc_mag(vp(i,:))
        radii(i) = calc_mag(xp(i,:))
      enddo
      flush(id)

      rhisto = 0
      vhisto = 0

!### generates histograms of particle radii and velocities

      do i=1,npart
        do j=1,100
          if(speed(i).ge. dv*dble(j-1).and.speed(i).lt.dv*dble(j))then
            vhisto(j) = vhisto(j) + 1
          endif
          if(radii(i).ge. dr*dble(j-1).and.radii(i).lt.dr*dble(j))then
            rhisto(j) = rhisto(j) + 1
          endif
        enddo
      enddo

      do i=1, 101
        write (id+60,'(1x,1i4,1x,1i9)') i, vhisto(i)
        write (id+65,'(1x,1i4,1x,1i9)') i, rhisto(i)
      enddo

      end subroutine out

      subroutine orbout(xp,id)

      implicit none
      include "common.inc"

      real*8,dimension(ndim) :: xp
      integer :: id

      write(id,'(3(1x,1pe20.11E3))') xp
      

      end subroutine orbout

c----------------------------------------------------------
c     assign density values
c----------------------------------------------------------
      subroutine charge_assign(x,rho,rhoi,M)

      implicit none

      include "common.inc"

      real*8,dimension(npart) :: M
      real*8,dimension(npart,ndim) :: x
      real*8,intent(inout) :: rho(nx,nx,nx)
      real*8,intent(inout) :: rhoi(nx,nx,nx)
      real*8,dimension(npart,ndim) :: A,B
      real*8,dimension(npart,ndim) :: x0
      real*8 Mvol
      integer,dimension(npart,ndim) :: ix
      integer i,j,k

      ix=ceiling((x+lmax)/dx)

      x0 = -lmax + (ix-1)* dx

      A = (x-x0)/dx
      B = 1.d0 - A

      do i = 1,npart

      Mvol = M(i)/(dx**3)

      rho(ix(i,1),ix(i,2),ix(i,3))=rho(ix(i,1),ix(i,2),ix(i,3))
     &                        + Mvol*B(i,1)*B(i,2)*B(i,3)
      rho(ix(i,1),ix(i,2),ix(i,3)+1)=rho(ix(i,1),ix(i,2),ix(i,3)+1)
     &                        + Mvol*B(i,1)*B(i,2)*A(i,3)
      rho(ix(i,1),ix(i,2)+1,ix(i,3))=rho(ix(i,1),ix(i,2)+1,ix(i,3))
     &                        + Mvol*B(i,1)*A(i,2)*B(i,3)
      rho(ix(i,1),ix(i,2)+1,ix(i,3)+1)=rho(ix(i,1),ix(i,2)+1,ix(i,3)+1)
     &                        + Mvol*B(i,1)*A(i,2)*A(i,3)
      rho(ix(i,1)+1,ix(i,2),ix(i,3))=rho(ix(i,1)+1,ix(i,2),ix(i,3))
     &                        + Mvol*A(i,1)*B(i,2)*B(i,3)
      rho(ix(i,1)+1,ix(i,2),ix(i,3)+1)=rho(ix(i,1)+1,ix(i,2),ix(i,3)+1)
     &                        + Mvol*A(i,1)*B(i,2)*A(i,3)
      rho(ix(i,1)+1,ix(i,2)+1,ix(i,3))=rho(ix(i,1)+1,ix(i,2)+1,ix(i,3))
     &                        + Mvol*A(i,1)*A(i,2)*B(i,3)
      rho(ix(i,1)+1,ix(i,2)+1,ix(i,3)+1)
     &                   = rho(ix(i,1)+1,ix(i,2)+1,ix(i,3)+1)
     &                        + Mvol*A(i,1)*A(i,2)*A(i,3)

      enddo

      end subroutine charge_assign

c----------------------------------------------------------
c     interpolate gradient at given position
c----------------------------------------------------------

      subroutine force_interp(x,gp_int,phi)
      use omp_lib
      implicit none

      include "common.inc"

      real*8, dimension(npart,ndim) :: x
      real*8, dimension(npart,ndim) :: x0
      real*8, intent(in) :: phi(nx,nx,nx)
      real*8, dimension(npart,ndim) :: gp_int
      real*8, dimension(npart,ndim) :: A,B
      real*8 grad(ndim,2,2,2)
      integer ix(npart,ndim),id,nt
      integer i,j,k,l


      ix = ceiling((x+lmax)/dx)
      x0 = -lmax + (ix-1)* dx
      A = (x-x0)/dx
      B = 1.d0 - A


      nt = 8
      do l=1,npart


      if( ix(l,1).le. 1 .or. ix(l,2) .le. 1 .or. ix(l,3) .le. 1 .or.
     & ix(l,1).ge.nx.or.ix(l,2).ge.nx.or.ix(l,3).ge.nx) then
        print *,' error: out of bounds',l
        print *, ix(l,:)
        print *,x(l,:)
        stop
      endif
      enddo

      do l=1,npart     


      do k = 0,1
      do j = 0,1
      do i = 0,1
      grad(1,1+i,1+j,1+k) = 
     &      ( phi(ix(l,1)+i+1,ix(l,2)+j,ix(l,3)+k)
     &      - phi(ix(l,1)+i-1,ix(l,2)+j,ix(l,3)+k) )/(2.d0*dx)

      grad(2,1+i,1+j,1+k) = 
     &      ( phi(ix(l,1)+i,ix(l,2)+j+1,ix(l,3)+k)
     &      - phi(ix(l,1)+i,ix(l,2)+j-1,ix(l,3)+k) )/(2.d0*dx)

      grad(3,1+i,1+j,1+k) = 
     &      ( phi(ix(l,1)+i,ix(l,2)+j,ix(l,3)+k+1)
     &      - phi(ix(l,1)+i,ix(l,2)+j,ix(l,3)+k-1) )/(2.d0*dx)
      enddo
      enddo
      enddo

      gp_int(l,:)= B(l,1)*B(l,2)*B(l,3)*grad(:,1,1,1) 
     &           + B(l,1)*B(l,2)*A(l,3)*grad(:,1,1,2)
     &           + B(l,1)*A(l,2)*B(l,3)*grad(:,1,2,1) 
     &           + B(l,1)*A(l,2)*A(l,3)*grad(:,1,2,2)
     &           + A(l,1)*B(l,2)*B(l,3)*grad(:,2,1,1) 
     &           + A(l,1)*B(l,2)*A(l,3)*grad(:,2,1,2)
     &           + A(l,1)*A(l,2)*B(l,3)*grad(:,2,2,1) 
     &           + A(l,1)*A(l,2)*A(l,3)*grad(:,2,2,2)


      enddo


      gp_int = -gp_int

      end subroutine force_interp

      real*8 function calc_mag(xvec)

      real*8, dimension(3), intent(in) :: xvec

      calc_mag = sqrt(xvec(1)**2 + xvec(2)**2 + xvec(3)**2 )

      end function calc_mag

      subroutine poisson(x0,y0,xi,yi,m,nt)
c      program poisson
c      x0 and y0 are the original 3D arrays of dimension 2**m
c      calls 'my' 3d fft routine
c      xt and yt are the transformed arrays, with itype = 1, -1 fixing direction

c      Construct 3D fft using 1D fft
c     This version uses OpenMP for outer do loops
c     needs -openmp compiler flag
      use omp_lib


      implicit none

      real*8, parameter ::       tpi = 8.d0*atan(1.d0)

      real*8 an1,an2,an3,fac
      real*8 x0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 y0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xi(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yi(0:2**m-1,0:2**m-1,0:2**m-1)
      integer n1,n2,n3,m,nt,id,i,j,k,n1c,n2c,n3c


      n1 = 2**m
      n2 = 2**m
      n3 = 2**m

c      n1c = n1/2
c      n2c = n2/2
c      n3c = n3/2



C      open(unit =10, file = 'oftr.dat', form = 'binary')
C      open(unit =11, file = 'ofti.dat', form = 'binary')

c      write(10)x0
c      write(11)y0

      call fft3d(x0,y0,xt,yt,m,1,nt)

c     Now construct fft of potential using finite difference formula
c     note singularity for i = j = k = 0

      an1 = 1.d0/dble(n1)
      an2 = 1.d0/dble(n2)
      an3 = 1.d0/dble(n3)


      !$omp parallel private(id,i)
      do k = 0,n3-1
      do j = 0,n2-1
        id = omp_get_thread_num()

      do i = id*n1/nt,(id+1)*n1/nt -1
c     do i = 0,n1-1
      if(i+j+k .eq. 0 )then
        fac = 0.d0
      else
        fac =-0.5d0/
     >   (cos(tpi*i*an1)+cos(tpi*j*an2)+cos(tpi*k*an3) - 3.d0)
      endif
      xt(i,j,k) = fac*xt(i,j,k)
      yt(i,j,k) = fac*yt(i,j,k)
      enddo
      enddo
      enddo

c      Now find potential using inverse fft

      !$omp end parallel

      call fft3d(xt,yt,xi,yi,m,-1,nt)

c      do i = 0,n1-1
c        write(61,'(i5,1p2e12.3)')i,xi(i,n2c,n3c),yi(i,n2c,n3c)
c      enddo


C      open(unit =12, file = 'rftr.dat', form = 'binary')
C      open(unit =13, file = 'rfti.dat', form = 'binary')

c      write(12)xi
c      write(13)yi

c      stop
c      end program
      return
      end

       subroutine fft3d(x0,y0,xt,yt,m,itype,nt)
c      program 3dft
c      x0 and y0 are the original 3D arrays of dimension 2**m
c      xt and yt are the transformed arrays, with itype = 1, -1 fixing direction
c      nt is the number of threads specified in main

c      Construct 3D fft using 1D fft
c     Remember that the normal order fft is (n1+1)*(n2+1)*n3+1)
c     This version uses OpenMP for outer do loops
c     needs -openmp compiler flag
      use omp_lib

      implicit none


      real*8 x(0:2**m-1),y(0:2**m-1)
      real*8 x0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 y0(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 xt(0:2**m-1,0:2**m-1,0:2**m-1)
      real*8 yt(0:2**m-1,0:2**m-1,0:2**m-1)
      integer m,n1,n2,n3,itype,nt
      integer id,i,j,k



c     set size of grid

      n1 = 2**m
      n2 = 2**m
      n3 = 2**m

c     Construct 3D fft by making n2*n3 1D passes of length n1
c     Followed by n1*n3 passes of length n2 on the second index
c     Followed by n1*n2 passes of length n3 on the third index
c     First decompose 3D array into a set of 1D vectors 

     
      !$omp parallel private(id,k,x,y)
        id = omp_get_thread_num()

      do k = id*n3/nt,(id+1)*n3/nt -1
       do j = 0,n2-1

        do i = 0,n1-1
	  x(i) = x0(i,j,k)
	  y(i) = y0(i,j,k)
        enddo


      call cfft(x,y,n1,m,itype)


        do i = 0,n1-1
	  xt(i,j,k) = x(i)
	  yt(i,j,k) = y(i)
        enddo

       enddo
      enddo

      !$omp end parallel

      !$omp parallel private(id,i,x,y)
        id = omp_get_thread_num()
      do i = id*n1/nt,(id+1)*n1/nt -1
        do k = 0,n3-1
         do j = 0,n2-1
	  x(j) = xt(i,j,k)
	  y(j) = yt(i,j,k)
         enddo

      call cfft(x,y,n2,m,itype)

         do j = 0,n2-1
	  xt(i,j,k) = x(j)
	  yt(i,j,k) = y(j)
         enddo

        enddo
       enddo

      !$omp end parallel

      !$omp parallel private(id,i,x,y)
        id = omp_get_thread_num()
      do i = id*n1/nt,(id+1)*n1/nt -1
        do j = 0,n2-1
         do k = 0,n3-1
	  x(k) = xt(i,j,k)
	  y(k) = yt(i,j,k)
         enddo

      call cfft(x,y,n3,m,itype)

         do k = 0,n3-1
	  xt(i,j,k) = x(k)
	  yt(i,j,k) = y(k)
         enddo

        enddo
       enddo

      !$omp end parallel

      return

      end

      
      SUBROUTINE CFFT( X, Y, N, M, ITYPE )
      implicit none
c
c  This is a very simple Cooley-Tukey Radix-2 DIF Complex FFT.
c 
c  The data sequence is of size N = 2**M.
c  X and Y contain the real and imaginary parts of the data.
c
c  ITYPE .ne. -1 for forward transform
c  ITYPE .eq. -1 for backward transform
c
c  The forward transform computes
c     Z(k) = sum_{j=0}^{N-1} z(j)*exp(-2ijk*pi/N)
c
c  The backward transform computes
c     z(j) = (1/N) * sum_{k=0}^{N-1} Z(k)*exp(2ijk*pi/N)
c
c
c  Steve Kifowit, 31 October 1998
c       
c
c ... Scalar arguments ...
      INTEGER  N, M, ITYPE
c ... Array arguments ...
      REAL*8  X(*), Y(*)
c ... Local scalars ...
      INTEGER  I, J, K, L, N1, N2, IE, IA
      REAL*8  C, S, XT, YT, P, A
c ... Parameters ...
      Real*8,PARAMETER :: TWOPI = 6.283185307179586476925287 
c ... Intrinsic functions
      INTRINSIC  SIN, COS
c
c ... Exe. statements ...
c
c ... Quick return ...
      IF ( N .EQ. 1 ) RETURN
c
c ... Conjugate if necessary ...
      IF ( ITYPE .EQ. -1 ) THEN
	 DO 1, I = 1, N
	    Y(I) = - Y(I)
 1       CONTINUE
      ENDIF
c
c ... Main loop ...
      P = TWOPI / N
      N2 = N
      DO 10, K = 1, M
         N1 = N2
         N2 = N2 / 2
         IE = N / N1
         IA = 1
         DO 20, J = 1, N2
	    A = ( IA - 1 ) * P
            C = COS( A ) 
            S = SIN( A )
            IA = IA + IE
            DO 30, I = J, N, N1
               L = I + N2
               XT = X(I) - X(L)
               X(I) = X(I) + X(L)
               YT = Y(I) - Y(L)
               Y(I) = Y(I) + Y(L)
               X(L) = C * XT + S * YT
               Y(L) = C * YT - S * XT
 30         CONTINUE
 20      CONTINUE
 10   CONTINUE
c
c ... Bit reversal permutation ...
 100  J = 1
      N1 = N - 1
      DO 104, I = 1, N1
         IF ( I .GE. J ) GOTO 101
         XT = X(J)
         X(J) = X(I)
         X(I) = XT
         YT = Y(J)
         Y(J) = Y(I)
         Y(I) = YT
 101     K = N / 2
 102     IF ( K .GE. J ) GOTO 103
         J = J - K
         K = K / 2
         GOTO 102
 103     J = J + K
 104  CONTINUE
c
c ... Conjugate and normalize if necessary ...
      IF ( ITYPE .EQ. -1 ) THEN
	 DO 3, I = 1, N
	    Y(I) = - Y(I) / N
            X(I) = X(I) / N
 3       CONTINUE
      ENDIF
      RETURN
c
c ... End of subroutine CFFT ...
c
      END

