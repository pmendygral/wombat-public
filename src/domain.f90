#include "config.h"

MODULE Mod_Domain

!######################################################################
!#
!# FILENAME: mod_domain.f90
!#
!# DESCRIPTION: This module provides a class for packaging and managing
!#  groups of patches as a single domain.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE OMP_LIB
USE Mod_Globals
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch
USE Mod_Problem
USE Mod_Decomposition
USE Mod_Error

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Domain

!### define a data type for this module ###
TYPE :: Domain

    !### default everything as private ###
    PRIVATE

    TYPE(Patch), PUBLIC, ALLOCATABLE :: domain1d(:), domain2d(:,:), domain3d(:,:,:)
    REAL(WRD), PUBLIC :: x0, y0, z0, dx, dt, t, maxsignal
    INTEGER(WIS), PUBLIC :: nsteps, ndim, xi0, yi0, zi0, npx, npy, npz, nthreads, nLivePatches
    LOGICAL(WIS), PUBLIC :: hasPatches, isLive, is1d, is2d, is3d, load_balanced
    LOGICAL(WIS) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound

    !### we kee track of the rank of neighbors for each Patch ###
    INTEGER(WIS), ALLOCATABLE :: neighbors1d(:,:), neighbors2d(:,:,:), neighbors3d(:,:,:,:)

    CONTAINS

    !### default everything as private ###
    PRIVATE

    !### private routines ###
    PROCEDURE :: initdomain
    PROCEDURE :: init_neighbors
    PROCEDURE :: pack_patch_bounds

    !### thread safe methods (called at the same time by all threads) ###
    PROCEDURE, PUBLIC :: build
    PROCEDURE, PUBLIC :: unbuild
    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: unbuild_all_patches
    PROCEDURE, PUBLIC :: unbuild_some_patches
    PROCEDURE, PUBLIC :: build_all_patches
    PROCEDURE, PUBLIC :: build_some_patches
    PROCEDURE, PUBLIC :: mark_all_patch_bounds_unresolved
    PROCEDURE, PUBLIC :: pack_some_patch_bounds
    PROCEDURE, PUBLIC :: unpack_all_patch_bounds_local
    PROCEDURE, PUBLIC :: broadcast_t_and_dt
    PROCEDURE, PUBLIC :: applyproblem_patch_bounds

END TYPE Domain

!### create an interface to the constructor ###
INTERFACE Domain

    MODULE PROCEDURE constructor

END INTERFACE Domain

!### object methods ###
CONTAINS

!### constructor for the class ###
!### accepts: ndim = # of dimensions ###
!###          npx[yz] = # of patches in that dimension ###
!###          [xyz]o = world grid position of (1,1,1) origin of domain (at zone center) ###
FUNCTION constructor(nthreads, ndim, npx, npy, npz, rankloc, simunits)

    TYPE(Domain) :: constructor
    INTEGER(WIS), INTENT(IN) :: nthreads, ndim, npx, npy, npz
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initdomain(nthreads, ndim, npx, npy, npz, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### actual initialization of object (not directly called by user) ###
SUBROUTINE initdomain(self, nthreads, ndim, npx, npy, npz, rankloc, simunits)

    CLASS(Domain) :: self
    INTEGER(WIS), INTENT(IN) :: nthreads, ndim, npx, npy, npz
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    !### assign the settings ###
    self%nthreads  = nthreads
    self%ndim      = ndim
    self%npx       = npx
    self%npy       = npy
    self%npz       = npz
    self%xi0       = rankloc%izone_origin(1)
    self%yi0       = rankloc%izone_origin(2)
    self%zi0       = rankloc%izone_origin(3)
    self%x0        = rankloc%rzone_origin(1)
    self%y0        = rankloc%rzone_origin(2)
    self%z0        = rankloc%rzone_origin(3)
    self%dx        = simunits%dx
    self%dt        = simunits%dt
    self%t         = simunits%t
    self%lx_bound  = rankloc%is_boundary(1,1)
    self%hx_bound  = rankloc%is_boundary(2,1)
    self%ly_bound  = rankloc%is_boundary(1,2)
    self%hy_bound  = rankloc%is_boundary(2,2)
    self%lz_bound  = rankloc%is_boundary(1,3)
    self%hz_bound  = rankloc%is_boundary(2,3)
    self%maxsignal = 0.0_WRD
    self%is1d      = .FALSE.
    self%is2d      = .FALSE.
    self%is3d      = .FALSE.
    IF (ndim .EQ. 1) THEN
        self%npy  = 1
        self%npz  = 1
        self%is1d = .TRUE.
    ELSE IF (ndim .EQ. 2) THEN
        self%npz  = 1
        self%is2d = .TRUE.
    ELSE IF (ndim .EQ. 3) THEN
        self%is3d = .TRUE.
    ELSE
        ASSERT(.FALSE., "An unsupported number of dimensions was requested.  Only 1-3d is supported.")
    END IF

    !### default to inactive ###
    self%hasPatches    = .FALSE.
    self%isLive        = .FALSE.
    self%load_balanced = .TRUE.

    self%nsteps = 0

    RETURN

END SUBROUTINE initdomain

!### initialize the Patch neighbors list assuming all internal Patches are local neighbors, and bounds follow the passed neighbors list ###
!### note that this is NOT thread safe ###
SUBROUTINE init_neighbors(self, myrank, neighbors)

    CLASS(Domain) :: self
    INTEGER(WIS), INTENT(IN) :: myrank, neighbors(:)
    INTEGER(WIS) :: pi, pj, pk

    !### here we're just following the expected ordering that comes from Mod_Decomposition ###
    IF (self%is1d) THEN

        self%neighbors1d             = myrank
        self%neighbors1d(1,1)        = neighbors(1)  ! left
        self%neighbors1d(self%npx,2) = neighbors(2)  ! right

    ELSE IF (self%is2d) THEN
        
        self%neighbors2d = myrank
        
        !### do the left and right faces and the corners for those that go to the left and right neighbors ###
        DO pj = 1, self%npy

            self%neighbors2d(1,pj,1)        = neighbors(1)
            self%neighbors2d(self%npx,pj,2) = neighbors(2)

            self%neighbors2d(1,pj,5)        = neighbors(1)
            self%neighbors2d(1,pj,7)        = neighbors(1)

            self%neighbors2d(self%npx,pj,6) = neighbors(2)
            self%neighbors2d(self%npx,pj,8) = neighbors(2)

        END DO

        !### do the bottom and top faces and the corners for those that go to the bottom and top neighbors ###
        DO pi = 1, self%npx

            self%neighbors2d(pi,1,3)        = neighbors(3)
            self%neighbors2d(pi,self%npy,4) = neighbors(4)

            self%neighbors2d(pi,1,5)        = neighbors(3)
            self%neighbors2d(pi,1,8)        = neighbors(3)

            self%neighbors2d(pi,self%npy,6) = neighbors(4)
            self%neighbors2d(pi,self%npy,7) = neighbors(4)

        END DO

        !### set the actual full domain corners (fixes the incorrect ones above where needed) ###
        self%neighbors2d(1,1,5)               = neighbors(5)
        self%neighbors2d(self%npx,self%npy,6) = neighbors(6)
        self%neighbors2d(1,self%npy,7)        = neighbors(7)
        self%neighbors2d(self%npx,1,8)        = neighbors(8)

    ELSE IF (self%is3d) THEN

        self%neighbors3d = myrank

        !### do the left and right along X faces ###
        DO pk = 1, self%npz

            DO pj = 1, self%npy

                self%neighbors3d(1,pj,pk,1)        = neighbors(1)
                self%neighbors3d(self%npx,pj,pk,2) = neighbors(2)

            END DO

        END DO

        !### set those using left and right neighbors for corners and edges ###
        DO pk = 1, self%npz

            DO pj = 1, self%npy

                self%neighbors3d(1,pj,pk,7)         = neighbors(1)
                self%neighbors3d(1,pj,pk,10)        = neighbors(1)
                self%neighbors3d(1,pj,pk,11)        = neighbors(1)
                self%neighbors3d(1,pj,pk,14)        = neighbors(1)

                self%neighbors3d(self%npx,pj,pk,8)  = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,9)  = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,12) = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,13) = neighbors(2)

                self%neighbors3d(1,pj,pk,19)        = neighbors(1)
                self%neighbors3d(1,pj,pk,21)        = neighbors(1)
                self%neighbors3d(1,pj,pk,23)        = neighbors(1)
                self%neighbors3d(1,pj,pk,26)        = neighbors(1)

                self%neighbors3d(self%npx,pj,pk,20) = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,22) = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,24) = neighbors(2)
                self%neighbors3d(self%npx,pj,pk,25) = neighbors(2)

            END DO

        END DO

        !### do the low and high Y faces ###
        DO pk = 1, self%npz

            DO pi = 1, self%npx

                self%neighbors3d(pi,1,pk,3)        = neighbors(3)
                self%neighbors3d(pi,self%npy,pk,4) = neighbors(4)

            END DO

        END DO
        
        !### set those using low and high Y neighbors for corners and edges ###
        DO pk = 1, self%npz

            DO pi = 1, self%npx
                
                self%neighbors3d(pi,1,pk,8)         = neighbors(3)
                self%neighbors3d(pi,1,pk,10)        = neighbors(3)
                self%neighbors3d(pi,1,pk,12)        = neighbors(3)
                self%neighbors3d(pi,1,pk,14)        = neighbors(3)

                self%neighbors3d(pi,self%npy,pk,7)  = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,9)  = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,11) = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,13) = neighbors(4)
                
                self%neighbors3d(pi,1,pk,16)        = neighbors(3)
                self%neighbors3d(pi,1,pk,18)        = neighbors(3)
                self%neighbors3d(pi,1,pk,24)        = neighbors(3)
                self%neighbors3d(pi,1,pk,26)        = neighbors(3)

                self%neighbors3d(pi,self%npy,pk,15) = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,17) = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,23) = neighbors(4)
                self%neighbors3d(pi,self%npy,pk,25) = neighbors(4)


            END DO

        END DO

        !### set the low and high Z faces ###
        DO pj = 1, self%npy

            DO pi = 1, self%npx

                self%neighbors3d(pi,pj,1,5)        = neighbors(5)
                self%neighbors3d(pi,pj,self%npz,6) = neighbors(6)

            END DO

        END DO

        !### set those using low and high Z neighbors for corners and edges ###
        DO pj = 1, self%npy

            DO pi = 1, self%npx
                
                self%neighbors3d(pi,pj,1,7)         = neighbors(5)
                self%neighbors3d(pi,pj,1,9)         = neighbors(5)
                self%neighbors3d(pi,pj,1,12)        = neighbors(5)
                self%neighbors3d(pi,pj,1,14)        = neighbors(5)

                self%neighbors3d(pi,pj,self%npz,8)  = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,10) = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,11) = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,13) = neighbors(6)
                
                self%neighbors3d(pi,pj,1,15)        = neighbors(5)
                self%neighbors3d(pi,pj,1,18)        = neighbors(5)
                self%neighbors3d(pi,pj,1,19)        = neighbors(5)
                self%neighbors3d(pi,pj,1,22)        = neighbors(5)

                self%neighbors3d(pi,pj,self%npz,16) = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,17) = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,20) = neighbors(6)
                self%neighbors3d(pi,pj,self%npz,21) = neighbors(6)


            END DO

        END DO

        !#### set the back bottom, front top, back top, and front bottom edges and those as the corners ###
        DO pi = 1, self%npx

            self%neighbors3d(pi,self%npy,1,15)        = neighbors(15)
            self%neighbors3d(pi,1,self%npz,16)        = neighbors(16)
            self%neighbors3d(pi,self%npy,self%npz,17) = neighbors(17)
            self%neighbors3d(pi,1,1,18)               = neighbors(18)

            self%neighbors3d(pi,self%npy,1,7)         = neighbors(15)
            self%neighbors3d(pi,self%npy,1,9)         = neighbors(15)

            self%neighbors3d(pi,1,self%npz,8)         = neighbors(16)
            self%neighbors3d(pi,1,self%npz,10)        = neighbors(16)

            self%neighbors3d(pi,self%npy,self%npz,11) = neighbors(17)
            self%neighbors3d(pi,self%npy,self%npz,13) = neighbors(17)

            self%neighbors3d(pi,1,1,12)               = neighbors(18)
            self%neighbors3d(pi,1,1,14)               = neighbors(18)

        END DO

        !#### set the left bottom, right top, left top, and right bottom edges and those as the corners ###
        DO pj = 1, self%npy

            self%neighbors3d(1,pj,1,19)               = neighbors(19)
            self%neighbors3d(self%npx,pj,self%npz,20) = neighbors(20)
            self%neighbors3d(1,pj,self%npz,21)        = neighbors(21)
            self%neighbors3d(self%npx,pj,1,22)        = neighbors(22)

            self%neighbors3d(1,pj,1,7)                = neighbors(19)
            self%neighbors3d(1,pj,1,14)               = neighbors(19)

            self%neighbors3d(self%npx,pj,self%npz,8)  = neighbors(20)
            self%neighbors3d(self%npx,pj,self%npz,13) = neighbors(20)

            self%neighbors3d(1,pj,self%npz,10)        = neighbors(21)
            self%neighbors3d(1,pj,self%npz,11)        = neighbors(21)

            self%neighbors3d(self%npx,pj,1,9)         = neighbors(22)
            self%neighbors3d(self%npx,pj,1,12)        = neighbors(22)

        END DO

        !#### set the back left, front right, back right, front left edges and those as the corners ###
        DO pk = 1, self%npz

            self%neighbors3d(1,self%npy,pk,23)        = neighbors(23)
            self%neighbors3d(self%npx,1,pk,24)        = neighbors(24)
            self%neighbors3d(self%npx,self%npy,pk,25) = neighbors(25)
            self%neighbors3d(1,1,pk,26)               = neighbors(26)

            self%neighbors3d(1,self%npy,pk,7)         = neighbors(23)
            self%neighbors3d(1,self%npy,pk,11)        = neighbors(23)

            self%neighbors3d(self%npx,1,pk,8)         = neighbors(24)
            self%neighbors3d(self%npx,1,pk,12)        = neighbors(24)

            self%neighbors3d(self%npx,self%npy,pk,9)  = neighbors(25)
            self%neighbors3d(self%npx,self%npy,pk,13) = neighbors(25)

            self%neighbors3d(1,1,pk,10)               = neighbors(26)
            self%neighbors3d(1,1,pk,14)               = neighbors(26)

        END DO
        
        !### finally fix up the full domain corners ###
        self%neighbors3d(1,self%npy,1,7)                = neighbors(7)
        self%neighbors3d(self%npx,1,self%npz,8)         = neighbors(8)
        self%neighbors3d(self%npx,self%npy,1,9)         = neighbors(9)
        self%neighbors3d(1,1,self%npz,10)               = neighbors(10)
        self%neighbors3d(1,self%npy,self%npz,11)        = neighbors(11)
        self%neighbors3d(self%npx,1,1,12)               = neighbors(12)
        self%neighbors3d(self%npx,self%npy,self%npz,13) = neighbors(13)
        self%neighbors3d(1,1,1,14)                      = neighbors(14)

    END IF

END SUBROUTINE init_neighbors

!### allocates patch array and initialize each patch (it does NOT allocate the patch data, however) ###
SUBROUTINE build(self, myrank, neighbors, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)

    CLASS(Domain) :: self
    INTEGER(WIS), INTENT(IN) :: neighbors(:)
    INTEGER(WIS), INTENT(IN) :: myrank, nvars, npass, ncrs, nx, ny, nz, nb
    INTEGER(WIS), INTENT(IN) :: ntotbms, ndmspbm, nslotspdm, nstepspdefrag
    REAL(WRD), INTENT(IN) :: DM_buff_fac
    INTEGER(WIS) :: pi, pj, pk, xi0, yi0, zi0, p
    LOGICAL(WIS), INTENT(IN) :: mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On

    !### perform the necessary allocate and initialization ###
    IF (self%is1d .AND. .NOT. ALLOCATED(self%domain1d)) THEN

        !$OMP BARRIER
        !$OMP SINGLE
        ALLOCATE(self%domain1d(self%npx), self%neighbors1d(self%npx, 2))
        CALL self%init_neighbors(myrank, neighbors)
        !$OMP END SINGLE

        !$OMP DO SCHEDULE(STATIC)
        DO pi = 1, self%npx

            !$OMP CRITICAL
            self%domain1d(pi) = Patch(self%ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, &
                                      (self%xi0 + (pi - 1)*nx), self%yi0, self%zi0, (self%x0 + (pi - 1)*nx*self%dx), self%y0, &
                                      self%z0, self%dx, self%dt, self%t, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)
            !$OMP END CRITICAL

        END DO
        !$OMP END DO

    ELSE IF (self%is2d .AND. .NOT. ALLOCATED(self%domain2d)) THEN

        !$OMP BARRIER
        !$OMP SINGLE
        ALLOCATE(self%domain2d(self%npx, self%npy), self%neighbors2d(self%npx, self%npy, 8))
        CALL self%init_neighbors(myrank, neighbors)
        !$OMP END SINGLE

        !$OMP DO SCHEDULE(STATIC)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)

            !$OMP CRITICAL
            self%domain2d(pi,pj) = Patch(self%ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, &
                 (self%xi0 + (pi - 1)*nx), (self%yi0 + (pj - 1)*ny), self%zi0, &
                 (self%x0 + (pi - 1)*nx*self%dx), (self%y0 + (pj - 1)*ny*self%dx), self%z0, self%dx, self%dt, self%t, &
                 ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)
            !$OMP END CRITICAL

        END DO
        !$OMP END DO

    ELSE IF(self%is3d .AND. .NOT. ALLOCATED(self%domain3d)) THEN

        !$OMP BARRIER
        !$OMP SINGLE
        ALLOCATE(self%domain3d(self%npx, self%npy, self%npz), self%neighbors3d(self%npx, self%npy, self%npz, 26))
        CALL self%init_neighbors(myrank, neighbors)
        !$OMP END SINGLE

        !$OMP DO SCHEDULE(STATIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)

            !$OMP CRITICAL
            self%domain3d(pi,pj,pk) = Patch(self%ndim, nvars, npass, ncrs, mhdOn, passOn, crsOn, statgravOn, vargravOn, DM_On, nx, ny, nz, nb, &
                 (self%xi0 + (pi - 1)*nx), (self%yi0 + (pj - 1)*ny), (self%zi0 + (pk - 1)*nz), &
                 (self%x0 + (pi - 1)*nx*self%dx), (self%y0 + (pj - 1)*ny*self%dx), (self%z0 + (pk - 1)*nz*self%dx), &
                 self%dx, self%dt, self%t, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, DM_buff_fac)
            !$OMP END CRITICAL

        END DO
        !$OMP END DO

    END IF

    !### set the domain as active ###
    !$OMP SINGLE
    self%hasPatches = .TRUE.
    !$OMP END SINGLE

END SUBROUTINE build

!### deallocates all patch arrays and makes the domain inactive ###
SUBROUTINE unbuild(self)

    CLASS(Domain) :: self
    INTEGER(WIS) :: pi, pj, pk, p

    !### for every patch we must unbuild it before we deallocate the array of patches ###
    IF (self%is1d .AND. ALLOCATED(self%domain1d)) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO pi = 1, self%npx

           CALL self%domain1d(pi)%unbuild()

        END DO
        !$OMP END DO

        !$OMP BARRIER
        !$OMP SINGLE
        DEALLOCATE(self%domain1d)
        DEALLOCATE(self%neighbors1d)
        !$OMP END SINGLE

    ELSE IF (self%is2d .AND. ALLOCATED(self%domain2d)) THEN
        
        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)

            CALL self%domain2d(pi,pj)%unbuild()

        END DO
        !$OMP END DO

        !$OMP BARRIER
        !$OMP SINGLE
        DEALLOCATE(self%domain2d)
        DEALLOCATE(self%neighbors2d)
        !$OMP END SINGLE

    ELSE IF (self%is3d .AND. ALLOCATED(self%domain3d)) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)

            CALL self%domain3d(pi,pj,pk)%unbuild()

        END DO
        !$OMP END DO

        !$OMP BARRIER
        !$OMP SINGLE
        DEALLOCATE(self%domain3d)
        DEALLOCATE(self%neighbors3d)
        !$OMP END SINGLE

    END IF

    !### set the domain as active ###
    !$OMP SINGLE
    self%hasPatches = .FALSE.
    !$OMP END SINGLE

END SUBROUTINE unbuild

!### totally disable and clean the object ###
SUBROUTINE destroy(self)

    CLASS(Domain) :: self

    !### clean up our patches ###
    CALL self%unbuild_all_patches()

    !### deallocate ###
    CALL self%unbuild()

    !### disable ###
    !$OMP SINGLE
    self%isLive = .FALSE.
    !$OMP END SINGLE

END SUBROUTINE destroy

!### method for unbuilding all patches within a domain ###
SUBROUTINE unbuild_all_patches(self)

    CLASS(Domain) :: self
    INTEGER(WIS) :: pi, pj, pk, p

    !### simply call build on each patch to allocate and touch it ###
    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO pi = 1, self%npx
          
            CALL self%domain1d(pi)%unbuild()
          
        END DO
        !$OMP END DO
       
    ELSE IF (self%is2d) THEN
       
        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)
             
            CALL self%domain2d(pi,pj)%unbuild()
          
        END DO
        !$OMP END DO
       
    ELSE IF (self%is3d) THEN

       !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)
                
            CALL self%domain3d(pi,pj,pk)%unbuild()
            
        END DO
        !$OMP END DO
       
    END IF

    !$OMP SINGLE
    self%hasPatches = .FALSE.
    !$OMP END SINGLE

END SUBROUTINE unbuild_all_patches

!### method for building some patches within a domain based on an input mask ###
! TODO, THIS ROUTINE HAS NOT BEEN CLEANED UP FOR THREADS
SUBROUTINE unbuild_some_patches(self, mask)

    CLASS(Domain) :: self
    LOGICAL(WIS), INTENT(IN) :: mask(:)
    REAL(WRD) :: load_var, load_avg
    INTEGER(WIS) :: i, pi, pj, pk, tid, load(0:self%nthreads-1)

    !### initialize the load array to relfect no work per thread ###
    !$OMP SINGLE
    load = 0
    !$OMP END SINGLE

    !### call build on each patch to allocate and touch it ###
    !### if we are allocating the patch OR if it is already allocated we need to increment ###
    !### our counter of how much work each thread is doing ###
    IF (self%is1d) THEN

       !$OMP DO SCHEDULE(DYNAMIC)
       DO pi = 1, self%npx
          
          IF (mask(pi)) THEN

             CALL self%domain1d(pi)%unbuild()

          END IF
          IF (self%domain1d(pi)%hasZones) THEN

             tid = OMP_GET_THREAD_NUM()
             load(tid) = load(tid) + 1
             
          END IF
          
       END DO
       !$OMP END DO
       
    ELSE IF (self%is2d) THEN
       
       !$OMP DO SCHEDULE(STATIC)
! removed collapsd
       DO pj = 1, self%npy
          
          DO pi = 1, self%npx
             
             IF (mask(pi + (pj - 1)*self%npx)) THEN

                CALL self%domain2d(pi,pj)%unbuild()

             END IF
             IF (self%domain2d(pi,pj)%hasZones) THEN

                tid = OMP_GET_THREAD_NUM()
                load(tid) = load(tid) + 1
             
             END IF
             
          END DO
          
       END DO
       !$OMP END DO
       
    ELSE IF (self%is3d) THEN

       !$OMP DO SCHEDULE(STATIC)
! removed collapsd
       DO pk = 1, self%npz
          
          DO pj = 1, self%npy
             
             DO pi = 1, self%npx
                
                IF (mask(pi + (pj - 1)*self%npx + (pk - 1)*self%npx*self%npy)) THEN

                   CALL self%domain3d(pi,pj,pk)%unbuild()

                END IF
                IF (self%domain3d(pi,pj,pk)%hasZones) THEN

                   tid = OMP_GET_THREAD_NUM()
                   load(tid) = load(tid) + 1
             
                END IF
                
             END DO
             
          END DO
          
       END DO
       !$OMP END DO
       
    END IF
    !$OMP BARRIER

    !### measure how uniform the load is on each thread.  if they are not very well balanced ###
    !### we'll want to set a flag that may be used to change the default thread scheduling ###
    !$OMP SINGLE
    
    load_var = 0.0_WRD
    load_avg = REAL(SUM(load),WRD) / self%nthreads
    DO i = tid, self%nthreads - 1

       load_var = load_var + (REAL(load(tid),WRD) - load_avg)**2

    END DO

    !### make make a ratio between the sample variance and the uniform average number of patches a thread ###
    !### would handle.  if that number is >= 10% we'll call it unbalanced (NOTE: MAKE THIS A PARAMETER SINCE IT'S A BALANCE OF BANDWIDTH AND COMPUTATIONAL INTENSITY) ###
    load_var = (self%nthreads * load_var) / (self%npx * self%npy * self%npz * (self%nthreads - 1))
    IF (load_var .GE. 0.1_WRD) THEN
       self%load_balanced = .FALSE.
    ELSE
       self%load_balanced = .TRUE.
    END IF

    !### ADD SOME LOGIC REGARDING PATCH POPULATION AND SETTING HASPATCHES

    !$OMP END SINGLE

END SUBROUTINE unbuild_some_patches

!### quick method for building all patches within a domain ###
SUBROUTINE build_all_patches(self, prob, rankloc, simunits, withInit)

    CLASS(Domain) :: self
    TYPE(Problem) :: prob
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS) :: pi, pj, pk, ierr, p, tid
    LOGICAL(WIS), INTENT(IN) :: withInit
    LOGICAL(WIS) :: patch_bound(2,3)

    tid = OMP_GET_THREAD_NUM() + 1

    !### simply call build on each patch to allocate and touch it ###
    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(STATIC)
        DO pi = 1, self%npx
          
            CALL self%domain1d(pi)%build()

            !### if this is an AMR Patch here is where we might add the prologation operator (somehow).  think about it ###

            !### if we're were told to initialize the Patch with the user initilaization then do so ###
            IF (withInit) THEN

                patch_bound = .FALSE.
                IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                CALL prob%patch_init(self%domain1d(pi), rankloc, simunits, patch_bound, tid, ierr)

            END IF
          
        END DO
        !$OMP END DO
       
    ELSE IF (self%is2d) THEN
       
        !$OMP DO SCHEDULE(STATIC)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)
            
            CALL self%domain2d(pi,pj)%build()

            !### if we're were told to initialize the Patch with the user initilaization then do so ###
            IF (withInit) THEN
                 
                patch_bound = .FALSE.
                IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
                IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
                CALL prob%patch_init(self%domain2d(pi,pj), rankloc, simunits, patch_bound, tid, ierr)

            END IF
          
        END DO
        !$OMP END DO
       
    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(STATIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)
                
            CALL self%domain3d(pi,pj,pk)%build()

            !### if we're were told to initialize the Patch with the user initilaization then do so ###
            IF (withInit) THEN
                
                patch_bound = .FALSE.
                IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
                IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
                IF (self%lz_bound .AND. pk .EQ. 1       ) patch_bound(1,3) = .TRUE.
                IF (self%hz_bound .AND. pk .EQ. self%npz) patch_bound(2,3) = .TRUE.
                CALL prob%patch_init(self%domain3d(pi,pj,pk), rankloc, simunits, patch_bound, tid, ierr)
                    
            END IF
          
        END DO
        !$OMP END DO
       
    END IF

    !$OMP SINGLE
    self%load_balanced = .TRUE.
    !$OMP END SINGLE

END SUBROUTINE build_all_patches

!### build up patches based on an input mask ###
!### the input mask is a collapsed, 1d logical array.  T = build it and F = don't ### 
! TODO, THIS ROUTINE HAS NOT BEEN CLEANED UP FOR THREADS
SUBROUTINE build_some_patches(self, mask, prob, rankloc, simunits, withInit)

    CLASS(Domain) :: self
    LOGICAL(WIS), INTENT(IN) :: mask(:)
    TYPE(Problem) :: prob
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: withInit
    REAL(WRD) :: load_var, load_avg
    INTEGER(WIS) :: i, pi, pj, pk, tid, ierr, load(1:self%nthreads)
    LOGICAL(WIS) :: patch_bound(2,3)

    tid = OMP_GET_THREAD_NUM() + 1

    !### initialize the load array to relfect no work per thread ###
    !$OMP SINGLE
    load = 0
    !$OMP END SINGLE

    !### call build on each patch to allocate and touch it ###
    !### if we are allocating the patch OR if it is already allocated we need to increment ###
    !### our counter of how much work each thread is doing ###
    IF (self%is1d) THEN

       !$OMP DO SCHEDULE(STATIC)
       DO pi = 1, self%npx
          
          IF (mask(pi)) THEN

             CALL self%domain1d(pi)%build()

             !### if we're were told to initialize the Patch with the user initilaization then do so ###
             IF (withInit) THEN
                 
                 patch_bound = .FALSE.
                 IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                 IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                 CALL prob%patch_init(self%domain1d(pi), rankloc, simunits, patch_bound, tid, ierr)
                 
             END IF

          END IF
          IF (self%domain1d(pi)%hasZones) THEN

             load(tid) = load(tid) + 1
             
          END IF
          
       END DO
       !$OMP END DO
       
    ELSE IF (self%is2d) THEN
       
       !$OMP DO SCHEDULE(STATIC)
! removed collapsd
       DO pj = 1, self%npy
          
          DO pi = 1, self%npx
             
             IF (mask(pi + (pj - 1)*self%npx)) THEN

                CALL self%domain2d(pi,pj)%build()

                !### if we're were told to initialize the Patch with the user initilaization then do so ###
                IF (withInit) THEN
                    
                    patch_bound = .FALSE.
                    IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                    IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                    IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
                    IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
                    CALL prob%patch_init(self%domain2d(pi,pj), rankloc, simunits, patch_bound, tid, ierr)
                    
                END IF

             END IF
             IF (self%domain2d(pi,pj)%hasZones) THEN

                load(tid) = load(tid) + 1
             
             END IF
             
          END DO
          
       END DO
       !$OMP END DO
       
    ELSE IF (self%is3d) THEN

       !$OMP DO SCHEDULE(STATIC)
! removed collapse
       DO pk = 1, self%npz
          
          DO pj = 1, self%npy
             
             DO pi = 1, self%npx
                
                IF (mask(pi + (pj - 1)*self%npx + (pk - 1)*self%npx*self%npy)) THEN

                   CALL self%domain3d(pi,pj,pk)%build()

                   !### if we're were told to initialize the Patch with the user initilaization then do so ###
                   IF (withInit) THEN
                       
                       patch_bound = .FALSE.
                       IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
                       IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
                       IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
                       IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
                       IF (self%lz_bound .AND. pk .EQ. 1       ) patch_bound(1,3) = .TRUE.
                       IF (self%hz_bound .AND. pk .EQ. self%npz) patch_bound(2,3) = .TRUE.
                       CALL prob%patch_init(self%domain3d(pi,pj,pk), rankloc, simunits, patch_bound, tid, ierr)
                       
                   END IF

                END IF
                IF (self%domain3d(pi,pj,pk)%hasZones) THEN

                   load(tid) = load(tid) + 1
             
                END IF
                
             END DO
             
          END DO
          
       END DO
       !$OMP END DO
       
    END IF
    !$OMP BARRIER

    !### measure how uniform the load is on each thread.  if they are not very well balanced ###
    !### we'll want to set a flag that may be used to change the default thread scheduling ###
    !$OMP SINGLE
    
    load_var = 0.0_WRD
    load_avg = REAL(SUM(load),WRD) / self%nthreads
    DO tid = 0, self%nthreads - 1

       load_var = load_var + (REAL(load(tid),WRD) - load_avg)**2

    END DO

    !### make make a ratio between the sample variance and the uniform average number of patches a thread ###
    !### would handle.  if that number is >= 10% we'll call it unbalanced (NOTE: MAKE THIS A PARAMETER SINCE IT'S A BALANCE OF BANDWIDTH AND COMPUTATIONAL INTENSITY) ###
    load_var = (self%nthreads * load_var) / (self%npx * self%npy * self%npz * (self%nthreads - 1))
    IF (load_var .GE. 0.1_WRD) THEN
       self%load_balanced = .FALSE.
    ELSE
       self%load_balanced = .TRUE.
    END IF
	
	!### ADD SOME LOGIC REGARDING PATCH POPULATION AND SETTING HASPATCHES

    !$OMP END SINGLE

END SUBROUTINE build_some_patches

!### mark all patch boundaries (both send and recv) as unresolved, which is needed at the begininng of a time step ###
SUBROUTINE mark_all_patch_bounds_unresolved(self, flavor, reset_pass_count)

    CLASS(Domain) :: self
    INTEGER(WIS), INTENT(IN) :: flavor
    LOGICAL(WIS), INTENT(IN) :: reset_pass_count
    INTEGER(WIS) :: pi, pj, pk, n_total, p

    !$OMP SINGLE
    self%nLivePatches = 0
    !$OMP END SINGLE
    n_total = 0

    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO pi = 1, self%npx

            IF (self%domain1d(pi)%hasZones) THEN

                CALL self%domain1d(pi)%mark_unresolved(flavor, 0, 1, reset_pass_count)
                CALL self%domain1d(pi)%mark_unresolved(flavor, 0, 2, reset_pass_count)
                n_total = n_total + 1

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is2d) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)

            IF (self%domain2d(pi,pj)%hasZones) THEN

                CALL self%domain2d(pi,pj)%mark_unresolved(flavor, 0, 1, reset_pass_count)
                CALL self%domain2d(pi,pj)%mark_unresolved(flavor, 0, 2, reset_pass_count)
                n_total = n_total + 1

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)

            IF (self%domain3d(pi,pj,pk)%hasZones) THEN
                
                CALL self%domain3d(pi,pj,pk)%mark_unresolved(flavor, 0, 1, reset_pass_count)
                CALL self%domain3d(pi,pj,pk)%mark_unresolved(flavor, 0, 2, reset_pass_count)
                n_total = n_total + 1

            END IF

        END DO
        !$OMP END DO NOWAIT

    END IF

    !$OMP CRITICAL
    self%nLivePatches = self%nLivePatches + n_total
    !$OMP END CRITICAL
    !$OMP BARRIER

END SUBROUTINE mark_all_patch_bounds_unresolved

!### pack up as many Patch boundaries as we can.  this will pack all local boundaries and as many non-local as there are RMA resources for.  ###
!### inform caller if we have completed all needed packing ###
SUBROUTINE pack_some_patch_bounds(self, decomp, flavor, all_packed, count_exchanges)

    CLASS(Domain) :: self
    TYPE(Decomposition) :: decomp
    LOGICAL(WIS), INTENT(OUT) :: all_packed
    LOGICAL(WIS), INTENT(IN) :: count_exchanges
    INTEGER(WIS), INTENT(IN) :: flavor
    INTEGER(WIS) :: pi, pj, pk, p
    LOGICAL(WIS) :: myall_packed, p_packed

    !### assume we have completed all packing ###
    all_packed = .TRUE.

    !### loop through all Patches ###
    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO pi = 1, self%npx

            IF (self%domain1d(pi)%hasZones) THEN

                CALL self%pack_patch_bounds(decomp, flavor, pi, 1, 1, p_packed, count_exchanges)
                IF (.NOT. p_packed) all_packed = .FALSE.

            END IF

        END DO
        !$OMP END DO

    ELSE IF (self%is2d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)

            IF (self%domain2d(pi,pj)%hasZones) THEN

                CALL self%pack_patch_bounds(decomp, flavor, pi, pj, 1, p_packed, count_exchanges)
                IF (.NOT. p_packed) all_packed = .FALSE.

            END IF

        END DO
        !$OMP END DO

    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)

            IF (self%domain3d(pi,pj,pk)%hasZones) THEN

                CALL self%pack_patch_bounds(decomp, flavor, pi, pj, pk, p_packed, count_exchanges)
                IF (.NOT. p_packed) all_packed = .FALSE.

            END IF

        END DO
        !$OMP END DO

    END IF
    
END SUBROUTINE pack_some_patch_bounds

!### pack boundaries for the requested variable from the given patch to neighboring patches (on and off rank) ###
SUBROUTINE pack_patch_bounds(self, decomp, flavor, pi, pj, pk, all_packed, count_exchanges)

    CLASS(Domain) :: self
    TYPE(Decomposition) :: decomp
    INTEGER(WIS), INTENT(IN) :: pi, pj, pk, flavor
    LOGICAL(WIS), INTENT(OUT) :: all_packed
    LOGICAL(WIS), INTENT(IN) :: count_exchanges
    INTEGER(WIS) :: n, m, d, i, dest_pi, dest_pj, dest_pk, offset
    LOGICAL(WIS) :: was_packed, packed

    !### we default to claiming that all boundaries were packed ###
    all_packed = .TRUE.

    IF (self%is1d) THEN

        !### no need to do anything if the flavor has already been packaged ###
        IF (self%domain1d(pi)%flavor_resolved(flavor,2)) RETURN

        !### left and right edge of the Patch ###
        DO n = 1, 2
            
            !### only proceed if this boundary has not already been sent ###
            IF (.NOT. self%domain1d(pi)%flavor_bound_resolved(n,flavor,2)) THEN

                !### set the receiver's edge ###
                m = n + 1
                IF (m .GT. 2) m = 1
                
                !### set the Patch on the receiving end that gets this boundary data  ###
                IF (n .EQ. 1) THEN
                    dest_pi = pi - 1
                    IF (dest_pi .LT. 1) dest_pi = dest_pi + self%npx
                ELSE
                    dest_pi = pi + 1
                    IF (dest_pi .GT. self%npx) dest_pi = dest_pi - self%npx
                END IF
                
                !### if the neighbor is not local we go through the decomposition object ###
                IF (self%neighbors1d(pi,n) .NE. decomp%myrank) THEN
                    
                    CALL decomp%pack_patch_bound(self%domain1d(pi), n, m, self%neighbors1d(pi,n), flavor, dest_pi, 1, 1, count_exchanges, packed)
                    
                    !### if it wasn't packed we'll update all_packed (this just means we'll have to try again later ###
                    IF (.NOT. packed) all_packed = .FALSE.
                    
                !### otherwise it's a local Patch and we can directly pack into it ###
                ELSE
                    
                    CALL self%domain1d(pi)%bounds_direct_pup(self%domain1d(dest_pi)%gridbounds, n, m, flavor, .TRUE., was_packed)
                    IF (.NOT. was_packed) THEN
                        
                        ! ERROR
                        
                    END IF

                END IF

            END IF

        END DO

    ELSE IF (self%is2d) THEN

        !### no need to do anything if the flavor has already been packaged ###
        IF (self%domain2d(pi,pj)%flavor_resolved(flavor,2)) RETURN

        !### direction (X, Y, -90, +90) ###
        DO d = 1, 4

            !### L and R in that direction ###
            DO i = 1, 2

                !### set the local boundary ID ###
                n = (d-1)*2 + i
            
                !### only proceed if this boundary has not already been sent ###
                IF (.NOT. self%domain2d(pi,pj)%flavor_bound_resolved(n,flavor,2)) THEN

                    !### set the receiver's boundary ID ###
                    m = n + 1
                    IF (i .EQ. 2) m = n - 1
                    
                    !### set the Patch on the receiving end that gets this boundary data ###
                    dest_pi = pi
                    dest_pj = pj
                    offset  = 2*i - 3
                    SELECT CASE (d)
                        CASE (1)
                            dest_pi = dest_pi + offset
                        CASE(2)
                            dest_pj = dest_pj + offset
                        CASE(3)
                            dest_pi = dest_pi + offset
                            dest_pj = dest_pj + offset
                        CASE(4)
                            dest_pi = dest_pi + offset
                            dest_pj = dest_pj - offset
                    END SELECT
                    IF (dest_pi .LT. 1       ) dest_pi = dest_pi + self%npx
                    IF (dest_pi .GT. self%npx) dest_pi = dest_pi - self%npx
                    IF (dest_pj .LT. 1       ) dest_pj = dest_pj + self%npy
                    IF (dest_pj .GT. self%npy) dest_pj = dest_pj - self%npy
                    
                    !### if the neighbor is not local we go through the decomposition object ###
                    IF (self%neighbors2d(pi,pj,n) .NE. decomp%myrank) THEN
                        
                        CALL decomp%pack_patch_bound(self%domain2d(pi,pj), n, m, self%neighbors2d(pi,pj,n), flavor, dest_pi, dest_pj, 1, count_exchanges, packed)
                        
                        !### if it wasn't packed we'll update all_packed (this just means we'll have to try again later ###
                        IF (.NOT. packed) all_packed = .FALSE.
                        
                    !### otherwise it's a local Patch and we can directly pack into it ###
                    ELSE
                        
                        CALL self%domain2d(pi,pj)%bounds_direct_pup(self%domain2d(dest_pi,dest_pj)%gridbounds, n, m, flavor, .TRUE., was_packed)
                        IF (.NOT. was_packed) THEN
                            
                            ! ERROR
                            
                        END IF
                        
                    END IF

                END IF

            END DO

        END DO

    ELSE IF (self%is3d) THEN

        !### no need to do anything if the flavor has already been packaged ###
        IF (self%domain3d(pi,pj,pk)%flavor_resolved(flavor,2)) RETURN

        !### all 13 directions ###
        DO d = 1, 13

            !### L and R in that direction ###
            DO i = 1, 2

                !### set the local boundary ID ###
                n = (d-1)*2 + i
            
                !### only proceed if this boundary has not already been sent ###
                IF (.NOT. self%domain3d(pi,pj,pk)%flavor_bound_resolved(n,flavor,2)) THEN

                    !### set the receiver's boundary ID ###
                    m = n + 1
                    IF (i .EQ. 2) m = n - 1
                    
                    !### set the Patch on the receiving end that gets this boundary data ###
                    dest_pi = pi
                    dest_pj = pj
                    dest_pk = pk
                    offset  = 2*i - 3
                    SELECT CASE(d)

                        !### along X ###
                        CASE(1)
                            dest_pi = dest_pi + offset
                            
                        !### along Y ###
                        CASE(2)
                            dest_pj = dest_pj + offset
                            
                        !### along Z ###
                        CASE(3)
                            dest_pk = dest_pk + offset
                            
                        !### back bottom left, front top right ###
                        CASE(4)
                            dest_pi = dest_pi + offset
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk + offset
                            
                        !### back bottom right, front top left ###
                        CASE(5)
                            dest_pi = dest_pi - offset
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk + offset
                            
                        !### back top left, front bottom right ###
                        CASE(6)
                            dest_pi = dest_pi + offset
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk - offset
                            
                        !### back top right, front bottom left ###
                        CASE(7)
                            dest_pi = dest_pi - offset
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk - offset

                        !### back bottom, front top ###
                        CASE(8)
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk + offset
                            
                        !### back top, front bottom ###
                        CASE(9)
                            dest_pj = dest_pj - offset
                            dest_pk = dest_pk - offset

                        !### left bottom, right top ###
                        CASE(10)
                            dest_pi = dest_pi + offset
                            dest_pk = dest_pk + offset

                        !### left top, right bottom ###
                        CASE(11)
                            dest_pi = dest_pi + offset
                            dest_pk = dest_pk - offset
                                
                        !### back left, front right ###
                        CASE(12)
                            dest_pi = dest_pi + offset
                            dest_pj = dest_pj - offset

                        !### back right, front left ###
                        CASE(13)
                            dest_pi = dest_pi - offset
                            dest_pj = dest_pj - offset
                    END SELECT
                    IF (dest_pi .LT. 1       ) dest_pi = dest_pi + self%npx
                    IF (dest_pi .GT. self%npx) dest_pi = dest_pi - self%npx
                    IF (dest_pj .LT. 1       ) dest_pj = dest_pj + self%npy
                    IF (dest_pj .GT. self%npy) dest_pj = dest_pj - self%npy
                    IF (dest_pk .LT. 1       ) dest_pk = dest_pk + self%npz
                    IF (dest_pk .GT. self%npz) dest_pk = dest_pk - self%npz
                    
                    !### if the neighbor is not local we go through the decomposition object ###
                    IF (self%neighbors3d(pi,pj,pk,n) .NE. decomp%myrank) THEN
                        
                        CALL decomp%pack_patch_bound(self%domain3d(pi,pj,pk), n, m, self%neighbors3d(pi,pj,pk,n), flavor, dest_pi, dest_pj, dest_pk, count_exchanges, packed)
                        
                        !### if it wasn't packed we'll update all_packed (this just means we'll have to try again later ###
                        IF (.NOT. packed) all_packed = .FALSE.
                        
                    !### otherwise it's a local Patch and we can directly pack into it ###
                    ELSE
                        
                        CALL self%domain3d(pi,pj,pk)%bounds_direct_pup(self%domain3d(dest_pi,dest_pj,dest_pk)%gridbounds, n, m, flavor, .TRUE., was_packed)
                        IF (.NOT. was_packed) THEN
                            
                            ! ERROR
                            
                        END IF

                    END IF
                
                END IF

            END DO

        END DO

    END IF

END SUBROUTINE pack_patch_bounds

!### unpack all local Patch boundaries ###
SUBROUTINE unpack_all_patch_bounds_local(self, myrank, flavor)

    CLASS(Domain) :: self
    INTEGER(WIS), INTENT(IN) :: myrank, flavor
    INTEGER(WIS) :: pi, pj, pk, d, i, n, p
    LOGICAL(WIS) :: was_packed

    IF (self%is1d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO pi = 1, self%npx

            !### no need to do anything if the flavor has already been unpacked ###
            IF (.NOT. self%domain1d(pi)%flavor_resolved(flavor,1)) THEN

                !### left and right edge of the Patch ###
                DO n = 1, 2
                    
                    !### only proceed if this boundary has not already been recv ###
                    IF (.NOT. self%domain1d(pi)%flavor_bound_resolved(n,flavor,1)) THEN
                    
                        !### we only unpack here if the neighbor is local ###
                        IF (self%neighbors1d(pi,n) .EQ. myrank) THEN
                        
                            CALL self%domain1d(pi)%bounds_direct_pup(self%domain1d(pi)%gridbounds, 0, n, flavor, .FALSE., was_packed)
                        
                        END IF

                    END IF

                END DO

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is2d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1,self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)

            !### no need to do anything if the flavor has already been unpacked ###
            IF (.NOT. self%domain2d(pi,pj)%flavor_resolved(flavor,1)) THEN

                !### direction (X, Y, -90, +90) ###
                DO d = 1, 4
                        
                    !### L and R in that direction ###
                    DO i = 1, 2
                            
                        !### set the local boundary ID ###
                        n = (d-1)*2 + i
                            
                        !### only proceed if this boundary has not already been recv ###
                        IF (.NOT. self%domain2d(pi,pj)%flavor_bound_resolved(n,flavor,1)) THEN
                            
                            !### we only unpack here if the neighbor is local ###
                            IF (self%neighbors2d(pi,pj,n) .EQ. myrank) THEN
                                
                                CALL self%domain2d(pi,pj)%bounds_direct_pup(self%domain2d(pi,pj)%gridbounds, 0, n, flavor, .FALSE., was_packed)
                                
                            END IF

                        END IF
                            
                    END DO

                END DO

            END IF

        END DO
        !$OMP END DO NOWAIT

    ELSE IF (self%is3d) THEN

        !$OMP DO SCHEDULE(GUIDED)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)

            !### no need to do anything if the flavor has already been unpacked ###
            IF (.NOT. self%domain3d(pi,pj,pk)%flavor_resolved(flavor,1)) THEN

                !### all 13 directions ###
                DO d = 1, 13
                            
                    !### L and R in that direction ###
                    DO i = 1, 2
                                
                        !### set the local boundary ID ###
                        n = (d-1)*2 + i
                                
                        !### only proceed if this boundary has not already been recv ###
                        IF (.NOT. self%domain3d(pi,pj,pk)%flavor_bound_resolved(n,flavor,1)) THEN
                                
                            !### we only unpack here if the neighbor is local ###
                            IF (self%neighbors3d(pi,pj,pk,n) .EQ. myrank) THEN
                                    
                                CALL self%domain3d(pi,pj,pk)%bounds_direct_pup(self%domain3d(pi,pj,pk)%gridbounds, 0, n, flavor, .FALSE., was_packed)
                                    
                            END IF

                        END IF

                    END DO

                END DO

            END IF

        END DO
        !$OMP END DO NOWAIT

    END IF

END SUBROUTINE unpack_all_patch_bounds_local

!### apply boundary conditions from Problem for the identified Patch only ###
SUBROUTINE applyproblem_patch_bounds(self, prob, rankloc, simunits, pi, pj, pk, maxsignal, ierr)

    CLASS(Domain) :: self
    TYPE(Problem) :: prob
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: pi, pj, pk
    INTEGER(WIS), INTENT(OUT) :: ierr
    REAL(WRD), INTENT(INOUT) :: maxsignal
    LOGICAL(WIS) :: patch_bound(2,3)
    INTEGER(WIS) :: tid

    tid = OMP_GET_THREAD_NUM() + 1

    IF (self%is1d) THEN

        patch_bound = .FALSE.
        IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
        IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
        CALL prob%patch_bounds(self%domain1d(pi), rankloc, simunits, patch_bound, maxsignal, tid, ierr)
       
    ELSE IF (self%is2d) THEN
       
       patch_bound = .FALSE.
       IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
       IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
       IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
       IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
       CALL prob%patch_bounds(self%domain2d(pi,pj), rankloc, simunits, patch_bound, maxsignal, tid, ierr)
       
    ELSE IF (self%is3d) THEN

       patch_bound = .FALSE.
       IF (self%lx_bound .AND. pi .EQ. 1       ) patch_bound(1,1) = .TRUE.
       IF (self%hx_bound .AND. pi .EQ. self%npx) patch_bound(2,1) = .TRUE.
       IF (self%ly_bound .AND. pj .EQ. 1       ) patch_bound(1,2) = .TRUE.
       IF (self%hy_bound .AND. pj .EQ. self%npy) patch_bound(2,2) = .TRUE.
       IF (self%lz_bound .AND. pk .EQ. 1       ) patch_bound(1,3) = .TRUE.
       IF (self%hz_bound .AND. pk .EQ. self%npz) patch_bound(2,3) = .TRUE.
       CALL prob%patch_bounds(self%domain3d(pi,pj,pk), rankloc, simunits, patch_bound, maxsignal, tid, ierr)
       
    END IF

END SUBROUTINE applyproblem_patch_bounds

!### broadcast a value of t and dt to all of our patches ###
SUBROUTINE broadcast_t_and_dt(self, t, dt)
    
    CLASS(Domain) :: self
    REAL(WRD), INTENT(IN) :: t, dt
    INTEGER(WIS) :: pi, pj, pk, p
    
    self%t  = t
    self%dt = dt
    IF (self%is1d) THEN
        
        !$OMP DO SCHEDULE(DYNAMIC)
        DO pi = 1, self%npx
            
            self%domain1d(pi)%t  = t
            self%domain1d(pi)%dt = dt
            
        END DO
        !$OMP END DO NOWAIT
        
    ELSE IF (self%is2d) THEN
        
        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1,self%npx*self%npy

            !### obtain the x/y location of the Patch ###
            CALL collapse2d(p, self%npx, self%npy, pi, pj)
                
            self%domain2d(pi,pj)%t  = t
            self%domain2d(pi,pj)%dt = dt
            
        END DO
        !$OMP END DO NOWAIT
        
    ELSE IF (self%is3d) THEN
        
        !$OMP DO SCHEDULE(DYNAMIC)
        DO p = 1, self%npx*self%npy*self%npz

            !### obtain the x/y/z location of the Patch ###
            CALL collapse3d(p, self%npx, self%npy, self%npz, pi, pj, pk)
                    
            self%domain3d(pi,pj,pk)%t  = t
            self%domain3d(pi,pj,pk)%dt = dt
            
        END DO
        !$OMP END DO NOWAIT
        
    END IF
    
END SUBROUTINE broadcast_t_and_dt

END MODULE Mod_Domain
