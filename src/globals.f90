#include "config.h" 

MODULE Mod_Globals

!######################################################################
!#
!# FILENAME: mod_globals.f90
!#
!# DESCRIPTION: This module defines global values for use throughout
!#  Wombat.
!#
!# DEPENDENCIES: The rest of Wombat
!#
!# HISTORY:
!#    7/29/2015  - Peter Mendygral - Adding this header
!#
!######################################################################

IMPLICIT NONE

REAL, PARAMETER :: WVERSION = 1.0

!### define the precision for data ###
!### these may need to be augmented for compilers and/or systems that do not support up to quad precision ###
!### in that case, you CAN change WRQ and WIQ (real and integer) types to be whatever at the cost of who knows? ###
INTEGER, PARAMETER :: &

     WRS = KIND(1.0),                                   &
     WRD = SELECTED_REAL_KIND(2*PRECISION(1.0_WRS)),    &
     WRQ = SELECTED_REAL_KIND(2*PRECISION(1.0_WRD)),    &
     WIS = WRS,                                         &
     WID = WRD,                                         &
     WIQ = WRQ,                                         &
#ifdef __GFORTRAN__
     WOMP_LOCK_SIZE = 4,                                &    !  GCC strangely use INT(4) for an OMP lock, but most use 8 as you'd expect
#else
     WOMP_LOCK_SIZE = 8,                                &    
#endif
     WADDRESS_KIND  = 8,                                &    ! we need a type that can be used for long indexing independent of precision elsewhere

     WMAX_ERR_LEN                 = 200,                &    ! maximum length of an error string
     WMAX_ERR_SRC_LEN             = 24,                 &    ! maximum length of a module name to be printed
     WMAX_NAMELIST_LEN            = 20480,              &    ! maximum size of the namelist file in bytes
     WNAMELIST_PATHLEN            = 100,                &    ! maximum length of the path to the namelist
     WLB_HEADER_SIZE              = 4,                  &    ! size of WOMBAT's internal MPI header (PatchID(3), flavor, more to come)
     WMPI_MAX_MESSAGE             = 1024,               &    ! the maximum MPI message size in MBytes (used only in Mod_IO for now)
     WRESTART_IOBUFFER_SIZE       = 512,                &    ! the default IO buffer size for restarts in MBytes
     WCOMPRESSED_IOBUFFER_SIZE    = 64,                 &    ! the default IO buffer size for compressed files in MBytes
     WMOVIE_IOBUFFER_SIZE         = 16,                 &    ! the default IO buffer size for movie files in MBytes
     WNUM_AIO_OPS_INFLIGHT        = 1,                  &    ! the maximum number of AIO operations that can be flight in any moment of any one flavor
     WMAX_IO_FIELDS               = 104,                &    ! the maximum number of output fields allowed in any of the file formats
     WMAX_IO_FORM_LEN             = 128,                &    ! the maximum length an IO field format can be

     WMAX_MG_LEVELS               = 32                       ! the maximum number of levels that can be used in the multigrid solver

!### store a string that contains the MPI rank of the process.  This is very useful for debug mode message and error printing ###
CHARACTER(10) :: WRANKSTR, WTHREADSTR = '0', WNTHREADSTR = '1'

!### many modules need the rank for a variety of reasons.  it is helpful to just have a global that stores it. ###
INTEGER(WIS) :: WRANK, WTHREAD = 0, WNTHREADS = 1

!### store the beta value for the Sweby limiter used in several places ###
REAL(WRD), PARAMETER :: WSWEBY_BETA       = 1.08_WRD,   &    ! the beta parameter to be used in Sweby slope limiters
                        WSIGNAL_TOLERANCE = 0.5_WRD          ! since we pass REALs for signalling, we need some tolerance around them (less than 1) to be sure we don't falsely trigger on
                                                             ! on tiny FP differences

!### at compile time we can set some debug mode flags ###
LOGICAL, PARAMETER :: &

#ifdef DEBUG
     WDEBUG = .TRUE.
#else
     WDEBUG = .FALSE.
#endif

!### we must declare thread private globals as such in the source module ###
!$OMP THREADPRIVATE(WTHREADSTR, WTHREAD, WNTHREADS, WNTHREADSTR)

CONTAINS

! NOTE, throughout the code I have used the !$OMP ... COLLAPSE clause.  This is relatively
! new, and it seems both Intel and CCE have real issues with it.  Intel actually produced
! garbarge indicies causing bus errors.  CCE just got wrong answers when using collapses even
! single threaded.  Thus, I am implementing my own collapse features here.

!### given a 2d loop determine the i,j values given a single index and ranges ###
SUBROUTINE collapse2d(n, nx, ny, i, j)

    INTEGER(WIS), INTENT(IN) :: n, nx, ny
    INTEGER(WIS), INTENT(OUT) :: i, j

    i = MOD(n, nx)
    IF (i .EQ. 0) i = nx
    j = CEILING(REAL(n, WRD) / REAL(nx, WRD))

END SUBROUTINE collapse2d

!### given a 3d loop determine the i,j,k values given a single index and ranges ###
SUBROUTINE collapse3d(n, nx, ny, nz, i, j, k)

    INTEGER(WIS), INTENT(IN) :: n, nx, ny, nz
    INTEGER(WIS), INTENT(OUT) :: i, j, k
    INTEGER(WIS) :: myn

    i = MOD(n, nx)
    IF (i .EQ. 0) i = nx
    k = CEILING(REAL(n, WRD) /  REAL(nx*ny, WRD))
    myn = n - (k-1)*nx*ny
    j = CEILING(REAL(myn, WRD) / REAL(nx, WRD))

END SUBROUTINE collapse3d

END MODULE Mod_Globals
