#include "config.h"

MODULE Mod_ProblemBase

!######################################################################
!#
!# FILENAME: problem_base.f90
!#
!# DESCRIPTION: This module provides a parent class for specific problems to
!#  inherit from.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    5/25/2016 - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: ProblemBase

!### define a data type for this module ###
TYPE, ABSTRACT :: ProblemBase

    !### default everything as private ###
    PRIVATE

    CONTAINS

    PROCEDURE, PUBLIC :: open_lx_bounds
    PROCEDURE, PUBLIC :: open_hx_bounds
    PROCEDURE, PUBLIC :: open_ly_bounds
    PROCEDURE, PUBLIC :: open_hy_bounds
    PROCEDURE, PUBLIC :: open_lz_bounds
    PROCEDURE, PUBLIC :: open_hz_bounds
    PROCEDURE, PUBLIC :: open_bounds
    PROCEDURE, PUBLIC :: rotate2d
    PROCEDURE, PUBLIC :: rotate3d

END TYPE ProblemBase

!### object methods ###
CONTAINS

!### open boundaries on all sides ###
SUBROUTINE open_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)

    CALL self%open_lx_bounds(workp, is_boundary)
    CALL self%open_hx_bounds(workp, is_boundary)
    CALL self%open_ly_bounds(workp, is_boundary)
    CALL self%open_hy_bounds(workp, is_boundary)
    CALL self%open_lz_bounds(workp, is_boundary)
    CALL self%open_hz_bounds(workp, is_boundary)

END SUBROUTINE open_bounds

!### open boundaries for the lower X edge ###
SUBROUTINE open_lx_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(1,1)) RETURN

    IF (workp%is1d) THEN

        DO i = 1-workp%nb, 0
            
            workp%grid1d(i,1) = workp%grid1d(1,1)
            workp%grid1d(i,2) = workp%grid1d(1,2)
            workp%grid1d(i,3) = workp%grid1d(1,3)
            workp%grid1d(i,4) = workp%grid1d(1,4)
            workp%grid1d(i,5) = workp%grid1d(1,5)
            workp%grid1d(i,6) = workp%grid1d(1,6)
            workp%grid1d(i,7) = workp%grid1d(1,7)
            workp%grid1d(i,8) = workp%grid1d(1,8)
            
        END DO
        
        IF (workp%passOn .AND. workp%npass .GT. 0) THEN
            
            DO i = 1-workp%nb, 0
                
                workp%pass1d(i,:) = workp%pass1d(1,:)
                
            END DO
            
        END IF
        
    ELSE IF (workp%is2d) THEN
        
        DO j = 1-workp%nb, workp%ny+workp%nb
            
            DO i = 1-workp%nb, 0
                
                workp%grid2d(i,j,1) = workp%grid2d(1,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(1,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(1,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(1,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(1,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(1,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(1,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(1,j,8)
                
            END DO
            
        END DO
        
        DO j = 1-workp%nb, workp%ny+workp%nb
            
            DO i = 1-workp%nb, 0
                
                workp%emf2d(i,j,:) = workp%emf2d(1,j,:)
                
            END DO
            
        END DO
        
        IF (workp%passOn .AND. workp%npass .GT. 0) THEN
            
            DO j = 1-workp%nb, workp%ny+workp%nb
                
                DO i = 1-workp%nb, 0
                    
                    workp%pass2d(i,j,:) = workp%pass2d(1,j,:)
                    
                END DO
                
            END DO
            
        END IF
        
    ELSE IF (workp%is3d) THEN
        
        DO k = 1-workp%nb, workp%nz+workp%nb
            
            DO j = 1-workp%nb, workp%ny+workp%nb
                
                DO i = 1-workp%nb, 0
                    
                    workp%grid3d(i,j,k,1) = workp%grid3d(1,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(1,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(1,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(1,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(1,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(1,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(1,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(1,j,k,8)
                    
                END DO
                
            END DO
            
        END DO
        
        DO k = 1-workp%nb, workp%nz+workp%nb
            
            DO j = 1-workp%nb, workp%ny+workp%nb
                
                DO i = 1-workp%nb, 0
                    
                    workp%emf3d(i,j,k,:) = workp%emf3d(1,j,k,:)
                    
                END DO
                
            END DO
            
        END DO
        
        IF (workp%passOn .AND. workp%npass .GT. 0) THEN
            
            DO k = 1-workp%nb, workp%nz+workp%nb
                
                DO j = 1-workp%nb, workp%ny+workp%nb
                    
                    DO i = 1-workp%nb, 0
                        
                        workp%pass3d(i,j,k,:) = workp%pass3d(1,j,k,:)
                        
                    END DO
                    
                END DO
                
            END DO
            
        END IF
        
    END IF

END SUBROUTINE open_lx_bounds

!### open boundaries for the upper X edge ###
SUBROUTINE open_hx_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(2,1)) RETURN

    IF (workp%is1d) THEN

        DO i = workp%nx+1, workp%nx+workp%nb

            workp%grid1d(i,1) = workp%grid1d(workp%nx,1)
            workp%grid1d(i,2) = workp%grid1d(workp%nx,2)
            workp%grid1d(i,3) = workp%grid1d(workp%nx,3)
            workp%grid1d(i,4) = workp%grid1d(workp%nx,4)
            workp%grid1d(i,5) = workp%grid1d(workp%nx,5)
            workp%grid1d(i,6) = workp%grid1d(workp%nx,6)
            workp%grid1d(i,7) = workp%grid1d(workp%nx,7)
            workp%grid1d(i,8) = workp%grid1d(workp%nx,8)

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%pass1d(i,:) = workp%pass1d(workp%nx,:)

            END DO

        END IF

    ELSE IF (workp%is2d) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(workp%nx,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(workp%nx,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(workp%nx,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(workp%nx,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(workp%nx,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(workp%nx,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(workp%nx,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(workp%nx,j,8)
                
            END DO

        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(workp%nx,j,:)
                
            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(workp%nx,j,:)

                END DO

            END DO

        END IF

    ELSE IF (workp%is3d) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(workp%nx,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(workp%nx,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(workp%nx,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(workp%nx,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(workp%nx,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(workp%nx,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(workp%nx,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(workp%nx,j,k,8)
                    
                END DO

            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(workp%nx,j,k,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = workp%nx+1, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(workp%nx,j,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE open_hx_bounds

!### open boundaries for the lower Y edge ###
SUBROUTINE open_ly_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(1,2)) RETURN

    IF (workp%is2d) THEN

        DO j = 1-workp%nb, 0

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,1,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,1,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,1,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,1,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,1,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,1,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,1,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,1,8)

            END DO

        END DO

        DO j = 1-workp%nb, 0

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,1,:)

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(i,1,:)

                END DO

            END DO

        END IF

    ELSE IF (workp%is3d) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,1,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,1,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,1,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,1,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,1,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,1,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,1,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,1,k,8)

                END DO

            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,1,k,:)

                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb
                
                DO j = 1-workp%nb, 0
                
                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,1,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE open_ly_bounds

!### open boundaries for the upper Y edge ###
SUBROUTINE open_hy_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(2,2)) RETURN

    IF (workp%is2d) THEN

        DO j = workp%ny+1, workp%ny+workp%nb

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,workp%ny,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,workp%ny,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,workp%ny,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,workp%ny,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,workp%ny,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,workp%ny,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,workp%ny,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,workp%ny,8)
                
            END DO

        END DO

        DO j = workp%ny+1, workp%ny+workp%nb

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,workp%ny,:)
                
            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(i,workp%ny,:)

                END DO

            END DO

        END IF

    ELSE IF (workp%is3d) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,workp%ny,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,workp%ny,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,workp%ny,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,workp%ny,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,workp%ny,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,workp%ny,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,workp%ny,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,workp%ny,k,8)

                END DO
                
            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,workp%ny,k,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb

                DO j = workp%ny+1, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,workp%ny,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE open_hy_bounds

!### open boundaries for the lower Z edge ###
SUBROUTINE open_lz_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(1,3)) RETURN

    IF (workp%is3d) THEN

        DO k = 1-workp%nb, 0

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,1,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,1,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,1,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,1,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,1,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,1,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,1,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,1,8)

                END DO

            END DO

        END DO

        DO k = 1-workp%nb, 0

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,1,:)

                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, 0

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,j,1,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE open_lz_bounds

!### open boundaries for the upper Z edge ###
SUBROUTINE open_hz_bounds(self, workp, is_boundary)

    CLASS(ProblemBase) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS) :: i, j, k

    IF (.NOT. is_boundary(2,3)) RETURN

    IF (workp%is3d) THEN

        DO k = workp%nz+1, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,workp%nz,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,workp%nz,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,workp%nz,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,workp%nz,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,workp%nz,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,workp%nz,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,workp%nz,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,workp%nz,8)

                END DO
                
            END DO

        END DO

        DO k = workp%nz+1, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,workp%nz,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = workp%nz+1, workp%nz+workp%nb

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,j,workp%nz,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE open_hz_bounds

!### Rotate input vector by phi (or -phi) ###
SUBROUTINE rotate2d(self, mat, A, Ainv, inv)

    CLASS(ProblemBase) :: self
    REAL(WRD), INTENT(INOUT) :: mat(2)
    REAL(WRD), INTENT(IN) :: A(2,2), Ainv(2,2)
    LOGICAL(WIS), INTENT(IN) :: inv
    REAL(WRD) :: work(2)
    INTEGER(WIS) :: i, j

    !### init work as 0 ###
    work = 0.0_WRD

    !### do the matrix multiplication ###
    IF (.NOT. inv) THEN

        DO i = 1, 2
            
            DO j = 1, 2
                
                work(i) = work(i) + mat(j) * A(i,j)
                
            END DO
            
        END DO

    ELSE

        DO i = 1, 2
            
            DO j = 1, 2
                
                work(i) = work(i) + mat(j) * Ainv(i,j)
                
            END DO
            
        END DO

    END IF

    !### update the input array ###
    mat = work

END SUBROUTINE rotate2d

!### Rotation of input around z-axis by phi, then around x' by theta (or inverse operation) ###
SUBROUTINE rotate3d(self, mat, AA, AAinv, inv)

    CLASS(ProblemBase) :: self
    REAL(WRD), INTENT(INOUT) :: mat(3)
    REAL(WRD), INTENT(IN) :: AA(3,3), AAinv(3,3)
    LOGICAL(WIS), INTENT(IN) :: inv
    REAL(WRD) :: work(3)
    INTEGER(WIS) :: i, j

    !### init work as 0 ###
    work = 0.0_WRD

    !### do the matrix multiplication ###
    IF (.NOT. inv) THEN

       DO i = 1, 3

          DO j = 1, 3

             work(i) = work(i) + mat(j) * AA(i,j)

          END DO

       END DO

    ELSE

       DO i = 1, 3

          DO j = 1, 3

             work(i) = work(i) + mat(j) * AAinv(i,j)

          END DO

       END DO

    END IF

    mat = work

END SUBROUTINE rotate3d

END MODULE Mod_ProblemBase
