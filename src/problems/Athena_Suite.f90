#include "../config.h"

MODULE Mod_Problem

!######################################################################
!#
!# FILENAME: mod_AthenaSuite.f90
!#
!# DESCRIPTION: This module provides a class for initializing a domain
!#   and setting boundary values to run 1,2 or 3-dimensional MHD 
!#   tests from the test paper for ATHENA (Stone et al. 2008)
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    4/30/2015  - Brian O'Neill
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch
USE Mod_Decomposition
USE Mod_ProblemBase

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Problem

!### define a data type for this module ###
TYPE, EXTENDS(ProblemBase) :: Problem

    !### default everything as private ###
    PRIVATE

    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD), PUBLIC :: gamma

    !### q0 -- initial state at t=0 for calculating linear wave convergence
    !### gardiner & stone 2005 eqn. 79:
    !###   L1 error vector: delq_k = 1/Nzones * Sum [ abs( q_k - q0_k ), domain ] 
    !### delq(:) -- Sum [ abs( q_k - q0_k )] for the rank, these must then be summed over the domain
    !###   L1 error norm: sqrt(sum( delq_k(:)**2 ))
!    REAL(WRD), ALLOCATABLE :: q0_1d(:,:,:), q0_2d(:,:,:,:), q0_3d(:,:,:,:,:), delq(:)

    !### user-added problem data structres ###
    !### left/right state variables: 1-density ; 2,3,4-velocity ; 5,6,7-Bfield ; 8-pressure
    REAL(WRD), DIMENSION(:,:), ALLOCATABLE :: left, right
    REAL(WRD) :: phi, theta, psi, A(2,2), Ainv(2,2), AA(3,3), AAinv(3,3)
    INTEGER(WIS) :: nstate = 8

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: patch_init
    PROCEDURE, PUBLIC :: patch_bounds
    PROCEDURE :: initproblem
    PROCEDURE :: test_Hydro_sound
    PROCEDURE :: test_Hydro_vy
    PROCEDURE :: test_Hydro_vz
    PROCEDURE :: test_Hydro_entropy
    PROCEDURE :: test_Sod
    PROCEDURE :: test_TwoBlastWaves
    PROCEDURE :: test_ShuOsher
    PROCEDURE :: test_Einfeldt
    PROCEDURE :: test_MHD_alfven
    PROCEDURE :: test_MHD_fast
    PROCEDURE :: test_MHD_slow
    PROCEDURE :: test_MHD_entropy
    PROCEDURE :: test_BrioWu
    PROCEDURE :: test_Torrilhon
    PROCEDURE :: test_RJ95_2a
    PROCEDURE :: test_RJ95_4d
    PROCEDURE :: test_dblmachrefl
    PROCEDURE :: test_LW_implosion
    PROCEDURE :: test_RayleighTaylor
    PROCEDURE :: test_CPAlfvenWave2D
    PROCEDURE :: test_CPAlfvenWave3D
    PROCEDURE :: test_BLoop_adv2D
    PROCEDURE :: test_BLoop_adv3D
    PROCEDURE :: test_OrszagTangVortex
    PROCEDURE :: test_MHD_Rotor
    PROCEDURE :: test_MagneticRT
    PROCEDURE :: test_MagBlastWave2D
    PROCEDURE :: test_MagBlastWave3D
    PROCEDURE :: test_Noh3d
    PROCEDURE :: test_current_sheet
    PROCEDURE :: init_linear_wave
    PROCEDURE :: init_tube_states
    PROCEDURE :: shock_tube_bounds1d
    PROCEDURE :: shock_tube_bounds2d
    PROCEDURE :: shock_tube_bounds3d
    PROCEDURE :: reflecting_bounds1d
    PROCEDURE :: reflecting_bounds2d
    PROCEDURE :: reflecting_bounds3d
    PROCEDURE :: dblmachrefl_bounds
    PROCEDURE :: upperNoh_bounds
    PROCEDURE :: rotate_2d
    PROCEDURE :: rotate_3d

END TYPE Problem

!### create an interface to the constructor ###
INTERFACE Problem

    MODULE PROCEDURE constructor

END INTERFACE Problem

!### object methods ###
CONTAINS

!### constructor for the class ###
!###   arguments are: the namelist file contents for grabbing parameters out of ###
FUNCTION constructor(namelist,nthreads, rankloc, simunits)

    TYPE(Problem) :: constructor
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initproblem(namelist, nthreads, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### this function will init the object and configure init and bound routine parameters ###
SUBROUTINE initproblem(self, namelist, nthreads, rankloc, simunits)

    CLASS(Problem) :: self
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD) :: gamma, phi, theta, psi

    NAMELIST /ProblemSetup/ init_routine, bounds_routine, gamma, phi, theta, psi

    !### set the routines we'll call ###
    READ(namelist, NML=ProblemSetup, END=9)
9   CONTINUE
    self%init_routine   = init_routine
    self%bounds_routine = bounds_routine

    !### we handle setting the adiabatic index ###
    self%gamma = gamma

    ALLOCATE(self%left(self%nstate, nthreads),self%right(self%nstate, nthreads))


    !### user-added values ###
    self%phi   = phi   * simunits%pi / 180.0_WRD
    self%theta = theta * simunits%pi / 180.0_WRD
    self%psi   = psi   * simunits%pi / 180.0_WRD

    self%A(1,1) = COS(self%theta)
    self%A(1,2) = SIN(self%theta)
    self%A(2,1) = -SIN(self%theta)
    self%A(2,2) = COS(self%theta)

    self%Ainv(1,1) = COS(self%theta)
    self%Ainv(1,2) = -SIN(self%theta)
    self%Ainv(2,1) = SIN(self%theta)
    self%Ainv(2,2) = COS(self%theta)

    !### 3d rotation: Euler angle rotation matrix (e.g. Goldstein p.153)

    self%AA(1,1) = DCOS(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DSIN(self%psi)
    self%AA(1,2) = DCOS(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DSIN(self%psi)
    self%AA(1,3) = DSIN(self%psi)*DSIN(self%theta)
    self%AA(2,1) =-DSIN(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DCOS(self%psi)
    self%AA(2,2) =-DSIN(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DCOS(self%psi)
    self%AA(2,3) = DCOS(self%psi)*DSIN(self%theta)
    self%AA(3,1) = DSIN(self%theta)*DSIN(self%phi)
    self%AA(3,2) =-DSIN(self%theta)*DCOS(self%phi)
    self%AA(3,3) = DCOS(self%theta)

    !### inverse 3d rotation: transpose of Euler angle rotation matrix (e.g. Goldstein p.153)

    self%AAinv(1,1) = DCOS(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DSIN(self%psi)
    self%AAinv(1,2) =-DSIN(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DCOS(self%psi)
    self%AAinv(1,3) = DSIN(self%theta)*DSIN(self%phi)
    self%AAinv(2,1) = DCOS(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DSIN(self%psi)
    self%AAinv(2,2) =-DSIN(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DCOS(self%psi)
    self%AAinv(2,3) =-DSIN(self%theta)*DCOS(self%phi)
    self%AAinv(3,1) = DSIN(self%theta)*DSIN(self%psi)
    self%AAinv(3,2) = DSIN(self%theta)*DCOS(self%psi)
    self%AAinv(3,3) = DCOS(self%theta)


END SUBROUTINE initproblem

SUBROUTINE destroy(self)

    CLASS(Problem) :: self
    
    IF (ALLOCATED(self%left)) THEN
        DEALLOCATE(self%left,self%right)
    END IF
    
    RETURN

END SUBROUTINE destroy

!### the routine called to initialize every Patch ###
SUBROUTINE patch_init(self, workp, rankloc, simunits, is_boundary, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%init_routine))

        CASE ('Test_LinearHydro_Sound')

            CALL self%test_Hydro_sound(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearHydro_VyContact')

            CALL self%test_Hydro_vy(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearHydro_VzContact')

            CALL self%test_Hydro_vz(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearHydro_Entropy')

            CALL self%test_Hydro_entropy(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0


        !### Stone et al. use gamma=1.4, run until t = 0.25
        CASE ('Test_Sod')

            CALL self%test_Sod(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        !### Woodward & Collela (1984), use gamma = 1.4, run until t=0.038
        CASE ('Test_TwoBlastWaves')

            IF ( workp%is1d ) THEN
               CALL self%test_TwoBlastWaves(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 1D')
               ierr = 1
            END IF

        !### Shu-Osher (1989): -1<x<1, shock at x=-.8, propagating into sinusoidally varying
        !### density, gamma = 1.4, run until t = 0.47
        CASE ('Test_ShuOsher')

            IF ( workp%is1d ) THEN
               CALL self%test_ShuOsher(workp, tid)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 1D')
               ierr = 1
            END IF

        !### Einfeldt strong rarefaction, gamma = 1.4, run until t = 0.1
        CASE ('Test_Einfeldt')

            CALL self%test_Einfeldt(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        CASE ('Test_LinearMHD_Alfven')

            CALL self%test_MHD_alfven(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearMHD_Fast')

            CALL self%test_MHD_fast(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearMHD_Slow')

            CALL self%test_MHD_slow(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        CASE ('Test_LinearMHD_Entropy')

            CALL self%test_MHD_entropy(tid)
            CALL self%init_linear_wave(workp, tid)
            ierr = 0

        !### Brio & Wu (1988) MHD shocktube, gamma = 2.0, run until t = 0.08
        CASE ('Test_BrioWu')

            CALL self%test_BrioWu(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        !### Torrilhon (2003) MHD shocktube
        CASE ('Test_Torrilhon')

            CALL self%test_Torrilhon(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        !### Run until t=0.2
        CASE ('Test_RJ95_2a')

            CALL self%test_RJ95_2a(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        !### Run until t=0.16
        CASE ('Test_RJ95_4d')

            CALL self%test_RJ95_4d(tid)
            CALL self%init_tube_states(workp, tid)
            ierr = 0

        !### From Woodward & Colella (1984), gamma = 1.4, run until t = 0.2
        !###   Domain: 3.25x1.0, nx: 260x80
        CASE ('Test_DoubleMachReflection')

            IF (workp%is2d) THEN
               CALL self%test_dblmachrefl(workp, tid)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Liska & Wendroff (2003) implosion problem. Domain: 0.3x0.3
        !###   gamma = 1.4, theta = pi/4, run until t=2.5
        CASE ('Test_LW_implosion')

            IF (workp%is2d) THEN
               CALL self%test_LW_implosion(workp, tid)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF
        !### Rayleigh-Taylor instability, needs gravity
        CASE ('Test_RayleighTaylor')

            IF (workp%is2d) THEN
               CALL self%test_RayleighTaylor(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Circularly polarized alfven wave test
        CASE ('Test_CircularlyPolarizedAlfven')

            IF (workp%is2d) THEN

               CALL self%test_CPAlfvenWave2D(workp)
               ierr = 0
            ELSE IF (workp%is3d) THEN

               CALL self%test_CPAlfvenWave3d(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D or 3D')
               ierr = 1
            END IF

        !### Advection of a field loop 
        CASE ('Test_BLoop_Advection')

            IF (workp%is2d) THEN

               CALL self%test_BLoop_adv2D(workp)
               ierr = 0
            ELSEIF (workp%is3d) THEN
               CALL self%test_BLoop_adv3D(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Orszag & Tang (1979) 
        CASE ('Test_OrszagTangVortex')

            IF (workp%is2d) THEN

               CALL self%test_OrszagTangVortex(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Rotating disk in magnetic field, version a la Balsara & Spicer (1999)
        CASE ('Test_MHD_Rotor')

            IF (workp%is2d) THEN

               CALL self%test_MHD_Rotor(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Magnetized Rayleigh Taylor problem
        CASE ('Test_MagneticRayleighTaylor')

            IF (workp%is2d) THEN

               CALL self%test_MagneticRT(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
               ierr = 1
            END IF

        !### Blast wave in a strong magnetic field
        CASE ('Test_MagneticBlastWave')
            IF (workp%is2d) THEN

               CALL self%test_MagBlastWave2D(workp)
               ierr = 0
            ELSE IF (workp%is3d) THEN

               CALL self%test_MagBlastWave3D(workp)
               ierr = 0
            ELSE

               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D or 3D')
               ierr = 1
            END IF

        !### Very strong blast wave (M->Infinity) in 3D. Noh (1987).
        CASE ('Test_NohStrongShock')

            IF (workp%is3d) THEN

               CALL self%test_Noh3D(workp)
               ierr = 0
            ELSE
               CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 3D')
               ierr = 1
            END IF
        CASE ('Test_CurrentSheet')

            IF (workp%is2d) THEN
                CALL self%test_current_sheet(workp)
                ierr = 0
            ELSE
                CALL Error('Mod_Problem','"' // TRIM(self%init_routine) // '" only setup for 2D')
                ierr = 1
            END IF
        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown initialization routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1


    END SELECT



END SUBROUTINE patch_init

!### the routine called to set boundary values (called for every Patch even if it is not a world boundary) ###
SUBROUTINE patch_bounds(self, workp, rankloc, simunits, is_boundary, maxsignal, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    LOGICAL(WIS) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    REAL(WRD), INTENT(INOUT) :: maxsignal
    INTEGER(WIS), INTENT(OUT) :: ierr
    INTEGER(WIS), INTENT(IN) :: tid

    maxsignal = MAX(maxsignal, 0.0_WRD)

    lx_bound = is_boundary(1,1)
    hx_bound = is_boundary(2,1)
    ly_bound = is_boundary(1,2)
    hy_bound = is_boundary(2,2)
    lz_bound = is_boundary(1,3)
    hz_bound = is_boundary(2,3)

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%bounds_routine))

       CASE('ContinuousBounds')

           IF (workp%is1d) THEN
   
               CALL self%shock_tube_bounds1d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)
   
           ELSE IF(workp%is2d) THEN
   
               CALL self%shock_tube_bounds2d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)
   
           ELSE IF(workp%is3d) THEN
   
               CALL self%shock_tube_bounds3d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

            END IF

            ierr = 0

       CASE('ReflectingBounds')

           IF (workp%is1d) THEN

              CALL self%reflecting_bounds1d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

           ELSE IF(workp%is2d) THEN

              CALL self%reflecting_bounds2d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

           ELSE IF(workp%is3d) THEN

              CALL self%reflecting_bounds3d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

           END IF

           ierr = 0

       CASE('PeriodicBounds')

           ierr = 0
           RETURN

       CASE('DoubleMachReflectionBounds')

           CALL self%dblmachrefl_bounds(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound, tid)
           ierr = 0

       CASE('NohShockBounds')

           IF (lx_bound .OR. ly_bound .OR. lz_bound) THEN
               CALL self%reflecting_bounds3d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)
           END IF
           IF (hx_bound .OR. hy_bound .OR. hz_bound) THEN
               CALL self%upperNoh_bounds(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)
           END IF
           ierr = 0

       CASE DEFAULT

           CALL Error('Mod_Problem', 'Unknown boundary routine requested => "' // TRIM(self%bounds_routine) // '"')
           ierr = 1

    END SELECT
    RETURN

END SUBROUTINE patch_bounds

!### linear hydro sound wave test
SUBROUTINE test_Hydro_sound(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 1.0_WRD
    self%right(2, tid) = 1.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 1.0_WRD / (self%gamma - 1.0_WRD)

    RETURN

END SUBROUTINE test_Hydro_sound

!### linear hydro vy contact wave test
SUBROUTINE test_Hydro_vy(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 1.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 0.0_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 1.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.0_WRD

    RETURN

END SUBROUTINE test_Hydro_vy

!### linear hydro vz contact wave test
SUBROUTINE test_Hydro_vz(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 1.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 0.0_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 1.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.0_WRD

    RETURN

END SUBROUTINE test_Hydro_vz

!### linear hydro entropy wave test
SUBROUTINE test_Hydro_entropy(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 1.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 1.0_WRD
    self%right(2, tid) = 1.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.5_WRD

    RETURN

END SUBROUTINE test_Hydro_entropy

!### Sod problem left and right states
SUBROUTINE test_Sod(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD


    !### right ###
    self%right(1, tid) = 0.125_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.1_WRD

    RETURN

END SUBROUTINE test_Sod

!### From Woodward & Collela (1984) Two interacting 1D blast waves
SUBROUTINE test_TwoBlastWaves(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: igamma1, P0, Pl, Pr, xx
    INTEGER(WIS) :: i

    igamma1 = 1.0_WRD/(self%gamma - 1.0_WRD)
    P0 = 0.01_WRD
    Pl = 1.0E3_WRD
    Pr = 1.0E2_WRD

    DO i = 1, workp%nx

        xx = workp%x0 + (i-1)*workp%dx

        workp%grid1d(i,1) = 1.0_WRD
        workp%grid1d(i,2) = 0.0_WRD
        workp%grid1d(i,3) = 0.0_WRD
        workp%grid1d(i,4) = 0.0_WRD
        workp%grid1d(i,5) = 0.0_WRD
        workp%grid1d(i,6) = 0.0_WRD
        workp%grid1d(i,7) = 0.0_WRD
        workp%grid1d(i,8) = P0 * igamma1

        IF (xx .LT. -0.4_WRD) THEN

           workp%grid1d(i,8) = Pl * igamma1

        ELSE IF (xx .GT. 0.4_WRD) THEN

           workp%grid1d(i,8) = Pr * igamma1

        END IF

    END DO


END SUBROUTINE test_TwoBlastWaves

!### Shu-Osher left and right states
SUBROUTINE test_ShuOsher(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: fivepi = 10.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: pressure, igamma1, xx
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    !### left ###
    self%left(1, tid) = 3.857143_WRD
    self%left(2, tid) = 2.639369_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 10.3333_WRD

    !### right ###
    self%right(1, tid) = 1.0_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 1.0_WRD


    !### loop through the grid along X only ###
    DO i = 1, workp%nx

        xx = workp%x0 + (i-1)*workp%dx
        !### left hand states (along x) ###
        IF (xx .LE. -0.8_WRD) THEN

            workp%grid1d(i,1) = self%left(1, tid)
            workp%grid1d(i,2) = workp%grid1d(i,1) * self%left(2, tid)
            workp%grid1d(i,3) = workp%grid1d(i,1) * self%left(3, tid)
            workp%grid1d(i,4) = workp%grid1d(i,1) * self%left(4, tid)
            workp%grid1d(i,5) = self%left(5, tid)
            workp%grid1d(i,6) = self%left(6, tid)
            workp%grid1d(i,7) = self%left(7, tid)
            pressure          = self%left(8, tid)

        !### right hand states (along x) ###
        ELSE

            workp%grid1d(i,1) = self%right(1, tid) + 0.2_WRD * SIN( fivepi*xx)
            workp%grid1d(i,2) = workp%grid1d(i,1) * self%right(2, tid)
            workp%grid1d(i,3) = workp%grid1d(i,1) * self%right(3, tid)
            workp%grid1d(i,4) = workp%grid1d(i,1) * self%right(4, tid)
            workp%grid1d(i,5) = self%right(5, tid)
            workp%grid1d(i,6) = self%right(6, tid)
            workp%grid1d(i,7) = self%right(7, tid)
            pressure          = self%right(8, tid)

        END IF

        !### now set energy ###
        workp%grid1d(i,8) = pressure * igamma1 + (0.5_WRD / workp%grid1d(i,1)) * (workp%grid1d(i,2)**2 + workp%grid1d(i,3)**2 + workp%grid1d(i,4)**2) + &
                            0.5_WRD * (workp%grid1d(i,5)**2 + workp%grid1d(i,6)**2 + workp%grid1d(i,7)**2)
   
    END DO

    RETURN

END SUBROUTINE test_ShuOsher

!### Einfeldt strong rarefaction test left and right states
SUBROUTINE test_Einfeldt(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) =-2.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.0_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 0.4_WRD


    !### right ###
    self%right(1, tid) = 1.0_WRD
    self%right(2, tid) = 2.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.4_WRD

    RETURN

END SUBROUTINE test_Einfeldt

!### linear MHD Alfven wave test
SUBROUTINE test_MHD_alfven(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 1.0_WRD
    self%left(6, tid) = SQRT(2.0_WRD)
    self%left(7, tid) = 0.5_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 0.0_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 1.0_WRD
    self%right(4, tid) =-2.0_WRD*SQRT(2.0_WRD)
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) =-1.0_WRD
    self%right(7, tid) = 2.0_WRD*SQRT(2.0_WRD)
    self%right(8, tid) = 0.0_WRD

    self%right(:, tid) = self%right(:, tid) / 3.0_WRD

    RETURN

END SUBROUTINE test_MHD_alfven

!### linear MHD fast wave test
SUBROUTINE test_MHD_fast(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 1.0_WRD
    self%left(6, tid) = SQRT(2.0_WRD)
    self%left(7, tid) = 0.5_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 6.0_WRD
    self%right(2, tid) = 12.0_WRD
    self%right(3, tid) =-4.0_WRD*SQRT(2.0_WRD)
    self%right(4, tid) =-2.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 8.0_WRD*SQRT(2.0_WRD)
    self%right(7, tid) = 4.0_WRD
    self%right(8, tid) = 27.0_WRD

    self%right(:, tid) = self%right(:, tid) / (6.0_WRD*SQRT(5.0_WRD))

    RETURN

END SUBROUTINE test_MHD_fast

!### linear MHD slow wave test
SUBROUTINE test_MHD_slow(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 1.0_WRD
    self%left(6, tid) = SQRT(2.0_WRD)
    self%left(7, tid) = 0.5_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 12.0_WRD
    self%right(2, tid) = 6.0_WRD
    self%right(3, tid) = 8.0_WRD*SQRT(2.0_WRD)
    self%right(4, tid) = 4.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) =-4.0_WRD*SQRT(2.0_WRD)
    self%right(7, tid) =-2.0_WRD
    self%right(8, tid) = 9.0_WRD


    self%right(:, tid) = self%right(:, tid) / (6.0_WRD*SQRT(5.0_WRD))

    RETURN

END SUBROUTINE test_MHD_slow

!### linear MHD entropy wave test
SUBROUTINE test_MHD_entropy(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### unperturbed state ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 1.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 1.0_WRD
    self%left(6, tid) = SQRT(2.0_WRD)
    self%left(7, tid) = 0.5_WRD
    self%left(8, tid) = 1.0_WRD / self%gamma


    !### right eigenvector ###
    self%right(1, tid) = 2.0_WRD
    self%right(2, tid) = 2.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.0_WRD
    self%right(6, tid) = 0.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 1.0_WRD


    self%right(:, tid) = self%right(:, tid) * 0.5_WRD

    RETURN

END SUBROUTINE test_MHD_entropy


!### Brio & Wu MHD Shock test left and right states
SUBROUTINE test_BrioWu(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.75_WRD
    self%left(6, tid) = 1.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD

    !### right ###
    self%right(1, tid) = 0.125_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 0.75_WRD
    self%right(6, tid) =-1.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.1_WRD

    RETURN

END SUBROUTINE test_BrioWu

!### Torrilhon MHD Shock test left and right states
SUBROUTINE test_Torrilhon(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 1.0_WRD
    self%left(6, tid) = 1.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD

    !### right ###
    self%right(1, tid) = 0.2_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 1.0_WRD
    self%right(6, tid) = COS(3.0_WRD)
    self%right(7, tid) = SIN(2.0_WRD)
    self%right(8, tid) = 0.2_WRD

    RETURN

END SUBROUTINE test_Torrilhon

!### RJ95 2a left and right states
SUBROUTINE test_RJ95_2a(self, tid)

    CLASS(Problem) :: self
    REAL(WRD) :: pi
    INTEGER(WIS), INTENT(IN) :: tid

    !### define pi ###
    pi = 2.0_WRD * ASIN(1.0_WRD)

    !### left ###
    self%left(1, tid) = 1.08_WRD
    self%left(2, tid) = 1.2_WRD
    self%left(3, tid) = 0.01_WRD
    self%left(4, tid) = 0.5_WRD
    self%left(5, tid) = 2.0_WRD / SQRT(4.0_WRD * pi)
    self%left(6, tid) = 3.6_WRD / SQRT(4.0_WRD * pi)
    self%left(7, tid) = 2.0_WRD / SQRT(4.0_WRD * pi)
    self%left(8, tid) = 0.95_WRD


    !### right ###
    self%right(1, tid) = 1.0_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 0.0_WRD
    self%right(5, tid) = 2.0_WRD / SQRT(4.0_WRD * pi)
    self%right(6, tid) = 4.0_WRD / SQRT(4.0_WRD * pi)
    self%right(7, tid) = 2.0_WRD / SQRT(4.0_WRD * pi)
    self%right(8, tid) = 1.0_WRD

    RETURN

END SUBROUTINE test_RJ95_2a

!### RJ95 4d left and right states
SUBROUTINE test_RJ95_4d(self, tid)

    CLASS(Problem) :: self
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1, tid) = 1.0_WRD
    self%left(2, tid) = 0.0_WRD
    self%left(3, tid) = 0.0_WRD
    self%left(4, tid) = 0.0_WRD
    self%left(5, tid) = 0.7_WRD
    self%left(6, tid) = 0.0_WRD
    self%left(7, tid) = 0.0_WRD
    self%left(8, tid) = 1.0_WRD

    !### right ###
    self%right(1, tid) = 0.3_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(4, tid) = 1.0_WRD
    self%right(5, tid) = 0.7_WRD
    self%right(6, tid) = 1.0_WRD
    self%right(7, tid) = 0.0_WRD
    self%right(8, tid) = 0.2_WRD
 
    RETURN

END SUBROUTINE test_RJ95_4d

!### Woodward & Colella 1984 - two interacting shock fronts
SUBROUTINE test_dblmachrefl(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pos(2), transvec(2), xshock
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i,j

    !### downstream ###
    self%left(1, tid) = 8.0_WRD
    self%left(2, tid) = self%left(1, tid)*8.25_WRD * SQRT(3.0_WRD)/2.0_WRD
    self%left(3, tid) =-self%left(1, tid)*8.25_WRD * 0.5_WRD
    self%left(8, tid) = 291.25_WRD + 0.5_WRD * (self%left(2, tid)**2 + self%left(3, tid)**2)/self%left(1, tid)

    !### upstream ###
    self%right(1, tid) = 1.4_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(8, tid) = 2.5_WRD

    !### downstream ###
    self%left(1, tid) = 8.0_WRD
    self%left(2, tid) = self%left(1, tid)*8.25_WRD * SQRT(3.0_WRD)/2.0_WRD
    self%left(3, tid) =-self%left(1, tid)*8.25_WRD * 0.5_WRD
    self%left(8, tid) = 291.25_WRD + 0.5_WRD * (self%left(2, tid)**2 +self%left(3, tid)**2)/self%left(1, tid)

    !### upstream ###
    self%right(1, tid) = 1.4_WRD
    self%right(2, tid) = 0.0_WRD
    self%right(3, tid) = 0.0_WRD
    self%right(8, tid) = 2.5_WRD

    !### total domain in W & C is 3.25 x 1
    transvec = (/ 1.625_WRD, 0.5_WRD /)

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + transvec(1)
          pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + transvec(2)
          xshock    = 1.0_WRD/6.0_WRD + pos(2) / SQRT(3.0_WRD)

          IF ( pos(1) .LT. xshock ) THEN

             workp%grid2d(i,j,1) = self%left(1, tid)
             workp%grid2d(i,j,2) = self%left(2, tid)
             workp%grid2d(i,j,3) = self%left(3, tid)
             workp%grid2d(i,j,8) = self%left(8, tid)

          ELSE

             workp%grid2d(i,j,1) = self%right(1, tid)
             workp%grid2d(i,j,2) = self%right(2, tid)
             workp%grid2d(i,j,3) = self%right(3, tid)
             workp%grid2d(i,j,8) = self%right(8, tid)

          END IF

          workp%grid2d(i,j,4)  = 0.0_WRD
          workp%grid2d(i,j,5)  = 0.0_WRD
          workp%grid2d(i,j,6)  = 0.0_WRD
          workp%grid2d(i,j,7)  = 0.0_WRD
          workp%bface2d(i,j,:) = 0.0_WRD

       END DO
    END DO


END SUBROUTINE test_dblmachrefl

!### Liska & Wendroff (2003) 2D implosion problem,
SUBROUTINE test_LW_implosion(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pos(2), transvec(2), pressure, igamma1
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i,j

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD) 

    !### inner ###
    self%left(1, tid) = 0.125_WRD
    self%left(8, tid) = 0.14_WRD

    !### outer ###
    self%right(1, tid) = 1.0_WRD
    self%right(8, tid) = 1.0_WRD


    transvec = (/ 0.15_WRD, 0.15_WRD /)

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + transvec(1)
          pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + transvec(2)

          workp%grid2d(i,j,1)  = self%right(1, tid)
          pressure             = self%right(8, tid)


          IF (pos(2) .LT. 0.15_WRD - pos(1)) THEN

              workp%grid2d(i,j,1)  = self%left(1, tid)
              pressure             = self%left(8, tid)

          END IF
          workp%grid2d(i,j,2) = 0.0_WRD
          workp%grid2d(i,j,3) = 0.0_WRD
          workp%grid2d(i,j,4) = 0.0_WRD
          workp%grid2d(i,j,5) = 0.0_WRD
          workp%grid2d(i,j,6) = 0.0_WRD
          workp%grid2d(i,j,7) = 0.0_WRD
          workp%grid2d(i,j,8) = pressure * igamma1
          workp%bface2d(i,j,:) = 0.0_WRD

       END DO
    END DO

END SUBROUTINE test_LW_implosion

!### nonlinear evolution of single mode of RT instability, box size (1/3 x 1.0), box size (1/3 x 1.0)
SUBROUTINE test_RayleighTaylor(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: thrpi = 6.0_WRD*ASIN(1.0_WRD)
    REAL(WRD) :: igamma1, gy, sixth
    REAL(WRD) :: pos(2), rhoup, rhodn, Py0, pres
    INTEGER(WIS) :: i,j

    rhoup = 2.0_WRD
    rhodn = 1.0_WRD

    gy = -0.1_WRD
    sixth = 1.0_WRD / 6.0_WRD

    !### fluid at hydrostat equilibrium throughout domain, soundspeed at interface in lighter fluid is 1, gamma = 1.4
    Py0 = 5.0_WRD / 7.0_WRD

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    DO j = 1-workp%nb, workp%ny+workp%nb
        DO i = 1-workp%nb, workp%nx+workp%nb

            pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
            pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx

            workp%grav2d(i,j,2) = -gy * pos(2)

            IF (pos(2) .GT. 0.0_WRD) THEN

                workp%grid2d(i,j,1) = rhoup
                workp%pass2d(i,j,1) = 1.0_WRD
                pres = Py0 + rhoup * gy * pos(2)

            ELSE

                workp%grid2d(i,j,1) = rhodn
                workp%pass2d(i,j,1) = 0.0_WRD
                pres = Py0 + rhodn * gy * pos(2)

            END IF

            workp%grid2d(i,j,2) = 0.0_WRD
            workp%grid2d(i,j,3) = 0.0_WRD
            workp%grid2d(i,j,4) = 0.0_WRD
            workp%grid2d(i,j,5:7) = 0.0_WRD

            !### initialize the interface with pertrubed velocity
            IF ( ABS(pos(2)) .LE.  workp%dx ) THEN
                workp%grid2d(i,j,3) = workp%grid2d(i,j,1) * 0.01_WRD * SIN(thrpi*(pos(1)+sixth))
            END IF

            workp%grid2d(i,j,8) = pres*igamma1 + 0.5_WRD*workp%grid2d(i,j,3)**2 / workp%grid2d(i,j,1)
            workp%bface2d(i,j,:) = 0.0_WRD

        END DO
    END DO

END SUBROUTINE test_RayleighTaylor


!### Gardiner & Stone (2005)
SUBROUTINE test_CPAlfvenWave2D(self, workp)
    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: igamma1
    REAL(WRD) :: Az(1-workp%nb-1:workp%nx+workp%nb+1,1-workp%nb-1:workp%ny+workp%nb+1)
    REAL(WRD) :: pos(2), posx(2), v(3), Amp, itpi, bz, idx
    INTEGER(WIS) :: i, j

    Amp = 0.1_WRD
    idx = 1.0_WRD / workp%dx
    itpi = 1.0_WRD / twopi
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)


    DO j = 1-workp%nb-1, workp%ny+workp%nb+1
       DO i = 1-workp%nb-1, workp%nx+workp%nb+1

          pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
          pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
          posx   = pos + 0.5_WRD*workp%dx
          CALL self%rotate_2d(posx,.TRUE.)

          Az(i,j) = Amp*itpi*COS(twopi * posx(1)) + posx(2)

       END DO
    END DO

    DO j = 1-workp%nb, workp%ny+workp%nb
        DO i = 1-workp%nb, workp%nx+workp%nb

            workp%bface2d(i,j,1) =  idx * (Az(i,j) - Az(i,j-1))
            workp%bface2d(i,j,2) = -idx * (Az(i,j) - Az(i-1,j))

        END DO
    END DO

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
          pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
          CALL self%rotate_2d(pos,.TRUE.)

          v     = (/ 0.0_WRD, Amp*SIN(twopi*pos(1)), Amp*COS(twopi*pos(1)) /)
          bz    = Amp*COS(twopi*pos(1))

          CALL self%rotate_2d(v(1:2),    .FALSE.)

          workp%grid2d(i,j,1)  = 1.0_WRD
          workp%grid2d(i,j,2)  = workp%grid2d(i,j,1)*v(1)
          workp%grid2d(i,j,3)  = workp%grid2d(i,j,1)*v(2)
          workp%grid2d(i,j,4)  = workp%grid2d(i,j,1)*v(3)

          workp%grid2d(i,j,7)  = bz


       END DO
    END DO

    !### loop back through and set Bx/y and energy ###
    DO j = 1, workp%ny
        DO i = 1, workp%nx

            workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
            workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))
            workp%grid2d(i,j,8) = 0.1_WRD * igamma1 + &
                (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                 0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)

        END DO
    END DO

END SUBROUTINE test_CPAlfvenWave2D

SUBROUTINE test_CPAlfvenWave3D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: Apot(3,1-workp%nb-1:workp%nx+workp%nb,1-workp%nb-1:workp%ny+workp%nb,1-workp%nb-1:workp%nz+workp%nb)
    REAL(WRD) :: igamma1
    REAL(WRD) :: posx(3), v(3), b(3), A(3), Amp, itpi, bz, idx
    INTEGER(WIS) :: i,j,k

    Amp = 0.1d0
    idx = 1.0_WRD / workp%dx
    itpi = 1.0_WRD / twopi
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    DO k = 1-workp%nb-1, workp%nz+workp%nb
        DO j = 1-workp%nb-1, workp%ny+workp%nb
            DO i = 1-workp%nb-1, workp%nx+workp%nb

                posx(1) = workp%x0 + (REAL((i-1),WRD) + 0.5_WRD)*workp%dx
                posx(2) = workp%y0 + (REAL((j-1),WRD) + 0.5_WRD)*workp%dx
                posx(3) = workp%z0 + (REAL((k-1),WRD) + 0.5_WRD)*workp%dx
                CALL self%rotate_3d(posx,.TRUE.)

                A(1) = 0.0_WRD
                A(2) = Amp*itpi*SIN(twopi * posx(1))
                A(3) = Amp*itpi*COS(twopi * posx(1)) + posx(2)
                CALL self%rotate_3d(A,.FALSE.)

                Apot(:,i,j,k) = A(:)

            END DO
        END DO
    END DO

    DO k = 1-workp%nb, workp%nz+workp%nb
        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%bface3d(i,j,k,1) = idx * (Apot(3,i,j,k) - Apot(3,i,j-1,k)) - idx * (Apot(2,i,j,k) - Apot(2,i,j,k-1))
                workp%bface3d(i,j,k,2) = idx * (Apot(1,i,j,k) - Apot(1,i,j,k-1)) - idx * (Apot(3,i,j,k) - Apot(3,i-1,j,k))
                workp%bface3d(i,j,k,3) = idx * (Apot(2,i,j,k) - Apot(2,i-1,j,k)) - idx * (Apot(1,i,j,k) - Apot(1,i,j-1,k))

            END DO
        END DO
    END DO

    DO k = 1-workp%nb, workp%nz+workp%nb
        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                posx(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
                posx(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
                posx(3) = workp%z0 + REAL((k-1),WRD)*workp%dx
                CALL self%rotate_3d(posx,.TRUE.)

                v = (/ 0.0_WRD, 0.1_WRD*SIN(twopi*posx(1)), 0.1_WRD*COS(twopi*posx(1)) /) 

                CALL self%rotate_3d(v,.FALSE.)

                workp%grid3d(i,j,k,1)  = 1.0_WRD
                workp%grid3d(i,j,k,2)  = v(1)
                workp%grid3d(i,j,k,3)  = v(2)
                workp%grid3d(i,j,k,4)  = v(3)

            END DO
        END DO
    END DO

    DO k = 1, workp%nz
        DO j = 1, workp%ny
            DO i = 1, workp%nx

                  workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i-1,j,k,1) + workp%bface3d(i,j,k,1))
                  workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j-1,k,2) + workp%bface3d(i,j,k,2))
                  workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k-1,3) + workp%bface3d(i,j,k,3))
                  workp%grid3d(i,j,k,8) = 0.1_WRD*igamma1 + &
                    0.5_WRD/workp%grid3d(i,j,k,1)*(workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2) + &
                                          0.5_WRD*(workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2)

            END DO
        END DO
    END DO

END SUBROUTINE test_CPAlfvenWave3D

!### Gardiner & Stone (2005)
SUBROUTINE test_BLoop_adv2D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), DIMENSION(:,:), ALLOCATABLE :: Az
    REAL(WRD) :: igamma1, idx, pos(2)
    REAL(WRD) :: A0, rho, pres, vx, vy, vz, rmag, rc
    INTEGER(WIS) :: i,j

    A0   = 1.0E-3_WRD
    rc   = 0.3_WRD
    rho  = 1.0_WRD
    pres = 1.0_WRD

    vx = 2.0_WRD
    vy = 1.0_WRD
    vz = 0.0_WRD

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    idx = 1.0_WRD / workp%dx

    !### vector potential to initialize field loop, defined at corners of grid
    ALLOCATE(Az(1-workp%nb-1:workp%nx+workp%nb,1-workp%nb-1:workp%ny+workp%nb))

    DO j = 1-workp%nb-1, workp%ny + workp%nb
        DO i = 1-workp%nb-1, workp%nx + workp%nb

            pos(1) = workp%x0 + (REAL((i-1),WRD) + 0.5_WRD)*workp%dx
            pos(2) = workp%y0 + (REAL((j-1),WRD) + 0.5_WRD)*workp%dx
            rmag   = SQRT(SUM(pos**2))

            Az(i,j) = 0.0_WRD
            IF(rmag .LE. rc) THEN
                Az(i,j) = A0 * (rc - rmag)
            END IF

        END DO
    END DO

    !### set B-field on faces using curl of A
    DO j = 1-workp%nb, workp%ny + workp%nb
        DO i = 1-workp%nb, workp%nx + workp%nb

            workp%bface2d(i,j,1) = (Az(i,j) - Az(i,j-1)) * idx
            workp%bface2d(i,j,2) =-(Az(i,j) - Az(i-1,j)) * idx

        END DO
    END DO

    DO j = 1, workp%ny + workp%nb
        DO i = 1, workp%nx + workp%nb

            workp%grid2d(i,j,1) = rho
            workp%grid2d(i,j,2) = rho*vx
            workp%grid2d(i,j,3) = rho*vy
            workp%grid2d(i,j,4) = rho*vz
            workp%grid2d(i,j,7) = 0.0_WRD

            workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i,j,1) + workp%bface2d(i-1,j,1))
            workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j,2) + workp%bface2d(i,j-1,2))

            workp%grid2d(i,j,8) = pres*igamma1 + 0.5_WRD*rho*(vx**2+vy**2+vz**2) + 0.5_WRD*SUM(workp%grid2d(i,j,5:6)**2)


        END DO
    END DO

    DEALLOCATE(Az)

END SUBROUTINE test_BLoop_adv2D

!### advection of oblique cylindrical B Loop (rotated aroung y-axis by atan[0.5]) (Gardiner & Stone 2008)
SUBROUTINE test_BLoop_adv3D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), DIMENSION(:,:,:), ALLOCATABLE :: Ax,Az
    REAL(WRD) :: pos(3), posp(3), igamma1, idx, irt5
    REAL(WRD) :: A0, rho, pres, vx, vy, vz, rmag, rc, A3
    INTEGER(WIS) :: i,j,k

    A0   = 1.0E-3_WRD
    rc   = 0.3_WRD
    rho  = 1.0_WRD
    pres = 1.0_WRD

    vx = 1.0_WRD
    vy = 1.0_WRD
    vz = 2.0_WRD

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    idx = 1.0_WRD / workp%dx
    irt5 = 1.0_WRD / SQRT(5.0_WRD)

    ALLOCATE(Ax(1-workp%nb:workp%nx+workp%nb,1-workp%nb:workp%ny+workp%nb,1-workp%nb:workp%nz+workp%nb))
    ALLOCATE(Az(1-workp%nb:workp%nx+workp%nb,1-workp%nb:workp%ny+workp%nb,1-workp%nb:workp%nz+workp%nb))

    !### vector potential to initialize field loop, defined at corners of grid
    DO k = 1 - workp%nb, workp%nz + workp%nb
        DO j = 1 - workp%nb, workp%ny + workp%nb
            DO i = 1 - workp%nb, workp%nx + workp%nb

                pos(1) = workp%x0 + (REAL((i-1),WRD)+0.5_WRD)*workp%dx
                pos(2) = workp%y0 + (REAL((j-1),WRD)+0.5_WRD)*workp%dx
                pos(3) = workp%z0 + (REAL((k-1),WRD)+0.5_WRD)*workp%dx

                posp(1) = (2.0_WRD*pos(1) + pos(3)) * irt5
                posp(2) = pos(2)
                posp(3) = (-pos(1) + 2.0_WRD*pos(3)) * irt5

                IF (posp(1) .LT. -irt5) posp(1) = posp(1) + 2.0_WRD*irt5
                IF (posp(1) .GT. irt5 ) posp(1) = posp(1) - 2.0_WRD*irt5

                rmag = SQRT(SUM(posp(1:2)**2))
                Ax(i,j,k) = 0.0_WRD
                Az(i,j,k) = 0.0_WRD
                IF (rmag .LT. rc) THEN
                    A3 = A0*(rc - rmag)
                    Ax(i,j,k) =-A3 * irt5
                    Az(i,j,k) = A3 * 2.0_WRD * irt5
                END IF

            END DO
        END DO
    END DO

    !### set B-field on faces using curl of A
    DO k = 2 - workp%nb, workp%nz + workp%nb
        DO j = 2 - workp%nb, workp%ny + workp%nb
            DO i = 2 - workp%nb, workp%nx + workp%nb

                workp%bface3d(i,j,k,1) = (Az(i,j,k) - Az(i,j-1,k)) * idx
                workp%bface3d(i,j,k,2) = (Ax(i,j,k) - Ax(i,j,k-1) - Az(i,j,k) + Az(i-1,j,k)) * idx
                workp%bface3d(i,j,k,3) = (-Ax(i,j,k) + Ax(i,j-1,k)) * idx

            END DO
        END DO
    END DO


    DO k = 3 - workp%nb, workp%nz + workp%nb
        DO j = 3 - workp%nb, workp%ny + workp%nb
            DO i = 3 - workp%nb, workp%nx + workp%nb

                workp%grid3d(i,j,k,1) = rho
                workp%grid3d(i,j,k,2) = rho*vx
                workp%grid3d(i,j,k,3) = rho*vy
                workp%grid3d(i,j,k,4) = rho*vz
                workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i,j,k,1) + workp%bface3d(i-1,j,k,1))
                workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j,k,2) + workp%bface3d(i,j-1,k,2))
                workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k,3) + workp%bface3d(i,j,k-1,3))
                workp%grid3d(i,j,k,8) = pres*igamma1 + 0.5_WRD*rho*(vx**2 + vy**2 + vz**2) &
                                      + 0.5_WRD*SUM(workp%grid3d(i,j,k,5:7)**2)

            END DO
        END DO
    END DO


    DEALLOCATE(Ax, Az)


END SUBROUTINE test_BLoop_adv3D

!### Orszag & Tang (1979)
SUBROUTINE test_OrszagTangVortex(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: pos(2), posx(2), posf(2,2), v(2), b(2), bf1(2), bf2(2)
    REAL(WRD), DIMENSION(1-workp%nb:workp%nx+workp%nb,1-workp%nb:workp%ny+workp%nb) :: Az
    REAL(WRD) :: rho0, P0, igamma1, irt4pi, i2pi, B0, idx
    INTEGER(WIS) :: i, j

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    i2pi = 1.0_WRD / twopi

    rho0    = 25.0_WRD / ( 18.0_WRD*twopi )
    P0      = 5.0_WRD / ( 6.0_WRD*twopi )
    B0      = 1.0_WRD / SQRT( 2.0_WRD* twopi )
    idx     = 1.0_WRD / workp%dx

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + 0.5_WRD
          pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + 0.5_WRD
          posx      = pos + 0.5_WRD*workp%dx

          v     = (/ -SIN(twopi*pos(2)), SIN(twopi*pos(1)) /)
          Az(i,j) = 0.5_WRD*B0*i2pi*COS(2.0_WRD*twopi*posx(1)) + B0*i2pi*COS(twopi*posx(2))

          workp%grid2d(i,j,1)  = rho0
          workp%grid2d(i,j,2)  = rho0*v(1)
          workp%grid2d(i,j,3)  = rho0*v(2)
          workp%grid2d(i,j,4)  = 0.0_WRD

          workp%grid2d(i,j,7)  = 0.0_WRD

       END DO
    END DO

    DO j = 2-workp%nb, workp%ny+workp%nb
       DO i = 2-workp%nb, workp%nx+workp%nb

          workp%bface2d(i,j,1) =  idx * (Az(i,j) - Az(i,j-1))
          workp%bface2d(i,j,2) = -idx * (Az(i,j) - Az(i-1,j))

       END DO
    END DO

    DO j = 1, workp%ny
       DO i = 1, workp%nx+workp%nb

          workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
          workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))
          workp%grid2d(i,j,8) = P0*igamma1 + 0.5_WRD/rho0*( workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 ) + &
               0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2)

       END DO
    END DO

    RETURN


!    CLASS(Problem) :: self
!    TYPE(Patch) :: workp
!    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
!    REAL(WRD) :: pos(2), posf(2,2), v(2), b(2), bf1(2), bf2(2)
!    REAL(WRD) :: rho0, P0, igamma1, irt4pi, Az
!    INTEGER(WIS) :: i, j
!
!    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
!    irt4pi  = 1.0_WRD / SQRT( 2.0_WRD* twopi )
!
!    rho0    = 25.0_WRD / ( 18.0_WRD*twopi )
!    P0      = 5.0_WRD / ( 6.0_WRD*twopi )
!
!    DO j = 1-workp%nb, workp%ny+workp%nb
!       DO i = 1-workp%nb, workp%nx+workp%nb
!
!          pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
!          pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
!          posf(1,1) = pos(1) + 0.5_WRD*workp%dx
!          posf(2,1) = pos(2)
!          posf(1,2) = pos(1)
!          posf(2,2) = pos(2) + 0.5_WRD*workp%dx
!
!          v     = (/ -SIN(twopi*pos(2)), SIN(twopi*pos(1)) /)
!          bf1   = (/ - irt4pi * SIN(twopi*posf(2,1))  , irt4pi * SIN(2.0_WRD*twopi*posf(1,1)) /)
!          bf2   = (/ - irt4pi * SIN(twopi*posf(2,2))  , irt4pi * SIN(2.0_WRD*twopi*posf(1,2)) /)
!
!          workp%grid2d(i,j,1)  = rho0
!          workp%grid2d(i,j,2)  = rho0*v(1)
!          workp%grid2d(i,j,3)  = rho0*v(2)
!          workp%grid2d(i,j,4)  = 0.0_WRD
!
!          workp%grid2d(i,j,7)  = 0.0_WRD
!
!          workp%bface2d(i,j,1) = bf1(1)
!          workp%bface2d(i,j,2) = bf2(2)
!
!       END DO
!    END DO
!
!
!    DO j = 1, workp%ny
!       DO i = 1, workp%nx+workp%nb
!
!          workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
!          workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))
!          workp%grid2d(i,j,8) = P0*igamma1 + 0.5_WRD*rho0*( v(1)**2 + v(2)**2 ) + &
!               0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2)
!
!       END DO
!    END DO
!
!    RETURN

END SUBROUTINE test_OrszagTangVortex

SUBROUTINE test_MHD_Rotor(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: rho, p, Bx, v(2), pos(2)
    REAL(WRD) :: r0, r1, r, v0, f, igamma1
    INTEGER(WIS) :: i,j

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    r0  = 0.1_WRD
    r1  = 0.115_WRD

    p   = 1.0_WRD
    v0  = 2.0_WRD
    Bx  = 5.0_WRD / SQRT( 2.0_WRD*twopi )

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
          pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
          r      = SQRT( pos(1)**2 + pos(2)**2 )

          IF ( r .LT. r0 ) THEN

             rho = 10.0_WRD
             v   = (/ -v0*pos(2)/r0, v0*pos(1)/r0 /)

          ELSE IF ( r .LE. r1 ) THEN

             f   = (r1 - r) / (r1 - r0)

             rho = 1.0_WRD + 9.0_WRD*f
             v   = (/ -f*v0*pos(2)/r, f*v0*pos(1)/r /)

          ELSE

             rho = 1.0_WRD
             v   = 0.0_WRD

          END IF

          workp%grid2d(i,j,1)  = rho
          workp%grid2d(i,j,2)  = rho*v(1)
          workp%grid2d(i,j,3)  = rho*v(2)
          workp%grid2d(i,j,4)  = 0.0_WRD
          workp%grid2d(i,j,5)  = Bx
          workp%grid2d(i,j,6)  = 0.0_WRD
          workp%grid2d(i,j,7)  = 0.0_WRD
          workp%grid2d(i,j,8)  = p*igamma1 + 0.5_WRD*rho*(v(1)**2 + v(2)**2) + 0.5_WRD*Bx**2

       END DO
    END DO

    DO j = 1-workp%nb, workp%ny+workp%nb
       DO i = 1-workp%nb, workp%nx+workp%nb

          workp%bface2d(i,j,1) = Bx
          workp%bface2d(i,j,2) = 0.0_WRD

       END DO
    END DO

    RETURN

END SUBROUTINE test_MHD_Rotor

!### Magnetized Rayleigh-Taylor test, box size = (0.1 x 0.2)
SUBROUTINE test_MagneticRT(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: pi = 2.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: pres, Py0
    REAL(WRD) :: pos(2), igamma1, Bx, rhoup, rhodn, gy
    INTEGER(WIS) :: i,j

    rhoup = 3.0_WRD
    rhodn = 1.0_WRD
    Bx    = 0.007_WRD

    gy  =-0.1_WRD
    Py0 = 3.0_WRD / 5.0_WRD

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    DO j = 1-workp%nb, workp%ny + workp%nb
        DO i = 1-workp%nb, workp%nx + workp%nb

            pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
            pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx

            workp%grav2d(i,j,2) = -gy * pos(2)

            IF (pos(2) .GT. 0.0_WRD) THEN

                workp%grid2d(i,j,1) = rhoup
                workp%pass2d(i,j,1) = 1.0_WRD
                pres = Py0 + rhoup * gy * pos(2) + 0.5_WRD*Bx**2

            ELSE

                workp%grid2d(i,j,1) = rhodn
                workp%pass2d(i,j,1) = 0.0_WRD
                pres = Py0 + rhodn * gy * pos(2) + 0.5_WRD*Bx**2

            END IF

            workp%grid2d(i,j,2) = 0.0_WRD
            workp%grid2d(i,j,3) = 0.0_WRD
            workp%grid2d(i,j,4) = 0.0_WRD
            workp%grid2d(i,j,5) = Bx
            workp%grid2d(i,j,6:7) = 0.0_WRD

            IF ( ABS(pos(2)) .LE.  workp%dx ) THEN
                workp%grid2d(i,j,3) = workp%grid2d(i,j,1) * 0.002_WRD * SIN(10.0_WRD*pi*(pos(1)+0.05_WRD))
            END IF

            workp%grid2d(i,j,8) = pres*igamma1 + 0.5_WRD * workp%grid2d(i,j,3)**2/workp%grid2d(i,j,1) + 0.5_WRD*Bx**2

            workp%bface2d(i,j,1) = Bx
            workp%bface2d(i,j,2) = 0.0_WRD

       END DO
    END DO

END SUBROUTINE test_MagneticRT

!### initial fluid conditions from Londrillo & Del Zanna (2000)
SUBROUTINE test_MagBlastWave2D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pos(2), posx(2), rmag, rc, igamma1, idx, Bx, Birt2, pres
    REAL(WRD) :: Az(1-workp%nb-1:workp%nx+workp%nb+1,1-workp%nb-1:workp%ny+workp%nb+1)
    INTEGER(WIS) :: i,j

    rc = 0.125_WRD
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    idx = 1.0_WRD / workp%dx
!    Bx = 1.0_WRD
    Bx = 10.0_WRD
    Birt2 = Bx / SQRT(2.0_WRD)

    DO j = 1-workp%nb-1, workp%ny+workp%nb+1
        DO i = 1-workp%nb-1, workp%nx+workp%nb+1

            pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
            pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
            posx(:)   = pos(:) + 0.5_WRD * workp%dx
            
            Az(i,j)   =  -Birt2*posx(1) + Birt2*posx(2) 

        END DO
    END DO

    DO j = 1-workp%nb, workp%ny + workp%nb
        DO i = 1-workp%nb, workp%nx + workp%nb

            workp%bface2d(i,j,1) =  idx * (Az(i,j) - Az(i,j-1))
            workp%bface2d(i,j,2) = -idx * (Az(i,j) - Az(i-1,j))

            workp%grid2d(i,j,1) = 1.0_WRD
            workp%grid2d(i,j,2) = 0.0_WRD
            workp%grid2d(i,j,3) = 0.0_WRD
            workp%grid2d(i,j,4) = 0.0_WRD

            workp%grid2d(i,j,7) = 0.0_WRD

        END DO
    END DO

    DO j = 1-workp%nb, workp%ny + workp%nb
        DO i = 1-workp%nb, workp%nx + workp%nb

            pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
            pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx

            rmag = SQRT(SUM(pos**2))

            IF (rmag .LT. rc) THEN
                pres = 100.0_WRD
            ELSE
                pres = 1.0_WRD
            END IF

            workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
            workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))

            workp%grid2d(i,j,8) = pres*igamma1 + 0.5_WRD*(workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2)

        END DO
    END DO


END SUBROUTINE test_MagBlastWave2D

SUBROUTINE test_MagBlastWave3D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pos(3), rmag, rc, igamma1, B0, Birt2, pres
    INTEGER(WIS) :: i,j,k

    rc = 0.125_WRD
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    B0 = 10.0_WRD
    Birt2 = B0 / SQRT(2.0_WRD)

    DO k = 1 - workp%nb, workp%nz + workp%nb
        DO j = 1 - workp%nb, workp%ny + workp%nb
            DO i = 1 - workp%nb, workp%nx + workp%nb

                pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
                pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
                pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx

                rmag = SQRT(SUM(pos**2))

                IF (rmag .LT. rc) THEN
                    pres = 100.0_WRD
                ELSE
                    pres = 1.0_WRD
                END IF

                workp%grid3d(i,j,k,1) = 1.0_WRD
                workp%grid3d(i,j,k,2) = 0.0_WRD
                workp%grid3d(i,j,k,3) = 0.0_WRD
                workp%grid3d(i,j,k,4) = 0.0_WRD
                workp%grid3d(i,j,k,5) = Birt2
                workp%grid3d(i,j,k,6) = Birt2
                workp%grid3d(i,j,k,7) = 0.0_WRD
                workp%grid3d(i,j,k,8) = pres*igamma1 + 0.5_WRD*B0**2

            END DO
        END DO
    END DO

    DO k = 1 - workp%nb, workp%nz + workp%nb
        DO j = 1 - workp%nb, workp%ny + workp%nb
            DO i = 1 - workp%nb, workp%nx + workp%nb

                workp%bface3d(i,j,k,1) = Birt2
                workp%bface3d(i,j,k,2) = Birt2
                workp%bface3d(i,j,k,3) = 0.0_WRD

            END DO
        END DO
    END DO

END SUBROUTINE test_MagBlastWave3D

SUBROUTINE test_Noh3D(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: igamma1, vel(3), pos(3), trans(3), pres
    INTEGER(WIS) :: i,j,k


    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    pres = 1.0E-6_WRD
    trans = (/ 0.5_WRD, 0.5_WRD, 0.5_WRD /)

    DO k = 1 - workp%nb, workp%nz + workp%nb
        DO j = 1 - workp%nb, workp%ny + workp%nb
            DO i = 1 - workp%nb, workp%nx + workp%nb
                
                pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + trans(1)
                pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + trans(2)
                pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx + trans(3)

                !### vel magnitude = 1, points toward origin
                vel = -pos / SQRT(SUM(pos**2))
                

                workp%grid3d(i,j,k,1) = 1.0_WRD
                workp%grid3d(i,j,k,2) = vel(1)
                workp%grid3d(i,j,k,3) = vel(2)
                workp%grid3d(i,j,k,4) = vel(3)
                workp%grid3d(i,j,k,5) = 0.0_WRD
                workp%grid3d(i,j,k,6) = 0.0_WRD
                workp%grid3d(i,j,k,7) = 0.0_WRD
                workp%grid3d(i,j,k,8) = pres*igamma1 + 0.5_WRD !### KE everywhere = 0.5

            END DO
        END DO
    END DO

    DO k = 1 - workp%nb, workp%nz + workp%nb
        DO j = 1 - workp%nb, workp%ny + workp%nb
            DO i = 1 - workp%nb, workp%nx + workp%nb

                workp%bface3d(i,j,k,1) = 0.0_WRD
                workp%bface3d(i,j,k,2) = 0.0_WRD
                workp%bface3d(i,j,k,3) = 0.0_WRD

            END DO
        END DO
    END DO

END SUBROUTINE test_Noh3D
!### Hawley & Stone (1995)
SUBROUTINE test_current_sheet(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: beta, v0, den, pres, B0, igamma1
    REAL(WRD) :: pos(2)
    INTEGER(WIS) :: i, j

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    beta = 0.1_WRD
    v0  = 0.1_WRD

    den = 1.0_WRD
    B0  = 1.0_WRD
    pres   = 0.5_WRD * beta * B0**2

    DO j = 1-workp%nb, workp%ny+workp%nb
        DO i = 1-workp%nb, workp%nx+workp%nb

            pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
            pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx

            workp%grid2d(i,j,1) = den

            workp%grid2d(i,j,2) = den * v0 * SIN(twopi * pos(2))
            workp%grid2d(i,j,3) = 0.0_WRD
            workp%grid2d(i,j,4) = 0.0_WRD

            workp%grid2d(i,j,7) = 0.0_WRD
            workp%bface2d(i,j,1) = 0.0_WRD

            IF (ABS(pos(1)) .GT. 0.25_WRD) THEN
                workp%bface2d(i,j,2) = -B0
            ELSE
                workp%bface2d(i,j,2) = B0
            END IF

        END DO
    END DO
    DO j = 1-workp%nb+1, workp%ny+workp%nb
        DO i = 1-workp%nb+1, workp%nx+workp%nb
            workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
            workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))            

            workp%grid2d(i,j,8) = pres * igamma1 + &
                (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                 0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)

        END DO
    END DO


END SUBROUTINE test_current_sheet
!### Domain initialized with state q0 = qbar + A * R_k * cos(2*pi*x); qbar is avg. background state,
!###   A is ampl. of wave, R_k is right eigenvector for mode (see Stone et al. 2008 appendix)
SUBROUTINE init_linear_wave(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD), DIMENSION(3,1-workp%nb-1:workp%nx+workp%nb+1,1-workp%nb-1:workp%ny+workp%nb+1,1-workp%nb-1:workp%nz+workp%nb+1) :: Apot
    REAL(WRD), PARAMETER :: twopi = 4.0_WRD * ASIN(1.0_WRD)
    REAL(WRD) :: pressure, mom(3), A(3), b(3), bf1(3), bf2(3), bf3(3), pos(3), posx(3), posf(3,3)
    REAL(WRD) :: Amp, igamma1, i2pi, idx
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i, j, k

    Amp = 1.0E-6_WRD
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    i2pi = 1.0_WRD / twopi
    idx = 1.0_WRD / workp%dx

    IF(workp%is1d) THEN

       DO i = 1, workp%nx

          pos(1) = workp%x0 + REAL(i-1,WRD)*workp%dx

          workp%grid1d(i,1) = self%left(1, tid) + Amp*self%right(1, tid)*COS(twopi*pos(1))
          workp%grid1d(i,2) = self%left(1, tid)*self%left(2, tid) + Amp*self%right(2, tid)*COS(twopi*pos(1))
          workp%grid1d(i,3) = self%left(1, tid)*self%left(3, tid) + Amp*self%right(3, tid)*COS(twopi*pos(1))
          workp%grid1d(i,4) = self%left(1, tid)*self%left(4, tid) + Amp*self%right(4, tid)*COS(twopi*pos(1))
          workp%grid1d(i,5) = self%left(5, tid) + Amp*self%right(5, tid)*COS(twopi*pos(1))
          workp%grid1d(i,6) = self%left(6, tid) + Amp*self%right(6, tid)*COS(twopi*pos(1))
          workp%grid1d(i,7) = self%left(7, tid) + Amp*self%right(7, tid)*COS(twopi*pos(1))
          workp%grid1d(i,8) = self%left(8, tid)*igamma1 &
!            + 0.5_WRD * (workp%grid1d(i,2)**2 + workp%grid1d(i,3)**2 + workp%grid1d(i,4)**2)/workp%grid1d(i,1) &
!            + 0.5_WRD * (workp%grid1d(i,5)**2 + workp%grid1d(i,6)**2 + workp%grid1d(i,7)**2) &
            + 0.5_WRD * (self%left(2, tid)**2 + self%left(3, tid)**2 + self%left(4, tid)**2)/self%left(1, tid) &
            + 0.5_WRD * (self%left(5, tid)**2 + self%left(6, tid)**2 + self%left(7, tid)**2) &
            + Amp*self%right(8,tid)*COS(twopi*pos(1))

       END DO

    ELSE IF(workp%is2d) THEN

       !### vary though the grid along X only ###
       DO j = 1-workp%nb, workp%ny+workp%nb
           DO i = 1-workp%nb, workp%nx+workp%nb

               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
               posx(1:2) = pos(1:2) + 0.5_WRD*workp%dx

               CALL self%rotate_2d(posx(1:2),.TRUE.)

               Apot(3,i,j,1) = -Amp*i2pi*self%right(6,tid)*SIN(twopi*posx(1)) - self%left(6,tid)*posx(1) + self%left(5,tid)*posx(2)

           END DO
       END DO

       DO j = 1-workp%nb, workp%ny+workp%nb
           DO i = 1-workp%nb, workp%nx+workp%nb

               workp%bface2d(i,j,1) = idx * (Apot(3,i,j,1) - Apot(3,i,j-1,1))
               workp%bface2d(i,j,2) =-idx * (Apot(3,i,j,1) - Apot(3,i-1,j,1))

           END DO
       END DO

       DO j = 1-workp%nb, workp%ny+workp%nb
           DO i = 1-workp%nb, workp%nx+workp%nb

               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx

               CALL self%rotate_2d(pos(1:2),.TRUE.)

               mom   = (/ self%left(1, tid)*self%left(2, tid) + Amp*self%right(2, tid)*COS(twopi*pos(1)),      &
                          self%left(1, tid)*self%left(3, tid) + Amp*self%right(3, tid)*COS(twopi*pos(1)),      &
                          self%left(1, tid)*self%left(4, tid) + Amp*self%right(4, tid)*COS(twopi*pos(1)) /)

               b(3) =  self%left(7,tid) + Amp*self%right(7,tid)*COS(twopi*pos(1))

               CALL self%rotate_2d(mom(1:2),  .FALSE.)

               workp%grid2d(i,j,1) = self%left(1, tid) + Amp*self%right(1, tid)*COS(twopi*pos(1))

               workp%grid2d(i,j,2) = mom(1)
               workp%grid2d(i,j,3) = mom(2)
               workp%grid2d(i,j,4) = mom(3)

               workp%grid2d(i,j,7) = b(3)

           END DO
       END DO

       DO j = 1, workp%ny
           DO i = 1, workp%nx

               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
               CALL self%rotate_2d(pos(1:2),.TRUE.)

               workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
               workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))            

               workp%grid2d(i,j,8) = self%left(8,tid) * igamma1 + &
                 (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                  0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2) + &
                  Amp*self%right(8, tid)*COS(twopi*pos(1))

        END DO
    END DO




    ELSE IF(workp%is3d) THEN

       DO k = 1-workp%nb-1, workp%nz+workp%nb
          DO j = 1-workp%nb-1, workp%ny+workp%nb
             DO i = 1-workp%nb-1, workp%nx+workp%nb

                  posx(1)    = workp%x0 + (REAL((i-1),WRD) + 0.5_WRD)*workp%dx
                  posx(2)    = workp%y0 + (REAL((j-1),WRD) + 0.5_WRD)*workp%dx
                  posx(3)    = workp%z0 + (REAL((k-1),WRD) + 0.5_WRD)*workp%dx
                  CALL self%rotate_3d(posx,.TRUE.)

                  A(1) =  0.0_WRD
                  A(2) =  Amp*i2pi*self%right(7,tid)*SIN(twopi*posx(1)) + self%left(7,tid)*posx(1)
                  A(3) = -Amp*i2pi*self%right(6,tid)*SIN(twopi*posx(1)) - self%left(6,tid)*posx(1) + self%left(5,tid)*posx(2)

                  CALL self%rotate_3d(A, .FALSE.)

                  Apot(:,i,j,k) = A(:)                  

             END DO
          END DO
       END DO

       DO k = 1-workp%nb, workp%nz+workp%nb
          DO j = 1-workp%nb, workp%ny+workp%nb
             DO i = 1-workp%nb, workp%nx+workp%nb

                workp%bface3d(i,j,k,1) = idx * (Apot(3,i,j,k) - Apot(3,i,j-1,k)) - idx * (Apot(2,i,j,k) - Apot(2,i,j,k-1))
                workp%bface3d(i,j,k,2) = idx * (Apot(1,i,j,k) - Apot(1,i,j,k-1)) - idx * (Apot(3,i,j,k) - Apot(3,i-1,j,k))
                workp%bface3d(i,j,k,3) = idx * (Apot(2,i,j,k) - Apot(2,i-1,j,k)) - idx * (Apot(1,i,j,k) - Apot(1,i,j-1,k))

             END DO
          END DO
       END DO

       DO k = 1-workp%nb, workp%nz+workp%nb
          DO j = 1-workp%nb, workp%ny+workp%nb
             DO i = 1-workp%nb, workp%nx+workp%nb

                pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
                pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
                pos(3) = workp%z0 + REAL((k-1),WRD)*workp%dx

                CALL self%rotate_3d(pos,.TRUE.)


                mom   = (/ self%left(1, tid)*self%left(2, tid) + Amp*self%right(2, tid)*COS(twopi*pos(1)),      &
                           self%left(1, tid)*self%left(3, tid) + Amp*self%right(3, tid)*COS(twopi*pos(1)),      &
                           self%left(1, tid)*self%left(4, tid) + Amp*self%right(4, tid)*COS(twopi*pos(1)) /)

                CALL self%rotate_3d(mom,.FALSE.)

                workp%grid3d(i,j,k,1) = self%left(1, tid) + Amp*self%right(1, tid)*COS(twopi*pos(1))
                workp%grid3d(i,j,k,2) = mom(1)
                workp%grid3d(i,j,k,3) = mom(2)
                workp%grid3d(i,j,k,4) = mom(3)

             END DO
          END DO
       END DO

       DO k = 1-workp%nb, workp%nz+workp%nb
          DO j = 1-workp%nb, workp%ny+workp%nb
             DO i = 1-workp%nb, workp%nx+workp%nb

                pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
                pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
                pos(3) = workp%z0 + REAL((k-1),WRD)*workp%dx

                CALL self%rotate_3d(pos,.TRUE.)

                workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i-1,j,k,1) + workp%bface3d(i,j,k,1))
                workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j-1,k,2) + workp%bface3d(i,j,k,2))
                workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k-1,3) + workp%bface3d(i,j,k,3))

                workp%grid3d(i,j,k,8) = self%left(8,tid) * igamma1 + &
!                + 0.5_WRD * (workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2)/workp%grid3d(i,j,k,1) &
!                + 0.5_WRD * (workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2) &
                0.5_WRD * (self%left(2, tid)**2 + self%left(3, tid)**2 + self%left(4, tid)**2)/self%left(1, tid) &
                + 0.5_WRD * (self%left(5, tid)**2 + self%left(6, tid)**2 + self%left(7, tid)**2) &
                + Amp*self%right(8,tid)*COS(twopi*pos(1))

             END DO
          END DO
       END DO

!       DO k = 1-workp%nb, workp%nz+workp%nb
!          DO j = 1-workp%nb, workp%ny+workp%nb
!               DO i = 1-workp%nb, workp%nx+workp%nb
!
!                  pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
!                  pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
!                  pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx
!                  posf(1,1) = pos(1) + 0.5_WRD*workp%dx
!                  posf(2,1) = pos(2)
!                  posf(3,1) = pos(3)
!                  posf(1,2) = pos(1)
!                  posf(2,2) = pos(2) + 0.5_WRD*workp%dx
!                  posf(3,2) = pos(3)
!                  posf(1,3) = pos(1)
!                  posf(2,3) = pos(2)
!                  posf(3,3) = pos(3) + 0.5_WRD*workp%dx
!                  CALL self%rotate_3d(pos,.TRUE.)
!                  CALL self%rotate_3d(posf(:,1),.TRUE.)
!                  CALL self%rotate_3d(posf(:,2),.TRUE.)
!                  CALL self%rotate_3d(posf(:,3),.TRUE.)
!
!                  mom   = (/ self%left(1, tid)*self%left(2, tid) + Amp*self%right(2, tid)*COS(twopi*pos(1)),      &
!                             self%left(1, tid)*self%left(3, tid) + Amp*self%right(3, tid)*COS(twopi*pos(1)),      &
!                             self%left(1, tid)*self%left(4, tid) + Amp*self%right(4, tid)*COS(twopi*pos(1)) /)
!                  bf1   = (/ self%left(5, tid)                   + Amp*self%right(5, tid)*COS(twopi*posf(1,1)),   &
!                             self%left(6, tid)                   + Amp*self%right(6, tid)*COS(twopi*posf(1,1)),   &
!                             self%left(7, tid)                   + Amp*self%right(7, tid)*COS(twopi*posf(1,1)) /)
!                  bf2   = (/ self%left(5, tid)                   + Amp*self%right(5, tid)*COS(twopi*posf(1,2)),   &
!                             self%left(6, tid)                   + Amp*self%right(6, tid)*COS(twopi*posf(1,2)),   &
!                             self%left(7, tid)                   + Amp*self%right(7, tid)*COS(twopi*posf(1,2)) /)
!                  bf3   = (/ self%left(5, tid)                   + Amp*self%right(5, tid)*COS(twopi*posf(1,3)),   &
!                             self%left(6, tid)                   + Amp*self%right(6, tid)*COS(twopi*posf(1,3)),   &
!                             self%left(7, tid)                   + Amp*self%right(7, tid)*COS(twopi*posf(1,3)) /)
!
!
!                  CALL self%rotate_3d(mom,  .FALSE.)
!                  CALL self%rotate_3d(bf1,  .FALSE.)
!                  CALL self%rotate_3d(bf2,  .FALSE.)
!                  CALL self%rotate_3d(bf3,  .FALSE.)
!
!                  workp%grid3d(i,j,k,1) = self%left(1, tid) + Amp*self%right(1, tid)*COS(twopi*pos(1))
!                  workp%grid3d(i,j,k,2) = mom(1)
!                  workp%grid3d(i,j,k,3) = mom(2)
!                  workp%grid3d(i,j,k,4) = mom(3)
!                  workp%grid3d(i,j,k,8) = self%left(8, tid)*igamma1 &
!                    + 0.5_WRD * (self%left(2, tid)**2 + self%left(3, tid)**2 + self%left(4, tid)**2)/self%left(1, tid) &
!                    + 0.5_WRD * (self%left(5, tid)**2 + self%left(6, tid)**2 + self%left(7, tid)**2) &
!!                    + 0.5_WRD * (workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2)/workp%grid3d(i,j,k,1) &
!!                    + 0.5_WRD * (workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2) &
!                    + Amp*self%right(8, tid)*COS(twopi*pos(1))
!
!                  workp%bface3d(i,j,k,1) = bf1(1)
!                  workp%bface3d(i,j,k,2) = bf2(2)
!                  workp%bface3d(i,j,k,3) = bf3(3)
!
!             END DO
!          END DO
!       END DO
!
!       DO k = 1, workp%nz
!   
!          DO j = 1, workp%ny
!   
!             DO i = 1, workp%nx
!
!                workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i-1,j,k,1) + workp%bface3d(i,j,k,1))
!                workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j-1,k,2) + workp%bface3d(i,j,k,2))
!                workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k-1,3) + workp%bface3d(i,j,k,3))
!
!             END DO
!          END DO
!       END DO

    END IF

    RETURN

END SUBROUTINE init_linear_wave


SUBROUTINE init_tube_states(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pressure, vl(3), vr(3), bl(3), br(3), pos(3), posf(3,3)
    REAL(WRD) :: igamma1
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i, j, k

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

    IF(workp%is1d) THEN

       !### loop through the grid along X only ###
       DO i = 1, workp%nx
   
           !### left hand states (along x) ###
           IF ((workp%x0 + (i-1)*workp%dx) .LE. 1.0E-14_WRD) THEN
   
               workp%grid1d(i,1) = self%left(1, tid)
               workp%grid1d(i,2) = workp%grid1d(i,1) * self%left(2, tid)
               workp%grid1d(i,3) = workp%grid1d(i,1) * self%left(3, tid)
               workp%grid1d(i,4) = workp%grid1d(i,1) * self%left(4, tid)
               workp%grid1d(i,5) = self%left(5, tid)
               workp%grid1d(i,6) = self%left(6, tid)
               workp%grid1d(i,7) = self%left(7, tid)
               pressure          = self%left(8, tid)
   
           !### right hand states (along x) ###
           ELSE
   
               workp%grid1d(i,1) = self%right(1, tid)
               workp%grid1d(i,2) = workp%grid1d(i,1) * self%right(2, tid)
               workp%grid1d(i,3) = workp%grid1d(i,1) * self%right(3, tid)
               workp%grid1d(i,4) = workp%grid1d(i,1) * self%right(4, tid)
               workp%grid1d(i,5) = self%right(5, tid)
               workp%grid1d(i,6) = self%right(6, tid)
               workp%grid1d(i,7) = self%right(7, tid)
               pressure          = self%right(8, tid)
   
           END IF
   
           !### now set energy ###
           workp%grid1d(i,8) = pressure * igamma1 + (0.5_WRD / workp%grid1d(i,1)) * (workp%grid1d(i,2)**2 + workp%grid1d(i,3)**2 + workp%grid1d(i,4)**2) + &
                               0.5_WRD * (workp%grid1d(i,5)**2 + workp%grid1d(i,6)**2 + workp%grid1d(i,7)**2)
   
       END DO


    ELSE IF(workp%is2d) THEN


       !### do the rotation ###
       vl = (/ self%left(2, tid) , self%left(3, tid), 0.0_WRD  /)
       bl = (/ self%left(5, tid) , self%left(6, tid), 0.0_WRD  /)
       vr = (/ self%right(2, tid), self%right(3, tid), 0.0_WRD /)
       br = (/ self%right(5, tid), self%right(6, tid), 0.0_WRD /)       

       CALL self%rotate_2d(vl(1:2),.FALSE.)
       CALL self%rotate_2d(bl(1:2),.FALSE.)
       CALL self%rotate_2d(vr(1:2),.FALSE.)
       CALL self%rotate_2d(br(1:2),.FALSE.)

       !### vary though the grid along X only ###
       DO j = 1-workp%nb, workp%ny+workp%nb
           DO i = 1-workp%nb, workp%nx+workp%nb

               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
               posf(1,1) = pos(1) + 0.5_WRD*workp%dx
               posf(2,1) = pos(2)
               posf(1,2) = pos(1)
               posf(2,2) = pos(2) + 0.5_WRD*workp%dx
               CALL self%rotate_2d(pos,.TRUE.)
               CALL self%rotate_2d(posf(:,1),.TRUE.)
               CALL self%rotate_2d(posf(:,2),.TRUE.)

               !### left hand states (along x) ###
               IF (pos(1) .LE. 1.0E-14_WRD) THEN

                   workp%grid2d(i,j,1)  = self%left(1, tid)
                   workp%grid2d(i,j,2)  = workp%grid2d(i,j,1) * vl(1)
                   workp%grid2d(i,j,3)  = workp%grid2d(i,j,1) * vl(2)
                   workp%grid2d(i,j,4)  = workp%grid2d(i,j,1) * self%left(4, tid)
                   workp%grid2d(i,j,7)  = self%left(7, tid)

               !### right hand states (along x) ###
               ELSE

                   workp%grid2d(i,j,1)  = self%right(2, tid)
                   workp%grid2d(i,j,2)  = workp%grid2d(i,j,1) * vr(1)
                   workp%grid2d(i,j,3)  = workp%grid2d(i,j,1) * vr(2)
                   workp%grid2d(i,j,4)  = workp%grid2d(i,j,1) * self%right(4, tid)
                   workp%grid2d(i,j,7)  = self%right(7, tid)

               END IF

               IF(posf(1,1) .LE. 1.0E-14_WRD) THEN
                   workp%bface2d(i,j,1) = bl(1)
               ELSE
                   workp%bface2d(i,j,1) = br(1)
               END IF

               IF(posf(1,2) .LE. 1.0E-14_WRD) THEN
                   workp%bface2d(i,j,2) = bl(2)
               ELSE
                   workp%bface2d(i,j,2) = br(2)
               END IF
   
           END DO
       END DO
   
       !### loop back through and set Bx/y and energy ###
       DO j = 1, workp%ny
           DO i = 1, workp%nx
   
               pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
               CALL self%rotate_2d(pos,.TRUE.)

               IF (pos(1) .LE. 1.0E-14_WRD) THEN
                   pressure = self%left(8, tid)
               ELSE
                   pressure = self%right(8, tid)
               END IF
               workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
               workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))
               workp%grid2d(i,j,8) = pressure * igamma1 + (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                    0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)
   
           END DO
       END DO

    ELSE IF(workp%is3d) THEN

       !### do the rotation ###
       vl = (/ self%left(2, tid) , self%left(3, tid) , self%left(4, tid)  /)
       bl = (/ self%left(5, tid) , self%left(6, tid) , self%left(7, tid)  /)
       vr = (/ self%right(2, tid), self%right(3, tid), self%right(4, tid) /)
       br = (/ self%right(5, tid), self%right(6, tid), self%right(7, tid) /)

       CALL self%rotate_3d(vl,.FALSE.)
       CALL self%rotate_3d(bl,.FALSE.)
       CALL self%rotate_3d(vr,.FALSE.)
       CALL self%rotate_3d(br,.FALSE.)     

       DO k = 1-workp%nb, workp%nz+workp%nb
           DO j = 1-workp%nb, workp%ny+workp%nb
               DO i = 1-workp%nb, workp%nx+workp%nb

                  pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
                  pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
                  pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx
                  posf(1,1) = pos(1) + 0.5_WRD*workp%dx
                  posf(2,1) = pos(2)
                  posf(3,1) = pos(3)
                  posf(1,2) = pos(1)
                  posf(2,2) = pos(2) + 0.5_WRD*workp%dx
                  posf(3,2) = pos(3)
                  posf(1,3) = pos(1)
                  posf(2,3) = pos(2)
                  posf(3,3) = pos(3) + 0.5_WRD*workp%dx
                  CALL self%rotate_3d(pos,.TRUE.)
                  CALL self%rotate_3d(posf(:,1),.TRUE.)
                  CALL self%rotate_3d(posf(:,2),.TRUE.)
                  CALL self%rotate_3d(posf(:,3),.TRUE.)

                  !### left hand states (along x) ###
                  IF (pos(1) .LE. 1.0E-14_WRD) THEN

                      workp%grid3d(i,j,k,1)  = self%left(1, tid)
                      workp%grid3d(i,j,k,2)  = workp%grid3d(i,j,k,1) * vl(1)
                      workp%grid3d(i,j,k,3)  = workp%grid3d(i,j,k,1) * vl(2)
                      workp%grid3d(i,j,k,4)  = workp%grid3d(i,j,k,1) * vl(3)
                    
                  !### right hand states (along x) ###
                  ELSE
                      
                      workp%grid3d(i,j,k,1)  = self%right(1, tid)
                      workp%grid3d(i,j,k,2)  = workp%grid3d(i,j,k,1) * vr(1)
                      workp%grid3d(i,j,k,3)  = workp%grid3d(i,j,k,1) * vr(2)
                      workp%grid3d(i,j,k,4)  = workp%grid3d(i,j,k,1) * vr(3)

                  END IF


                  IF(posf(1,1) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,1) = bl(1)
                  ELSE
                      workp%bface3d(i,j,k,1) = br(1)
                  END IF
   
                  IF(posf(1,2) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,2) = bl(2)
                  ELSE
                      workp%bface3d(i,j,k,2) = br(2)
                  END IF

                  IF(posf(1,3) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,3) = bl(3)
                  ELSE
                      workp%bface3d(i,j,k,3) = br(3)
                  END IF

               END DO
           END DO
       END DO

       !### loop back through and set grid Bs and energy ###
       DO k = 1, workp%nz
           DO j = 1, workp%ny
               DO i = 1, workp%nx
   
                  pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
                  pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
                  pos(3) = workp%z0 + REAL((k-1),WRD)*workp%dx
                  CALL self%rotate_3d(pos,.TRUE.)

                  IF (pos(1) .LE. 1.0E-14_WRD) THEN
                      pressure = self%left(8, tid)
                  ELSE
                      pressure = self%right(8, tid)
                  END IF
                  workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i-1,j,k,1) + workp%bface3d(i,j,k,1))
                  workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j-1,k,2) + workp%bface3d(i,j,k,2))
                  workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k-1,3) + workp%bface3d(i,j,k,3))
                  workp%grid3d(i,j,k,8) = pressure * igamma1 + (0.5_WRD / workp%grid3d(i,j,k,1)) * (workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2) + &
                       0.5_WRD * (workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2)
   
               END DO
           END DO
       END DO

    END IF

    RETURN

END SUBROUTINE init_tube_states

!### define open boundaries ###
SUBROUTINE shock_tube_bounds1d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i

    IF (lx_bound) THEN

        DO i = 1-workp%nb, 0

            workp%grid1d(i,1) = workp%grid1d(1,1)
            workp%grid1d(i,2) = workp%grid1d(1,2)
            workp%grid1d(i,3) = workp%grid1d(1,3)
            workp%grid1d(i,4) = workp%grid1d(1,4)
            workp%grid1d(i,5) = workp%grid1d(1,5)
            workp%grid1d(i,6) = workp%grid1d(1,6)
            workp%grid1d(i,7) = workp%grid1d(1,7)
            workp%grid1d(i,8) = workp%grid1d(1,8)

        END DO

    END IF
    IF (hx_bound) THEN

        DO i = workp%nx+1, workp%nx+workp%nb

            workp%grid1d(i,1) = workp%grid1d(workp%nx,1)
            workp%grid1d(i,2) = workp%grid1d(workp%nx,2)
            workp%grid1d(i,3) = workp%grid1d(workp%nx,3)
            workp%grid1d(i,4) = workp%grid1d(workp%nx,4)
            workp%grid1d(i,5) = workp%grid1d(workp%nx,5)
            workp%grid1d(i,6) = workp%grid1d(workp%nx,6)
            workp%grid1d(i,7) = workp%grid1d(workp%nx,7)
            workp%grid1d(i,8) = workp%grid1d(workp%nx,8)

        END DO

    END IF

END SUBROUTINE shock_tube_bounds1d

!### define open boundaries for 2D ###
SUBROUTINE shock_tube_bounds2d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i, j

    IF (hx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = workp%nx+1, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(workp%nx,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(workp%nx,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(workp%nx,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(workp%nx,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(workp%nx,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(workp%nx,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(workp%nx,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(workp%nx,j,8)
                
            END DO
        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = workp%nx+1, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(workp%nx,j,:)
                
            END DO
        END DO

    END IF
    IF (hy_bound) THEN

        DO j = workp%ny+1, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,workp%ny,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,workp%ny,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,workp%ny,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,workp%ny,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,workp%ny,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,workp%ny,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,workp%ny,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,workp%ny,8)
                
            END DO
        END DO

        DO j = workp%ny+1, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,workp%ny,:)
                
            END DO
        END DO

    END IF
    IF (lx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, 0

                workp%grid2d(i,j,1) = workp%grid2d(1,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(1,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(1,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(1,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(1,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(1,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(1,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(1,j,8)

            END DO
        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, 0

                workp%emf2d(i,j,:) = workp%emf2d(1,j,:)

            END DO
        END DO

    END IF

    IF (ly_bound) THEN

        DO j = 1-workp%nb, 0
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,1,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,1,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,1,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,1,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,1,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,1,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,1,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,1,8)

            END DO
        END DO

        DO j = 1-workp%nb, 0
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,1,:)

            END DO
        END DO

    END IF

END SUBROUTINE shock_tube_bounds2d

!### define open boundaries for 3D ###
SUBROUTINE shock_tube_bounds3d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i, j, k

    IF (hx_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(workp%nx,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(workp%nx,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(workp%nx,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(workp%nx,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(workp%nx,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(workp%nx,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(workp%nx,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(workp%nx,j,k,8)
                    
                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(workp%nx,j,k,:)
                
                END DO
            END DO
        END DO

    END IF
    IF (hy_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = workp%ny+1, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,workp%ny,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,workp%ny,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,workp%ny,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,workp%ny,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,workp%ny,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,workp%ny,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,workp%ny,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,workp%ny,k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = workp%ny+1, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,workp%ny,k,:)
                
                END DO
            END DO
        END DO

    END IF
    IF (hz_bound) THEN

        DO k = workp%nz+1, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,workp%nz,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,workp%nz,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,workp%nz,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,workp%nz,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,workp%nz,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,workp%nz,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,workp%nz,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,workp%nz,8)

                END DO
            END DO
        END DO

        DO k = workp%nz+1, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,workp%nz,:)
                
                END DO
            END DO
        END DO

    END IF
    IF (lx_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, 0

                    workp%grid3d(i,j,k,1) = workp%grid3d(1,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(1,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(1,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(1,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(1,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(1,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(1,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(1,j,k,8)
                    
                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, 0

                    workp%emf3d(i,j,k,:) = workp%emf3d(1,j,k,:)

                END DO
            END DO
        END DO

    END IF

    IF (ly_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, 0
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,1,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,1,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,1,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,1,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,1,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,1,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,1,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,1,k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb
            DO j = 1-workp%nb, 0
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,1,k,:)

                END DO
            END DO
        END DO

    END IF

    IF (lz_bound) THEN

        DO k = 1-workp%nb, 0
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,1,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,1,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,1,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,1,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,1,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,1,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,1,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,1,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, 0
            DO j = 1-workp%nb, workp%ny+workp%nb
                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,1,:)

                END DO
            END DO
        END DO

    END IF

END SUBROUTINE shock_tube_bounds3d


SUBROUTINE reflecting_bounds1d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i

    IF (lx_bound) THEN

        DO i = 1-workp%nb, 0

            workp%grid1d(i,1) = workp%grid1d(1-i,1)
            workp%grid1d(i,2) =-workp%grid1d(1-i,2)
            workp%grid1d(i,3) = workp%grid1d(1-i,3)
            workp%grid1d(i,4) = workp%grid1d(1-i,4)
            workp%grid1d(i,5) = workp%grid1d(1-i,5)
            workp%grid1d(i,6) = workp%grid1d(1-i,6)
            workp%grid1d(i,7) = workp%grid1d(1-i,7)
            workp%grid1d(i,8) = workp%grid1d(1-i,8)

        END DO

    END IF
    IF (hx_bound) THEN

        DO i = workp%nx+1, workp%nx+workp%nb

            workp%grid1d(i,1) = workp%grid1d(2*workp%nx+1-i,1)
            workp%grid1d(i,2) =-workp%grid1d(2*workp%nx+1-i,2)
            workp%grid1d(i,3) = workp%grid1d(2*workp%nx+1-i,3)
            workp%grid1d(i,4) = workp%grid1d(2*workp%nx+1-i,4)
            workp%grid1d(i,5) = workp%grid1d(2*workp%nx+1-i,5)
            workp%grid1d(i,6) = workp%grid1d(2*workp%nx+1-i,6)
            workp%grid1d(i,7) = workp%grid1d(2*workp%nx+1-i,7)
            workp%grid1d(i,8) = workp%grid1d(2*workp%nx+1-i,8)

        END DO

    END IF

END SUBROUTINE reflecting_bounds1d

SUBROUTINE reflecting_bounds2d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i, j

    IF (hx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = workp%nx+1, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(2*workp%nx+1-i,j,1)
                workp%grid2d(i,j,2) =-workp%grid2d(2*workp%nx+1-i,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(2*workp%nx+1-i,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(2*workp%nx+1-i,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(2*workp%nx+1-i,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(2*workp%nx+1-i,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(2*workp%nx+1-i,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(2*workp%nx+1-i,j,8)
                
            END DO
        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = workp%nx+1, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(2*workp%nx+1-i,j,:)
                
            END DO
        END DO

    END IF
    IF (hy_bound) THEN

        DO j = workp%ny+1, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,2*workp%ny+1-j,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,2*workp%ny+1-j,2)
                workp%grid2d(i,j,3) =-workp%grid2d(i,2*workp%ny+1-j,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,2*workp%ny+1-j,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,2*workp%ny+1-j,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,2*workp%ny+1-j,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,2*workp%ny+1-j,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,2*workp%ny+1-j,8)
                
            END DO
        END DO

        DO j = workp%ny+1, workp%ny+workp%nb
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,2*workp%ny+1-j,:)
                
            END DO
        END DO

    END IF
    IF (lx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, 0

                workp%grid2d(i,j,1) = workp%grid2d(1-i,j,1)
                workp%grid2d(i,j,2) =-workp%grid2d(1-i,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(1-i,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(1-i,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(1-i,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(1-i,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(1-i,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(1-i,j,8)

            END DO
        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb
            DO i = 1-workp%nb, 0

                workp%emf2d(i,j,:) = workp%emf2d(1-i,j,:)

            END DO
        END DO

    END IF

    IF (ly_bound) THEN

        DO j = 1-workp%nb, 0
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,1-j,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,1-j,2)
                workp%grid2d(i,j,3) =-workp%grid2d(i,1-j,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,1-j,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,1-j,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,1-j,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,1-j,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,1-j,8)

            END DO
        END DO

        DO j = 1-workp%nb, 0
            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,1-j,:)

            END DO
        END DO

    END IF

END SUBROUTINE reflecting_bounds2d

SUBROUTINE reflecting_bounds3d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i, j, k

    IF (hx_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = workp%nx+1, workp%nx + workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(2*workp%nx+1-i,j,k,1)
                    workp%grid3d(i,j,k,2) =-workp%grid3d(2*workp%nx+1-i,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(2*workp%nx+1-i,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(2*workp%nx+1-i,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(2*workp%nx+1-i,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(2*workp%nx+1-i,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(2*workp%nx+1-i,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(2*workp%nx+1-i,j,k,8)

                END DO
            END DO
        END DO


        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = workp%nx+1, workp%nx + workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(2*workp%nx+1-i,j,k,:)

                END DO
            END DO
        END DO


    END IF

    IF (hy_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = workp%ny+1, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,2*workp%ny+1-j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,2*workp%ny+1-j,k,2)
                    workp%grid3d(i,j,k,3) =-workp%grid3d(i,2*workp%ny+1-j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,2*workp%ny+1-j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,2*workp%ny+1-j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,2*workp%ny+1-j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,2*workp%ny+1-j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,2*workp%ny+1-j,k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = workp%ny+1, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,2*workp%ny+1-j,k,:)

                END DO
            END DO
        END DO

    END IF

    IF (hz_bound) THEN

        DO k = workp%nz+1, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,2*workp%nz+1-k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,2*workp%nz+1-k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,2*workp%nz+1-k,3)
                    workp%grid3d(i,j,k,4) =-workp%grid3d(i,j,2*workp%nz+1-k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,2*workp%nz+1-k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,2*workp%nz+1-k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,2*workp%nz+1-k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,2*workp%nz+1-k,8)

                END DO
            END DO
        END DO

        DO k = workp%nz+1, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,2*workp%nz+1-k,:)

                END DO
            END DO
        END DO

    END IF

    IF (lx_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, 0

                    workp%grid3d(i,j,k,1) = workp%grid3d(1-i,j,k,1)
                    workp%grid3d(i,j,k,2) =-workp%grid3d(1-i,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(1-i,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(1-i,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(1-i,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(1-i,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(1-i,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(1-i,j,k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, 0

                    workp%emf3d(i,j,k,:) = workp%emf3d(1-i,j,k,:)

                END DO
            END DO
        END DO

    END IF

    IF (ly_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, 0
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,1-j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,1-j,k,2)
                    workp%grid3d(i,j,k,3) =-workp%grid3d(i,1-j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,1-j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,1-j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,1-j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,1-j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,1-j,k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, 0
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,1-j,k,:)

                END DO
            END DO
        END DO

    END IF

    IF (lz_bound) THEN

        DO k = 1-workp%nb, 0
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,1-k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,1-k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,1-k,3)
                    workp%grid3d(i,j,k,4) =-workp%grid3d(i,j,1-k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,1-k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,1-k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,1-k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,1-k,8)

                END DO
            END DO
        END DO

        DO k = 1-workp%nb, 0
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,1-k,:)

                END DO
            END DO
        END DO

    END IF


END SUBROUTINE reflecting_bounds3d

!### For the Noh problem, we simulate an octant of a strong, spherically symmetric shock wave. This routine
!###    handles the time-dependant outer boundaries
SUBROUTINE upperNoh_bounds(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    REAL(WRD) :: igamma1, twogam1, vel(3), pos(3), trans(3), rmag, rho, pres, P0
    INTEGER(WIS) :: i, j, k


    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    twogam1 = (2*(self%gamma+1.0_WRD))
    P0 = 1.0E-6_WRD
    trans = (/ 0.5_WRD, 0.5_WRD, 0.5_WRD /)

    IF (hx_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = workp%nx+1, workp%nx + workp%nb


                    pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + trans(1)
                    pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + trans(2)
                    pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx + trans(3)

                    rmag = SQRT(SUM(pos**2))
 
                    !### vel magnitude = 1, points toward origin
                    vel = -pos / rmag
                    !### density and pressure on the boundaries vary with time
                    rho = (1.0_WRD + workp%t/rmag)**2
                    pres = P0*(1.0_WRD + workp%t/rmag)**twogam1
                    
                    workp%grid3d(i,j,k,1) = rho
                    workp%grid3d(i,j,k,2) = rho*vel(1)
                    workp%grid3d(i,j,k,3) = rho*vel(2)
                    workp%grid3d(i,j,k,4) = rho*vel(3)
                    workp%grid3d(i,j,k,5) = 0.0_WRD
                    workp%grid3d(i,j,k,6) = 0.0_WRD
                    workp%grid3d(i,j,k,7) = 0.0_WRD
                    workp%grid3d(i,j,k,8) = pres*igamma1


                END DO
            END DO
        END DO


    END IF

    IF (hy_bound) THEN

        DO k = 1-workp%nb, workp%nz + workp%nb
            DO j = workp%ny+1, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + trans(1)
                    pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + trans(2)
                    pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx + trans(3)

                    rmag = SQRT(SUM(pos**2))
 
                    !### vel magnitude = 1, points toward origin
                    vel = -pos / rmag
                    !### density and pressure on the boundaries vary with time
                    rho = (1.0_WRD + workp%t/rmag)**2
                    pres = P0*(1.0_WRD + workp%t/rmag)**twogam1
                    
                    workp%grid3d(i,j,k,1) = rho
                    workp%grid3d(i,j,k,2) = rho*vel(1)
                    workp%grid3d(i,j,k,3) = rho*vel(2)
                    workp%grid3d(i,j,k,4) = rho*vel(3)
                    workp%grid3d(i,j,k,5) = 0.0_WRD
                    workp%grid3d(i,j,k,6) = 0.0_WRD
                    workp%grid3d(i,j,k,7) = 0.0_WRD
                    workp%grid3d(i,j,k,8) = pres*igamma1

                END DO
            END DO
        END DO

    END IF

    IF (hz_bound) THEN

        DO k = workp%nz+1, workp%nz + workp%nb
            DO j = 1-workp%nb, workp%ny + workp%nb
                DO i = 1-workp%nb, workp%nx + workp%nb

                    pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + trans(1)
                    pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx + trans(2)
                    pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx + trans(3)

                    rmag = SQRT(SUM(pos**2))
 
                    !### vel magnitude = 1, points toward origin
                    vel = -pos / rmag
                    !### density and pressure on the boundaries vary with time
                    rho = (1.0_WRD + workp%t/rmag)**2
                    pres = P0*(1.0_WRD + workp%t/rmag)**twogam1
                    
                    workp%grid3d(i,j,k,1) = rho
                    workp%grid3d(i,j,k,2) = rho*vel(1)
                    workp%grid3d(i,j,k,3) = rho*vel(2)
                    workp%grid3d(i,j,k,4) = rho*vel(3)
                    workp%grid3d(i,j,k,5) = 0.0_WRD
                    workp%grid3d(i,j,k,6) = 0.0_WRD
                    workp%grid3d(i,j,k,7) = 0.0_WRD
                    workp%grid3d(i,j,k,8) = pres*igamma1

                END DO
            END DO
        END DO

    END IF

END SUBROUTINE upperNoh_bounds

!### Special bounds for oblique shock front, time dependent upper boundary, fixed bounds on left and right,
!###  reflecting on bottom
SUBROUTINE dblmachrefl_bounds(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    REAL(WRD) :: pos(2), transvec(2), xshock
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS) :: i, j


    !### total domain in W & C is 3.25 x 1, translation vector
    transvec = (/ 1.625_WRD, 0.5_WRD /)

    !### shock position on upper boundary
    xshock = 1.0_WRD/6.0_WRD + (1.0_WRD + 20.0_WRD*workp%t) / SQRT(3.0_WRD)



    IF (lx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = 1-workp%nb, 0

                workp%grid2d(i,j,1) = self%left(1, tid)
                workp%grid2d(i,j,2) = self%left(2, tid)
                workp%grid2d(i,j,3) = self%left(3, tid)
                workp%grid2d(i,j,4) = 0.0_WRD
                workp%grid2d(i,j,5) = 0.0_WRD
                workp%grid2d(i,j,6) = 0.0_WRD
                workp%grid2d(i,j,7) = 0.0_WRD
                workp%grid2d(i,j,8) = self%left(8, tid)

                workp%emf2d(i,j,:) = 0.0_WRD

            END DO

        END DO


    END IF
    IF (hx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%grid2d(i,j,1) = self%right(1, tid)
                workp%grid2d(i,j,2) = self%right(2, tid)
                workp%grid2d(i,j,3) = self%right(3, tid)
                workp%grid2d(i,j,4) = 0.0_WRD
                workp%grid2d(i,j,5) = 0.0_WRD
                workp%grid2d(i,j,6) = 0.0_WRD
                workp%grid2d(i,j,7) = 0.0_WRD
                workp%grid2d(i,j,8) = self%right(8, tid)
                
                workp%emf2d(i,j,:) = 0.0_WRD
            END DO

        END DO

    END IF

    IF (ly_bound) THEN

        DO j = 1-workp%nb, 0

            DO i = 1-workp%nb, workp%nx+workp%nb

               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + transvec(1)

               IF( pos(1) .LT. 1.0_WRD/6.0_WRD ) THEN

                  workp%grid2d(i,j,1) = self%left(1, tid)
                  workp%grid2d(i,j,2) = self%left(2, tid)
                  workp%grid2d(i,j,3) = self%left(3, tid)
                  workp%grid2d(i,j,8) = self%left(8, tid)

               ELSE !### reflected otherwise ###

                  workp%grid2d(i,j,1) = workp%grid2d(i,1-j,1)
                  workp%grid2d(i,j,2) = workp%grid2d(i,1-j,2)
                  workp%grid2d(i,j,3) =-workp%grid2d(i,1-j,3)
                  workp%grid2d(i,j,8) = workp%grid2d(i,1-j,8)

               END IF

               workp%grid2d(i,j,4) = 0.0_WRD
               workp%grid2d(i,j,5) = 0.0_WRD
               workp%grid2d(i,j,6) = 0.0_WRD
               workp%grid2d(i,j,7) = 0.0_WRD

               workp%emf2d(i,j,:) = 0.0_WRD

            END DO

        END DO



    END IF

    IF (hy_bound) THEN !### upper bound is time dependent ###

        DO j = workp%ny+1, workp%ny+workp%nb

            DO i = 1-workp%nb, workp%nx+workp%nb
                
               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx + transvec(1)

               IF( pos(1) .LT. xshock ) THEN

                  workp%grid2d(i,j,1) = self%left(1, tid)
                  workp%grid2d(i,j,2) = self%left(2, tid)
                  workp%grid2d(i,j,3) = self%left(3, tid)
                  workp%grid2d(i,j,8) = self%left(8, tid)

               ELSE

                  workp%grid2d(i,j,1) = self%right(1, tid)
                  workp%grid2d(i,j,2) = self%right(2, tid)
                  workp%grid2d(i,j,3) = self%right(3, tid)
                  workp%grid2d(i,j,8) = self%right(8, tid)

               END IF

               workp%grid2d(i,j,4) = 0.0_WRD
               workp%grid2d(i,j,5) = 0.0_WRD
               workp%grid2d(i,j,6) = 0.0_WRD
               workp%grid2d(i,j,7) = 0.0_WRD

               workp%emf2d(i,j,:) = 0.0_WRD

            END DO

        END DO


    END IF

END SUBROUTINE dblmachrefl_bounds

!SUBROUTINE CalcL1error(self, workp, decomp)
!
!    CLASS(Problem) :: self
!    TYPE(Patch) :: workp
!    TYPE(Decomposition) :: decomp
!    REAL(WRD), DIMENSION(self%nstate) :: dq
!    REAL(WRD) :: L1norm
!    INTEGER(WIS), DIMENSION(self%nstate) :: dqid
!    INTEGER(WIS) :: nzones
!    INTEGER(WIS) :: i, j, k, n
!
!    dq = 0.0_WRD
!    dqid = 0
!
!    IF (workp%is1d) THEN
!
!        DO n = 1, decomp%nthreads
!            DO i = 1, workp%nx
!
!                dq(:) = dq(:) + ABS(workp%grid1d(i,:) - self%q0_1d(i,:))
!
!            END DO
!
!    ELSE IF (workp%is2d) THEN
!
!        DO j = 1, workp%ny
!            DO i = 1, workp%nx
!
!                dq(:) = dq(:) + ABS(workp%grid2d(i,j,:) - self%q0_2d(i,j,:))
!                
!            END DO
!        END DO
!
!    ELSE IF (workp%is3d) THEN
!
!        DO k = 1, workp%nz
!            DO j = 1, workp%ny
!                DO i = 1, workp%nx
!
!                    dq(:) = dq(:) + ABS(workp%grid3d(i,j,k,:) - self%q0_3d(i,j,k,:))
!
!                END DO
!            END DO
!        END DO
!
!    END IF
!
!    nzones = decomp%nranks_x * decomp%nranks_y * decomp%nranks_z * decomp%nthreads * workp%nx * workp%ny * workp%nz
!
!    DO n = 1, self%nstate
!
!        decomp%start_allreduce_sum(dq(n), self%delq_k(n), dqid(n))
!        decomp%end_allreduce(dqid(n))
!
!    END DO
!
!    self%delq_k = self%delq_k / REAL(nzones,WRD)
!
!    L1norm = SQRT(SUM(self%delq_k**2))
!
!END SUBROUTINE CalcL1error


!### Rotate input vector by theta (or -theta) ###
SUBROUTINE rotate_2d(self, mat, inv)

    CLASS(Problem) :: self
    REAL(WRD), INTENT(INOUT) :: mat(2)
    LOGICAL(WIS), INTENT(IN) :: inv
    REAL(WRD) :: work(2)
    INTEGER(WIS) :: i, j

    !### init work as 0 ###
    work = 0.0_WRD

    !### do the matrix multiplication ###
    IF (.NOT. inv) THEN

        DO i = 1, 2
            DO j = 1, 2
                
                work(i) = work(i) + mat(j) * self%A(i,j)
                
            END DO
        END DO

    ELSE

        DO i = 1, 2
            DO j = 1, 2
                
                work(i) = work(i) + mat(j) * self%Ainv(i,j)
                
            END DO
        END DO
    END IF

    !### update the input array ###
    mat = work

END SUBROUTINE rotate_2d

!### 
SUBROUTINE rotate_3d(self, mat, inv)

    CLASS(Problem) :: self
    REAL(WRD), INTENT(INOUT) :: mat(3)
    LOGICAL(WIS), INTENT(IN) :: inv
    REAL(WRD) :: work(3)
    INTEGER(WIS) :: i, j

    !### init work as 0 ###
    work = 0.0_WRD

    !### do the matrix multiplication ###
    IF (.NOT. inv) THEN

       DO i = 1, 3
          DO j = 1, 3

             work(i) = work(i) + mat(j) * self%AA(i,j)

          END DO
       END DO

    ELSE

       DO i = 1, 3
          DO j = 1, 3

             work(i) = work(i) + mat(j) * self%AAinv(i,j)

          END DO
       END DO
    END IF

    mat = work

END SUBROUTINE rotate_3d

END MODULE Mod_Problem
