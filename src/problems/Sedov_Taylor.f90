#include "../config.h"

MODULE Mod_Problem

!######################################################################
!#
!# FILENAME: mod_SedovTaylor.f90
!#
!# DESCRIPTION: This module provides a class for initializing a domain
!#   and setting boundary values for 1d, 2d, and 3d Sedov-Taylor sim.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    11/5/2015	- Paul Edmon
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch
USE Mod_ProblemBase

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Problem

!### define a data type for this module ###
TYPE :: Problem

    !### default everything as private ###
    PRIVATE

    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD), PUBLIC :: gamma, amplitude, pressure, a, sigma, z1, z2, ux, density_ratio

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: patch_init
    PROCEDURE, PUBLIC :: patch_bounds
    PROCEDURE :: initproblem
    PROCEDURE :: st1d_init
    PROCEDURE :: st_bounds1d
	PROCEDURE :: st2d_init
	PROCEDURE :: st_bounds2d
	PROCEDURE :: st3d_init
	PROCEDURE :: st_bounds3d

END TYPE Problem

!### create an interface to the constructor ###
INTERFACE Problem

    MODULE PROCEDURE constructor

END INTERFACE Problem

!### object methods ###
CONTAINS

!### constructor for the class ###
!###   arguments are: the namelist file contents for grabbing parameters out of ###
FUNCTION constructor(namelist, nthreads, rankloc, simunits)

    TYPE(Problem) :: constructor
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initproblem(namelist, nthreads, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### this function will init the object and configure init and bound routine parameters ###
SUBROUTINE initproblem(self, namelist, nthreads, rankloc, simunits)

    CLASS(Problem) :: self
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD) :: gamma, amplitude, pressure, a, sigma, z1, z2, ux, density_ratio

    NAMELIST /ProblemSetup/ init_routine, bounds_routine, gamma

    !### set the routines we'll call ###
    READ(namelist, NML=ProblemSetup)
    self%init_routine   = init_routine
    self%bounds_routine = bounds_routine

    !### we handle setting the adiabatic index ###
    self%gamma         = gamma

END SUBROUTINE initproblem

SUBROUTINE destroy(self)

    CLASS(Problem) :: self
    
    RETURN

END SUBROUTINE destroy

!### the routine called to initialize every Patch ###
SUBROUTINE patch_init(self, workp, rankloc, simunits, is_boundary, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%init_routine))

        CASE ('ST')

            IF(workp%is1d) THEN

            	CALL self%st1d_init(workp)
            	ierr = 0

			ENDIF

			IF(workp%is2d) THEN

				CALL self%st2d_init(workp)
				ierr = 0

			ENDIF

			IF(workp%is3d) THEN

				CALL self%st3d_init(workp)
				ierr = 0
			ENDIF

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown initialization routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT

END SUBROUTINE patch_init

!### the routine called to set boundary values (called for every Patch even if it is not a world boundary) ###
SUBROUTINE patch_bounds(self, workp, rankloc, simunits, is_boundary, maxsignal, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    LOGICAL(WIS) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    REAL(WRD), INTENT(INOUT) :: maxsignal
    INTEGER(WIS), INTENT(OUT) :: ierr
    INTEGER(WIS), INTENT(IN) :: tid

    maxsignal = MAX(maxsignal, 0.0_WRD)

    lx_bound = is_boundary(1,1)
    hx_bound = is_boundary(2,1)
    ly_bound = is_boundary(1,2)
    hy_bound = is_boundary(2,2)
    lz_bound = is_boundary(1,3)
    hz_bound = is_boundary(2,3)

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%bounds_routine))

       CASE('STBounds')

            IF(workp%is1d) THEN
   
            	CALL self%st_bounds1d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

            END IF

            IF(workp%is2d) THEN
   
            	CALL self%st_bounds2d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

            END IF

			IF (workp%is3d) THEN

            	CALL self%st_bounds3d(workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

			ENDIF

            ierr = 0

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown boundary routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT

END SUBROUTINE patch_bounds

!### Sedov Taylor blast wave 1-d ###
SUBROUTINE st1d_init(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pressure, pi, x
    INTEGER(WIS) :: i, j, k

    pi = 2.0_WRD * ASIN(1.0_WRD)

	do i=1, workp%nx
		x = workp%x0 + REAL((i-1),WRD)*workp%dx

		workp%grid1d(i,1) = 1.0_WRD
		workp%grid1d(i,2) = 0.0_WRD
		workp%grid1d(i,3) = 0.0_WRD
		workp%grid1d(i,4) = 0.0_WRD
		workp%grid1d(i,5) = 1.d-8
		workp%grid1d(i,6) = 0.0_WRD
		workp%grid1d(i,7) = 0.0_WRD

		if (dabs(x) .LE. 1.0_WRD) then
			pressure = 6d5
			if (workp%passOn .AND. workp%npass .GT. 0) then
				workp%pass1d(i,:) = 1.0_WRD
			endif
		else
			pressure = 0.6_WRD
			if (workp%passOn .AND. workp%npass .GT. 0) then
				workp%pass1d(i,:) = 0.0_WRD
			endif
		endif

		workp%grid1d(i,8) = pressure / (self%gamma - 1.0_WRD) + &
						    (0.5_WRD / workp%grid1d(i,1)) * (workp%grid1d(i,2)**2 + workp%grid1d(i,3)**2 + workp%grid1d(i,4)**2) + & 
							0.5_WRD * (workp%grid1d(i,5)**2 + workp%grid1d(i,6)**2 + workp%grid1d(i,7)**2)
	enddo

END SUBROUTINE st1d_init

!### Sedov Taylor blast wave 2-d ###
SUBROUTINE st2d_init(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pressure, pi, xyz(2)
    INTEGER(WIS) :: i, j, k

    pi = 2.0_WRD * ASIN(1.0_WRD)

	do j = 1-workp%nb, workp%ny+workp%nb
		xyz(2) = workp%y0 + REAL((j-1),WRD)*workp%dx

		do i=1-workp%nb, workp%nx+workp%nb
			xyz(1) = workp%x0 + REAL((i-1),WRD)*workp%dx

			workp%grid2d(i,j,1) = 1.0_WRD
			workp%grid2d(i,j,2) = 0.0_WRD
			workp%grid2d(i,j,3) = 0.0_WRD
			workp%grid2d(i,j,4) = 0.0_WRD
			workp%bface2d(i,j,1) = 1.d-8
			workp%bface2d(i,j,2) = 0.0_WRD
			workp%grid2d(i,j,7) = 0.0_WRD

		enddo
	enddo

	do j = 1, workp%ny
		xyz(2) = workp%y0 + REAL((j-1),WRD)*workp%dx

		do i=1, workp%nx
			xyz(1) = workp%x0 + REAL((i-1),WRD)*workp%dx

            workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i,j,1) + workp%bface2d(i-1,j,1))
            workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j,2) + workp%bface2d(i,j-1,2))

			if (dsqrt(SUM(xyz(:)**2)) .LE. 1.0_WRD) then
				pressure = 6d5
				if (workp%passOn .AND. workp%npass .GT. 0) then
					workp%pass2d(i,j,:) = 1.0_WRD
				endif
			else
				pressure = 0.6_WRD
				if (workp%passOn .AND. workp%npass .GT. 0) then
					workp%pass2d(i,j,:) = 0.0_WRD
				endif
			endif

			workp%grid2d(i,j,8) = pressure / (self%gamma - 1.0_WRD) + &
						    	(0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + & 
								0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)
		enddo
	enddo

END SUBROUTINE st2d_init

!### Sedov Taylor blast wave 3-d ###
SUBROUTINE st3d_init(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    REAL(WRD) :: pressure, pi, xyz(3)
    INTEGER(WIS) :: i, j, k

    pi = 2.0_WRD * ASIN(1.0_WRD)

	do k = 1-workp%nb, workp%nz+workp%nb
		xyz(3) = workp%z0 + REAL((k-1),WRD)*workp%dx

		do j = 1-workp%nb, workp%ny+workp%nb
			xyz(2) = workp%y0 + REAL((j-1),WRD)*workp%dx

			do i=1-workp%nb, workp%nx+workp%nb
				xyz(1) = workp%x0 + REAL((i-1),WRD)*workp%dx

				workp%grid3d(i,j,k,1) = 1.0_WRD
				workp%grid3d(i,j,k,2) = 0.0_WRD
				workp%grid3d(i,j,k,3) = 0.0_WRD
				workp%grid3d(i,j,k,4) = 0.0_WRD
				workp%bface3d(i,j,k,1) = 1.d-8
				workp%bface3d(i,j,k,2) = 0.0_WRD
				workp%bface3d(i,j,k,3) = 0.0_WRD
			enddo
		enddo
	enddo

	do k = 1, workp%nz
		xyz(3) = workp%z0 + REAL((k-1),WRD)*workp%dx

		do j = 1, workp%ny
			xyz(2) = workp%y0 + REAL((j-1),WRD)*workp%dx

			do i=1, workp%nx
				xyz(1) = workp%x0 + REAL((i-1),WRD)*workp%dx

        	    workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i,j,k,1) + workp%bface3d(i-1,j,k,1))
        	    workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j,k,2) + workp%bface3d(i,j-1,k,2))
				workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k,3) + workp%bface3d(i,j,k-1,3))

				if (dsqrt(SUM(xyz(:)**2)) .LE. 1.0_WRD) then
					pressure = 6d5
					if (workp%passOn .AND. workp%npass .GT. 0) then
						workp%pass3d(i,j,k,:) = 1.0_WRD
					endif
				else
					pressure = 0.6_WRD
					if (workp%passOn .AND. workp%npass .GT. 0) then
						workp%pass3d(i,j,k,:) = 0.0_WRD
					endif
				endif
	
				workp%grid3d(i,j,k,8) = pressure / (self%gamma - 1.0_WRD) + &
							    	(0.5_WRD / workp%grid3d(i,j,k,1)) * &
									(workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2) + & 
									0.5_WRD * (workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2)
			enddo
		enddo
	enddo

END SUBROUTINE st3d_init

!### define open boundaries for 1D ###
SUBROUTINE st_bounds1d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
	INTEGER(WIS) :: i, j, k

    IF (lx_bound) THEN

        DO i = 1-workp%nb, 0

            workp%grid1d(i,1) = workp%grid1d(1,1)
            workp%grid1d(i,2) = workp%grid1d(1,2)
            workp%grid1d(i,3) = workp%grid1d(1,3)
            workp%grid1d(i,4) = workp%grid1d(1,4)
            workp%grid1d(i,5) = workp%grid1d(1,5)
            workp%grid1d(i,6) = workp%grid1d(1,6)
            workp%grid1d(i,7) = workp%grid1d(1,7)
            workp%grid1d(i,8) = workp%grid1d(1,8)

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO i = 1-workp%nb, 0

                workp%pass1d(i,:) = workp%pass1d(1,:)

            END DO

        END IF

    END IF
    IF (hx_bound) THEN

        DO i = workp%nx+1, workp%nx+workp%nb

            workp%grid1d(i,1) = workp%grid1d(workp%nx,1)
            workp%grid1d(i,2) = workp%grid1d(workp%nx,2)
            workp%grid1d(i,3) = workp%grid1d(workp%nx,3)
            workp%grid1d(i,4) = workp%grid1d(workp%nx,4)
            workp%grid1d(i,5) = workp%grid1d(workp%nx,5)
            workp%grid1d(i,6) = workp%grid1d(workp%nx,6)
            workp%grid1d(i,7) = workp%grid1d(workp%nx,7)
            workp%grid1d(i,8) = workp%grid1d(workp%nx,8)

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%pass1d(i,:) = workp%pass1d(workp%nx,:)

            END DO

        END IF

    END IF

END SUBROUTINE st_bounds1d

!### define open boundaries for 2D ###
SUBROUTINE st_bounds2d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
	INTEGER(WIS) :: i, j, k

    IF (hx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(workp%nx,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(workp%nx,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(workp%nx,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(workp%nx,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(workp%nx,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(workp%nx,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(workp%nx,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(workp%nx,j,8)
                
            END DO

        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = workp%nx+1, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(workp%nx,j,:)
                
            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(workp%nx,j,:)

                END DO

            END DO

        END IF

    END IF
    IF (hy_bound) THEN

        DO j = workp%ny+1, workp%ny+workp%nb

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,workp%ny,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,workp%ny,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,workp%ny,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,workp%ny,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,workp%ny,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,workp%ny,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,workp%ny,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,workp%ny,8)
                
            END DO

        END DO

        DO j = workp%ny+1, workp%ny+workp%nb

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,workp%ny,:)
                
            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(i,workp%ny,:)

                END DO

            END DO

        END IF

    END IF
    IF (lx_bound) THEN

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = 1-workp%nb, 0

                workp%grid2d(i,j,1) = workp%grid2d(1,j,1)
                workp%grid2d(i,j,2) = workp%grid2d(1,j,2)
                workp%grid2d(i,j,3) = workp%grid2d(1,j,3)
                workp%grid2d(i,j,4) = workp%grid2d(1,j,4)
                workp%grid2d(i,j,5) = workp%grid2d(1,j,5)
                workp%grid2d(i,j,6) = workp%grid2d(1,j,6)
                workp%grid2d(i,j,7) = workp%grid2d(1,j,7)
                workp%grid2d(i,j,8) = workp%grid2d(1,j,8)

            END DO

        END DO

        DO j = 1-workp%nb, workp%ny+workp%nb

            DO i = 1-workp%nb, 0

                workp%emf2d(i,j,:) = workp%emf2d(1,j,:)

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, 0

                    workp%pass2d(i,j,:) = workp%pass2d(1,j,:)

                END DO

            END DO

        END IF

    END IF

    IF (ly_bound) THEN

        DO j = 1-workp%nb, 0

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%grid2d(i,j,1) = workp%grid2d(i,1,1)
                workp%grid2d(i,j,2) = workp%grid2d(i,1,2)
                workp%grid2d(i,j,3) = workp%grid2d(i,1,3)
                workp%grid2d(i,j,4) = workp%grid2d(i,1,4)
                workp%grid2d(i,j,5) = workp%grid2d(i,1,5)
                workp%grid2d(i,j,6) = workp%grid2d(i,1,6)
                workp%grid2d(i,j,7) = workp%grid2d(i,1,7)
                workp%grid2d(i,j,8) = workp%grid2d(i,1,8)

            END DO

        END DO

        DO j = 1-workp%nb, 0

            DO i = 1-workp%nb, workp%nx+workp%nb

                workp%emf2d(i,j,:) = workp%emf2d(i,1,:)

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%pass2d(i,j,:) = workp%pass2d(i,1,:)

                END DO

            END DO

        END IF

    END IF
END SUBROUTINE st_bounds2d

!### define open boundaries for 3D ###
SUBROUTINE st_bounds3d(self, workp, lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    LOGICAL(WIS), INTENT(IN) :: lx_bound, hx_bound, ly_bound, hy_bound, lz_bound, hz_bound
    INTEGER(WIS) :: i, j, k

    IF (hx_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(workp%nx,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(workp%nx,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(workp%nx,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(workp%nx,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(workp%nx,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(workp%nx,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(workp%nx,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(workp%nx,j,k,8)
                    
                END DO

            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(workp%nx,j,k,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = workp%nx+1, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(workp%nx,j,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF
    IF (hy_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,workp%ny,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,workp%ny,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,workp%ny,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,workp%ny,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,workp%ny,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,workp%ny,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,workp%ny,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,workp%ny,k,8)

                END DO
                
            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = workp%ny+1, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,workp%ny,k,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb

                DO j = workp%ny+1, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,workp%ny,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF
    IF (hz_bound) THEN

        DO k = workp%nz+1, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,workp%nz,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,workp%nz,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,workp%nz,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,workp%nz,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,workp%nz,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,workp%nz,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,workp%nz,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,workp%nz,8)

                END DO
                
            END DO

        END DO

        DO k = workp%nz+1, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,workp%nz,:)
                
                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = workp%nz+1, workp%nz+workp%nb

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,j,workp%nz,:)

                    END DO

                END DO

            END DO

        END IF

    END IF
    IF (lx_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, 0

                    workp%grid3d(i,j,k,1) = workp%grid3d(1,j,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(1,j,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(1,j,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(1,j,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(1,j,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(1,j,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(1,j,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(1,j,k,8)
                    
                END DO
                    
            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, 0

                    workp%emf3d(i,j,k,:) = workp%emf3d(1,j,k,:)

                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = 1-workp%nb, 0

                        workp%pass3d(i,j,k,:) = workp%pass3d(1,j,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

    IF (ly_bound) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,1,k,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,1,k,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,1,k,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,1,k,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,1,k,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,1,k,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,1,k,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,1,k,8)

                END DO

            END DO

        END DO

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, 0

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,1,k,:)

                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, workp%nz+workp%nb
                
                DO j = 1-workp%nb, 0
                
                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,1,k,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

    IF (lz_bound) THEN

        DO k = 1-workp%nb, 0

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%grid3d(i,j,k,1) = workp%grid3d(i,j,1,1)
                    workp%grid3d(i,j,k,2) = workp%grid3d(i,j,1,2)
                    workp%grid3d(i,j,k,3) = workp%grid3d(i,j,1,3)
                    workp%grid3d(i,j,k,4) = workp%grid3d(i,j,1,4)
                    workp%grid3d(i,j,k,5) = workp%grid3d(i,j,1,5)
                    workp%grid3d(i,j,k,6) = workp%grid3d(i,j,1,6)
                    workp%grid3d(i,j,k,7) = workp%grid3d(i,j,1,7)
                    workp%grid3d(i,j,k,8) = workp%grid3d(i,j,1,8)

                END DO

            END DO

        END DO

        DO k = 1-workp%nb, 0

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, workp%nx+workp%nb

                    workp%emf3d(i,j,k,:) = workp%emf3d(i,j,1,:)

                END DO

            END DO

        END DO

        IF (workp%passOn .AND. workp%npass .GT. 0) THEN

            DO k = 1-workp%nb, 0

                DO j = 1-workp%nb, workp%ny+workp%nb

                    DO i = 1-workp%nb, workp%nx+workp%nb

                        workp%pass3d(i,j,k,:) = workp%pass3d(i,j,1,:)

                    END DO

                END DO

            END DO

        END IF

    END IF

END SUBROUTINE st_bounds3d

END MODULE Mod_Problem
