#include "../config.h"

MODULE Mod_Problem

!######################################################################
!#
!# FILENAME: mod_ClusterJet.f90
!#
!# DESCRIPTION: This module provides a class for initializing a domain
!#   and setting boundary values to drive an AGN jet in a cluster
!#   using data from Klaus Dolag's simulations
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    4/20/2015  - Brian O'Neill
!#        Adapted from WOMBAT v1.0: ClusterJets.f90 - Pete Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_ProblemBase
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Problem

!### define a data type for this module ###
TYPE, EXTENDS(ProblemBase) :: Problem

    !### default everything as private ###
    PRIVATE
    
    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD), PUBLIC :: gamma
    
    !### user-added problem data structres ###
    REAL(WRD) :: phi, theta, psi, A(2,2), Ainv(2,2), AA(3,3), AAinv(3,3), jetvel, jetrho, jetbeta, jetlocX, jetlocY, jetlocZ, jetpres, jetopres, jetoprest, &
         jetdutycycle, jetblendt, jetperiod, jetnperiods, CRden, CRslope, CRdenjet, CRslopejet, bscale, jetseparation, &
         jetorbitperiod, icmpressure, icmvx, icmvy, icmvz, icmdensity, icmbx, icmby, icmbz, X_bdry_state(8), pressuret, maxsignal
    REAL(WRD), ALLOCATABLE :: jetorbitvec(:,:), jetorbitvelvec(:,:)
    
    !### Klaus's inputs files ###
    CHARACTER*256 :: denfile, tempfile, vxfile, vyfile, vzfile, bxfile, byfile, bzfile, gravfile
    
    !### some size options for the jet ###
    INTEGER :: nrjet, nrbuff, nljet, jetpresr
    
    !### set if we have already read in the data ###
    LOGICAL :: flatbackground = .FALSE., dualjets = .FALSE., X_bdry_ovr = .FALSE.
    
    CONTAINS
    
    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: patch_init
    PROCEDURE, PUBLIC :: patch_bounds
    PROCEDURE :: initproblem
    PROCEDURE :: InitPatch3dJet
    PROCEDURE :: SetBounds3dJet
!    PROCEDURE :: ReadJetSetup
!    PROCEDURE :: ReadSPHCubes
!    PROCEDURE :: UnpackSPHCube
!    PROCEDURE :: ReadGravProf
    PROCEDURE :: SetTRamp
    PROCEDURE :: JetDriving
    PROCEDURE :: GetRotate2d
    PROCEDURE :: GetInvRotate2d
    PROCEDURE :: DefineJetField
    PROCEDURE :: ExpandJetField
    PROCEDURE :: GetJetPressure
    PROCEDURE :: EstablishJetField
    PROCEDURE :: SetZRamp

END TYPE Problem

!### create an interface to the constructor ###
INTERFACE Problem

    MODULE PROCEDURE constructor

END INTERFACE Problem

!### object methods ###
CONTAINS

!#--------------------------------------------------
!#
!#      Required Routines
!#
!#
!#--------------------------------------------------

!### constructor for the class ###
!###   arguments are: the namelist file contents for grabbing parameters out of ###
FUNCTION constructor(namelist, nthreads, rankloc, simunits)

    TYPE(Problem) :: constructor
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initproblem(namelist, nthreads, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### this function will init the object and configure init and bound routine parameters ###
SUBROUTINE initproblem(self, namelist, nthreads, rankloc, simunits)

    CLASS(Problem) :: self
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER(30) :: init_routine, bounds_routine
    REAL(WRD) :: gamma

    REAL(WRD) :: AA(3,3), AAinv(3,3), jetvel, jetrho, jetbeta, jetlocX, jetlocY, jetlocZ, jetpres, jetopres, jetoprest, jetphi, jettheta, jetpsi, &
                 jetdutycycle, jetblendt, jetperiod, jetnperiods, CRden, CRslope, CRdenjet, CRslopejet, bscale, jetseparation, &
                 jetorbitperiod, icmpressure, icmvx, icmvy, icmvz, icmdensity, icmbx, icmby, icmbz, X_bdry_state(8), pressuret

    CHARACTER*256 :: denfile, tempfile, vxfile, vyfile, vzfile, bxfile, byfile, bzfile, gravfile
    INTEGER(WIS) :: nrjet, nrbuff, nljet, jetpresr
    LOGICAL(WIS) :: flatbackground, dualjets, X_bdry_ovr

    NAMELIST /ProblemSetup/ init_routine, bounds_routine, gamma, denfile, tempfile, vxfile, vyfile, vzfile, &
                           nrjet, nrbuff, nljet, gravfile, jetvel, jetrho, jetlocX, jetlocY, jetlocZ, jetpresr, &
                           jetbeta, jetblendt, jetperiod, jettheta, jetphi, jetpsi, bxfile, byfile, bzfile, CRden, CRslope, &
                           CRdenjet, CRslopejet, bscale, flatbackground, jetpres, jetopres, jetoprest, jetnperiods, jetdutycycle, &
                           jetseparation, jetorbitperiod, dualjets, icmpressure, icmvx, icmvy, icmvz, icmdensity, &
                           icmbx, icmby, icmbz, X_bdry_ovr, X_bdry_state

    READ(namelist, NML=ProblemSetup)

    !### set the routines we'll call ###
    self%init_routine   = init_routine
    self%bounds_routine = bounds_routine

    !### we handle setting the adiabatic index ###
    self%gamma = gamma

    !### set jet angles and get rotation matrices ###
    self%phi   = jetphi * simunits%pi / 180.0_WRD
    self%theta = jettheta * simunits%pi / 180.0_WRD
    self%psi   = jetpsi * simunits%pi / 180.0_WRD

    self%AA(1,1)    = DCOS(self%psi  )*DCOS(self%phi  ) - DCOS(self%theta)*DSIN(self%phi  )*DSIN(self%psi  )
    self%AA(1,2)    = DCOS(self%psi  )*DSIN(self%phi  ) + DCOS(self%theta)*DCOS(self%phi  )*DSIN(self%psi  )
    self%AA(1,3)    = DSIN(self%psi  )*DSIN(self%theta)
    self%AA(2,1)    =-DSIN(self%psi  )*DCOS(self%phi  ) - DCOS(self%theta)*DSIN(self%phi  )*DCOS(self%psi  )
    self%AA(2,2)    =-DSIN(self%psi  )*DSIN(self%phi  ) + DCOS(self%theta)*DCOS(self%phi  )*DCOS(self%psi  )
    self%AA(2,3)    = DCOS(self%psi  )*DSIN(self%theta)
    self%AA(3,1)    = DSIN(self%theta)*DSIN(self%phi  )
    self%AA(3,2)    =-DSIN(self%theta)*DCOS(self%phi  )
    self%AA(3,3)    = DCOS(self%theta)

    self%AAinv(1,1) = DCOS(self%psi  )*DCOS(self%phi  ) - DCOS(self%theta)*DSIN(self%phi  )*DSIN(self%psi  )
    self%AAinv(1,2) =-DSIN(self%psi  )*DCOS(self%phi  ) - DCOS(self%theta)*DSIN(self%phi  )*DCOS(self%psi  )
    self%AAinv(1,3) = DSIN(self%theta)*DSIN(self%phi  )
    self%AAinv(2,1) = DCOS(self%psi  )*DSIN(self%phi  ) + DCOS(self%theta)*DCOS(self%phi  )*DSIN(self%psi  )
    self%AAinv(2,2) =-DSIN(self%psi  )*DSIN(self%phi  ) + DCOS(self%theta)*DCOS(self%phi  )*DCOS(self%psi  )
    self%AAinv(2,3) =-DSIN(self%theta)*DCOS(self%phi  )
    self%AAinv(3,1) = DSIN(self%theta)*DSIN(self%psi  )
    self%AAinv(3,2) = DSIN(self%theta)*DCOS(self%psi  )
    self%AAinv(3,3) = DCOS(self%theta)

    !### store the inputs into our object ###
    self%denfile        = denfile
    self%vxfile         = vxfile
    self%vyfile         = vyfile
    self%vzfile         = vzfile
    self%bxfile         = bxfile
    self%byfile         = byfile
    self%bzfile         = bzfile
    self%tempfile       = tempfile
    self%gravfile       = gravfile
    self%flatbackground = flatbackground
    self%bscale         = bscale
    self%CRden          = CRden
    self%CRslope        = CRslope
    self%nrjet          = nrjet
    self%nrbuff         = nrbuff
    self%nljet          = nljet 
    self%jetopres       = jetopres  
    self%jetpresr       = jetpresr
    self%CRdenjet       = CRdenjet
    self%CRslopejet     = CRslopejet  
    self%jetbeta        = jetbeta 
    self%dualjets       = dualjets
    self%icmdensity     = icmdensity / simunits%mass_density_cgs
    self%icmvx          = icmvx / simunits%velocity_cgs
    self%icmvy          = icmvy / simunits%velocity_cgs
    self%icmvz          = icmvz / simunits%velocity_cgs
    self%icmbx          = icmbx / simunits%magnetic_cgs
    self%icmby          = icmby / simunits%magnetic_cgs
    self%icmbz          = icmbz / simunits%magnetic_cgs
    self%icmpressure    = icmpressure / simunits%energy_density_cgs
    self%jetseparation  = jetseparation / simunits%length_cgs
    self%jetorbitperiod = jetorbitperiod / simunits%time_cgs
    self%jetlocX        = jetlocX / simunits%length_cgs
    self%jetlocY        = jetlocY / simunits%length_cgs
    self%jetlocZ        = jetlocZ / simunits%length_cgs
    self%jetrho         = jetrho / simunits%mass_density_cgs
    self%jetvel         = jetvel / simunits%velocity_cgs
    self%jetpres        = jetpres / simunits%energy_density_cgs
    self%jetoprest      = jetoprest / simunits%time_cgs
    self%jetblendt      = jetblendt / simunits%time_cgs
    self%jetperiod      = jetperiod / simunits%time_cgs
    self%jetnperiods    = jetnperiods
    self%jetdutycycle   = jetdutycycle
    self%X_bdry_ovr     = X_bdry_ovr

    !### if we are to override the X boundary we need to scale the state vector into code units (it is assumed to be CGS) ###
    IF (self%X_bdry_ovr) THEN

        self%X_bdry_state(1) = X_bdry_state(1) / simunits%mass_density_cgs
        self%X_bdry_state(2) = self%X_bdry_state(1) * X_bdry_state(2) / simunits%velocity_cgs
        self%X_bdry_state(3) = self%X_bdry_state(1) * X_bdry_state(3) / simunits%velocity_cgs
        self%X_bdry_state(4) = self%X_bdry_state(1) * X_bdry_state(4) / simunits%velocity_cgs
        self%X_bdry_state(5) = X_bdry_state(5) / simunits%magnetic_cgs
        self%X_bdry_state(6) = X_bdry_state(6) / simunits%magnetic_cgs
        self%X_bdry_state(7) = X_bdry_state(7) / simunits%magnetic_cgs
        self%X_bdry_state(8) = (X_bdry_state(8) / simunits%energy_density_cgs)  / (self%gamma - 1.0_WRD) + &
             (0.5_WRD / self%X_bdry_state(1)) * (self%X_bdry_state(2)**2 + &
             self%X_bdry_state(3)**2 + self%X_bdry_state(4)**2) + 0.5_WRD * & 
             (self%X_bdry_state(5)**2 + self%X_bdry_state(6)**2 + &
             self%X_bdry_state(7)**2)

    END IF

!    !### allocate space to the thread specific pressure average array ###
!    ALLOCATE(self%jetorbitvec(3,grido%nthreads), self%jetorbitvelvec(3,grido%nthreads))

END SUBROUTINE initproblem

SUBROUTINE destroy(self)

    CLASS(Problem) :: self

    RETURN

END SUBROUTINE destroy

!### the routine called to initialize every Patch ###
SUBROUTINE patch_init(self, workp, rankloc, simunits, is_boundary, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%init_routine))

        CASE ('InitPatch3dJet')
        
            CALL self%InitPatch3dJet(workp)
            ierr = 0

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown initialization routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT


END SUBROUTINE patch_init

!### the routine called to set boundary values (called for every Patch even if it is not a world boundary) ###
SUBROUTINE patch_bounds(self, workp, rankloc, simunits, is_boundary, maxsignal, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    REAL(WRD), INTENT(INOUT) :: maxsignal
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%bounds_routine))

        CASE('SetBounds3dJet')

            CALL self%SetBounds3dJet(workp, is_boundary)
            ierr = 0
            maxsignal = MAX(maxsignal, self%maxsignal)

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown boundary routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT

END SUBROUTINE patch_bounds


!#--------------------------------------------------
!#
!#      Initialization Routines
!#
!#--------------------------------------------------


!### this routine will setup the initial conditions for a jet run from Klaus's simulation data ###
!### i assume that the input files are of the same resolution as the simulation we're running ###
SUBROUTINE InitPatch3dJet(self, workp)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
!    TYPE(CubicSpline) :: cs
!    REAL(WRD), TARGET :: rprof(10000), massprof(10000)
!    REAL(WRD), POINTER :: prprof(:), pmassprof(:)
!    REAL(WRD) :: n(grido%crso(1)%npbins), g(grido%crso(1)%npbins)     no CRs yet
    REAL(WRD) :: idl, idenscale, ivelscale, ipressscale, Gscale, ips, igamma1, r, innermass, &
              ibscale, Bmag, Ntot, r_outer, scale_phi, vxfixed, vscale, bscale
    INTEGER :: i, j, k, ilast, nb

    !### hold onto the number of boundary zones ###
    nb = workp%nb

    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)

!!$!### No read in capability yet  ###
!!$    !### now read in Klaus's data files if we're supposed to ###
!!$    IF (.NOT. self%flatbackground) THEN
!!$
!!$       IF (WRANK .EQ. 0) CALL Message('Mod_Problem','Reading in SPH cubes.')
!!$!       CALL self%ReadSPHCubes(workp)                            !### need to port
!!$       
!!$       !### read in the total mass profile ###
!!$       IF (WRANK .EQ. 0) CALL Message('Mod_Problem','Computing potential profile.')
!!$!       CALL self%ReadGravProf(workp, rprof, massprof, ilast)    !### need to port
!!$
!!$       !### now we have all of Klaus's data read in (in CGS units) ###
!!$       !### we have to convert to fully conservative quantities (e.g. momx rather than vx) in simulation units ###
!!$       
!!$       !### loop over the entire grid including boundaries ###
!!$       DO k = 1-nb, workp%nz+nb
!!$           
!!$          DO j = 1-nb, workp%ny+nb
!!$               
!!$             DO i = 1-nb, workp%nx+nb
!!$                   
!!$                !### before we scale density make the energy term actually contain pressure instead of just ###
!!$                !### temperature from the SPH cubes ###
!!$!                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,1) * workp%physo%kB * workp%grid3d(i,j,k,8) * ips
!!$                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,1) * workp%grid3d(i,j,k,8)
!!$
!!$                !### convert into simulation conservative units ###
!!$                workp%grid3d(i,j,k,1) = workp%grid3d(i,j,k,1) !* idenscale
!!$                workp%grid3d(i,j,k,2) = workp%grid3d(i,j,k,1) * workp%grid3d(i,j,k,2) !* ivelscale
!!$                workp%grid3d(i,j,k,3) = workp%grid3d(i,j,k,1) * workp%grid3d(i,j,k,3) !* ivelscale
!!$                workp%grid3d(i,j,k,4) = workp%grid3d(i,j,k,1) * workp%grid3d(i,j,k,4) !* ivelscale
!!$
!!$                !### convert the fields into our funny units ###
!!$                workp%grid3d(i,j,k,5) = workp%grid3d(i,j,k,5) !* ibscale
!!$                workp%grid3d(i,j,k,6) = workp%grid3d(i,j,k,6) !* ibscale
!!$                workp%grid3d(i,j,k,7) = workp%grid3d(i,j,k,7) !* ibscale
!!$
!!$                !### in the energy term we only have gas pressure right now. ###
!!$                !### put in kinetic energy as well ###
!!$!                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,8) * ipressscale * igamma1 + (0.5d0 / workp%grid3d(i,j,k,1)) * &
!!$!                                        SUM(workp%grid3d(2:i,j,k,4)**2) + 0.5d0 * SUM(workp%grid3d(5:i,j,k,7)**2)
!!$                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,8) * igamma1 + (0.5_WRD / workp%grid3d(i,j,k,1)) * &
!!$                                        SUM(workp%grid3d(i,j,k,2:4)**2) + 0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)
!!$
!!$                !### make the color 0 for the ambient material ###
!!$                IF (workp%npass .GT. 0) workp%pass3d(i,j,k,1) = 0.d0
!!$
!!$             END DO
!!$
!!$          END DO
!!$
!!$       END DO
!!$
!!$    !### otherwise we'll just apply a uniform background typically used for debugging purposes ###
!!$    ELSE
!!$
!!$       IF (WRANK .EQ. 0) CALL Message('Mod_Problem','Uniform background assumed.')

       !### loop over the entire grid including boundaries ###
       DO k = 1-nb, workp%nz+nb
           
          DO j = 1-nb, workp%ny+nb
               
             DO i = 1-nb, workp%nx+nb

                workp%grid3d(i,j,k,1)   = self%icmdensity
                workp%grid3d(i,j,k,2)   = workp%grid3d(i,j,k,1) * self%icmvx
                workp%grid3d(i,j,k,3)   = workp%grid3d(i,j,k,1) * self%icmvy
                workp%grid3d(i,j,k,4)   = workp%grid3d(i,j,k,1) * self%icmvz
                workp%grid3d(i,j,k,5)   = self%icmbx
                workp%grid3d(i,j,k,6)   = self%icmby
                workp%grid3d(i,j,k,7)   = self%icmbz
                workp%grid3d(i,j,k,8)   = self%icmpressure * igamma1 + (0.5_WRD / workp%grid3d(i,j,k,1)) * &
                                          SUM(workp%grid3d(i,j,k,2:4)**2) + 0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)

                !### make the color 0 for the ambient material ###
                IF (workp%npass .GT. 0) workp%pass3d(i,j,k,1) = 0.d0

             END DO

          END DO

       END DO

    !### Klaus's fields are zone center values, so we have to interpolate them to face centered (this is sort of OK) ###
    DO k = 1, workp%nz

        DO j = 1, workp%ny
        
            DO i = 1, workp%nx

                !### interpolate with just an average (the inverse of what is done to get zone centered from face centered) ###
                workp%bface3d(i,j,k,1) = 0.5_WRD * (workp%grid3d(i,j,k,5) + workp%grid3d(i+1,j,k,5))
                workp%bface3d(i,j,k,2) = 0.5_WRD * (workp%grid3d(i,j,k,6) + workp%grid3d(i,j+1,k,6))
                workp%bface3d(i,j,k,3) = 0.5_WRD * (workp%grid3d(i,j,k,7) + workp%grid3d(i,j,k+1,7))

            END DO

        END DO

    END DO

!### no gravity/ readin yet
!    !### if gravity is on define gravity ###
!    IF (workp%gravOn) THEN
!
!        !### convert the mass profile into simulation units ###
!        rprof(:)    = rprof(:) !/ grido%physo%lengthscale
!        massprof(:) = massprof(:) !/ (grido%physo%denscale * grido%physo%lengthscale**3)
!
!        !### initialize a Cubic Spline object to use for interpolation ###
!        prprof    => rprof(1:ilast)
!        pmassprof => massprof(1:ilast)
!        CALL Cubic_Init(cs, pmassprof, prprof)
!!        scale_phi = Gscale * Cubic_Interpolate(cs, r_outer) / r_outer
!        scale_phi = Cubic_Interpolate(cs, r_outer) / r_outer
!
!        !### now loop through the grid interpolating the gravitating mass to set the gravity parameters ###
!        DO k = 1-nb, workp%nz+nb
!            
!           DO j = 1-nb, workp%ny+nb
!                
!              DO i = 1-nb, workp%nx+nb
!
!                    !### determine the cluster centric distance ###
!                    r = SQRT(SUM(xyz(1:3)**2))
!
!                    !### determine the interior mass at this location ###
!                    innermass = Cubic_Interpolate(cs, r)
!
!                    !### set the potential and acceleration (PROTECT FROM R = 0) ###
!                    IF (r .GT. 0.d0) THEN
!
!!                        workp%grav3d(i,j,k,1) = Gscale * innermass / r - scale_phi  ! with the scale phi should be negative
!                        workp%grav3d(i,j,k,1) = innermass / r - scale_phi  ! with the scale phi should be negative
!
!                    ELSE
!
!                        workp%grav3d(i,j,k,1) = -scale_phi 
!
!                    END IF
!
!                END DO
!
!            END DO
!
!        END DO
!
!        !### clean up the cubic spline variables ###
!        CALL Cubic_Destruct(cs)
!
!
!!### Mod_PM needs to be plugged in to compute acceleration, this will be deferred to solve in mod_domainsolver
!        !### now derive the acceleration from the potential ###
!        CALL Gravity_ComputeStaticGAccel(grido%gravo)
!
!    END IF

!### no CRs yet
!    !### if CRs are on let's set their distribution ###
!    IF (grido%crso(1)%iCR .EQ. 1) THEN
!        
!        !### scale the total number density by the code value ###
!        Ntot = grido%probo%CRden / grido%physo%nscale
!        
!        !### let's get the distribution across all bins ###
!        CALL CR_Distribute(grido%crso(1), n, g, 1, grido%crso(1)%npbins, Ntot, grido%probo%CRslope)
!        
!        !### loop back through the grid setting the distribution ###
!        DO k = grido%mhdtvdo(1)%info%nlow+nb, nb+grido%nz
!            
!            DO j = grido%mhdtvdo(1)%info%nlow+nb, nb+grido%ny
!                
!                DO i = grido%mhdtvdo(1)%info%nlow+nb, nb+grido%nx
!                    
!                    grido%pass3d(i,j,k,2:2+grido%crso(1)%npbins-1)                        = n
!                    grido%pass3d(i,j,k,2+grido%crso(1)%npbins:2+2*grido%crso(1)%npbins-1) = g
!                    
!                END DO
!                
!            END DO
!            
!        END DO
!        
!    END IF

END SUBROUTINE InitPatch3dJet

!#--------------------------------------------------
!#
!#  Boundary Routines
!#
!#
!#--------------------------------------------------

!### SetBounds will set the boundary zone values ###
SUBROUTINE SetBounds3dJet(self, workp, is_boundary)

    CLASS(Problem) :: self
    TYPE(Patch),INTENT(INOUT) :: workp
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    REAL(WRD), ALLOCATABLE :: bjet(:,:,:,:)
!    REAL(WRD) :: crn(grido%crso(1)%npbins), crg(grido%crso(1)%npbins), Ntot
    REAL(WRD) :: p0, h0, g, p
    INTEGER :: length, ii, jj, nb, n, k, j, i

    nb = workp%nb

    !### allocate space to our jet field array ###
    ALLOCATE(bjet(1-nb:workp%nx+nb,1-nb:workp%ny+nb,1-nb:workp%nz+nb,3))

    !### if we're at the beginning, let's evacuate as much field as we can by adding some pressure ###
    IF (workp%t .EQ. 0) CALL self%ExpandJetField(workp)

    !### only enforce the internal bounds on the first cut for an update ###
    IF (workp%t .GT. self%jetoprest) THEN

       CALL self%DefineJetField(workp, bjet)
       CALL self%EstablishJetField(workp, bjet)
       CALL self%JetDriving(workp)

    END IF

    !### if we are overriding the X boundary state, apply the given values ###
    IF (self%X_bdry_ovr .AND. is_boundary(1,1)) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = 1-workp%nb, 1

                    workp%grid3d(i,j,k,:) = self%X_bdry_state(:)

                END DO

            END DO

        END DO

    END IF

    !### we do need open boundaries on the upper X bound ###
    IF (is_boundary(2,1)) THEN

        DO k = 1-workp%nb, workp%nz+workp%nb

            DO j = 1-workp%nb, workp%ny+workp%nb

                DO i = workp%nx+1, workp%nx+workp%nb

                    workp%grid3d(i,j,k,:) = workp%grid3d(workp%nx,j,k,:)

                END DO

            END DO

        END DO

    END IF

    !### cleanup the jet field array ###
    DEALLOCATE(bjet)

    !### For now we'll just apply periodic boundaries for the domain, so we're done here ###
    RETURN

END SUBROUTINE SetBounds3dJet

!### ExpandJetField will slightly overpressure the jet launching region prior to the jet turning on in an attempt to evacuate field ###
SUBROUTINE ExpandJetField(self,workp)

    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
    REAL(WRD) :: p, dr, dbuff, dswirl, xyz(3), igamma1, mom(3), vmag, theta
    INTEGER :: i, j, k, nc, njets

    !### we'll over pressure just a single zone centered in the jet cylinder ###
    dbuff   = 2.0_WRD * workp%dx
    dswirl  = self%nrjet * workp%dx
    igamma1 = 1.0_WRD / (self%gamma - 1.0_WRD)
    vmag    = 1.0_WRD

    njets = 1
    IF (self%dualjets) njets = 2

    !### loop over the entire grid (including boundaries) ###
    DO k = 1-workp%nb, workp%nz + workp%nb

       DO j = 1-workp%nb, workp%ny + workp%nb
        
          DO i = 1-workp%nb, workp%nx + workp%nb

             DO nc = 1, njets 

                xyz(1) = workp%x0 + REAL(i-1,WRD) * workp%dx
                xyz(2) = workp%y0 + REAL(j-1,WRD) * workp%dx
                xyz(3) = workp%z0 + REAL(k-1,WRD) * workp%dx

                !### recenter our position from the jet origin ###
                xyz(1) = xyz(1) - self%jetlocX
                xyz(2) = xyz(2) - self%jetlocY
                xyz(3) = xyz(3) - self%jetlocZ
!                xyz    = xyz - (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)
                CALL self%rotate3d(xyz, self%AA, self%AAinv, .TRUE.)

                !### now compare our position to a sphere with radius of the jet buffer ###
                dr = SQRT(SUM(xyz**2))

                !### if we're outside the vortex region move on ###
                IF (dr .GT. dswirl) CYCLE

                !### determine what angle we're at with respect to the circle center ###
                theta = ATAN2(xyz(2),xyz(1))

                !### get the 2d rotation matrix ###
                CALL self%GetRotate2d(theta)

                !### get the current total energy ###
                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,8) - 0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1)

                !### add in the vortex (as a solid body rotator) ###
                mom(1) = 0.0_WRD
                mom(2) = vmag * workp%grid3d(i,j,k,1) * dr / dswirl
                mom(3) = SIGN(1.0_WRD, xyz(3)) * vmag * workp%grid3d(i,j,k,1) * dr / dswirl

                !### rotate that momentum vector back into the grid frame ###
                CALL self%rotate2d(mom(1:2), self%A, self%Ainv, .FALSE.)
                CALL self%rotate3d(mom, self%AA, self%AAinv, .FALSE.)

                !### and update the momentum and energy ###
                workp%grid3d(i,j,k,2:4) = mom
                workp%grid3d(i,j,k,8)   = workp%grid3d(i,j,k,8) + 0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1)
                
                !### if we're outside the cylinder there is nothing motr to do ###
                IF (dr .GT. dbuff) CYCLE

                !### get the current pressure ###
                p = (self%gamma - 1.0_WRD) * (workp%grid3d(i,j,k,8) - 0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1) - &
                                            0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2))

                !### increase it by our added amount ###
                p = p * (1.0_WRD + self%jetopres)

                !### redefine the energy ###
                workp%grid3d(i,j,k,8) = p * igamma1 + 0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1) + &
                                        0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)

             END DO

          END DO

       END DO

    END DO

END SUBROUTINE ExpandJetField

!### this routine defines the source magnetic field for the jet driving region ###
SUBROUTINE DefineJetField(self, workp, bjet)

    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
    REAL(WRD), INTENT(INOUT) :: bjet(1-workp%nb:workp%nx+workp%nb,1-workp%nb:workp%ny+workp%nb,1-workp%nb:workp%nz+workp%nb,3)
    REAL(WRD), ALLOCATABLE :: vector_pot(:,:,:,:)
    REAL(WRD) :: dr, djet, dzjet, dbuff, dz, jetB, xyz(3), idx, zramp, A(3), fourth, ztmp1, ztmp2
    INTEGER :: i, j, k, nb, nc, njets

    !### hold onto the number of boundary zones ###
    nb = workp%nb

    !### set the normailzation values ###
    idx    = 1.0_WRD / workp%dx
    fourth = 1.0_WRD / 4.0_WRD
    
    !### compute the length scales of the jet cylinder ###
    djet  = workp%dx * REAL(self%nrjet - self%nrbuff, WRD)
    dbuff = workp%dx * REAL(self%nrjet, WRD)
    dzjet = workp%dx * REAL(self%nljet, WRD)

    !### determine the asymptotic jet field magnitude ###
    jetB = SQRT(2.0_WRD * self%jetpres / self%jetbeta)

    !### allocate space to the vector potential (note, this array is thread private, but loop indices make that work) ###
    ALLOCATE(vector_pot(1-nb:workp%nx+nb,1-nb:workp%ny+nb,1-nb:workp%nz+nb,3))
    vector_pot = 0.0_WRD
    bjet       = 0.0_WRD

    njets = 1
    IF (self%dualjets) njets = 2

    !### let's do another loop to set face centered fields ###
    DO k = 1-workp%nb, workp%nz + workp%nb

       DO j = 1-workp%nb, workp%ny + workp%nb

          DO i = 1-workp%nb, workp%nx + workp%nb

             DO nc = 1, njets 

                xyz(1) = workp%x0 + REAL(i-1,WRD) * workp%dx
                xyz(2) = workp%y0 + REAL(j-1,WRD) * workp%dx
                xyz(3) = workp%z0 + REAL(k-1,WRD) * workp%dx

                !### recenter our position from the jet origin and move to the top right and back corner of the voxel ###
                xyz(1) = xyz(1) - self%jetlocX + 0.5_WRD * workp%dx
                xyz(2) = xyz(2) - self%jetlocY + 0.5_WRD * workp%dx
                xyz(3) = xyz(3) - self%jetlocZ + 0.5_WRD * workp%dx
!                xyz    = xyz - (DBLE(nc) - 1.5_WRD) * self%jetorbitvec(:,tid)

                !### rotate this into the jet frame ###
                CALL self%rotate3d(xyz, self%AA, self%AAinv, .TRUE.)

                !### define our cylinder distances ###
                xyz(3) = xyz(3) - 0.5_WRD * workp%dx  ! this keeps the driving centered
                dz     = ABS(xyz(3))
                dr     = SQRT(SUM(xyz(1:2)**2))

                !### determine the spatial ramp we'll be giving velocity.  we'll use this to ramp the magnitude of the field ###
                IF (xyz(3) .GE. 0.0_WRD) THEN

                   CALL self%SetZRamp(workp%dx, xyz(3) + workp%dx, ztmp1)
                   CALL self%SetZRamp(workp%dx, xyz(3), ztmp2)
                   zramp = ABS(ztmp1 - ztmp2)
      
                ELSE

                   CALL self%SetZRamp(workp%dx, xyz(3) - workp%dx, ztmp1)
                   CALL self%SetZRamp(workp%dx, xyz(3), ztmp2)
                   zramp = ABS(ztmp1 - ztmp2)

                END IF

                !### set the potential in the jet frame (i include the amplitude term that scales the field loop magnitude by ###
                !### what's advected through the zone, dB = dt * jetB * dV/dx, include the derivative in the vector potential) ###
                A = 0.0_WRD
                
                !### we want B = B(r / rjet) out to the jet radius which then drops off with a very steep slope (say to the 5th) ###
                !### the derivative must go to zero for b->0, but make A go to zero outside of the cylinder by adding a constant ###
                IF (dz .LE. dzjet) THEN

                    IF (dr .LT. djet) THEN

                        A(3) = 0.5_WRD * (dr / djet)**2 - 0.5_WRD * (5.0_WRD * fourth - fourth * (djet / dbuff)**4)

                    ELSE IF (dr .LT. dbuff) THEN
                    
                        A(3) = 0.5_WRD * (5.0_WRD * fourth - fourth * (djet / dr)**4) - 0.5_WRD * (5.0_WRD * fourth - fourth * (djet / dbuff)**4)

                    END IF

                END IF

                A(3) = A(3) * idx * zramp * djet * self%jetvel * jetB

                !### rotate that back into the grid frame ###
                CALL self%rotate3d(A, self%AA, self%AAinv, .FALSE.)
                vector_pot(i,j,k,:) = vector_pot(i,j,k,:) + A

             END DO

          END DO

       END DO

    END DO

    !### now compute the magenetic field from that vector potential, which will be used as the jet source term ###
    DO k = 2-nb, workp%nz+nb

        DO j = 2-nb, workp%ny+nb

            DO i = 2-nb, workp%nx+nb

                !### determine the field from the vector potential (b = curl(A)) ###
                bjet(i,j,k,1) = idx * ((vector_pot(i,j,k,3) - vector_pot(i,j-1,k,3)) - &
                                       (vector_pot(i,j,k,2) - vector_pot(i,j,k-1,2)))
                bjet(i,j,k,2) = idx * ((vector_pot(i,j,k,1) - vector_pot(i,j,k-1,1)) - &
                                       (vector_pot(i,j,k,3) - vector_pot(i-1,j,k,3)))
                bjet(i,j,k,3) = idx * ((vector_pot(i,j,k,2) - vector_pot(i-1,j,k,2)) - &
                                       (vector_pot(i,j,k,1) - vector_pot(i,j-1,k,1)))

            END DO

        END DO

    END DO

    !### we're done with the vector potential ###
    DEALLOCATE(vector_pot)

END SUBROUTINE DefineJetField

!### EstablishJetField will snip the existing face centered cluster field lines that go through the jet cylinder and establish the jet field ###
!### the field is set to the asymptotic value ###
SUBROUTINE EstablishJetField(self, workp, bjet)

    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
    REAL(WRD), INTENT(INOUT) :: bjet(1-workp%nb:workp%nx+workp%nb,1-workp%nb:workp%ny+workp%nb,1-workp%nb:workp%nz+workp%nb,3)
    REAL(WRD) :: tramp
    INTEGER :: i, j, k, nb

    !### define the temporal jet amplitude for this time ###
    CALL self%SetTRamp(workp, tramp)

    !### hold onto the number of boundary zones ###
    nb  = workp%nb

    !### let's do another loop to set face centered fields ###
    DO k = 1-nb, workp%nz

       DO j = 1-nb, workp%ny

          DO i = 1-nb, workp%nx

              !### now add in the source term to the face centered field based on the evacuation rate ###
              workp%bface3d(i,j,k,:) = workp%bface3d(i,j,k,:) + tramp * bjet(i,j,k,:) * workp%dt

          END DO

       END DO

    END DO

    !### loop back through and reset the zone centered fields ###
    DO k = 1, workp%nz 

        DO j = 1, workp%ny

            DO i = 1, workp%nx
               
                !### energy is not conserved in the cylinder because we added field ###
                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,8) - 0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)
                workp%grid3d(i,j,k,5) = workp%grid3d(i,j,k,5) + tramp * workp%dt * 0.5_WRD * (bjet(i,j,k,1) + bjet(i-1,j,k,1))
                workp%grid3d(i,j,k,6) = workp%grid3d(i,j,k,6) + tramp * workp%dt * 0.5_WRD * (bjet(i,j,k,2) + bjet(i,j-1,k,2))
                workp%grid3d(i,j,k,7) = workp%grid3d(i,j,k,7) + tramp * workp%dt * 0.5_WRD * (bjet(i,j,k,3) + bjet(i,j,k-1,3))
                workp%grid3d(i,j,k,8) = workp%grid3d(i,j,k,8) + 0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)

            END DO

        END DO

    END DO

END SUBROUTINE EstablishJetField

!### JetDriving will apply the jet driving on the grid for each pass ###
!### the jet cylinder is modeled as an impenetrable perfect conductor.  this means there is no magnetic flux ###
!### through the surface normal.  this prevents both ambient mass and magnetic field from getting destroyed ###
!### as gas moves around it.  the inspiration in hydro was woodward and porter's solar convection run.  the idea ###
!### for a conducting surface i'll have to take credit for, but i'm sure someone has done it before. ###
!### this routine handles jet launching at arbitrary angles, which is new for us.  in theory, with minor modifications ###
!### this routine will allow us to do X sources by rotating to different angles over time. ###
SUBROUTINE JetDriving(self, workp)
  
    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
!### No CRs yet
!    REAL(WRD) :: n(grido%crso(1)%npbins), g(grido%crso(1)%npbins), Ntot, nbg(grido%crso(1)%npbins), gbg(grido%crso(1)%npbins), Ntotbg
    REAL(WRD) :: collarq(8), dz, dr, mom(3), vel(3), gammam1, xyzLow(3), &
              dbuff, djet, dzjet, dzbuff, xyz(3), lxyz(3), tramp, p, theta, xy(2), Pjet, rhosw, rramp, zramp
    INTEGER :: i, j, k, dijk(3), nb, nc, njets

    xyzLow(:) = (/ workp%x0, workp%y0, workp%z0 /) - 0.5_WRD * workp%dx

    !### set the physical size of the jet buffer ###
    djet   = workp%dx * REAL(self%nrjet - self%nrbuff, WRD)
    dbuff  = workp%dx * REAL(self%nrjet, WRD)
    dzjet  = workp%dx * REAL(self%nljet, WRD)
    dzbuff = dzjet + workp%dx * REAL(self%nrbuff, WRD)

    gammam1 = self%gamma - 1.0_WRD

    !### define the temporal jet amplitude for this time ###
    CALL self%SetTRamp(workp, tramp)
    
    !### we set a hard switch for density in the Z buffer ###
    rhosw = 0.0_WRD
    IF (tramp .GT. 0.1_WRD) rhosw = 1.0_WRD

    !### get the jet pressure we should use, for multiple jets, add loop over # of jets ###
    CALL self%GetJetPressure(workp, Pjet)

    !### get the number of boundary zones ###
    nb = workp%nb

!###    No CRs yet
!    !### get the cosmic ray distributions for the jet and background ###
!    !### scale the total number density by the code value ###
!    Ntot   = grido%probo%CRdenjet / grido%physo%nscale
!    Ntotbg = grido%probo%CRden / grido%physo%nscale
!                        
!    !### let's get the distribution across all bins ###
!    CALL CR_Distribute(grido%crso(tid), n, g, 1, grido%crso(tid)%npbins, Ntot, grido%probo%CRslopejet)
!    CALL CR_Distribute(grido%crso(tid), nbg, gbg, 1, grido%crso(tid)%npbins, Ntotbg, grido%probo%CRslope)

    njets = 1
    IF (self%dualjets) njets = 2

    !### loop over the entire grid (including boundaries) ###
    DO k = 1-nb, workp%nz+nb

       DO j = 1-nb, workp%ny+nb
        
          DO i = 1-nb, workp%nx+nb

             DO nc = 1, njets 

                !### determine the cluster centric distance ###
                xyz(1) = workp%x0 + REAL(i-1,WRD) * workp%dx
                xyz(2) = workp%y0 + REAL(j-1,WRD) * workp%dx
                xyz(3) = workp%z0 + REAL(k-1,WRD) * workp%dx

                !### recenter our position from the jet origin ###
                xyz(1) = xyz(1) - self%jetlocX
                xyz(2) = xyz(2) - self%jetlocY
                xyz(3) = xyz(3) - self%jetlocZ
!                xyz    = xyz - (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)

                !### rotate this position into the coordinates of the jet cylinder ###
                CALL self%rotate3d(xyz, self%AA, self%AAinv, .TRUE.)

                !### now compare our position with the extent of the jet cylinder ###
                dz = ABS(xyz(3))
                dr = SQRT(SUM(xyz(1:2)**2))
                
                !### if we're outside the cylinder there is nothing to do ###
                IF (dr .GT. dbuff .OR. dz .GT. dzbuff) CYCLE

                !### set our spatial ramps ###
                rramp = MIN(1.0_WRD, EXP(-5.0_WRD * MAX(0.0_WRD,(dr - djet)) / (workp%dx * self%nrbuff)))
                CALL self%SetZRamp(workp%dx, xyz(3), zramp)

                !### determine what angle we're at with respect to the cylinder theta axis ###
                !### I'll make this comment here: ATAN2 is the one to use here as it does not return degenerate answers ###
                !### for various combinations of X and Y.  look it up. ###
                theta = ATAN2(xyz(2),xyz(1))
                    
                !### get the 2d rotation matrices ###
                CALL self%GetRotate2d(theta)
                CALL self%GetInvRotate2d(theta)

                !### if we are inside the jet cylinder or along the sides of the jet cylinder ###
                !### we transition from reflecting to pure jet conditions ###
                IF (dz .LE. dzjet .OR. (dz .GT. dzjet .AND. dr .GT. djet)) THEN

                    !### we need to determine which zone is the zone we'll reflect conditions from ###

                    !### rotate our position into a frame where our X is normal to the cylinder side ###
                    xy = xyz(1:2)
                    CALL self%rotate2d(xy, self%A, self%Ainv, .TRUE.)
                    
                    !### now determine what position in this frame corresponds to a zone whose values we'll mirror ###
                    !### recall that X is our normal vector here ###
                    lxyz(1) = dbuff + (dbuff - dr)
                    lxyz(2) = xy(2)
                    lxyz(3) = xyz(3)
                    
                    !### rotate that position back into the jet frame and then the grid frame ###
                    CALL self%rotate2d(lxyz(1:2), self%A, self%Ainv, .FALSE.)
                    CALL self%rotate3d(lxyz, self%AA, self%AAinv, .FALSE.)
                    lxyz(1) = lxyz(1) + self%jetlocX
                    lxyz(2) = lxyz(2) + self%jetlocY
                    lxyz(3) = lxyz(3) + self%jetlocZ
!                    lxyz    = lxyz + (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)

                    !### determine the coordinates of that zone ###
                    dijk(:) = FLOOR( (lxyz - xyzLow) / workp%dx ) + 1

                    !### if we get our current location do another step ###
                    IF (i .EQ. dijk(1) .AND. j .EQ. dijk(2) .AND. k .EQ. dijk(3)) THEN

                       lxyz(1) = dbuff + (dbuff - dr) + workp%dx
                       lxyz(2) = xy(2)
                       lxyz(3) = xyz(3)

                       CALL self%rotate2d(lxyz(1:2), self%A, self%Ainv, .FALSE.)
                       CALL self%rotate3d(lxyz, self%AA, self%AAinv, .FALSE.)
                       lxyz(1) = lxyz(1) + self%jetlocX
                       lxyz(2) = lxyz(2) + self%jetlocY
                       lxyz(3) = lxyz(3) + self%jetlocZ
!                       lxyz    = lxyz + (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)

                       dijk(:) = FLOOR( (lxyz - xyzLow) / workp%dx ) + 1

                    END IF
                    
                    !### make certain those positions exist on our patch (we don't do extra communications) ###
                    IF ((dijk(1) .LT. 1-nb .OR. dijk(1) .GT. workp%nx+nb) .OR. (dijk(2) .LT. 1-nb .OR. dijk(2) .GT. workp%ny+nb) .OR. &
                        (dijk(3) .LT. 1-nb .OR. dijk(3) .GT. workp%nz+nb)) THEN
                        
                        CALL ERROR('Mod_Problem','Jet cylinder attempted to get values from a zone it cannot access, set jet origin well within the bounds of a patch')
                        
                    END IF
                    
                    !### copy all of those values (exclude the field, which is handled in the field generator below) ###
                    collarq(:) = workp%grid3d(dijk(1),dijk(2),dijk(3),:)
                    collarq(8) = collarq(8) - (0.5d0 / collarq(1)) * SUM(collarq(2:4)**2) - 0.5d0 * SUM(collarq(5:7)**2)
                    
                    !### get the momentum (well, velocity) ###
                    mom = collarq(2:4) / collarq(1)
                    
                    !### rotate those into the jet frame and then the cylinder normal frame ###
                    CALL self%rotate3d(mom, self%AA, self%AAinv, .TRUE.)
                    CALL self%rotate2d(mom(1:2), self%A, self%Ainv, .TRUE.)
                    
                    !### mirror the X momentum (the normal component) ###
                    mom(1)   = -mom(1)
                    vel(1:2) = 0.0_WRD
                    vel(3)   = zramp * self%jetvel * tramp

                    !### rotate those back into the grid frame ###
                    CALL self%rotate2d(mom(1:2), self%A, self%Ainv, .FALSE.)
                    CALL self%rotate3d(mom, self%AA, self%AAinv, .FALSE.)
                    CALL self%rotate2d(vel(1:2), self%A, self%Ainv, .FALSE.)
!                    vel = vel + 2.d0 * (DBLE(nc) - 1.5d0) * grido%probo%jetorbitvelvec(:,tid)   ! add on the orbital motion before the last rotation
                    CALL self%rotate3d(vel, self%AA, self%AAinv,.FALSE.)

                    !### update our values ###
                    workp%grid3d(i,j,k,1)   = rramp * self%jetrho + (1.0_WRD - rramp) * collarq(1)
                    workp%grid3d(i,j,k,2:4) = workp%grid3d(i,j,k,1) * (rramp * vel + (1.0_WRD - rramp) * mom)
                    workp%grid3d(i,j,k,8)   = (rramp * Pjet / gammam1 + (1.0_WRD - rramp) * collarq(8)) + &
                                              (0.5_WRD / workp%grid3d(i,j,k,1)) * SUM(workp%grid3d(i,j,k,2:4)**2) + &
                                              0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)

                    !### make the color ramp off ###
                    IF (workp%npass .GT. 0) THEN

                       workp%pass3d(i,j,k,1) = rramp
                       IF (dz .GT. dzjet) workp%pass3d(i,j,k,1) = rramp * tramp * rhosw

                    END IF 

!###    No CRs yet
!                    !### if CRs are on let's set their distribution to the background values (get's rid of any stuck hot spots around the cylinder) ###
!                    IF (grido%crso(tid)%iCR .GT. 0) THEN
!
!                        !### set the distribution spatially transitioning between the two ditributions ###
!                        grido%pass3d(i,j,k,2:2+grido%crso(tid)%npbins-1)                          = n * rramp + nbg * (1.d0 - rramp)
!                        grido%pass3d(i,j,k,2+grido%crso(tid)%npbins:2+2*grido%crso(tid)%npbins-1) = g * rramp + gbg * (1.d0 - rramp)
!			IF (dz .GT. dzjet) THEN
!
!			    grido%pass3d(i,j,k,2:2+grido%crso(tid)%npbins-1)                          = n * rramp * tramp + nbg * (1.d0 - rramp) * (1.d0 - tramp)
!                            grido%pass3d(i,j,k,2+grido%crso(tid)%npbins:2+2*grido%crso(tid)%npbins-1) = g * rramp * tramp + gbg * (1.d0 - rramp) * (1.d0 - tramp)
!
!			END IF
!
!                    END IF
!
                !### if we're in the jet cylinder's Z buffer things are tricky as we need to reflect as in the Z buffer ###
                !### when the jet is off, but need to transition to outflow when the jet is on. ###
                ELSE

                    !### set where in Z we'll look ###
                    lxyz(1:2) = xyz(1:2)
                    lxyz(3)   = SIGN(1.0_WRD, xyz(3)) * (dzbuff + (dzbuff-dz))

                    !### rotate that position back into the grid frame ###
                    CALL self%rotate3d(lxyz, self%AA, self%AAinv, .FALSE.)
                    lxyz(1) = lxyz(1) + self%jetlocX
                    lxyz(2) = lxyz(2) + self%jetlocY
                    lxyz(3) = lxyz(3) + self%jetlocZ
!                    lxyz    = lxyz + (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)

                    !### determine the coordinates of that zone ###
                    dijk(:) = FLOOR( (lxyz - xyzLow) / workp%dx ) + 1

                    !### if we get our current location do another step ###
                    IF (i .EQ. dijk(1) .AND. j .EQ. dijk(2) .AND. k .EQ. dijk(3)) THEN

                        lxyz(1:2) = xyz(1:2)
                        lxyz(3)   = SIGN(1.0_WRD, xyz(3)) * (dzbuff + (dzbuff-dz) + workp%dx)

                        CALL self%rotate3d(lxyz, self%AA, self%AAinv, .FALSE.)
                        lxyz(1) = lxyz(1) + self%jetlocX
                        lxyz(2) = lxyz(2) + self%jetlocY
                        lxyz(3) = lxyz(3) + self%jetlocZ
!                        lxyz    = lxyz + (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)

                        dijk(:) = FLOOR( (lxyz - xyzLow) / workp%dx ) + 1

                    END IF

                    !### make certain those positions exist on our patch (we don't do extra communications) ###
                    IF ((dijk(1) .LT. 1-nb .OR. dijk(1) .GT. workp%nx+nb) .OR. (dijk(2) .LT. 1-nb .OR. dijk(2) .GT. workp%ny+nb) .OR. &
                        (dijk(3) .LT. 1-nb .OR. dijk(3) .GT. workp%nz+nb)) THEN
                        
                        CALL ERROR('Mod_Problem','Jet cylinder attempted to get values from a zone it cannot access, set jet origin well within the bounds of a patch')
                        
                    END IF

                    !### copy all of those values (exclude the field, which is handled in the field generator below) ###
                    collarq(:) = workp%grid3d(dijk(1),dijk(2),dijk(3),:)
                    collarq(8) = collarq(8) - (0.5_WRD / collarq(1)) * SUM(collarq(2:4)**2) - 0.5_WRD * SUM(collarq(5:7)**2)
                    
                    !### get the momentum (well, velocity) ###
                    mom = collarq(2:4) / collarq(1)
                    
                    !### rotate those into the jet frame and then the cylinder normal frame ###
                    CALL self%rotate3d(mom, self%AA, self%AAinv, .TRUE.)
                    CALL self%rotate2d(mom(1:2), self%A, self%Ainv, .TRUE.)
                    
                    !### mirror the Z momentum (the normal component) ###
                    mom(3)   = -mom(3)
                    
                    !### rotate those back into the grid frame ###
                    CALL self%rotate2d(mom(1:2), self%A, self%Ainv, .FALSE.)
                    CALL self%rotate3d(mom, self%AA, self%AAinv, .FALSE.)

                    !### get the gas pressure and velocity of the current zone ###
                    p   = workp%grid3d(i,j,k,8) - (0.5_WRD  / workp%grid3d(i,j,k,1)) * SUM(workp%grid3d(i,j,k,2:4)**2) - 0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)
                    vel = workp%grid3d(i,j,k,2:4) / workp%grid3d(i,j,k,1)

                    !### now set the zones values as a time ramp between the reflecting conditions and what they are integrated to ###
                    !### note that i ramp velocity.  this does a better job of ensuring there are no spikes in jet luminosity ###
                    workp%grid3d(i,j,k,1)   = workp%grid3d(i,j,k,1) * rhosw + collarq(1) * (1.0_WRD - rhosw)
                    workp%grid3d(i,j,k,2:4) = workp%grid3d(i,j,k,1) * (vel(:) * tramp + mom(:) * (1.0_WRD - tramp))
                    workp%grid3d(i,j,k,8)   = (p * tramp + collarq(8) * (1.0_WRD - tramp)) + &
                                              0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1) + &
                                              0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2)

                 
                    !### make the color ramp to the value it wants over time ###
                    IF (workp%npass .GT. 0) workp%pass3d(i,j,k,1) = tramp * rhosw 


!###    No CRs yet
!                    !### if CRs are on let's set their distribution ###
!                    IF (grido%crso(tid)%iCR .GT. 0) THEN
!
!                        !### set the distribution temporally transitioning between the two ditributions ###
!                        grido%pass3d(i,j,k,2:2+grido%crso(tid)%npbins-1)                          = n * tramp + nbg * (1.d0 - tramp)
!                        grido%pass3d(i,j,k,2+grido%crso(tid)%npbins:2+2*grido%crso(tid)%npbins-1) = g * tramp + gbg * (1.d0 - tramp)
!
!                    END IF

                END IF

             END DO

          END DO

       END DO

    END DO

END SUBROUTINE JetDriving

!### SetTRamp will return a number between 0 and 1 that defines the ramp to a full ON or OFF position for the jet ###
SUBROUTINE SetTRamp(self, workp, tramp)

    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
    REAL(WRD), INTENT(INOUT) :: tramp
!### No CRs yet 
!    REAL(WRD) :: n(workp%crso(1)%npbins), g(workp%crso(1)%npbins), Ntot
    REAL(WRD) :: nextoff, sw, maxsignal, jetfreq, jetorbitspeed, pi
    REAL(WRD) :: nexton

    pi = 2.0_WRD * DASIN(1.0_WRD)

    !### get the setup data for the jet ###
    nexton  = DBLE(FLOOR(workp%t / self%jetperiod))*self%jetperiod + self%jetoprest
    nextoff = nexton + self%jetdutycycle * self%jetperiod

    !### if we've done all of the periods we want, set things so that the jet never turns ON again ###
    IF (FLOOR(workp%t / self%jetperiod) .GE. self%jetnperiods) THEN

       nexton  = 1.0E8_WRD  ! way off into the future
       nextoff = nexton + self%jetdutycycle * self%jetperiod

    END IF

    !### define our default min wave speed ###
    maxsignal = self%jetvel + SQRT(self%gamma * self%jetpres / self%jetrho)

    !### we'll use a piece-wise function to switch between ON and OFF curves ###
    sw = 1.0_WRD
    IF (workp%t .GE. 0.5_WRD * (nexton + nextoff)) sw = 0.0_WRD

    !### we'll use a TANH to ramp the jet.  it works nicely with the delayed dt reduction ###
    tramp = sw * MAX(0.0_WRD, TANH(2.0_WRD*(workp%t - nexton) / self%jetblendt)) + &
               (1.0_WRD - sw) * MAX(0.0_WRD, -TANH(2.0_WRD*(workp%t - nextoff) / self%jetblendt))
!IF (WRANK .EQ. 0) print *, "TRAMP = ", tramp

    !### Don't worry about jet signal for now
    !### to warn the time step reduction pipeline of upcoming injection, define a minimum wave speed ###
    IF (workp%t .LE. (self%jetoprest + self%jetblendt)) THEN

        self%maxsignal = maxsignal

    ELSE IF (workp%t .GE. (nexton - self%jetblendt) .AND. workp%t .LE. (nexton + self%jetblendt)) THEN

        !### we ramp this up ahead of TRamp ###
        self%maxsignal = maxsignal * MIN(tramp+0.5_WRD, 1.0_WRD)

    ELSE

        self%maxsignal = 0.0_WRD

    END IF

!### no jet orbit for now
!    !### define the orientation of the orbit vector for the jets.  this is defined as a vector from the center of one cylinder to the center of the other ###
!    jetfreq = 2.0_WRD * pi / self%jetorbitperiod
!    self%jetorbitvec(3,tid) = 0.0_WRD ! in the jet frame there is no Z component
!    self%jetorbitvec(1,tid) = self%jetseparation * DCOS((workp%t-self%jetoprest) * jetfreq)
!    self%jetorbitvec(2,tid) = self%jetseparation * DSIN((workp%t-self%jetoprest) * jetfreq)
!
!    !### I need an orthogonal vector to define a the orbital velocity vector ###
!    !### determine the orbital speed (LEAVE THIS IN THE JET FRAME SINCE WE'LL ROTATE IT LATER) ###
!    jetorbitspeed = pi * self%jetseparation / self%jetorbitperiod
!    self%jetorbitvelvec(3,tid) = 0.0_WRD ! no motion in Z in jet frame
!    self%jetorbitvelvec(1,tid) = jetorbitspeed * -DSIN((workp%t-self%jetoprest) * jetfreq)
!    self%jetorbitvelvec(2,tid) = jetorbitspeed * DCOS((workp%t-self%jetoprest) * jetfreq) 
!
!    !### rotate this back to grid coordinates ###
!    CALL self%Rotate3d(self%jetorbitvec(:,tid), .FALSE.)
!
    RETURN

END SUBROUTINE SetTRamp

!### SetZRamp defines the ramping profile along the jet axis ###
SUBROUTINE SetZRamp(self, dx, z, zramp)

    CLASS(Problem) :: self
    REAL(WRD), INTENT(IN) :: dx, z
    REAL(WRD), INTENT(INOUT) :: zramp
    REAL(WRD) :: dzjet

    !### define the length of the jet cylinder ###
    dzjet = dx * REAL(self%nljet, WRD)

    !### use a tanh with an extra stretch to make certain we get to one near the extent of the cylinder ###
    !### 1.8 was trial and error done by minimizing or eliminating protection flux replacements ###
    zramp = TANH(1.8_WRD * (z / dzjet)**3)

    RETURN

END SUBROUTINE  SetZRamp

!### GetJetPressure samples the jet cylinder surroundings and determines what the jet pressure should be at launching ###
SUBROUTINE GetJetPressure(self, workp, Pjet)
  
    CLASS(Problem) :: self
    TYPE(Patch), INTENT(INOUT) :: workp
    REAL(WRD), INTENT(INOUT) :: Pjet
    REAL(WRD) :: InvRot2d(2,2), dz, dr, dpressr, dpressz, gammam1, &
              dbuff, djet, dzjet, dzbuff, xyz(3), p, tramp
    REAL(WRD) :: jetP
    INTEGER :: i, j, k, nb, npz, np

    !### set the physical size of the jet buffer ###
    dbuff   = workp%dx * DBLE(self%nrjet)
    djet    = workp%dx * DBLE(self%nrjet - self%nrbuff)
    dzjet   = workp%dx * DBLE(self%nljet)
    dzbuff  = dzjet + workp%dx * DBLE(self%nrbuff)
    dpressr = dbuff + workp%dx * DBLE(self%jetpresr)
    dpressz = dzbuff + workp%dx * DBLE(self%jetpresr)
    gammam1 = self%gamma - 1.0_WRD

    !### define the temporal jet amplitude for this time ###
    CALL self%SetTRamp(workp, tramp)

    !### get the number of boundary zones ###
    nb = workp%nb

    !### initialize or pressure averaging ###
    self%pressuret = 0.0_WRD
    npz            = 0

    !### first we need to determine the pressure we'll use inside of the jet ####
    !### it should be in approximate pressure equlibrium with the surroundings ###
    DO k = 1-nb, workp%nz+nb

       DO j = 1-nb, workp%ny+nb
        
          DO i = 1-nb, workp%nx+nb

                !### determine the cluster centric distance ###
                xyz(1) = workp%x0 + REAL(i-1,WRD) * workp%dx
                xyz(2) = workp%y0 + REAL(j-1,WRD) * workp%dx
                xyz(3) = workp%z0 + REAL(k-1,WRD) * workp%dx

                !### recenter our position from the jet origin ###
                xyz(1) = xyz(1) - self%jetlocX
                xyz(2) = xyz(2) - self%jetlocY
                xyz(3) = xyz(3) - self%jetlocZ
!                xyz    = xyz - (DBLE(nc) - 1.5d0) * self%jetorbitvec(:,tid)
                
                !### rotate this position into the coordinates of the jet cylinder ###
                CALL self%rotate3d(xyz, self%AA, self%AAinv, .TRUE.)

                !### now compare our position with the extent of the jet cylinder ###
                dz = ABS(xyz(3))
                dr = SQRT(SUM(xyz(1:2)**2))
                
                !### if we're not inside the averging volume move on ###
                IF (dr .GT. dpressr .OR. dr .LE. dbuff .OR. dz .GT. dpressz .OR. dz .LE. dzbuff) CYCLE

                !### determine the pressure here ###
                p = gammam1 * (workp%grid3d(i,j,k,8) - 0.5_WRD * SUM(workp%grid3d(i,j,k,2:4)**2) / workp%grid3d(i,j,k,1) - &
                               0.5_WRD * SUM(workp%grid3d(i,j,k,5:7)**2))

                !### add it to our average ###
                self%pressuret = self%pressuret + p
                npz            = npz + 1

            END DO

        END DO

    END DO

    !### find our local value ###
    self%pressuret = self%pressuret / REAL(MAX(npz, 1), WRD)

    !### we ramp between the local pressure and an asymptotic final pressure ###
    jetP = self%jetpres * tramp + MAX(1.0E-50_WRD, self%pressuret) * (1.0_WRD - tramp)

    !### set our value ###
    Pjet = jetP

END SUBROUTINE GetJetPressure

SUBROUTINE GetRotate2d(self, theta)

    CLASS(Problem) :: self
    REAL(WRD), INTENT(IN) :: theta

    self%A(1,1) = COS(theta)
    self%A(1,2) = -SIN(theta)
    self%A(2,1) = SIN(theta)
    self%A(2,2) = COS(theta)

END SUBROUTINE GetRotate2d

SUBROUTINE GetInvRotate2d(self, theta)

    CLASS(Problem) :: self
    REAL(WRD), INTENT(IN) :: theta
    
    self%Ainv(1,1) = COS(theta)
    self%Ainv(1,2) = SIN(theta)
    self%Ainv(2,1) = -SIN(theta)
    self%Ainv(2,2) = COS(theta)

END SUBROUTINE GetInvRotate2d

END MODULE Mod_Problem
