#include "config.h"

Module Mod_DM_Manager

!######################################################################
!#
!# FILENAME: mod_dm_manager.f90
!#
!# DESCRIPTION: This module manages a set of of DM block managers and is the one pulled into a Patch object.  It's job is to route particles into the block managers based on their population information.
!# It is also the front-end to moving particles between Patches.  
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    9/8/14  - Brian O'Neill
!#
!######################################################################

!### require the needed modules ###
USE Mod_DM
Use Mod_DMBlock_Manager
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: DM_Manager

!### define a data type for this module ###
TYPE :: DM_Manager

    !### default everything as private ###
    PRIVATE

    !### keep a list of block managers
    TYPE(DMBlock_Manager), PUBLIC, ALLOCATABLE :: dmbm(:)

    REAL(WRD), PUBLIC ::  xLow, xHi, yLow, yHi, zLow, zHi

    !### DM_buff_fac - tunes size of particle buffer
    REAL(WRD), PUBLIC :: DM_buff_fac

    !### needed for collecting mass of particles in boundary zones
    REAL(WRD), PUBLIC, ALLOCATABLE :: OOBmass(:)

    !### offsets to divide OOBmass up amongst neighbors
    INTEGER(WIS), PUBLIC, ALLOCATABLE :: massoffs(:)

    !### accounting ###
    !### ntotbms = # of DM block managers we will manage, nactive = # of active block managers, ndmspbm = # of DM blocks each active block manager can manage ###
    !### nslotspdm = # of particles a DM block can hold, navailslots = # of potential available slots across all active block managers (ie. nopenslots in each active DM across active BMs + nslotspdm * # of DMs available to build in active BMs) ((this is probably unneccessary))
    !### nstepspdefrag = # of time steps between calls to defragment block managers, nstepsuntildefrag = # of steps until next defrag
    INTEGER(WIS), PUBLIC :: ndim, ntotbms, nactive, ndmspbm, nslotspdm, navailslots, nstepspdefrag, nstepsuntildefrag
    !### isLive = object is constructed, hasDMBMs = object has active block managers, hasDMBMs = object has allocated DMBMs, hasOOBslots = patch has particles flagged out-of-bounds that must be moved to a neighbor, gotParticles - patch has accepted particles from a neighbor during the current update_pass
    LOGICAL(WIS), PUBLIC :: isLive, hasDMBMs, hasOOBslots, gotParticles

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE :: initdm_manager
    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: add_particles
    PROCEDURE, PUBLIC :: defragment
    PROCEDURE, PUBLIC :: find_out_of_bounds
    PROCEDURE, PUBLIC :: set_hasOOBslots_flag
    PROCEDURE, PUBLIC :: copy_MPI_buffer_out
    PROCEDURE, PUBLIC :: copy_MPI_buffer_in
    PROCEDURE, PUBLIC :: build
    PROCEDURE, PUBLIC :: unbuild

END TYPE DM_Manager

INTERFACE DM_Manager

    MODULE PROCEDURE constructor

END INTERFACE DM_Manager

!### object methods ###
CONTAINS

!### a constructor for the class ###
FUNCTION constructor(ndim, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, xLow, xHi, yLow, yHi, zLow, zHi, DM_buff_fac)

    TYPE(DM_Manager) :: constructor
    INTEGER(WIS), INTENT(IN) :: ndim, ntotbms, ndmspbm, nslotspdm, nstepspdefrag
    REAL(WRD), INTENT(IN) :: xLow, xHi, yLow, yHi, zLow, zHi, DM_buff_fac

    CALL constructor%initdm_manager(ndim, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, xLow, xHi, yLow, yHi, zLow, zHi, DM_buff_fac)
    RETURN

END FUNCTION constructor

!### actual initialization of object (not directly called by user) ###
SUBROUTINE initdm_manager(self, ndim, ntotbms, ndmspbm, nslotspdm, nstepspdefrag, xLow, Xhi, yLow, yHi, zLow, zHi, DM_buff_fac)

    CLASS(DM_Manager) :: self
    INTEGER(WIS), INTENT(IN) :: ndim, ntotbms, ndmspbm, nslotspdm, nstepspdefrag
    REAL(WRD), INTENT(IN) :: xLow, xHi, yLow, yHi, zLow, zHi, DM_buff_fac

    self%ndim              = ndim
    self%ntotbms           = ntotbms
    self%ndmspbm           = ndmspbm
    self%nslotspdm         = nslotspdm
    self%nstepspdefrag     = nstepspdefrag
    self%nstepsuntildefrag = nstepspdefrag
    self%navailslots       = 0
    self%nactive           = 0
    self%xLow              = xLow
    self%xHi               = xHi
    self%yLow              = yLow
    self%yHi               = yHi
    self%zLow              = zLow
    self%zHi               = zHi
    self%DM_buff_fac       = DM_buff_fac
    
    !### register as Live, but with no allocated BMs or DMs
    self%isLive         = .TRUE.
    self%hasDMBMs       = .FALSE.
    self%hasOOBslots    = .FALSE.
    self%gotParticles   = .FALSE.

END SUBROUTINE initdm_manager

!### create DMBM list, but do not build individual block managers (deferred to as-needed), allocate OOBmass buffer
SUBROUTINE build(self, nx, ny, nz, nb)

    CLASS(DM_Manager) :: self
    INTEGER(WIS), INTENT(IN) :: nx, ny, nz, nb


    IF (self%isLive .AND. .NOT. self%hasDMBMs) THEN
       ALLOCATE(self%dmbm(self%ntotbms))
       self%hasDMBMs = .TRUE.
    END IF

    !### allocate out of bounds mass array and define offsets
    IF (.NOT. ALLOCATED(self%OOBmass)) THEN

        SELECT CASE(self%ndim)
        CASE(1)

            ALLOCATE(self%OOBmass(2*nb), self%massoffs(2))
            self%massoffs(:) = (/ 0, nb /)

        CASE(2)


            ALLOCATE(self%OOBmass(2*(nx+ny+2*nb)*nb), self%massoffs(8))
            self%massoffs(1:3) = (/ 0              , ny*nb              , 2*ny*nb           /)
            self%massoffs(4:6) = (/ (2*ny+nx)*nb   , 2*(nx+ny)*nb       , (2*(ny+nx)+nb)*nb /)
            self%massoffs(7:8) = (/ 2*(ny+nx+nb)*nb, (2*(nx+ny)+3*nb)*nb /)

        CASE(3)

            ALLOCATE(self%OOBmass(2*(ny*nz+nx*nz+nx*ny)*nb+8*nb**3+4*(nx+ny+nz)*nb**2), self%massoffs(26))

            self%massoffs(1:5)   = (/ 0, ny*nz*nb, 2*ny*nz*nb, (2*ny*nz+nx*nz)*nb, 2*(ny*nz+nx*nz)*nb /)
            self%massoffs(6:7)   = self%massoffs(5)  + nb*nx*ny * (/ 1, 2 /)
            self%massoffs(8:15)  = self%massoffs(7)  + nb**3 * (/ 1, 2, 3, 4, 5, 6, 7, 8 /)
            self%massoffs(16:19) = self%massoffs(15) + nb**2*nx * (/ 1, 2, 3, 4 /)
            self%massoffs(20:23) = self%massoffs(19) + nb**2*ny * (/ 1, 2, 3, 4 /)
            self%massoffs(24:26) = self%massoffs(23) + nb**2*nz * (/ 1, 2, 3 /)

        END SELECT

    END IF


END SUBROUTINE build
SUBROUTINE unbuild(self)

    CLASS(DM_Manager) :: self

    IF (self%isLive .AND. self%hasDMBMs) THEN
       DEALLOCATE(self%dmbm)
       self%hasDMBMs = .FALSE.
    END IF

    IF ( ALLOCATED(self%OOBmass) ) THEN
        DEALLOCATE(self%OOBmass, self%massoffs)
    END IF

END SUBROUTINE unbuild

SUBROUTINE destroy(self)

    CLASS(DM_Manager) :: self

    CALL self%unbuild()
    self%isLive = .FALSE.

END SUBROUTINE destroy

!### this routine is meant to be called during problem initialization. Enough block managers are built to contain all particles in 
!    the input particlelist, then particles are passed to each dmbm to be stored.
SUBROUTINE add_particles(self, particlelist)

    CLASS(DM_Manager) :: self
    REAL(WRD), INTENT(IN) :: particlelist(:,:) !### indices : 1-particle number; 2-pos, vel, acc ###
    INTEGER(WIS) :: ntotlist, newbms, nsublist, passes2go
    INTEGER(WIS) :: ii, bmi                    !### ii - input particle index, bmi - block manager index

    ntotlist = SIZE(particlelist,1)
    passes2go = ntotlist

    newbms = CEILING(REAL(ntotlist - self%navailslots, WRD) / REAL(self%nslotspdm * self%ndmspbm, WRD))
    IF (newbms .GT. self%ntotbms) THEN
        ASSERT(.FALSE., 'Ran out of dark matter block managers')
    END IF    


    DO bmi = self%nactive + 1, self%nactive + newbms
       self%dmbm(bmi) = DMBlock_Manager(self%ndim, self%ndmspbm, self%nslotspdm, &
           self%xLow, self%xHi, self%yLow, self%yHi, self%zLow, self%zHi)
       CALL self%dmbm(bmi)%build()
    END DO
    self%nactive = self%nactive + newbms
    self%navailslots = self%navailslots + newbms * self%nslotspdm

    !### loop through block managers, determine whether we will fill each dmbm or not, call self%dmbm(bmi)%add_particles(sublist)
    ii = 0

    DO bmi = 1, self%nactive
       nsublist = MIN(passes2go,self%dmbm(bmi)%navailslots)
       CALL self%dmbm(bmi)%add_particles(particlelist(ii+1:ii+nsublist,:), nsublist, .TRUE.)
       ii = ii + nsublist
       passes2go = passes2go - nsublist
    END DO

    self%navailslots = self%navailslots - ntotlist

END SUBROUTINE add_particles

!### loop through block managers, just calls dmbm%defrag() for each block manager, does not defragment and destroy empty block managers themselves
SUBROUTINE defragment(self)

    CLASS(DM_Manager) :: self
    INTEGER(WIS) :: bmi     !### bmi - block manager index

    !### no op if empty
    IF (.NOT. self%hasDMBMs) RETURN

    DO bmi = 1, self%nactive
       IF (.NOT. self%dmbm(bmi)%isFilled) THEN
          CALL self%dmbm(bmi)%defragment()
       END IF
    END DO

END SUBROUTINE defragment

!### after particle positions have been updated, this is called to identify the OOB particles in each dmbm
SUBROUTINE find_out_of_bounds(self, rankloc, simunits)

    CLASS(DM_Manager) :: self
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS) :: bmi

    self%hasOOBslots = .FALSE.
    IF (.NOT. self%hasDMBMs) RETURN

    DO bmi = 1,self%nactive
       CALL self%dmbm(bmi)%find_out_of_bounds(rankloc, simunits)
       IF (.NOT. self%hasOOBslots .AND. SUM(self%dmbm(bmi)%lens) .GT. 0) THEN
          self%hasOOBslots = .TRUE.
       END IF

    END DO


END SUBROUTINE find_out_of_bounds
!### need to reset the hasOOBslots flag for flavor=6 at the beginning of each update pass
SUBROUTINE set_hasOOBslots_flag(self)

    CLASS(DM_Manager) :: self
    INTEGER(WIS) :: bmi

    self%hasOOBslots = .FALSE.
    IF (.NOT. self%hasDMBMs) RETURN

    DO bmi = 1, self%nactive
        IF (SUM(self%dmbm(bmi)%lens) .GT. 0) THEN
            self%hasOOBslots = .TRUE.
            RETURN
        END IF
    END DO

END SUBROUTINE set_hasOOBslots_flag

!### called by the patch for each neighboring destination, therefore routine searches out-buffers of each active block manager for
!    particles bound for the input destination
SUBROUTINE copy_MPI_buffer_out(self, buffer, buffer_length, dest)

    CLASS(DM_Manager) :: self
    INTEGER(WIS), INTENT(INOUT) :: buffer_length       !### buffer_length - on input, gives buffer length; on output # of elements copied into buffer
    REAL(WRD), INTENT(INOUT) :: buffer(buffer_length)  !### buffer to fill with elements bound for neighbor ###
    INTEGER(WIS), INTENT(IN) :: dest                   !### dest - neighbor we are collecting particles to send to ###
    INTEGER(WIS) :: max_buffer, copy_length, length2pass, nvars, lastbm, n2pass, curoff
    INTEGER(WIS) :: bmi     !### bmi  - block manager index ###
    LOGICAL(WIS) :: buffer_filled

    max_buffer = buffer_length
    buffer_length = 1
    buffer(1) = 0.0_WRD

    !### no op if no particles out-of-bounds
    IF (.NOT. self%hasOOBslots) RETURN

    !### assume we have room in buffer to take care of all out-of-bounds particles for this dest
    buffer_filled = .FALSE.

    length2pass = 0

    DO bmi = 1, self%nactive
        length2pass = length2pass + self%dmbm(bmi)%lens(dest)
        IF (length2pass .GE. max_buffer-1) THEN
            length2pass = max_buffer-1
            EXIT
        END IF
    END DO

    nvars = 3 * self%ndim
    lastbm = MIN(bmi, self%nactive)
    n2pass = length2pass / nvars

    !### 1st slot in buffer stores # of particles packed into buffer
    buffer(1) = REAL(n2pass, WRD)
    self%navailslots = self%navailslots + n2pass
    curoff = 1

    !### loop through block managers
    DO bmi = 1, lastbm
       !### check if this dmbm has OOB particles
       IF (self%dmbm(bmi)%lens(dest).GT.0) THEN
          copy_length = MIN(self%dmbm(bmi)%lens(dest), max_buffer - buffer_length)
          buffer_length = buffer_length + copy_length

          !### if we have run out of space in buffer, this patch still has particles bound for dest to copy out on next update_pass
          IF (copy_length .LT. self%dmbm(bmi)%lens(dest))   buffer_filled = .TRUE.

          CALL self%dmbm(bmi)%copy_MPI_buffer_out(dest, buffer, n2pass, curoff)

          IF (buffer_filled) RETURN
          curoff = curoff + copy_length / nvars

       END IF
    END DO


END SUBROUTINE copy_MPI_buffer_out

!### loop over active block managers, available space in each block manager determines what chunk of buffer is passed to each 
!    block manager's copy_MPI_buffer_in routine. build extra block managers if more space is needed, error if we run out of block managers
SUBROUTINE copy_MPI_buffer_in(self, buffer, update_pass_count, success)

    CLASS(DM_Manager) :: self
    REAL(WRD), INTENT(IN) :: buffer(:)
    LOGICAL(WIS), INTENT(OUT) :: success                     !### flag false if run out of block managers
    INTEGER(WIS), INTENT(IN) :: update_pass_count
    INTEGER(WIS) :: totpass, npassed, thispass, passes2go, nvars
    LOGICAL(WIS) :: first_pass
    INTEGER(WIS) :: bmi, ii
    
    !### first slot in buffer contains # of particles being passed
    totpass = NINT(buffer(1), WIS)
    passes2go = totpass
    npassed = 0
    bmi = 1

    !### assume we have enough space so all particles will be passed
    success = .TRUE.

    !### if this is the first time we are copying in particles during this update_pass, update gotParticles flag
    IF (totpass .GT. 0 .AND. .NOT. self%gotParticles) THEN
        self%gotParticles = .TRUE.
    END IF


    !### assume this is the first_pass
    first_pass = .TRUE.
    IF (update_pass_count .GT. 0)   first_pass = .FALSE.

    DO WHILE (npassed .LT. totpass)
       IF (bmi .GT. self%ntotbms) THEN
          !### no more block managers available, generates error
          success  = .FALSE.
          RETURN
       END IF
       !### build another block manager if needed
       IF (bmi .GT. self%nactive) THEN
          self%dmbm(bmi) = DMBlock_Manager(self%ndim, self%ndmspbm, self%nslotspdm, &
                           self%xLow, self%xHi, self%yLow, self%yHi, self%zLow, self%zHi)
          self%nactive = self%nactive + 1
          self%navailslots = self%navailslots + self%nslotspdm * self%ndmspbm
       END IF
       thispass = MIN(totpass-npassed, self%dmbm(bmi)%navailslots)
       IF (.NOT. self%dmbm(bmi)%isFilled) THEN
          CALL self%dmbm(bmi)%copy_MPI_buffer_in(buffer(2:), totpass, npassed, thispass, first_pass)
       END IF
       npassed = npassed + thispass
       bmi = bmi + 1
    END DO

    self%navailslots = self%navailslots - totpass

END SUBROUTINE copy_MPI_buffer_in

END MODULE Mod_DM_Manager
