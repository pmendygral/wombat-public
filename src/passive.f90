#include "config.h"

MODULE Mod_Passive

!######################################################################
!#
!# FILENAME: mod_mhdtvd.f90
!#
!# DESCRIPTION: This module 
!#
!# DEPENDENCIES: Mod_MHDTVD for defining data types
!#
!# HISTORY:
!#    9/2/2015  - Peter Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_Patch
USE Mod_PassivePool

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Passive

!### define a data type for this module ###
TYPE :: Passive

    !### default everything as private ###
    PRIVATE

    INTEGER(WIS), PUBLIC :: channel_low, channel_hi  ! keep track of the range of passive channels this object will update
    LOGICAL(WIS), PUBLIC :: is_compressive

    CONTAINS
    PRIVATE

    PROCEDURE, PUBLIC :: initpass
    PROCEDURE, PUBLIC :: passive_solve
    PROCEDURE :: passive_solve1d
    PROCEDURE :: passive_solve2d
    PROCEDURE :: passive_solve3d
    PROCEDURE :: compute_slopes1d
    PROCEDURE :: van_leerflux1d
    PROCEDURE :: van_leer1d
    PROCEDURE :: van_leer_halfXupdate2d
    PROCEDURE :: van_leer_halfYupdate2d
    PROCEDURE :: computeXYfluxes2d
    PROCEDURE :: computeXYfluxes3d
    PROCEDURE :: computeZfluxes3d
    PROCEDURE :: van_leer2d
    PROCEDURE :: van_leer_halfXupdate3d
    PROCEDURE :: van_leer_halfYupdate3d
    PROCEDURE :: van_leer_halfZupdate3d
    PROCEDURE :: van_leer3d

END TYPE Passive

INTERFACE Passive

    MODULE PROCEDURE constructor

END INTERFACE

!### object methods ###
CONTAINS

!### constructor ###
FUNCTION constructor(namelist, cl, ch, compressive)

    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: cl, ch
    LOGICAL(WIS), INTENT(IN) :: compressive
    TYPE(Passive) :: constructor

    CALL constructor%initpass(namelist, cl, ch, compressive)
    RETURN

END FUNCTION constructor

!### initialize the object ###
SUBROUTINE initpass(self, namelist, cl, ch, compressive)

    CLASS(Passive) :: self
    CHARACTER(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: cl, ch
    LOGICAL(WIS), INTENT(IN) :: compressive

    self%channel_low    = cl
    self%channel_hi     = ch
    self%is_compressive = compressive

    ! no namelist parameters yet.  maybe some later

END SUBROUTINE initpass

!### top-level interface for performing a VanLeer update ###
SUBROUTINE passive_solve(self, workp, pool, maxsignal, update_cmplt)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    REAL(WRD), INTENT(INOUT) :: maxsignal
    LOGICAL(WIS), INTENT(OUT) :: update_cmplt

    !### it takes only a single pass to complete the passive update ###
    update_cmplt  = .TRUE.
    pool%max_wave = 0.0_WRD

    !### based on the number of dimensions we'll forward this to the appropriate solver ###
    IF (workp%is1d) THEN

        CALL self%passive_solve1d(workp, pool)

    ELSE IF (workp%is2d) THEN

        CALL self%passive_solve2d(workp, pool)

    ELSE IF (workp%is3d) THEN

        CALL self%passive_solve3d(workp, pool)

    END IF

    !### possibly update the maximum signal value with our computed maximum wave speed ###
    maxsignal = MAX(maxsignal, pool%max_wave)

END SUBROUTINE passive_solve

!### run the 1d update ###
SUBROUTINE passive_solve1d(self, workp, pool)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS) :: ilow, ihi, lilow, lihi, c

    pool%dt = workp%dt
    pool%dx = workp%dx
    ilow    = 1 + workp%nb - 2
    ihi     = workp%nx + workp%nb + 2
    lilow   = 1 - 2
    lihi    = workp%nx + 2

    !### pull out just the X time-averaged velocity ###
    pool%vtavg2d(:,1) = workp%vtavg1d(:,1)

    !### loop over all variables we will work on ###
    DO c = self%channel_low, self%channel_hi

        !### place the passive values into our work array ###
        pool%pwork2d(:,1) = workp%pass1d(:,c)

        !### compute the slopes ###
        CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, ilow, ihi, 1, 1)

        !### compute the flux ###
        CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%flux2d, pool%slope2d, ilow, ihi, 1, 1, pool%dt, pool%dx, 1)

        !### perform the update ###
        CALL self%van_leer1d(workp%pass1d(lilow:lihi,c), pool%vtavg2d(lilow:lihi,1), pool%flux2d(lilow:lihi,1,1), lilow, lihi, pool%dt, pool%dx)

    END DO

END SUBROUTINE passive_solve1d

!### run the 2d update ###
SUBROUTINE passive_solve2d(self, workp, pool)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS) :: ilow, ihi, lilow, lihi, jlow, jhi, ljlow, ljhi, c, i, j

    pool%dt = workp%dt
    pool%dx = workp%dx

    !### loop over all variables we will work on ###
    DO c = self%channel_low, self%channel_hi

        !### phase 1 is to compute fluxes needed for a preconditioned step ###

        !### set our array spans ###
        ilow  = 1 + workp%nb - 4
        ihi   = workp%nx + workp%nb + 4
        lilow = 1 - 4
        lihi  = workp%nx + 4
        jlow  = 1 + workp%nb - 4
        jhi   = workp%ny + workp%nb + 4
        ljlow = 1 - 4
        ljhi  = workp%ny + 4

        CALL self%computeXYfluxes2d(workp, pool, c, ilow, ihi, jlow, jhi, lilow, lihi, ljlow, ljhi, .FALSE.)

        !### phase 2 is to use those fluxes to precondition and then compute final fluxes ###

        !### set our array spans ###
        ilow  = ilow + 2
        ihi   = ihi - 2
        lilow = lilow + 2
        lihi  = lihi - 2
        jlow  = jlow + 2
        jhi   = jhi - 2
        ljlow = ljlow + 2
        ljhi  = ljhi - 2

        CALL self%computeXYfluxes2d(workp, pool, c, ilow, ihi, jlow, jhi, lilow, lihi, ljlow, ljhi, .TRUE.)
        
        !### perform the final un-split update ###
        CALL self%van_leer2d(workp%pass2d, workp%vtavg2d, pool%flux2d, c, ilow, ihi, jlow, jhi, pool%dt, pool%dx)

    END DO

END SUBROUTINE passive_solve2d

!### run the 3d update ###
SUBROUTINE passive_solve3d(self, workp, pool)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS) :: ilow, ihi, lilow, lihi, jlow, jhi, ljlow, ljhi, klow, khi, lklow, lkhi, c, i, j, k

    pool%dt = workp%dt
    pool%dx = workp%dx

    !### loop over all variables we will work on ###
    DO c = self%channel_low, self%channel_hi

        !### phase 1 is to compute fluxes needed for a preconditioned step ###

        !### set our array spans ###
        ilow  = 1 + workp%nb - 4
        ihi   = workp%nx + workp%nb + 4
        lilow = 1 - 4
        lihi  = workp%nx + 4
        jlow  = 1 + workp%nb - 4
        jhi   = workp%ny + workp%nb + 4
        ljlow = 1 - 4
        ljhi  = workp%ny + 4
        klow  = 1 + workp%nb - 4
        khi   = workp%nz + workp%nb + 4
        lklow = 1 - 4
        lkhi  = workp%nz + 4

        CALL self%computeXYfluxes3d(workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, .FALSE.)
        CALL self%computeZfluxes3d(workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, .FALSE.)

        !### phase 2 is to use those fluxes to precondition and then compute final fluxes ###

        !### set our array spans ###
        ilow  = ilow + 2
        ihi   = ihi - 2
        lilow = lilow + 2
        lihi  = lihi - 2
        jlow  = jlow + 2
        jhi   = jhi - 2
        ljlow = ljlow + 2
        ljhi  = ljhi - 2
        klow  = klow + 2
        khi   = khi - 2
        lklow = lklow + 2
        lkhi  = lkhi - 2

        CALL self%computeXYfluxes3d(workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, .TRUE.)
        CALL self%computeZfluxes3d(workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, .TRUE.)
        
        !### perform the final un-split update ###
        CALL self%van_leer3d(workp%pass3d, workp%vtavg3d, pool%flux3d, c, ilow, ihi, jlow, jhi, klow, khi, pool%dt, pool%dx)

    END DO

END SUBROUTINE passive_solve3d

!##############
!# INTERNAL ROUTINES
!##############

!### compute the slopes ###
SUBROUTINE compute_slopes1d(self, q, v, slope, ilow, ihi, jlow, jhi)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: q(:,:), v(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: slope(:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD) :: vavg, s, r
    INTEGER(WIS) :: i, j

    !### pass through the full grid and compute the limited slope ###
    DO j = jlow, jhi

        DO i = ilow+2, ihi-1

            vavg = 0.5_WRD * (v(i-1,j) + v(i,j))
            IF (vavg .GE. 0.0_WRD) THEN

                r = (q(i-1,j) - q(i-2,j)) / (q(i,j) - q(i-1,j))

            ELSE

                r = (q(i+1,j) - q(i,j)) / (q(i,j) - q(i-1,j))

            END IF

            !### as in MHDTVD, we are using the Sweby limiter here ###
            slope(i,j) = MAX(0.0_WRD, MIN(1.0_WRD, WSWEBY_BETA * r), MIN(WSWEBY_BETA, r))

        END DO

    END DO

END SUBROUTINE compute_slopes1d

!### compute the VanLerr flux ###
SUBROUTINE van_leerflux1d(self, q, v, flux, slope, ilow, ihi, jlow, jhi, dt, dx, d)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: q(:,:), v(:,:), slope(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(OUT) :: flux(:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, d
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx, vavg, dxl, theta
    INTEGER(WIS) :: i, j

    dtdx = dt / dx

    !### loop over the grid and compute the fluxes ###
    DO j = jlow, jhi

        DO i = ilow+2, ihi-1

            vavg = 0.5_WRD * (v(i-1,j) + v(i,j))
            dxl  = ABS(vavg * dtdx)
            IF (vavg .GE. 0.0_WRD) THEN

                theta = 1.0_WRD

            ELSE

                theta = -1.0_WRD

            END IF

            ! placeholder here for a version that handles full convection
            !flux(i,j,d) = 0.5_WRD * vavg * ((1.0_WRD + theta)*q(i-1,j) + (1.0_WRD - theta)*q(i,j)) + &
            !    0.5_WRD * ABS(vavg) * (1.0_WRD - dxl) * slope(i,j) * (q(i,j) - q(i-1,j))

            flux(i,j,d) = 0.5_WRD * ((1.0_WRD + theta)*q(i-1,j) + (1.0_WRD - theta)*q(i,j)) + &
                0.5_WRD * SIGN(1.0_WRD,vavg) * (1.0_WRD - dxl) * slope(i,j) * (q(i,j) - q(i-1,j))

        END DO

    END DO

END SUBROUTINE van_leerflux1d

!### perform a 1d update ###
SUBROUTINE van_leer1d(self, q, v, flux, ilow, ihi, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:), v(:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i

    dtdx = dt / dx

    !### perform the update ###
    DO i = ilow+2, ihi-2

        q(i) = q(i) - v(i) * (flux(i+1) - flux(i)) * dtdx

    END DO

END SUBROUTINE van_leer1d

!### a half-step CTU preconditioner update ###
SUBROUTINE van_leer_halfXupdate2d(self, q, v, flux, ilow, ihi, jlow, jhi, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:), v(:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = 0.5_WRD * dt / dx

    !### perform the update for X using Y fluxes ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j) = q(i,j) - v(i,j) * (flux(i,j+1,2) - flux(i,j,2)) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer_halfXupdate2d

!### a half-step CTU preconditioner update ###
SUBROUTINE van_leer_halfYupdate2d(self, q, v, flux, ilow, ihi, jlow, jhi, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:), v(:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = 0.5_WRD * dt / dx

    !### perform the update for X using Y fluxes (note the rotation in the fluxes) ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j) = q(i,j) - v(i,j) * (flux(j,i+1,1) - flux(j,i,1)) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer_halfYupdate2d

!### perform a 2d update ###
SUBROUTINE van_leer2d(self, q, v, flux, c, ilow, ihi, jlow, jhi, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:), v(:,:,:)
    INTEGER(WIS), INTENT(IN) :: c, ilow, ihi, jlow, jhi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = dt / dx

    !### perform the un-split update ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j,c) = q(i,j,c) - (v(i,j,1) * (flux(i+1,j,1) - flux(i,j,1)) + v(i,j,2) * (flux(i,j+1,2) - flux(i,j,2))) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer2d

!### half X update for 3d ###
SUBROUTINE van_leer_halfXupdate3d(self, q, v, flux, ilow, ihi, jlow, jhi, k, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:,:), v(:,:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, k
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = 0.5_WRD * dt / dx

    !### perform the update for X using Y and Z fluxes ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j) = q(i,j) - (v(i,j,k,2) * (flux(i,j+1,k,2) - flux(i,j,k,2)) + v(i,j,k,3) * (flux(i,j,k+1,3) - flux(i,j,k,3))) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer_halfXupdate3d

!### half Y update for 3d ###
SUBROUTINE van_leer_halfYupdate3d(self, q, v, flux, ilow, ihi, jlow, jhi, k, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:,:), v(:,:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, k
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = 0.5_WRD * dt / dx

    !### perform the update for X using Y and Z fluxes ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j) = q(i,j) - (v(j,i,k,1) * (flux(j+1,i,k,1) - flux(j,i,k,1)) + v(j,i,k,3) * (flux(j,i,k+1,3) - flux(j,i,k,3))) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer_halfYupdate3d

!### half Z update for 3d ###
SUBROUTINE van_leer_halfZupdate3d(self, q, v, flux, ilow, ihi, jlow, jhi, k, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:,:), v(:,:,:,:)
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, jlow, jhi, k
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j

    dtdx = 0.5_WRD * dt / dx

    !### perform the update for X using Y and Z fluxes ###
    DO j = jlow+2, jhi-2

        DO i = ilow+2, ihi-2

            q(i,j) = q(i,j) - (v(j,k,i,1) * (flux(j+1,k,i,1) - flux(j,k,i,1)) + v(j,k,i,2) * (flux(j,k+1,i,2) - flux(j,k,i,2))) * dtdx

        END DO

    END DO

END SUBROUTINE van_leer_halfZupdate3d

!### perform a 3d update ###
SUBROUTINE van_leer3d(self, q, v, flux, c, ilow, ihi, jlow, jhi, klow, khi, dt, dx)

    CLASS(Passive) :: self
    REAL(WRD), CONTIGUOUS, INTENT(INOUT) :: q(:,:,:,:)
    REAL(WRD), CONTIGUOUS, INTENT(IN) :: flux(:,:,:,:), v(:,:,:,:)
    INTEGER(WIS), INTENT(IN) :: c, ilow, ihi, jlow, jhi, klow, khi
    REAL(WRD), INTENT(IN) :: dt, dx
    REAL(WRD) :: dtdx
    INTEGER(WIS) :: i, j, k

    dtdx = dt / dx

    !### perform the un-split update ###
    DO k = klow+2, khi-2

        DO j = jlow+2, jhi-2

            DO i = ilow+2, ihi-2

                q(i,j,k,c) = q(i,j,k,c) - (v(i,j,k,1) * (flux(i+1,j,k,1) - flux(i,j,k,1)) + v(i,j,k,2) * (flux(i,j+1,k,2) - flux(i,j,k,2)) + v(i,j,k,3) * (flux(i,j,k+1,3) - flux(i,j,k,3))) * dtdx

            END DO

        END DO

    END DO

END SUBROUTINE van_leer3d

!### compute the X/Y fluxes with a possible predondition ###
SUBROUTINE computeXYfluxes2d(self, workp, pool, c, ilow, ihi, jlow, jhi, lilow, lihi, ljlow, ljhi, halfdt)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, lilow, lihi, jlow, jhi, ljlow, ljhi, c
    LOGICAL(WIS), INTENT(IN) :: halfdt
    INTEGER(WIS) :: i, j

    !### place the passive values into our work array ###
    DO j = ljlow, ljhi

        pool%pwork2d(lilow:lihi,j) = workp%pass2d(lilow:lihi,j,c)
        
    END DO

    !### if we're doing a halfdt precondition do so now ###
    IF (halfdt) THEN

        !### pull out the Y velocity ###
        DO j = ljlow-2, ljhi+2

            pool%vtavg2d(lilow-2:lihi+2,j) = workp%vtavg2d(lilow-2:lihi+2,j,2)

        END DO

        !### perform a half-step X update using Y fluxes ###
        CALL self%van_leer_halfXupdate2d(pool%pwork2d, pool%vtavg2d, pool%flux2d, ilow-2, ihi+2, jlow-2, jhi+2, pool%dt, pool%dx)

    END IF

    !### pull out the X velocity ###
    DO j = ljlow, ljhi

        pool%vtavg2d(lilow:lihi,j) = workp%vtavg2d(lilow:lihi,j,1)

    END DO

    !### compute the X slopes and fluxes ###
    CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, ilow, ihi, jlow, jhi)
    CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%flux2d, pool%slope2d, ilow, ihi, jlow, jhi, pool%dt, pool%dx, 1)

    !### place the passive values into our work array transposed to be contiguous ###
    DO i = lilow, lihi

        DO j = ljlow, ljhi
            
            pool%pwork2d(j,i) = workp%pass2d(i,j,c)

        END DO

    END DO

    !### if we're doing a halfdt precondition do so now ###
    IF (halfdt) THEN

        !### pull out the X velocity ###
        DO i = lilow-2, lihi+2

            DO j = ljlow-2, ljhi+2

                pool%vtavg2d(j,i) = workp%vtavg2d(i,j,1)

            END DO

        END DO

        !### perform a half-step Y update using X fluxes ###
        CALL self%van_leer_halfYupdate2d(pool%pwork2d, pool%vtavg2d, pool%flux2d, jlow-2, jhi+2, ilow-2, ihi+2, pool%dt, pool%dx)

    END IF

    !### pull out the Y velocity ###
    DO i = lilow, lihi

        DO j = ljlow, ljhi

            pool%vtavg2d(j,i) = workp%vtavg2d(i,j,2)

        END DO

    END DO

    !### compute the Y slopes and fluxes ###
    CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, jlow, jhi, ilow, ihi)
    CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%workFlux2d, pool%slope2d, jlow, jhi, ilow, ihi, pool%dt, pool%dx, 2)

    !### rotate those fluxes back into our regular array ###
    DO j = ljlow, ljhi

        DO i = lilow, lihi

            pool%flux2d(i,j,2) = pool%workFlux2d(j,i,2)

        END DO

    END DO

END SUBROUTINE computeXYfluxes2d

!### compute the X/Y fluxes with a possible predondition ###
SUBROUTINE computeXYfluxes3d(self, workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, halfdt)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, lilow, lihi, jlow, jhi, ljlow, ljhi, klow, khi, lklow, lkhi, c
    LOGICAL(WIS), INTENT(IN) :: halfdt
    INTEGER(WIS) :: i, j, k

    !### loop over XY planes ###
    DO k = lklow, lkhi

        !### X pass ###

        !### place the passive values into our work array ###
        DO j = ljlow, ljhi

            pool%pwork2d(lilow:lihi,j) = workp%pass3d(lilow:lihi,j,k,c)

        END DO

        !### if we're doing a half step precondition now ###
        IF (halfdt) THEN

            !### perform a half-step X update using Y and Z fluxes ###
            CALL self%van_leer_halfXupdate3d(pool%pwork2d, workp%vtavg3d, pool%flux3d, ilow-2, ihi+2, jlow-2, jhi+2, k+workp%nb, pool%dt, pool%dx)

        END IF

        !### pull out the X velocity ###
        DO j = ljlow, ljhi

            pool%vtavg2d(lilow:lihi,j) = workp%vtavg3d(lilow:lihi,j,k,1)

        END DO

        !### compute the X slopes and fluxes ###
        CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, ilow, ihi, jlow, jhi)
        CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%workFlux2d, pool%slope2d, ilow, ihi, jlow, jhi, pool%dt, pool%dx, 1)

        !### place those fluxes into the larger 3d array ###
        DO j = ljlow, ljhi

            DO i = lilow, lihi

                pool%flux3d(i,j,k,1) = pool%workFlux2d(i,j,1)

            END DO

        END DO

        !### Y pass ###

        !### place the passive values into our work array transposed to be contiguous ###
        DO i = lilow, lihi

            DO j = ljlow, ljhi

                pool%pwork2d(j,i) = workp%pass3d(i,j,k,c)

            END DO

        END DO

        !### if we're doing a half step precondition now ###
        IF (halfdt) THEN

            !### perform a half-step Y update using X and Z fluxes ###
            CALL self%van_leer_halfYupdate3d(pool%pwork2d, workp%vtavg3d, pool%flux3d, jlow-2, jhi+2, ilow-2, ihi+2, k+workp%nb, pool%dt, pool%dx)

        END IF

        !### pull out the Y velocity ###
        DO i = lilow, lihi
            
            DO j = ljlow, ljhi
                
                pool%vtavg2d(j,i) = workp%vtavg3d(i,j,k,2)

            END DO

        END DO

        !### compute the Y slopes and fluxes ###
        CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, jlow, jhi, ilow, ihi)
        CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%workFlux2d, pool%slope2d, jlow, jhi, ilow, ihi, pool%dt, pool%dx, 2)

        !### rotate those fluxes back into our regular array ###
        DO j = ljlow, ljhi

            DO i = lilow, lihi

                pool%flux3d(i,j,k,2) = pool%workFlux2d(j,i,2)

            END DO

        END DO

    END DO

END SUBROUTINE computeXYfluxes3d

!### compute Z fluxes with a possible precondition ###
SUBROUTINE computeZfluxes3d(self, workp, pool, c, ilow, ihi, jlow, jhi, klow, khi, lilow, lihi, ljlow, ljhi, lklow, lkhi, halfdt)

    CLASS(Passive) :: self
    TYPE(Patch) :: workp
    TYPE(PassivePool) :: pool
    INTEGER(WIS), INTENT(IN) :: ilow, ihi, lilow, lihi, jlow, jhi, ljlow, ljhi, klow, khi, lklow, lkhi, c
    LOGICAL(WIS), INTENT(IN) :: halfdt
    INTEGER(WIS) :: i, j, k

    !### Z pass ###
    DO j = ljlow, ljhi

        !### place the passive values into our work array ###
        DO i = lilow, lihi

            DO k = lklow, lkhi

                pool%pwork2d(k,i) = workp%pass3d(i,j,k,c)

            END DO

        END DO

        !### if we're doing a half step precondition now ###
        IF (halfdt) THEN

            !### perform a half-step Z update using X and Y fluxes ###
            CALL self%van_leer_halfZupdate3d(pool%pwork2d, workp%vtavg3d, pool%flux3d, klow-2, khi+2, ilow-2, ihi+2, j+workp%nb, pool%dt, pool%dx)

        END IF

        !### pull out the Z velocity ###
        DO i = lilow, lihi

            DO k = lklow, lkhi

                pool%vtavg2d(k,i) = workp%vtavg3d(i,j,k,3)

            END DO

        END DO

        !### compute the Z slopes and fluxes ###
        CALL self%compute_slopes1d(pool%pwork2d, pool%vtavg2d, pool%slope2d, klow, khi, ilow, ihi)
        CALL self%van_leerflux1d(pool%pwork2d, pool%vtavg2d, pool%workFlux2d, pool%slope2d, klow, khi, ilow, ihi, pool%dt, pool%dx, 3)

        !### rotate those fluxes back into our regular array ###
        DO k = lklow, lkhi

            DO i = lilow, lihi

                pool%flux3d(i,j,k,3) = pool%workFlux2d(k,i,3)

            END DO

        END DO

    END DO

END SUBROUTINE computeZfluxes3d

END MODULE Mod_Passive
