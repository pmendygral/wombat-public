#include "config.h"

MODULE Mod_Error

!######################################################################
!#
!# FILENAME: mod_error.f90
!#
!# DESCRIPTION: This module provides routines for error messages
!#
!# DEPENDENCIES:
!#
!# HISTORY:
!#    5/7/12  - Peter Mendygral
!#
!######################################################################

USE Mod_Globals

IMPLICIT NONE

CONTAINS

SUBROUTINE Error(source, msg)

    CHARACTER(*) :: source
    CHARACTER(*) :: msg

    PRINT '("[", A, " ERROR] (Rank ", A, " | Thread ", A,") :: ", A)', TRIM(source), TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(msg)
    STOP

END SUBROUTINE Error

SUBROUTINE Message(source, msg)

    CHARACTER(*) :: source
    CHARACTER(*) :: msg

#ifdef DEBUG
    PRINT '("DEBUG MODE [", A, " MESSAGE] (Rank ", A, " | Thread ", A, ") :: ", A)', TRIM(source), TRIM(WRANKSTR),  TRIM(WTHREADSTR), TRIM(msg)
#else
    PRINT '("[", A, " MESSAGE] (Rank ", A, " | Thread ", A, ") :: ", A)', TRIM(source), TRIM(WRANKSTR),  TRIM(WTHREADSTR), TRIM(msg)
#endif

END SUBROUTINE Message

SUBROUTINE Assert_Info(expr, msg, srcfile, line)
    
    LOGICAL(WIS) :: expr
    CHARACTER(*) :: srcfile
    INTEGER(4) :: line
    CHARACTER(*) :: msg

    IF (expr) & 
        RETURN
        
    PRINT '("[",A,":",A,"] - ", A," :", I4,"   ")', TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(srcfile), line      
    PRINT '("               ",A)', TRIM(msg)
    PRINT '("ABORTING ...")'

    STOP
END SUBROUTINE

SUBROUTINE Assert_Info_Root(expr, msg, srcfile, line)
    
    LOGICAL(WIS) :: expr
    CHARACTER(*) :: srcfile
    INTEGER(4) :: line
    CHARACTER(*) :: msg

    IF (expr) & 
        RETURN
        
    IF (WRANK .EQ. 0) THEN
        PRINT '("[",A,":",A,"] - ", A," :", I4,"   ")', TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(srcfile), line      
        PRINT '("               ",A)', TRIM(msg)
        PRINT '("ABORTING ...")'

    ENDIF

    STOP
END SUBROUTINE

SUBROUTINE Msg_Info(msg, srcfile, line)
    
    CHARACTER(*) :: srcfile
    INTEGER(4) :: line
    CHARACTER(*) :: msg

    PRINT '("[",A,":",A,"] - ", A," :", I4," - ", A)', TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(srcfile), line, TRIM(msg)      
    
END SUBROUTINE

SUBROUTINE Msg_Info_Root(msg, srcfile, line)
    
    CHARACTER(*) :: srcfile
    INTEGER(4) :: line
    CHARACTER(*) :: msg

    IF (WRANK .EQ. 0) &
        PRINT '("[",A,":",A,"] - ", A," :", I4," - ", A)', TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(srcfile), line, TRIM(msg)      
    
END SUBROUTINE

SUBROUTINE Debugmsg_Info(msg, srcfile, line)
    
    CHARACTER(*) :: srcfile
    INTEGER(4) :: line
    CHARACTER(*) :: msg

#ifdef DEBUG
    IF (WRANK .EQ. 0) &
        PRINT '("[",A,":",A,"] - ", A," :", I4, " - " , A)', TRIM(WRANKSTR), TRIM(WTHREADSTR), TRIM(srcfile), line,TRIM(msg)  
#endif
END SUBROUTINE

END MODULE Mod_Error
