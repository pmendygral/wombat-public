/*
There are a few routines WOMBAT needs from C.  Define the wrappers here.
 */
#include "config.h"
#include <string.h>
#include <stdint.h>

void memcpy_(void * dest, void * src, int * num) {

  memcpy(dest, src, *num);

  return ;	
}

void sigcmpgt_(double **v1, int64_t *p, double *v2, double *tol, int *s, double *val) {

    if ((*v1)[*p-(int64_t)1] > *v2+*tol)
        *s = 1;
    else
        *s = 0;
    *val = (*v1)[*p-(int64_t)1];

	return ;
}

void sigcmplt_(double **v1, int64_t *p, double *v2, double *tol, int *s) {

    if ((*v1)[*p-(int64_t)1]+*tol < *v2)
        *s = 1;
    else
        *s = 0;

	return ;
}
