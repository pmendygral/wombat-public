### !/bin/bash
### Run edit namelist files to change the grid size, number of ranks/patches, or variables included in the compressed output.

tests[0]='1a'
tests[1]='1b'
tests[2]='2a'
tests[3]='2b'
tests[4]='3a'
tests[5]='3b'
tests[6]='4a'
tests[7]='4b'
tests[8]='4c'
tests[9]='4d'
tests[10]='5a'
tests[11]='5b'

angles[0]='0d'
angles[1]='22p5d'
angles[2]='30d'
angles[3]='45d'
angles[4]='60d'

for (( i = 0 ; i < 1; i++ ))
do
	for (( j = 0 ; j < 1; j++ ))
	do
		filename='wombat.'
		filename+=${tests[i]}
		filename+=${angles[j]}

		echo $filename
		
		### EDIT after this to change namelist files:
		### change number of dimensions:
		#sed -i 's/ndims\t\t\t\t\t= 2,/ndims\t\t\t\t\t= 2,/g' $filename
		### change number of ranks:
		sed -i 's/nranks_x\t\t\t\t= 4,/nranks_x\t\t\t\t= 4,/g' $filename
		sed -i 's/nranks_y\t\t\t\t= 4,/nranks_y\t\t\t\t= 4,/g' $filename
		sed -i 's/nranks_z\t\t\t\t= 1,/nranks_z\t\t\t\t= 1,/g' $filename
		sed -i 's/naio\t\t\t\t\t= 1,/naio\t\t\t\t\t= 1,/g' $filename
		#sed -i 's/nthreads\t\t\t\t= 1,/nthreads\t\t\t\t= 1,/g' $filename
		### change number of patches per rank:
		sed -i 's/n_xpatches\t\t\t\t= 1,/n_xpatches\t\t\t\t= 1,/g' $filename
		sed -i 's/n_ypatches\t\t\t\t= 1,/n_ypatches\t\t\t\t= 1,/g' $filename
		sed -i 's/n_zpatches\t\t\t\t= 1,/n_zpatches\t\t\t\t= 1,/g' $filename
		### change dimensions of patch:
		sed -i 's/patch_nx\t\t\t\t= 64,/patch_nx\t\t\t\t= 64,/g' $filename
		sed -i 's/patch_ny\t\t\t\t= 32,/patch_ny\t\t\t\t= 32,/g' $filename
		sed -i 's/patch_nz\t\t\t\t= 1,/patch_nz\t\t\t\t= 1,/g' $filename
		sed -i 's/patch_nb\t\t\t\t= 5/patch_nb\t\t\t\t= 5/g' $filename
		### change MHD solver settings:
		sed -i 's/eps1\t\t\t\t\t= 0.2d0,/eps1\t\t\t\t\t= 0.2d0,/g' $filename
		sed -i 's/eps2\t\t\t\t\t= 0.1d0,/eps2\t\t\t\t\t= 0.1d0,/g' $filename
		sed -i 's/eps3\t\t\t\t\t= 0.05d0,/eps3\t\t\t\t\t= 0.05d0,/g' $filename
		sed -i 's/eps4\t\t\t\t\t= 0.1d0,/eps4\t\t\t\t\t= 0.1d0,/g' $filename
		sed -i 's/zero_value\t\t\t\t= 1.d-15,/zero_value\t\t\t\t= 1.d-14,/g' $filename
		sed -i 's/min_pressure\t\t\t= 1.d-15,/min_pressure\t\t\t= 1.d-15,/g' $filename
		sed -i 's/min_density\t\t\t\t= 1.d-15,/min_density\t\t\t\t= 1.d-15,/g' $filename
		sed -i 's/prot_min_pressure\t\t= 1.d-9,/prot_min_pressure\t\t= 1.d-9,/g' $filename
		sed -i 's/prot_min_density\t\t= 1.d-9/prot_min_density\t\t= 1.d-9/g' $filename
		### change compressed interval
		sed -i 's/compressedInterval\t\t= 1.d-2,/compressedInterval\t\t= 1.d-2,/g' $filename
		### change which variables are included in compressed format output
		### use this example to change any/all of the compressed outputs to whichever vairables you wish
		#sed -i 's/compressedFormat(9)  \t= " 10  PRS  Gas_Pressure      real4     log_e   1.e-3      1e3",/compressedFormat(9)  \t= " 11  PS0  Color             real4     linear     0.       1.",/g' $filename
	done
done
