program brickcut
    implicit none
    !### size of domain in each dimension ###
    integer :: xlen, ylen, zlen, i, j, k, recnum, n
    !### rotation angles ###
    real :: phi, theta, psi, temp
    character(128), dimension(8) :: arg
    character(128) :: filename, outputfile
    real, allocatable :: brick(:,:,:)
    real, allocatable :: lineValues(:)
    real, dimension(3,3) :: A
    real, dimension(3) :: bounds
    integer, dimension(3) :: lineVec, newLineVec
    type :: coordinates
        integer, dimension(3) :: coords
        integer :: inbox
    end type coordinates
    type(coordinates), allocatable :: line(:)
    type(coordinates) center
    
    !### Get arguments passed from command line ###
    !### Should be passed like this: (1) BOF file (2-4) x, y, and z size of cube (5-7) phi, theta, psi euler rotation angles, see Goldstein pg 162
    do i = 1, iargc()
        call getarg(i,arg(i))
    end do
    
    !### Make sure we have at least the file path ###
    if(iargc()<1)then
        print *, "Please provide file path, and cube dimensions"
        goto 100
    else
        filename = trim(arg(1))
    end if
    
    !### and the dimensions
    if(iargc()>=4) then
        read(arg(2),*) xlen
        read(arg(3),*) ylen
        read(arg(4),*) zlen
    else 
        print *, "Please provide cube dimensions"
        goto 100
    end if
    
    !### Find Cube Center - in case we want to eventually make this more general ###
    center%coords = (/ceiling(xlen/2.),ceiling(ylen/2.),ceiling(zlen/2.)/)
    
    !### allocate memory for the line and the brick and line values###
    allocate(line(xlen*sizeof(center)))    
    allocate(brick(4*xlen,4*ylen,4*zlen))
    
    !### Get the euler angles, if not given assume 0
    if(iargc()>=7)then
        read(arg(5),*) phi
        read(arg(6),*) theta
        read(arg(7),*) psi
    else
        phi=0
        theta=0
        psi=0
    end if
    
    !### Get output filename, if not given name lineout.dat###
    if(iargc()>=8)then
        outputfile = trim(arg(8))
    else
        outputfile = 'lineout.dat'
    end if
    
    !### Define rotation matrix ###
    A(1,1) = cos(psi)*cos(phi) - cos(theta)*sin(phi)*sin(psi)
    A(1,2) = cos(psi)*sin(phi) + cos(theta)*cos(phi)*sin(psi)
    A(1,3) = sin(psi)*sin(theta)
    A(2,1) =-sin(psi)*cos(phi) - cos(theta)*sin(phi)*cos(psi)
    A(2,2) =-sin(psi)*sin(phi) + cos(theta)*cos(phi)*cos(psi)
    A(2,3) = cos(psi)*sin(theta)
    A(3,1) = sin(theta)*sin(phi)
    A(3,2) =-sin(theta)*cos(phi)
    A(3,3) = cos(theta)
    
    !### Open the given file ###
    open(unit=10,file=trim(filename),access='direct',recl=4)
    
    !### Read the file ###
    recnum = 1    
    do k = 1, zlen
        do j = 1, ylen
            do i = 1, xlen
                read(10,rec=recnum) brick(i,j,k)
                recnum=recnum+1
            end do
        end do
    end do
    
    !### Define the bounds of the box
    bounds = (/ceiling(xlen/2.),ceiling(ylen/2.),ceiling(zlen/2.)/)
    
    !### Define our line (prior to rotation) as the normal to the interface at the center of the cube ###
    !### This means the line goes along y=z=0 from -x/2 to x/2 ###
    !### Now find this line after the rotation ###
    do i = 1, xlen
        lineVec(1) = ceiling(-xlen/2.0)+(i-1)
        lineVec(2) = 0
        lineVec(3) = 0
        line(i)%inbox = 1
        do j = 1, 3
            temp = 0
            do k = 1, 3
                temp = temp + A(j,k)*lineVec(k)
            end do
            newLineVec(j) = ceiling(temp)
            if(newLineVec(j)>bounds(j) .or. newLineVec(j)<=-bounds(j))then
                line(i)%inbox = line(i)%inbox*0
            else
                line(i)%inbox = line(i)%inbox*1
            end if
        end do
        line(i)%coords = newLineVec
    end do
    
    !### shift coordinates to grid coodinates: (1-256 instead of -127-128 etc) ###
    do i = 1, xlen
        line(i)%coords(1) = line(i)%coords(1) + ceiling(xlen/2.0)
        line(i)%coords(2) = line(i)%coords(2) + ceiling(ylen/2.0)
        line(i)%coords(3) = line(i)%coords(3) + ceiling(zlen/2.0)
    end do

    !### count how many cells make up our linear cut ###    
    n = 0
    do i = 1, xlen

!### different way of finding 'inbox' state ###
!        if(line(i)%coords(1)>xlen .or. line(i)%coords(1)<1 .or. line(i)%coords(2)>ylen .or. line(i)%coords(2) < 1 &
!        &.or. line(i)%coords(3) > zlen .or. line(i)%coords(3) < 1) then
!            line(i)%inbox = 0
!        end if

        n = n + line(i)%inbox
    end do
    
    !### allocate memory for the line ###
    
    allocate(lineValues(n*4))
    
    !### take the values from the brick ###
    j = 1
    do i = 1, xlen
        if(line(i)%inbox==1)then
            lineValues(j) = brick(line(i)%coords(1),line(i)%coords(2),line(i)%coords(3))
            j = j + 1
        end if
    end do
    
    !### write the values out to a file ###
    open(unit = 11, file=trim(outputfile))
!    do i = n, 1, -1
    do i = 1, n !### <----somtimes this give me a 'backward' line cut (ie opposite direction down the line as I expect)
        write(11,*) lineValues(i)
    end do
    
    !### deallocate allocated memory ###
    deallocate(brick)
    deallocate(line)
    deallocate(lineValues)
    
100 continue
end program brickcut
