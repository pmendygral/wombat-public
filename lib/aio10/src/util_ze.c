#ifdef MY_WIN32
#include <windows.h>
#include <process.h>
#include <io.h>
#include <winbase.h>
typedef __int64 Offset_t;
#else
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
typedef long long Offset_t;
// #include <pthread.h>
#endif

#include <stdio.h>
#include <string.h>
#include <malloc.h>
//  lude "mnt.h"
// #include <errmsg.h>

/*******************************************************************
 *  Wrapper for compression and endian routines
 */

#ifdef MY_WIN32
#define sw_compress_ SW_COMPRESS
#define sw_compress2_ SW_COMPRESS2
#endif

#define CARG(t,s,n) i = 0; while(i<n  &&  s[i] != ' ') { t[i] = s[i]; i++; } t[i] = 0

#include "zlib.h"

int sw_compress2_(int *inbytes, void *inbuf, int *outbytes, void *outbuf, char *ctype, int n1)
{
  char comprtype[1024];
  int i, ierr, igot, level;
  uLong sourceLen;
  uLongf destLen;
  CARG(comprtype,ctype,n1);

  sourceLen = (uLong)(*inbytes);
  destLen   = (uLongf)(*outbytes);
  level    = -2;
  igot = sscanf(comprtype, "Nzlib%d\n", &level);
  if(-1 > level || level > 9) { level = Z_DEFAULT_COMPRESSION; }
  
  ierr = compress2((Bytef *)outbuf, &destLen, (const Bytef *)inbuf, sourceLen, level);
  *outbytes = (int)destLen;

  return ierr;
}

int sw_compress_(int *inbytes, void *inbuf, int *outbytes, void *outbuf)
{
  int i, ierr;
  uLong sourceLen;
  uLongf destLen;

  sourceLen = (uLong)(*inbytes);
  destLen   = (uLongf)(*outbytes);
  ierr = compress((Bytef *)outbuf, &destLen, (const Bytef *)inbuf, sourceLen);
  *outbytes = (int)destLen;

  return ierr;
}

