
      integer maxread, maxsrc, maxpatches, maxlevels
      parameter(maxread    = 100000)
      parameter(maxsrc     = 1000)
      parameter(maxpatches = 1000)
      parameter(maxlevels =    50)
      real xsize, ysize, zsize, qrxyz(5,maxsrc), crit_fac
      integer nprocs, myrank, nread, nb, nsrc,nrep_fac
      integer nx,ny,nz, nbx,nby,nbz, npx,npy,npz
      integer npatches, ixyzoff(3,maxpatches), level(maxpatches)
      integer nrefine(maxpatches), idump
      integer maxbuf, nbuf(maxlevels)
      real xyz0(3,maxpatches), xyz1(3,maxpatches)
      character*(maxread) cBuffer
      character*5 cName

      common /amrout/ xsize, ysize, zsize, qrxyz, crit_fac, xyz0, xyz1
      common /amrout/ nprocs, myrank, nread, nb, nsrc,nrep_fac
      common /amrout/ nx,ny,nz, nbx,nby,nbz, npx,npy,npz
      common /amrout/ npatches, ixyzoff, level, nrefine, idump
      common /amrout/ maxbuf, nbuf
      common /amrout/ cName, cBuffer

